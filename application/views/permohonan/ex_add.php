					
                    <!-- CSS dan JS untuk Search -->
					<script src="<?php echo base_url();?>assets/js/bootstrap-select.js" defer></script>
					<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-select.css">
					
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Permohonan External 
                                <small>Input Permohonan Eksternal</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="#">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Permohonan</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">External</span>
                        </li>
                    </ul>
                   
                    <div class="row ">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-share font-dark"></i>
                                        <span class="caption-subject font-dark bold uppercase">Input Permohonan</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
									<form action="<?php echo base_url();?>permohonan/addPermohonan_mitra" method="post" enctype="multipart/form-data">	
									<input type="hidden" name="prm_id" value="<?php echo $detilpermohonan['prm_id'];?>" /> 
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Unit Kerja</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<div class="form-group">
													<select id="prm_unit" name="prm_unit" data-live-search="true"  class="form-control select2 selectpicker">
														<?php 
														foreach($unit as $Hunit){ ?>
														<option value="<?php echo $Hunit['id'];?>" <?php if($detilpermohonan['prm_unit'] == $Hunit['id']){ echo "selected";}?>><?php echo $Hunit['nama_unit'];?></option>
														
														<?php
														}
														?>
														
													</select>
												</div>
											</div>
										</div>  
									</div>		
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Jenis Permohonan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<div class="form-group">
													<select id="jns_id" data-live-search="true" name="jns_id" class="form-control select2 selectpicker">
													
														<?php 
														foreach($jenis as $Hjenis){ ?>
														<option value="<?php echo $Hjenis['jns_id'];?>" <?php if($detilpermohonan['jns_id'] == $Hjenis['jns_id']){ echo "selected";}?>><?php echo $Hjenis['jns_kerjasama'];?></option>
														
														<?php
														}
														?>
														
													</select>
												</div>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Judul Permohonan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input name="prm_judul" class="form-control spinner" value="<?php echo $detilpermohonan['prm_judul'];?>" type="text" placeholder="Judul Permohonan" required /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Deskripsi Permohonan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<textarea name="prm_deskripsi" class="form-control spinner" required ><?php echo $detilpermohonan['prm_deskripsi'];?></textarea>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>File</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<div id="div_file">
													<input name="prm_file" type="file" accept=".pdf,.doc,.docx,.xlsx" /> 
												</div>
												<?php 
												if($detilpermohonan['prm_file'] == ''){ ?>
													
												<?php
												}else { ?>
													<!--
													<div id="div_download">
														<a href="<?php echo base_url();?>assets/upload/<?php echo $detilpermohonan['prm_file'];?>">Klik disini</a>
														<button type="button" id="gantifile" class="btn green">Ganti File</button>
													</div>
													-->
												<?php
												}
												?>
											</div>
										</div>  
									</div>
									
									
									</div>
									<div style="#36c6d3;width:100%;padding:2vh 0vh 2vh 0vh; ">
										<strong style="color:red">Catatan</strong>: 
										<br>
										* Ketika "<strong>Edit</strong>" Silahkan Upload Kembali Jika Tidak File akan terhapus                                
										<br>
									</div>
									<div class="portlet-body">
										<div class="modal-footer">
											<a href="<?php echo base_url();?>permohonan"><button type="button" class="btn dark btn-outline" data-dismiss="modal">Batal</button></a>
											<button type="submit" class="btn green">Simpan</button>
										</div>
									</div>
                                   </form>
                                </div>
                            </div>
                            <!-- END SAMPLE FORM PORTLET-->
							
                        </div>
						
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
          
        </div>
		
<!-- untuk   -->
<script>
document.getElementById("gantifile").onclick = function() {file()};

function file() {
   // $("#div_file").hide();
   // $("#div_download").show();
}
</script>
