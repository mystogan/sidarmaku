					
                    <!-- CSS dan JS untuk Search -->
					<script src="<?php echo base_url();?>assets/js/bootstrap-select.js" defer></script>
					<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-select.css">
					
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Permohonan External 
                                <small>Detail Permohonan Eksternal</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="#">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Permohonan</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">External</span>
							<i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Detail</span>
							
                        </li>
                    </ul>
                   
                    <div class="row ">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-share font-dark"></i>
                                        <span class="caption-subject font-dark bold uppercase">Detail Permohonan</span> 	
										<a type="button" href="<?php echo base_url();?>permohonan" class="btn grey-salsa btn-outline">Kembali	</a>
												
                                    </div>
                                </div>
                                <div class="portlet-body">
									
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Nama Instansi</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" value="<?php echo $detail['mtr_namainstansi'];?>" type="text" disabled /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Judul Permohonan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" value="<?php echo $detail['prm_judul'];?>" type="text" disabled /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Tanggal Permohonan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" value="<?php echo $detail['tanggal_permohonan'];?>" type="text" disabled /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Deskripsi Permohonan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<textarea name="prm_deskripsi" class="form-control spinner" disabled ><?php echo $detail['prm_deskripsi'];?></textarea>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Jenis Kerja Sama</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" value="<?php echo $detail['jns_kerjasama'];?>" type="text" disabled /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Unit Kerja Sama</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" value="<?php echo $unit['nama_unit'];?>" type="text" disabled /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Alamat Mitra</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" value="<?php echo $detail['mtr_alamat'];?>" type="text" disabled /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Kecamatan Mitra</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" value="<?php echo $detail['nama_kec'];?>" type="text" disabled /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Kabupaten Mitra</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" value="<?php echo $detail['nama_kab'];?>" type="text" disabled /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Provinsi Mitra</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" value="<?php echo $detail['nama_prov'];?>" type="text" disabled /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Telefon Mitra</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" value="<?php echo $detail['mtr_telepon'];?>" type="text" disabled /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Handphone Mitra</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" value="<?php echo $detail['mtr_handphone'];?>" type="text" disabled /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Email Mitra</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" value="<?php echo $detail['mtr_email'];?>" type="text" disabled /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>File</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<div id="div_file">
													<a href="<?php echo base_url();?>assets/upload/<?php echo $detail['prm_file'];?>">Klik disini</a>
														
												</div>
												
											</div>
										</div>  
									</div>
									
									
									</div>
                                </div>
                            </div>
                            <!-- END SAMPLE FORM PORTLET-->
							
                        </div>
						
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
          
        </div>
		
<!-- untuk   -->
<script>
document.getElementById("gantifile").onclick = function() {file()};

function file() {
   // $("#div_file").hide();
   // $("#div_download").show();
}
</script>
