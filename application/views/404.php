<!--<html>
  <head><meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

  	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/reset.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/css.css" type="text/css "/>

  </head>
  <body>
		<div class="container">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="conpage">404</div>
				<div class="conpage1">Page Not Found</div>
			</div>
			<div class="foot col-xs-12 col-sm-12 col-md-12">
				&copy Pengajuan UKT Mahasiswa UIN Sunan Ampel Surabaya
			</div>
		</div>
  </body>
 </html>
[if !IE]><!-->


<html lang="en">

    <head>
        <meta charset="utf-8" />
		<title>SIDARMA-KU Sistem Kerjasama</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #4 for bootstrap inputs, input groups, custom checkboxes and radio controls and more" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="<?php echo base_url();?>assets/fonts/open-sans/font.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url();?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
		 
        <link href="<?php echo base_url();?>assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url();?>assets/css/themes/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <!-- <link href="assets/css/themes/components.min.css" rel="stylesheet" id="style_components" type="text/css" /> -->
        <link href="<?php echo base_url();?>assets/css/themes/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/css/themes/error.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url();?>assets/css/themes/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url();?>assets/css/themes/custom.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/css/sidarmaku.css" rel="stylesheet" type="text/css" />	
		
		<!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url();?>assets/plugins/jquery.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url();?>assets/plugins/jquery-ui.js" ></script>
        <script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/js/themes/app.min.js" type="text/javascript"></script> 
        <!-- END CORE PLUGINS -->
		
		<!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="<?php echo base_url();?>favicon.ico" /> 
		
        
    <!-- END HEAD -->

    <body class=" page-500-full-page">
        <div class="row">
            <div class="col-md-12 page-500">
                <div class=" number font-green"> 404 </div>
                <div class=" details">
                    <h3>Oops! Something went wrong.</h3>
                    <p> SISTEM INFORMASI KERJASAMA UIN SUNAN AMPEL SURABAYA
                        <br/> </p>
                    <p>
                        <a href="<?php echo base_url();?>" class="btn green btn-outline"> Return home </a>
                        <br> </p>
                </div>
            </div>
        </div>
    </body>

</html>