					
                    <!-- CSS dan JS untuk Search -->
					<script src="<?php echo base_url();?>assets/js/bootstrap-select.js" defer></script>
					<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-select.css">
					
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Input laporan
                                <small>Input Laporan</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="#">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Laporan</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Input</span>
                        </li>
                    </ul>
                   
                    <div class="row ">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-share font-dark"></i>
                                        <span class="caption-subject font-dark bold uppercase">Input Laporan</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
									<form action="<?php echo base_url();?>laporan/addlaporan" method="post" enctype="multipart/form-data">	
									<input type="text" name="lak_id" value="<?php echo $detilpermohonan['prm_id'];?>" /> 
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Kesepakatan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<div class="form-group">
													<select id="spk_id" name="spk_id" data-live-search="true"  class="form-control select2 selectpicker">
														<?php 
														foreach($kesepakatan as $Hkesepakatan){ ?>
														<option value="<?php echo $Hkesepakatan['spk_id'];?>" <?php if($laporan['lak_id'] == $Hkesepakatan['spk_id']){ echo "selected";}?>><?php echo $Hkesepakatan['spk_judul'];?></option>
														
														<?php
														}
														?>
														
													</select>
												</div>
											</div>
										</div>  
									</div>		
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Jenis Laporan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<div class="form-group">
													<select id="lak_jenis" name="lak_jenis" class="form-control">
														<option value="1" >Laporan Progress</option>
														<option value="2" >Laporan Akhir</option>
													</select>
												</div>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Periode</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input name="lak_periode" class="form-control spinner" value="<?php echo $detilpermohonan['prm_judul'];?>" type="text" placeholder="Periode" required /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Anggaran</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input name="lak_anggaran" class="form-control spinner" value="<?php echo $detilpermohonan['prm_judul'];?>" type="text" placeholder="Anggaran" required /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Tanggal</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input name="lak_tanggal" class="form-control spinner" value="<?php echo $detilpermohonan['prm_judul'];?>" type="date" placeholder="Judul Permohonan" required /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Tanggal Pengesahan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input name="lak_pengesahan" class="form-control spinner" value="<?php echo $detilpermohonan['prm_judul'];?>" type="date" placeholder="Judul Permohonan" required /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Tanggal Penandatanganan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input name="lak_penandatangan" class="form-control spinner" value="<?php echo $detilpermohonan['prm_judul'];?>" type="text" placeholder="Judul Permohonan" required /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Latar Belakang</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<textarea name="lak_latarbelakang" class="form-control spinner" required ><?php echo $detilpermohonan['prm_deskripsi'];?></textarea>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Tujuan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<textarea name="lak_tujuan" class="form-control spinner" required ><?php echo $detilpermohonan['prm_deskripsi'];?></textarea>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Program</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<textarea name="lak_program" class="form-control spinner" required ><?php echo $detilpermohonan['prm_deskripsi'];?></textarea>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Pelaksanaan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<textarea name="lak_pelaksanaan" class="form-control spinner" required ><?php echo $detilpermohonan['prm_deskripsi'];?></textarea>
											</div>
										</div>  
									</div>
									
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Hasil</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<textarea name="lak_hasil" class="form-control spinner" required ><?php echo $detilpermohonan['prm_deskripsi'];?></textarea>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Rencana Tindak Lanjut</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<textarea name="lak_rtl" class="form-control spinner" required ><?php echo $detilpermohonan['prm_deskripsi'];?></textarea>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Kesimpulan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<textarea name="lak_kesimpulan" class="form-control spinner" required ><?php echo $detilpermohonan['prm_deskripsi'];?></textarea>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Saran</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<textarea name="lak_saran" class="form-control spinner" required ><?php echo $detilpermohonan['prm_deskripsi'];?></textarea>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>File Laporan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<div id="div_file">
													<input name="lak_filelaporan" type="file" /> 
												</div>
												<?php 
												if($detilpermohonan['prm_file'] == ''){ ?>
													
												<?php
												}else { ?>
													<!--
													<div id="div_download">
														<a href="<?php echo base_url();?>assets/upload/<?php echo $detilpermohonan['prm_file'];?>">Klik disini</a>
														<button type="button" id="gantifile" class="btn green">Ganti File</button>
													</div>
													-->
												<?php
												}
												?>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>File Lampiran</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<div id="div_file">
													<input name="lak_filelampiran" type="file" /> 
												</div>
												<?php 
												if($detilpermohonan['prm_file'] == ''){ ?>
													
												<?php
												}else { ?>
													<!--
													<div id="div_download">
														<a href="<?php echo base_url();?>assets/upload/<?php echo $detilpermohonan['prm_file'];?>">Klik disini</a>
														<button type="button" id="gantifile" class="btn green">Ganti File</button>
													</div>
													-->
												<?php
												}
												?>
											</div>
										</div>  
									</div>
									
									
									</div>
									<div class="portlet-body">
										<div class="modal-footer">
											<a href="<?php echo base_url();?>permohonan"><button type="button" class="btn dark btn-outline" data-dismiss="modal">Batal</button></a>
											<button type="submit" class="btn green">Simpan</button>
										</div>
									</div>
                                   </form>
                                </div>
                            </div>
                            <!-- END SAMPLE FORM PORTLET-->
							
                        </div>
						
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
          
        </div>
		
<!-- untuk   -->
<script>
document.getElementById("gantifile").onclick = function() {file()};

function file() {
   // $("#div_file").hide();
   // $("#div_download").show();
}
</script>
