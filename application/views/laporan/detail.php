  <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Laporan
                                <small>Laporan</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Laporan</span>
                        </li>
                    </ul>
                    </div>
                    
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Laporan (<?php echo $this->session->userdata('user_nama');?>) </span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="btn-group">
                                                    <a href="<?php echo base_url();?>laporan/add"><button id="sample_editable_1_new" class="btn btn-success btn-sm"> Tambah Data 
														<i class="fa fa-plus"></i>
                                                    </button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
									<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center;">No</th>
                                                <th style="text-align:center;">Judul Kesepakatan</th>
                                                <th style="text-align:center;">Periode</th>
                                                <th style="text-align:center;">Kesimpulan</th>
                                                <th style="text-align:center;">File</th>
                                                <th style="text-align:center;">Status</th>
                                                <th style="text-align:center;"> Detail </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
											$i=1;
											foreach($laporan as $Hlaporan){
											
											?>
                                            <tr class="odd gradeX">                                           
                                                <td><?php echo $i;?></td>
												<td><?php echo $Hlaporan['spk_judul'];?></td>
												<td><?php echo $Hlaporan['lak_periode'];?></td>
												<td><?php echo $Hlaporan['lak_kesimpulan'];?></td>
												
												<td>
													<?php
													if($Hlaporan['lak_filelaporan'] == ""){
														echo "Tidak Ada File";
													}else { ?>
														<a href="<?php echo base_url();?>assets/upload/<?php echo $Hlaporan['lak_filelaporan'];?>">Klik disini</a>
													<?php													
													}
													?>
												</td>
												<td><?php 
													$cek = 0;
													if($wadek3['nama_jastruk'] == "Wakil Dekan Bidang Kemahasiswaan dan Kerjasama" 
                                                        && $Hlaporan['dis_jastrukasal'] == $wadek3['id'] ){
                                                        if($Hlaporan['spk_status'] == 3)
                                                        {
                                                            echo "<span style='color:red'>Wadek 3 Telah Membatalkan Validasi</span>";
                                                        }else 
                                                        {
                                                            echo "Wadek 3 Belum Validasi";
                                                        }
                                                    }else if($dekan['nama_dekan'] == "Dekan Fakultas"
                                                        && $Hlaporan['dis_jastrukasal'] == $dekan['id'] ){
                                                        if($Hlaporan['spk_status'] == 3)
                                                        {
                                                            echo "<span style='color:red'>Dekan Telah Membatalkan Validasi</span>";
                                                        }else 
                                                        {
                                                        echo "Dekan Belum Validasi";
                                                        }
                                                    }else if($Hlaporan['dis_jastrukasal'] == 1){
                                                        if($Hlaporan['spk_status'] == 3)
                                                        {
                                                            echo "<span style='color:red'>Rektor Telah Membatalkan Validasi</span>";
                                                        }else 
                                                        {
                                                        echo "Rektor Belum Validasi";
                                                        }
                                                    }else if($Hlaporan['dis_jastrukasal'] == 4){
                                                        if($Hlaporan['spk_status'] == 3)
                                                        {
                                                            echo "<span style='color:red'>Wakil Rektor 3 Telah Membatalkan Validasi</span>";
                                                        }else 
                                                        {
                                                        echo "Wakil Rektor 3 Belum Validasi";
                                                        }

                                                    }else if($Hlaporan['dis_jastrukasal'] == 353){
                                                        if($Hlaporan['spk_status'] == 3)
                                                        {
                                                            echo "<span style='color:red'>Biro Administrasi Akademik, Kemahasiswaan dan Kerjasama Telah Membatalkan Validasi</span>";
                                                        }else 
                                                        {
                                                        echo "Biro Administrasi Akademik, Kemahasiswaan dan Kerjasama Belum Validasi";
                                                        }
                                                    }else if($Hlaporan['dis_jastrukasal'] == 369){
                                                        if($Hlaporan['spk_status'] == 3)
                                                        {
                                                            echo "<span style='color:red'>Kepala Bagian Kerjasama Telah Membatalkan Validasi</span>";
                                                        }else 
                                                        {
                                                        echo "Kepala Bagian Kerjasama Belum Validasi";
                                                        }
                                                    }else if($Hlaporan['dis_jastrukasal'] == 370){
                                                        if($Hlaporan['spk_status'] == 3)
                                                        {
                                                            echo "<span style='color:red'>Kepala Sub Bagian Kerjasama Telah Membatalkan Validasi</span>";
                                                        }else 
                                                        {
                                                        echo "Kepala Sub Bagian Kerjasama Belum Validasi";
                                                        }
                                                    }else if($Hlaporan['spk_pegawaipihak1'] == $Hlaporan['dis_pegawaiasal']){
                                                        if($Hlaporan['spk_status'] == 3)
                                                        {
                                                            echo "<span style='color:red'>Penanggung Jawab yang bernama ".$Hlaporan['namaPegawai']." Telah Membatalkan Validasi</span>";
                                                        }else 
                                                        {
                                                        echo "Penanggung Jawab yang bernama { ".$Hlaporan['namaPegawai']." } Belum Validasi";
                                                        }
                                                    }else if($Hlaporan['spk_pegawaipic'] == $Hlaporan['dis_pegawaiasal']){
                                                        if($Hlaporan['spk_status'] == 3)
                                                        {
                                                            echo "<span style='color:red'>PIC yang bernama ".$Hlaporan['namaPegawai']." Telah Membatalkan Validasi</span>";
                                                        }else 
                                                        {
                                                        echo "PIC yang bernama { ".$Hlaporan['namaPegawai']." } Belum Validasi";
                                                        }
                                                    }else if($Hlaporan['dis_jastrukasal'] == 375){
                                                        echo "Proses Cetak Surat";
                                                    }else {
                                                        echo "Terjadi Kesalahan silahkan menghubungi pihak kampus";
                                                    }
												?></td>
												 <td style="text-align:center;">	
													
													<?php 
													if($cek == 0){ ?>
													<a href="<?php echo base_url();?>laporan/add/<?php echo ($Hlaporan['lak_id']);?>" title="Detail" class="btn btn-outline yellow btn-sm active">
														<i class="fa fa-edit"></i>  
													</a>	
													<?php
													}else { ?>
													<a href="<?php echo base_url();?>laporan/add2/<?php echo ($Hlaporan['lak_id']);?>" title="Detail" class="btn btn-outline yellow btn-sm active">
														<i class="fa fa-edit"></i>  
													</a>
													<?php
													}
													?>
												
                                                 </td>
												
                                            </tr>
                                            <?php
											$i++;
											}
											?>
                                        </tbody>
                                    </table>
									<div style="border-top:1px solid #36c6d3;width:100%;padding:2vh 0vh 2vh 0vh; ">
										<strong style="color:red">Catatan</strong>: Ketika Status "<strong>Wakil Rektor 3 Belum Disposisi</strong>" Masih Bisa Diedit
									</div>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                    </div>
                </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>