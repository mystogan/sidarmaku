					
                    <!-- CSS dan JS untuk Search -->
					<script src="<?php echo base_url();?>assets/js/bootstrap-select.js" defer></script>
					<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-select.css">
					
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Permohonan Internal 
                                <small>Input Permohonan Internal</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="#">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Permohonan</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">External</span>
                        </li>
                    </ul>
                   
                    <div class="row ">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-tag font-dark"></i>
                                        <span class="caption-subject font-dark bold uppercase">Disposisi Permohonan Kerjasama</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
										<div class="col-md-12">
											<div class="portlet light portlet-fit bordered">
												<div class="portlet-title">
													<div class="row">
														<div class="col-md-6">
															<div class="caption">
																<div class="row">
																	<div class="col-md-12">
																		<span class="bold"> Permohonan :</span>
																	</div>
																	<div class="col-md-12">
																		<span class=""><?php echo $detail['prm_judul'];?></span>
																	</div>
																</div>
																</br>
																<div class="row">
																	<div class="col-md-12">
																		<span class="bold"> Tanggal :</span>
																	</div>
																	<div class="col-md-12">
																		<span class="caption-helper"><?php echo $detail['tanggal_permohonan'];?></span>
																	</div>
																</div>
															</div>
														</div>
														
													</div>
												</div>
												<div class="portlet-body">
													<div class="timeline">
														<!-- TIMELINE ITEM -->
														<div class="timeline-item">
															<div class="timeline-badge">
																<img class="timeline-badge-userpic" src="<?php echo base_url();?>assets/img/layout/avatar.png"> </div>
															<div class="timeline-body">
																<div class="timeline-body-arrow"> </div>
																<div class="timeline-body-head">
																	<div class="timeline-body-head-caption">
																		<a href="javascript:;" class="timeline-body-title font-blue-madison">Wakil Dekan 3</a>
																		<span class="timeline-body-time font-grey-cascade">
																			<?php 
																			if($catatan[0]['tanggal_permohonan'] == "00-00-0000 00:00"){
																				echo "-";
																			}else {
																				echo $catatan[0]['tanggal_permohonan']; 
																			}
																			?>
																		</span>
																	</div>
																</div>
																<div class="timeline-body-content">
																	<span class="font-grey-cascade">
																	<?php
																	if($catatan[0]['dis_catatan'] == ''){
																		echo "-";
																	}else {
																		echo $catatan[0]['dis_catatan'];
																	}
																	?>
																	</span>
																</div>
															</div>
														</div>
														<div class="timeline-item">
															<div class="timeline-badge">
																<img class="timeline-badge-userpic" src="<?php echo base_url();?>assets/img/layout/avatar.png"> </div>
															<div class="timeline-body">
																<div class="timeline-body-arrow"> </div>
																<div class="timeline-body-head">
																	<div class="timeline-body-head-caption">
																		<a href="javascript:;" class="timeline-body-title font-blue-madison">Dekan</a>
																		<span class="timeline-body-time font-grey-cascade">
																			<?php 
																			if($catatan[1]['tanggal_permohonan'] == "00-00-0000 00:00"){
																				echo "-";
																			}else {
																				echo $catatan[1]['tanggal_permohonan']; 
																			}
																			?>
																		</span>
																	</div>
																</div>
																<div class="timeline-body-content">
																	<span class="font-grey-cascade">
																	<?php
																	if($catatan[1]['dis_catatan'] == ''){
																		echo "-";
																	}else {
																		echo $catatan[1]['dis_catatan'];
																	}
																	?>
																	</span>
																</div>
															</div>
														</div>
														<div class="timeline-item">
															<div class="timeline-badge">
																<img class="timeline-badge-userpic" src="<?php echo base_url();?>assets/img/layout/avatar.png"> </div>
															<div class="timeline-body">
																<div class="timeline-body-arrow"> </div>
																<div class="timeline-body-head">
																	<div class="timeline-body-head-caption">
																		<a href="javascript:;" class="timeline-body-title font-blue-madison">Rektor</a>
																		<span class="timeline-body-time font-grey-cascade">
																			<?php 
																			if($catatan[2]['tanggal_permohonan'] == "00-00-0000 00:00"){
																				echo "-";
																			}else {
																				echo $catatan[2]['tanggal_permohonan']; 
																			}
																			?>
																		</span>
																	</div>
																</div>
																<div class="timeline-body-content">
																	<span class="font-grey-cascade">
																	<?php
																	if($catatan[2]['dis_catatan'] == ''){
																		echo "-";
																	}else {
																		echo $catatan[2]['dis_catatan'];
																	}
																	?>
																	</span>
																</div>
															</div>
														</div>
														<div class="timeline-item">
															<div class="timeline-badge">
																<img class="timeline-badge-userpic" src="<?php echo base_url();?>assets/img/layout/avatar.png"> </div>
															<div class="timeline-body">
																<div class="timeline-body-arrow"> </div>
																<div class="timeline-body-head">
																	<div class="timeline-body-head-caption">
																		<a href="javascript:;" class="timeline-body-title font-blue-madison">Wakil Rektor III</a>
																		<span class="timeline-body-time font-grey-cascade">
																			<?php 
																			if($catatan[3]['tanggal_permohonan'] == "00-00-0000 00:00"){
																				echo "-";
																			}else {
																				echo $catatan[3]['tanggal_permohonan']; 
																			}
																			?>
																		</span>
																	</div>
																</div>
																<div class="timeline-body-content">
																	<span class="font-grey-cascade"> 
																	<?php
																	if($catatan[3]['dis_catatan'] == ''){
																		echo "-";
																	}else {
																		echo $catatan[3]['dis_catatan'];
																	}
																	?>
																	</span>
																</div>
															</div>
														</div>
														<div class="timeline-item">
															<div class="timeline-badge">
																<img class="timeline-badge-userpic" src="<?php echo base_url();?>assets/img/layout/avatar.png"> </div>
															<div class="timeline-body">
																<div class="timeline-body-arrow"> </div>
																<div class="timeline-body-head">
																	<div class="timeline-body-head-caption">
																		<a href="javascript:;" class="timeline-body-title font-blue-madison">Kabiro AAK</a>
																		<span class="timeline-body-time font-grey-cascade">
																		<?php 
																			if($catatan[4]['tanggal_permohonan'] == "00-00-0000 00:00"){
																				echo "-";
																			}else {
																				echo $catatan[4]['tanggal_permohonan']; 
																			}
																			?>
																		</span>
																	</div>
																</div>
																<div class="timeline-body-content">
																	<span class="font-grey-cascade">
																	<?php
																	if($catatan[4]['dis_catatan'] == ''){
																		echo "-";
																	}else {
																		echo $catatan[4]['dis_catatan'];
																	}
																	?>
																	</span>
																</div>
															</div>
														</div>
														<div class="timeline-item">
															<div class="timeline-badge">
																<img class="timeline-badge-userpic" src="<?php echo base_url();?>assets/img/layout/avatar.png"> </div>
															<div class="timeline-body">
																<div class="timeline-body-arrow"> </div>
																<div class="timeline-body-head">
																	<div class="timeline-body-head-caption">
																		<a href="javascript:;" class="timeline-body-title font-blue-madison">Kepala Bagian Kerjasama</a>
																		<span class="timeline-body-time font-grey-cascade"><?php 
																			if($catatan[5]['tanggal_permohonan'] == "00-00-0000 00:00"){
																				echo "-";
																			}else {
																				echo $catatan[5]['tanggal_permohonan']; 
																			}
																			?></span>
																	</div>
																</div>
																<div class="timeline-body-content">
																	<span class="font-grey-cascade">
																	<?php
																	if($catatan[5]['dis_catatan'] == ''){
																		echo "-";
																	}else {
																		echo $catatan[5]['dis_catatan'];
																	}
																	?>
																	</span>
																</div>
															</div>
														</div>
														<div class="timeline-item">
															<div class="timeline-badge">
																<img class="timeline-badge-userpic" src="<?php echo base_url();?>assets/img/layout/avatar.png"> </div>
															<div class="timeline-body">
																<div class="timeline-body-arrow"> </div>
																<div class="timeline-body-head">
																	<div class="timeline-body-head-caption">
																		<a href="javascript:;" class="timeline-body-title font-blue-madison">Kasubag Bagian Kerjasama</a>
																		<span class="timeline-body-time font-grey-cascade"><?php 
																			if($catatan[6]['tanggal_permohonan'] == "00-00-0000 00:00"){
																				echo "-";
																			}else {
																				echo $catatan[6]['tanggal_permohonan']; 
																			}
																			?></span>
																	</div>
																</div>
																<div class="timeline-body-content">
																	<span class="font-grey-cascade">
																	<?php
																	if($catatan[6]['dis_catatan'] == ''){
																		echo "-";
																	}else {
																		echo $catatan[6]['dis_catatan'];
																	}
																	?>
																	</span>
																</div>
															</div>
														</div>
														<!-- END TIMELINE ITEM -->
													</div>
												</div>
											</div>
										</div>
									</div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
										<div class="col-md-12">
											<div class="portlet light portlet-fit bordered">
											  <form method="POST" action="<?php echo base_url();?>permohonan/update_catatan_internal">
												<div class="portlet-title">
													<div class="caption">
														<span class="caption-subject bold font-green uppercase">Catatan</span>
													</div>
												</div>
												<input type="hidden" name="dis_id" value="<?php echo $dis_id; ?>" />
												<input type="hidden" name="prm_id" value="<?php echo $detail['prm_id']; ?>" />
												<div class="portlet-body">
													<textarea class="wysihtml5 form-control" name="dis_catatan" rows="6"><?php echo $detail['dis_catatan']; ?></textarea>
												</div>
												<div class="portlet-body">
													<button type="submit" class="btn green">
														<i class="fa fa-check"></i> Disposisi</button>
													<a type="button" href="<?php echo base_url();?>permohonan" class="btn grey-salsa btn-outline">Cancel</a>
												</div>
											</div>
										</div>
									</div>
                                </div>
								
								<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Permohonan Kerjasama</h4>
                                                </div>
                                                <div class="modal-body"> 
													<div class="row">
														<div class="col-md-4">
															<address>
																<strong>Nomor </strong> 
															</address>
														</div>
														<div class="col-md-1">
															:
														</div>
														<div class="col-md-7 ket">
															5 (Lima)
														</div>
													</div>
													<div class="row">
														<div class="col-md-4">
															<address>
																<strong>Judul Permohonan Kerjasama</strong> 
															</address>
														</div>
														<div class="col-md-1">
															:
														</div>
														<div class="col-md-7 ket">
															Pengembangan Sumber Daya Manusia dan Pengabdian Masyarakat
														</div>
													</div>
													<div class="row">
														<div class="col-md-4">
															<address>
																<strong>Tanggal</strong> 
															</address>
														</div>
														<div class="col-md-1">
															:
														</div>
														<div class="col-md-7 ket">
															02 Juli 2018
														</div>
													</div>
													<div class="row">
														<div class="col-md-4">
															<address>
																<strong>Nama Pihak 1</strong> 
															</address>
														</div>
														<div class="col-md-1">
															:
														</div>
														<div class="col-md-7 ket">
															Pak Saputra
														</div>
													</div>
													<div class="row">
														<div class="col-md-4">
															<address>
																<strong>Jabatan Pihak 1</strong> 
															</address>
														</div>
														<div class="col-md-1">
															:
														</div>
														<div class="col-md-7 ket">
															Penanggung Jawab
														</div>
													</div>
													<div class="row">
														<div class="col-md-4">
															<address>
																<strong>Nama Pihak 2</strong> 
															</address>
														</div>
														<div class="col-md-1">
															:
														</div>
														<div class="col-md-7 ket">
															Pak Mahardika
														</div>
													</div>
													<div class="row">
														<div class="col-md-4">
															<address>
																<strong>Jabatan Pihak 2</strong> 
															</address>
														</div>
														<div class="col-md-1">
															:
														</div>
														<div class="col-md-7 ket">
															Penanggung Jawab
														</div>
													</div>
												</div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                            </div>
                            <!-- END SAMPLE FORM PORTLET-->
							
                        </div>
						
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
          
        </div>
		
<!-- untuk   -->
<script>
document.getElementById("gantifile").onclick = function() {file()};

function file() {
   // $("#div_file").hide();
   // $("#div_download").show();
}
</script>
