  <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Laporan 
                                <small>Laporan</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Laporan</span>
                        </li>
                    </ul>
                    </div>
                    
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> List Kesepakatan  (<?php echo $this->session->userdata('user_nama');?>) </span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                        </div>
                                    </div>
										  <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center;">No</th>
                                                <th style="text-align:center;">Judul Kesepakatan</th>
                                                <th style="text-align:center;">Penandatanganan</th>
                                                <th style="text-align:center;">Tipe Laporan</th>
                                                <th style="text-align:center;">Tanggal</th>
                                                <th style="text-align:center;">File</th>
                                                <th style="text-align:center;"> Aksi </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
											$i=1;
											foreach($kesepakatan as $Hdisposisi){
											
											?>
                                            <tr class="odd gradeX">                                           
                                                <td><?php echo $i;?></td>
												<td><?php echo $Hdisposisi['spk_judul'];?></td>
												<td><?php echo $Hdisposisi['lak_penandatangan'];?></td>
												<td><?php 
													if($Hdisposisi['lak_jenis'] == 2){
														echo "Laporan Akhir";
													}else {
														echo "Laporan Progress";
														
													}
												
												?></td>
												<td><?php echo $Hdisposisi['tanggal_permohonan'];?></td>
												
												<td>
													<?php
													if($Hdisposisi['prm_file'] == ""){
														echo "Tidak Ada File";
													}else { ?>
														<a href="<?php echo base_url();?>assets/upload/<?php echo $Hdisposisi['prm_file'];?>">Klik disini</a>
													
													<?php													
													}
													?>
												</td>
												 <td style="text-align:center;">	
													<!--
													<a href="viewmou.html" title="Detail" class="btn btn-outline green btn-sm active">
                                                        <i class="fa fa-binoculars"></i> </a>
                                                    -->
													<a href="<?php echo base_url();?>laporan/details/<?php echo ($Hdisposisi['spk_id']);?>" title="Detail" class="btn btn-outline yellow btn-sm active">
                                                        <i class="fa fa-edit"></i>  </a>
                                                 </td>
												
												
												
                                            </tr>
                                            <?php
											$i++;
											}
											?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                    </div>
                </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>