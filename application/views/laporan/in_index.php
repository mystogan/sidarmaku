  <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Permohonan Kerjasama Internal
                                <small>Permohonan Kerjasama Internal</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Permohonan Kerjasama Internal</span>
                        </li>
                    </ul>
                    </div>
                    
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Entri Permohonan Internal (<?php echo $this->session->userdata('user_nama');?>) </span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="btn-group">
                                                    <a href="<?php echo base_url();?>permohonan/in_add"><button id="sample_editable_1_new" class="btn btn-success btn-sm"> Tambah Data 
														<i class="fa fa-plus"></i>
                                                    </button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
									<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center;">No</th>
                                                <th style="text-align:center;">Jenis Kerjasama</th>
                                                <th style="text-align:center;">Judul</th>
                                                <th style="text-align:center;">Deskripsi</th>
                                                <th style="text-align:center;">Tanggal</th>
                                                <th style="text-align:center;">Status</th>
                                                <th style="text-align:center;">File</th>
                                                <th style="text-align:center;"> Aksi </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
											$i=1;
											foreach($permohonan as $Hpermohonan){
											
											?>
                                            <tr class="odd gradeX">                                           
                                                <td><?php echo $i;?></td>
												<td><?php echo $Hpermohonan['jns_kerjasama'];?></td>
												<td><?php echo $Hpermohonan['prm_judul'];?></td>
												<td><?php echo $Hpermohonan['prm_deskripsi'];?></td>
												<td><?php echo $Hpermohonan['tanggal_permohonan'];?></td>
												<td><?php 
													if($Hpermohonan['prm_status'] == 1){
														echo "Draf";
													}else if($Hpermohonan['prm_status'] == 2){
														echo "Proses";
													}else if($Hpermohonan['prm_status'] == 3){
														echo "Accept";
													}else {
														echo "Terhapus";
													}
												?></td>
												<td>
													<?php
													if($Hpermohonan['prm_file'] == ""){
														echo "Tidak Ada File";
													}else { ?>
														<a href="<?php echo base_url();?>assets/upload/<?php echo $Hpermohonan['prm_file'];?>">Klik disini</a>
													
													<?php													
													}
													?>
												</td>
												
												 <td style="text-align:center;">	
													<!--
													<a href="viewmou.html" title="Detail" class="btn btn-outline green btn-sm active">
                                                        <i class="fa fa-binoculars"></i> </a>
                                                    -->
													<?php 
													if($Hpermohonan['prm_status'] == 1){ ?>
														<a href="<?php echo base_url();?>permohonan/in_add/<?php echo ($Hpermohonan['prm_id']);?>" title="Edit" class="btn btn-outline yellow btn-sm active">
															<i class="fa fa-edit"></i>  </a>		
													<?php
													}
													?>
												
                                                 </td>
												
                                            </tr>
                                            <?php
											$i++;
											}
											?>
                                        </tbody>
                                    </table>
									<div style="border-top:1px solid #36c6d3;width:100%;padding:2vh 0vh 2vh 0vh; ">
										<strong style="color:red">Catatan</strong>: Ketika Status "<strong>Draf</strong>" Masih Bisa Diedit
									</div>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                    </div>
                </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>