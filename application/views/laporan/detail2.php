  <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Laporan
                                <small>Laporan</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Laporan</span>
                        </li>
                    </ul>
                    </div>
                    
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Laporan (<?php echo $this->session->userdata('user_nama');?>) </span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                
                                            </div>
                                        </div>
                                    </div>
									<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center;">No</th>
                                                <th style="text-align:center;">Judul Kesepakatan</th>
                                                <th style="text-align:center;">Periode</th>
                                                <th style="text-align:center;">Kesimpulan</th>
                                                
                                                <th style="text-align:center;">File</th>
												<?php 
												if($this->session->userdata('id_tugastambahan') != 375){ ?>												
													<th style="text-align:center;">Status</th>
													<th style="text-align:center;">Catatan</th>
												<?php
												}
												?>
												<th style="text-align:center;"> Detail </th>
												
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
											$i=1;
											foreach($laporan as $Hlaporan){
											
											?>
                                            <tr class="odd gradeX">                                           
                                                <td><?php echo $i;?></td>
												<td><?php echo $Hlaporan['spk_judul'];?></td>
												<td><?php echo $Hlaporan['lak_periode'];?></td>
												<td><?php echo $Hlaporan['lak_kesimpulan'];?></td>
												
												<td>
													<?php
													if($Hlaporan['lak_filelaporan'] == ""){
														echo "Tidak Ada File";
													}else { ?>
														<a href="<?php echo base_url();?>assets/upload/<?php echo $Hlaporan['lak_filelaporan'];?>">Klik disini</a>
													<?php													
													}
													?>
												</td>
												<?php 
												if($this->session->userdata('id_tugastambahan') != 375){ ?>												
													<td style="text-align:center;">	
														<a href="<?php echo base_url();?>laporan/catatan/<?php echo ($Hlaporan['dis_id']);?>" class="btn btn-outline blue btn-sm active">
															Catatan</a>
													</td>
													<td><?php 
														if($Hlaporan['dis_status'] == 1){
															echo '<button type="button" class="btn btn-warning">Belum di Validasi</button>';
														}else if($Hlaporan['dis_status'] == 2){
															echo '<button type="button" class="btn btn-success">Sudah di Validasi</button>';
														
														}else if($Hlaporan['dis_status'] == 3){
															echo '<button type="button" class="btn btn-danger">Batal</button>';
														}else {
															echo '<button type="button" class="btn btn-link">Terhapus</button>';
															
														}
													?></td>
												<?php
												}
												?>
												 <td style="text-align:center;">	
													<a href="<?php echo base_url();?>laporan/add2/<?php echo ($Hlaporan['lak_id']);?>" title="Detail" class="btn btn-outline yellow btn-sm active">
														<i class="fa fa-edit"></i>  
													</a>
												
                                                 </td>
												
                                            </tr>
                                            <?php
											$i++;
											}
											?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                    </div>
                </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>