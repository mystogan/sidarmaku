					
                    <!-- CSS dan JS untuk Search -->
					<script src="<?php echo base_url();?>assets/js/bootstrap-select.js" defer></script>
					<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-select.css">
					
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Laporan
                                <small>Proses Laporan</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="#">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Laporan</span>
                            <i class="fa fa-circle"></i>
                        </li>
                    </ul>
                   
                    <div class="row ">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-tag font-dark"></i>
                                        <span class="caption-subject font-dark bold uppercase">Validasi Laporan</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
										<div class="col-md-12">
											<div class="portlet light portlet-fit bordered">
												<div class="portlet-title">
													<div class="row">
														<div class="col-md-6">
															<div class="caption">
																<div class="row">
																	<div class="col-md-12">
																		<span class="bold"> Laporan :</span>
																	</div>
																	<div class="col-md-12">
																		<span class=""><?php echo $detail['spk_judul'];?></span>
																	</div>
																</div>
																</br>
																<div class="row">
																	<a type="button" href="<?php echo base_url();?>laporan/add2/<?php echo $detail['lak_id'];?>" class="btn grey-salsa btn-primary">Detail</a>
												
																</div>
															</div>
														</div>
														
													</div>
												</div>
												<div class="portlet-body">
													<div class="timeline">
														<!-- TIMELINE ITEM -->
														<div class="timeline-item">
															<div class="timeline-badge">
																<img class="timeline-badge-userpic" src="<?php echo base_url();?>assets/img/layout/avatar.png"> </div>
															<div class="timeline-body">
																<div class="timeline-body-arrow"> </div>
																<div class="timeline-body-head">
																	<div class="timeline-body-head-caption">
																		<a href="javascript:;" class="timeline-body-title font-blue-madison">Penanggung Jawab Kesepakatan { <?php echo $catatan[0]['nama']; ?> }</a>
																		<span class="timeline-body-time font-grey-cascade">
																			<?php 
																			if($catatan[0]['tanggal_permohonan'] == "00-00-0000 00:00"){
																				echo "-";
																			}else {
																				echo $catatan[0]['tanggal_permohonan']; 
																			}
																			?>
																		</span>
																	</div>
																</div>
																<div class="timeline-body-content">
																	<span class="font-grey-cascade"> 
																	<?php
																	if($catatan[0]['dis_catatan'] == ''){
																		echo "-";
																	}else {
																		echo $catatan[0]['dis_catatan'];
																	}
																	?>
																	</span>
																</div>
															</div>
														</div>
														<div class="timeline-item">
															<div class="timeline-badge">
																<img class="timeline-badge-userpic" src="<?php echo base_url();?>assets/img/layout/avatar.png"> </div>
															<div class="timeline-body">
																<div class="timeline-body-arrow"> </div>
																<div class="timeline-body-head">
																	<div class="timeline-body-head-caption">
																		<a href="javascript:;" class="timeline-body-title font-blue-madison">Wakil Rektor III</a>
																		<span class="timeline-body-time font-grey-cascade">
																			<?php 
																			if($catatan[1]['tanggal_permohonan'] == "00-00-0000 00:00"){
																				echo "-";
																			}else {
																				echo $catatan[1]['tanggal_permohonan']; 
																			}
																			?>
																		</span>
																	</div>
																</div>
																<div class="timeline-body-content">
																	<span class="font-grey-cascade"> 
																	<?php
																	if($catatan[1]['dis_catatan'] == ''){
																		echo "-";
																	}else {
																		echo $catatan[1]['dis_catatan'];
																	}
																	?>
																	</span>
																</div>
															</div>
														</div>
														<div class="timeline-item">
															<div class="timeline-badge">
																<img class="timeline-badge-userpic" src="<?php echo base_url();?>assets/img/layout/avatar.png"> </div>
															<div class="timeline-body">
																<div class="timeline-body-arrow"> </div>
																<div class="timeline-body-head">
																	<div class="timeline-body-head-caption">
																		<a href="javascript:;" class="timeline-body-title font-blue-madison">Kabiro AAK</a>
																		<span class="timeline-body-time font-grey-cascade">
																		<?php 
																			if($catatan[2]['tanggal_permohonan'] == "00-00-0000 00:00"){
																				echo "-";
																			}else {
																				echo $catatan[2]['tanggal_permohonan']; 
																			}
																			?>
																		</span>
																	</div>
																</div>
																<div class="timeline-body-content">
																	<span class="font-grey-cascade">
																	<?php
																	if($catatan[2]['dis_catatan'] == ''){
																		echo "-";
																	}else {
																		echo $catatan[2]['dis_catatan'];
																	}
																	?>
																	</span>
																</div>
															</div>
														</div>
														<div class="timeline-item">
															<div class="timeline-badge">
																<img class="timeline-badge-userpic" src="<?php echo base_url();?>assets/img/layout/avatar.png"> </div>
															<div class="timeline-body">
																<div class="timeline-body-arrow"> </div>
																<div class="timeline-body-head">
																	<div class="timeline-body-head-caption">
																		<a href="javascript:;" class="timeline-body-title font-blue-madison">Kepala Bagian Kerjasama</a>
																		<span class="timeline-body-time font-grey-cascade"><?php 
																			if($catatan[3]['tanggal_permohonan'] == "00-00-0000 00:00"){
																				echo "-";
																			}else {
																				echo $catatan[3]['tanggal_permohonan']; 
																			}
																			?></span>
																	</div>
																</div>
																<div class="timeline-body-content">
																	<span class="font-grey-cascade">
																	<?php
																	if($catatan[3]['dis_catatan'] == ''){
																		echo "-";
																	}else {
																		echo $catatan[3]['dis_catatan'];
																	}
																	?>
																	</span>
																</div>
															</div>
														</div>
														<div class="timeline-item">
															<div class="timeline-badge">
																<img class="timeline-badge-userpic" src="<?php echo base_url();?>assets/img/layout/avatar.png"> </div>
															<div class="timeline-body">
																<div class="timeline-body-arrow"> </div>
																<div class="timeline-body-head">
																	<div class="timeline-body-head-caption">
																		<a href="javascript:;" class="timeline-body-title font-blue-madison">Kasubag Bagian Kerjasama</a>
																		<span class="timeline-body-time font-grey-cascade"><?php 
																			if($catatan[4]['tanggal_permohonan'] == "00-00-0000 00:00"){
																				echo "-";
																			}else {
																				echo $catatan[4]['tanggal_permohonan']; 
																			}
																			?></span>
																	</div>
																</div>
																<div class="timeline-body-content">
																	<span class="font-grey-cascade">
																	<?php
																	if($catatan[4]['dis_catatan'] == ''){
																		echo "-";
																	}else {
																		echo $catatan[4]['dis_catatan'];
																	}
																	?>
																	</span>
																</div>
															</div>
														</div>
														<!-- END TIMELINE ITEM -->
													</div>
												</div>
											</div>
										</div>
									</div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
										<div class="col-md-12">
											<div class="portlet light portlet-fit bordered">
											  <form method="POST" action="<?php echo base_url();?>laporan/update_catatan">
												<div class="portlet-title">
													<div class="caption">
														<span class="caption-subject bold font-green uppercase">Catatan</span>
													</div>
												</div>
												<input type="hidden" name="dis_id" value="<?php echo $dis_id; ?>" />
												<input type="hidden" name="prm_id" value="<?php echo $detail['lak_id']; ?>" />
												<input type="hidden" name="spk_id" value="<?php echo $detail['spk_id']; ?>" />
												<div class="portlet-body">
													<textarea class="wysihtml5 form-control" name="dis_catatan" rows="6"><?php echo $detail['dis_catatan']; ?></textarea>
												</div>
												<div class="portlet-body">
													<button type="submit" class="btn green">
														<i class="fa fa-check"></i> Validasi</button>
													<a type="button" href="<?php echo base_url();?>laporan/details/<?php echo $detail['lak_id']; ?>" class="btn grey-salsa btn-outline">Cancel</a>
												</div>
											  </form>
											</div>
										</div>
									</div>
                                </div>
								
				           </div>
                            <!-- END SAMPLE FORM PORTLET-->
							
                        </div>
						
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
          
        </div>
		
<!-- untuk   -->
<script>
document.getElementById("gantifile").onclick = function() {file()};

function file() {
   // $("#div_file").hide();
   // $("#div_download").show();
}
</script>
