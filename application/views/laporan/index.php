  <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Permohonan Kerjasama
                                <small>Permohonan Kerjasama</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Permohonan Kerjasama</span>
                        </li>
                    </ul>
                    </div>
                    
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Laporan (<?php echo $this->session->userdata('user_nama');?>) </span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    
									<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center;">No</th>
                                                <th style="text-align:center;">Mitra</th>
                                                <th style="text-align:center;">Judul Kesepakatan</th>
                                                <th style="text-align:center;">Pihak 1</th>
                                                <th style="text-align:center;">Pihak 2</th>
                                                <th style="text-align:center;">File</th>
                                                <th style="text-align:center;"> Detail </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
											$i=1;
											foreach($kesepakatan as $Hkesepakatan){
											
											?>
                                            <tr class="odd gradeX">                                           
                                                <td><?php echo $i;?></td>
												<td><?php echo $Hkesepakatan['mtr_namainstansi'];?></td>
												<td><?php echo $Hkesepakatan['spk_judul'];?></td>
												<td><?php echo $Hkesepakatan['spk_namapihak1'];?></td>
												<td><?php echo $Hkesepakatan['spk_namapihak2'];?></td>
												
												<td>
													<?php
													if($Hkesepakatan['spk_file'] == ""){
														echo "Tidak Ada File";
													}else { ?>
														<a href="<?php echo base_url();?>assets/upload/<?php echo $Hkesepakatan['spk_file'];?>">Klik disini</a>
													<?php													
													}
													?>
												</td>
												
												 <td style="text-align:center;">	
													<a href="<?php echo base_url();?>laporan/detail/<?php echo ($Hkesepakatan['spk_id']);?>" title="Detail" class="btn btn-outline yellow btn-sm active">
														<i class="fa fa-edit"></i>  
													</a>
												
                                                 </td>
												
                                            </tr>
                                            <?php
											$i++;
											}
											?>
                                        </tbody>
                                    </table>
									<div style="border-top:1px solid #36c6d3;width:100%;padding:2vh 0vh 2vh 0vh; ">
										<strong style="color:red">Catatan</strong>: Ketika Status "<strong>Draf</strong>" Masih Bisa Diedit
									</div>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                    </div>
                </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>