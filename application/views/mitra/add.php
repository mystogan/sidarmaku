
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Master Mitra
                                <small>Input Mitra</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Mitra</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Add</span>
                        </li>
                    </ul>
                   
                    <div class="row ">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-share font-dark"></i>
                                        <span class="caption-subject font-dark bold uppercase">Input Mitra</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
									<form action="<?php echo base_url();?>m_mitra/save" method="post">
									<input type="hidden" name="mtr_id" value="<?php echo $detilmitra['mtr_id'];?>" class="form-control spinner"  /> 
											
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Instansi Induk</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<div class="form-group">
													<label for="single" class="control-label">Pilih Induk Mitra (Jika Tidak ada Pilih Tidak ada)</label>
													<select id="single" class="form-control select2" name="mtr_parent">
														<option value="0">Tidak Ada</option>
														<?php 
														foreach($mitra as $Hmitra){ ?>
														<option value="<?php echo $Hmitra['mtr_id'];?>" <?php if($detilmitra['mtr_parent'] == $Hmitra['mtr_id']){ echo "selected";}?> ><?php echo $Hmitra['mtr_namainstansi'];?></option>
														
														<?php
														}
														?>
													</select>
												</div>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Nama Instansi</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input name="mtr_namainstansi" class="form-control spinner" value="<?php echo $detilmitra['mtr_namainstansi'];?>" type="text" placeholder="Nama Instansi" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Alamat Instansi</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input name="mtr_alamat" class="form-control spinner" value="<?php echo $detilmitra['mtr_alamat'];?>" type="text" placeholder="Alamat Instansi" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Provinsi</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<div class="form-group">
													<select id="mtr_propinsi" name="mtr_propinsi" onclick="pilih_kabupaten();" class="form-control select2">
													
														<?php 
														foreach($prov as $Hprov){ ?>
														<option value="<?php echo $Hprov['id_propinsi'];?>" <?php if($detilmitra['mtr_propinsi'] == $Hprov['id_propinsi']){ echo "selected";}?>><?php echo $Hprov['Nama_Prop'];?></option>
														
														<?php
														}
														?>
														
													</select>
												</div>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Kabupaten</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<div class="form-group">
													<select id="mtr_kota" name="mtr_kota" onclick="pilih_kecamatan();" class="form-control select2">
														<?php 
														if($detilmitra['mtr_kota'] == ""){ ?>
															<option>Silahkan Pilih Provinsi</option>
															
														<?php
														}else { ?>
															<option value="<?php echo $detilmitra['mtr_kota'];?>"><?php echo $detilmitra['nama_kabupaten'];?></option>
														<?php
														}
														?>
														
													</select>
												</div>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Kecamatan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<div class="form-group">
													<select id="mtr_kecamatan" name="mtr_kecamatan" class="form-control select2">
														<?php 
														if($detilmitra['mtr_kecamatan'] == ""){ ?>
															<option>Silahkan Pilih Kabupaten</option>
															
														<?php
														}else { ?>
															<option value="<?php echo $detilmitra['mtr_kecamatan'];?>"><?php echo $detilmitra['nama_kecamatan'];?></option>
														<?php
														}
														?>
														
														
													</select>
												</div>
											</div>
										</div>  
									</div>
									
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Telepon Instansi</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input name="mtr_telepon" value="<?php echo $detilmitra['mtr_telepon'];?>" class="form-control spinner" type="text" placeholder="Telepon Instansi" /> 
											</div>
										</div>  
									</div>
									 
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>HP Instansi</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input name="mtr_handphone" value="<?php echo $detilmitra['mtr_handphone'];?>" class="form-control spinner" type="text" placeholder="HP Instansi" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Email Instansi</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input type="email" value="<?php echo $detilmitra['mtr_email'];?>" name="mtr_email" class="form-control spinner" type="text" placeholder="Email Instansi" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Username</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input type="text" value="<?php echo $detilmitra['mtr_username'];?>" name="mtr_username" class="form-control spinner" type="text" placeholder="Masukkan Username untuk Login" /> 
											</div>
										</div>  
									</div>
									<div style="width:100%;padding:2vh 0vh 2vh 0vh; ">
										<strong style="color:red">Catatan</strong>: Untuk Password sama dengan "<strong>Nama Instansi</strong>"
									</div>
									
									
									</div>
									<div class="portlet-body">
										<div class="modal-footer">
											<a href="<?php echo base_url();?>m_mitra"><button type="button" class="btn dark btn-outline" data-dismiss="modal">Batal</button></a>
											<button type="submit" class="btn green">Simpan</button>
										</div>
									</div>
                                   </form>
                                </div>
                            </div>
                            <!-- END SAMPLE FORM PORTLET-->
							
                        </div>
						
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
          
        </div>
		
<!-- untuk load Kabupaten  -->
<script>
function pilih_kabupaten(){
	var id_propinsi = $('#mtr_propinsi').val();
	$('#mtr_kota').load("<?php echo base_url(); ?>m_mitra/pilih_kabupaten/"+id_propinsi);
	$('#mtr_kecamatan').load("<?php echo base_url(); ?>m_mitra/reset_kecamatan/");

}
function pilih_kecamatan(){
	var mtr_kota = $('#mtr_kota').val();
	$('#mtr_kecamatan').load("<?php echo base_url(); ?>m_mitra/pilih_kecamatan/"+mtr_kota);
}
</script>
