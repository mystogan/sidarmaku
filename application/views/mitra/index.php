  <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Master Mitra
                                <small>Master Mitra</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Master Mitra</span>
                        </li>
                    </ul>
                    </div>
                    
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Daftar Mitra </span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="btn-group">
                                                    <a href="<?php echo base_url();?>m_mitra/add"><button id="sample_editable_1_new" class="btn btn-success btn-sm"> Tambah Data 
														<i class="fa fa-plus"></i>
                                                    </button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
										  <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center;"> No </th>
                                                <th style="text-align:center;"> Nama  </th>
                                                <th style="text-align:center;"> Alamat </th>
                                                <th style="text-align:center;"> Telefon </th>
                                                <th style="text-align:center;"> Email</th>
                                                <th style="text-align:center;"> Aksi </th>
                                            </tr>
                                        </thead>
                                        <tbody>
											<?php 
											$i=1;
											foreach($mitra as $Hmitra){
											
											?>
                                            <tr class="odd gradeX">                                           
                                                <td><?php echo $i;?></td>
												<td><?php echo $Hmitra['mtr_namainstansi'];?></td>
												<td><?php echo $Hmitra['mtr_alamat'];?></td>
												<td><?php echo $Hmitra['mtr_handphone'];?></td>
												<td><?php echo $Hmitra['mtr_email'];?></td>
												
												 <td style="text-align:center;">	
													<a href="viewmou.html" title="Detail" class="btn btn-outline green btn-sm active">
                                                        <i class="fa fa-binoculars"></i> </a>
                                                    <a href="<?php echo base_url();?>m_mitra/add/<?php echo ($Hmitra['mtr_id']);?>" title="Edit" class="btn btn-outline yellow btn-sm active">
                                                        <i class="fa fa-edit"></i>  </a>
                                                 </td>
												
                                            </tr>
                                            <?php
											$i++;
											}
											?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                    </div>
                </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>