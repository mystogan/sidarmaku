<!-- CSS dan JS untuk Search -->
<script
    src="<?php echo base_url();?>assets/js/bootstrap-select.js"
    defer="defer"></script>
<link
    rel="stylesheet"
    href="<?php echo base_url();?>assets/css/bootstrap-select.css">

<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Kesepakatan Internal
            <small>Input Kesepakatan Internal</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="#">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">Kesepakatan</span>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">Internal</span>
    </li>
</ul>

<div class="row ">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Input Kesepakatan</span>
                </div>
            </div>
            <div class="portlet-body">
                <form
                    action="<?php echo base_url();?>kesepakatan/addKesepakatan_unit"
                    method="post"
                    enctype="multipart/form-data">
                    <input
                        type="hidden"
                        name="spk_id"
                        value="<?php echo $kesepakatan['spk_id'];?>"/>
                    <input type="hidden" name="spk_unit" value="<?php echo $unit['kode_unit'];?>"/>
                    <input type="hidden" name="spk_internal" value="1"/>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Mitra</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <div class="form-group">
                                    <select
                                        id="mtr_id"
                                        data-live-search="true"
                                        name="mtr_id"
                                        class="form-control select2 selectpicker">
                                        <?php
										foreach($mitra as $Hmitra){ ?>
                                        <option
                                            value="<?php echo $Hmitra['mtr_id'];?>"
                                            <?php if($kesepakatan['mtr_id'] == $Hmitra['mtr_id']){ echo "selected";}?>><?php echo $Hmitra['mtr_namainstansi'];?></option>

                                        <?php
										}
										?>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Kesepahaman</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <div class="form-group">
                                    <select
                                        id="sph_id"
                                        data-live-search="true"
                                        name="sph_id"
                                        class="form-control select2 selectpicker">

                                        <?php
										foreach($kesepahaman as $value){ ?>
                                        <option
                                            value="<?php echo $value['sph_id'];?>"
                                            <?php if($kesepakatan['sph_id'] == $value['sph_id']){ echo "selected";}?>><?php echo $value['sph_judul'];?></option>

                                        <?php
										}
										?>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Jenis Kesepakatan</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <div class="form-group">
                                    <select id="jns_id" data-live-search="true" name="jns_id" class="form-control select2 selectpicker">

                                        <?php
                                        foreach($jenis as $Hjenis){ ?>
                                        <option value="<?php echo $Hjenis['jns_id'];?>" <?php if($kesepakatan['jns_id'] == $Hjenis['jns_id']){ echo "selected";}?>><?php echo $Hjenis['jns_kerjasama'];?></option>

                                        <?php
                                        }
                                        ?>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Nomor Kesepakatan</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="spk_nomor"
                                    class="form-control spinner"
                                    value="<?php echo $kesepakatan['spk_nomor'];?>"
                                    type="number"
                                    placeholder="Nomor Kesepakatan"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Judul Kesepakatan</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="spk_judul"
                                    class="form-control spinner"
                                    value="<?php echo $kesepakatan['spk_judul'];?>"
                                    type="text"
                                    placeholder="Judul kesepakatan"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Tanggal Kesepakatan</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="spk_tanggal"
                                    class="form-control spinner"
                                    value="<?php echo $kesepakatan['spk_tanggal'];?>"
                                    type="date"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Nama Pihak 1</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="spk_namapihak1"
                                    class="form-control spinner"
                                    value="<?php echo $kesepakatan['spk_namapihak1'];?>"
                                    type="text"
                                    placeholder="Masukkan Nama Pihak 1"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Jabatan Pihak 1</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="spk_jabatanpihak1"
                                    class="form-control spinner"
                                    value="<?php echo $kesepakatan['spk_jabatanpihak1'];?>"
                                    type="text"
                                    placeholder="Masukkan Jabatan Pihak 1"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>SK Pihak 1</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="spk_skpihak1"
                                    class="form-control spinner"
                                    value="<?php echo $kesepakatan['spk_skpihak1'];?>"
                                    type="text"
                                    placeholder="Masukkan SK Pihak 1"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Nama Pegawai</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <div class="form-group">
                                    <select
                                        id="spk_pegawaipihak1"
                                        data-live-search="true"
                                        name="spk_pegawaipihak1"
                                        class="form-control select2 selectpicker">

                                        <?php
										foreach($pegawai as $value){ ?>
                                        <option
                                            value="<?php echo $value['id'];?>"
                                            <?php if($kesepakatan['spk_pegawaipihak1'] == $value['id']){ echo "selected";}?>><?php echo $value['nama'];?></option>

                                        <?php
										}
										?>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Nama Pihak 2</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="spk_namapihak2"
                                    class="form-control spinner"
                                    value="<?php echo $kesepakatan['spk_namapihak2'];?>"
                                    type="text"
                                    placeholder="Masukkan Nama Pihak 2"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Jabatan Pihak 2</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="spk_jabatanpihak2"
                                    class="form-control spinner"
                                    value="<?php echo $kesepakatan['spk_jabatanpihak2'];?>"
                                    type="text"
                                    placeholder="Masukkan Jabatan Pihak 2"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>SK Pihak 2</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="spk_skpihak2"
                                    class="form-control spinner"
                                    value="<?php echo $kesepakatan['spk_skpihak2'];?>"
                                    type="text"
                                    placeholder="Masukkan SK Pihak 2"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Tujuan Kesepakatan</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <textarea name="spk_tujuan" class="form-control spinner" required="required"><?php echo $kesepakatan['spk_tujuan'];?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Tanggal Mulai</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="spk_mulai"
                                    class="form-control spinner"
                                    value="<?php echo $kesepakatan['spk_mulai'];?>"
                                    type="date"
                                    placeholder="Judul Permohonan"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Tanggal Akhir</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="spk_akhir"
                                    class="form-control spinner"
                                    value="<?php echo $kesepakatan['spk_akhir'];?>"
                                    type="date"
                                    placeholder="Judul Permohonan"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>File</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <div id="div_file">
                                    <input name="spk_file" type="file" required />
                                </div>
                                	<?php
										if($kesepahaman['spk_file'] == ''){ ?>

                           			 <?php
									}else { ?>
									<!-- <div id="div_download"> <a href="<?php echo
									base_url();?>assets/upload/<?php echo $kesepahaman['sph_file'];?>">Klik
									disini</a> <button type="button" id="gantifile" class="btn green">Ganti
									File</button> </div> -->
									<?php
									}
									?>
                            </div>

                        </div>
                        <div style="#36c6d3;width:100%;padding:2vh 0vh 2vh 0vh; ">
                                <strong style="color:red">Catatan</strong>:
                                <br>
                                * Ketika "<strong>Edit</strong>" Silahkan Upload Kembali Jika Tidak File akan terhapus
                                <br>
                            </div>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="modal-footer">
                        <a href="<?php echo base_url();?>kesepakatam">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Batal</button>
                        </a>
                        <button type="submit" class="btn green">Simpan</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->

</div>

</div>
<!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>

</div>

<!-- untuk -->
<script>
document
.getElementById("gantifile")
.onclick = function () {
file()
};

function file() {
// $("#div_file").hide(); $("#div_download").show();
}
</script>
