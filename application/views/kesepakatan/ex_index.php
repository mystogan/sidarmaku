<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Kesepakatan Kerjasama
            <small>Kesepakatan Kerjasama</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="index.html">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">Kesepakatan Kerjasama</span>
    </li>
</ul>
</div>

<div class="row">
<div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-dark">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject bold uppercase">
                    Entri Kesepakatan Mitra (<?php echo $this->session->userdata('user_nama');?>)
                </span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-toolbar">
                <div class="row">
                    <div class="col-md-12">
                        <form action="<?php echo base_url();?>kesepakatan" method="post">
                            <div class="col-md-3">
                                <select name="yearPost" id="yearPost" class="form-control">
                                    <?php
                                for ($i=0; $i < 5; $i++) { ?>
                                    <option
                                        value="<?php echo (date('Y')-$i)?>"
                                        <?php if((date('Y')-$i) == $yearPost){ echo "selected";}?>><?php echo (date('Y')-$i)?></option>
                                    <?php
                                }
                                ?>
                                </select>
                            </div>
                            <!-- <div class="col-md-3">
                                <select name="internalPost" id="internalPost" class="form-control">
                                    <option value="1" <?php if($internalPost == '1'){ echo "selected"; }?>>Internal</option>
                                    <option value="2" <?php if($internalPost == '2'){ echo "selected"; }?>>Eksternal</option>

                                </select>
                            </div> -->
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-primary">Cari</button>
                            </div>
                        </form>

                    </div>
                    <br>
                    <br>
                    <hr>
                    <div class="col-md-6">
                        <div class="btn-group">
                            <a href="<?php echo base_url();?>kesepakatan/add">
                                <button id="sample_editable_1_new" class="btn btn-success btn-sm">
                                    Tambah Data
                                    <i class="fa fa-plus"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <table
                class="table table-striped table-bordered table-hover table-checkable order-column"
                id="sample_1">
                <thead>
                    <tr>
                        <th style="text-align:center;">No</th>
                        <th style="text-align:center;">Nama Kesepahaman</th>
                        <th style="text-align:center;">Nomor Kesepakatan</th>
                        <th style="text-align:center;">Judul Kesepakatan</th>
                        <th style="text-align:center;">Tujuan</th>
                        <th style="text-align:center;">Tanggal</th>
                        <th style="text-align:center;">Status</th>
                        <th style="text-align:center;">File</th>
                        <th style="text-align:center;">
                            Aksi
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $i=1;
                        foreach($kesepakatan as $Hpermohonan){
                        
                        ?>
                    <tr class="odd gradeX">
                        <td><?php echo $i;?></td>
                        <td><?php echo $Hpermohonan['sph_judul'];?></td>
                        <td><?php echo $Hpermohonan['spk_nomor'];?></td>
                        <td><?php echo $Hpermohonan['spk_judul'];?></td>
                        <td><?php echo $Hpermohonan['spk_tujuan'];?></td>
                        <td><?php echo $Hpermohonan['spk_tanggal'];?></td>
                        <td>
                        <?php 
                            if($wadek3['nama_jastruk'] == "Wakil Dekan Bidang Kemahasiswaan dan Kerjasama" 
                            && $Hpermohonan['dis_jastrukasal'] == $wadek3['id'] ){
                            if($Hpermohonan['spk_status'] == 3)
                            {
                                echo "<span style='color:red'>Wadek 3 Telah Membatalkan Disposisi</span>";
                            }else 
                            {
                                echo "Wadek 3 Belum Disposisi";
                            }
                        }else if($dekan['nama_dekan'] == "Dekan Fakultas"
                            && $Hpermohonan['dis_jastrukasal'] == $dekan['id'] ){
                            if($Hpermohonan['spk_status'] == 3)
                            {
                                echo "<span style='color:red'>Dekan Telah Membatalkan Disposisi</span>";
                            }else 
                            {
                              echo "Dekan Belum Disposisi";
                            }
                        }else if($Hpermohonan['dis_jastrukasal'] == 1){
                            if($Hpermohonan['spk_status'] == 3)
                            {
                                echo "<span style='color:red'>Rektor Telah Membatalkan Disposisi</span>";
                            }else 
                            {
                              echo "Rektor Belum Disposisi";
                            }
                        }else if($Hpermohonan['dis_jastrukasal'] == 4){
                            if($Hpermohonan['spk_status'] == 3)
                            {
                                echo "<span style='color:red'>Wakil Rektor 3 Telah Membatalkan Disposisi</span>";
                            }else 
                            {
                              echo "Wakil Rektor 3 Belum Disposisi";
                            }

                        }else if($Hpermohonan['dis_jastrukasal'] == 353){
                            if($Hpermohonan['spk_status'] == 3)
                            {
                                echo "<span style='color:red'>Biro Administrasi Akademik, Kemahasiswaan dan Kerjasama Telah Membatalkan Disposisi</span>";
                            }else 
                            {
                              echo "Biro Administrasi Akademik, Kemahasiswaan dan Kerjasama Belum Disposisi";
                            }
                        }else if($Hpermohonan['dis_jastrukasal'] == 369){
                            if($Hpermohonan['spk_status'] == 3)
                            {
                                echo "<span style='color:red'>Kepala Bagian Kerjasama Telah Membatalkan Disposisi</span>";
                            }else 
                            {
                              echo "Kepala Bagian Kerjasama Belum Disposisi";
                            }
                        }else if($Hpermohonan['dis_jastrukasal'] == 370){
                            if($Hpermohonan['spk_status'] == 3)
                            {
                                echo "<span style='color:red'>Kepala Sub Bagian Kerjasama Telah Membatalkan Disposisi</span>";
                            }else 
                            {
                              echo "Kepala Sub Bagian Kerjasama Belum Disposisi";
                            }
                        }else if($Hpermohonan['spk_pegawaipihak1'] == $Hpermohonan['dis_pegawaiasal']){
                            if($Hpermohonan['spk_status'] == 3)
                            {
                                echo "<span style='color:red'>Penanggung Jawab yang bernama ".$Hpermohonan['namaPegawai']." Telah Membatalkan Disposisi</span>";
                            }else 
                            {
                              echo "Penanggung Jawab yang bernama { ".$Hpermohonan['namaPegawai']." } Belum Disposisi";
                            }
                        }else if($Hpermohonan['spk_pegawaipic'] == $Hpermohonan['dis_pegawaiasal']){
                            if($Hpermohonan['spk_status'] == 3)
                            {
                                echo "<span style='color:red'>PIC yang bernama ".$Hpermohonan['namaPegawai']." Telah Membatalkan Disposisi</span>";
                            }else 
                            {
                              echo "PIC yang bernama { ".$Hpermohonan['namaPegawai']." } Belum Disposisi";
                            }
                        }else if($Hpermohonan['dis_jastrukasal'] == 375){
                            echo "Proses Cetak Surat";
                        }else {
                            echo "Terjadi Kesalahan silahkan menghubungi pihak kampus";
                        }
                        ?>
                        </td>
                        <td>
                        <?php
                            if($Hpermohonan['spk_file'] == ""){
                                echo "Tidak Ada File";
                            }else { ?>
                            <a
                                href="<?php echo base_url();?>assets/upload/<?php echo $Hpermohonan['sph_file'];?>">Klik disini</a>

                            <?php													
                            }
                            ?>
                        </td>

                        <td style="text-align:center;">
                            <!-- <a href="viewmou.html" title="Detail" class="btn btn-outline green btn-sm
                            active"> <i class="fa fa-binoculars"></i> </a> -->
                            <?php 
                                if($Hpermohonan['spk_status'] == 1){ ?>
                            <a
                                href="<?php echo base_url();?>kesepakatan/add/<?php echo ($Hpermohonan['spk_id']);?>"
                                title="Edit"
                                class="btn btn-outline yellow btn-sm active">
                                <i class="fa fa-edit"></i>
                            </a>
                            <?php
                            }
                            ?>

                        </td>

                    </tr>
                    <?php
                        $i++;
                        }
                        ?>
                </tbody>
            </table>
            <div style="border-top:1px solid #36c6d3;width:100%;padding:2vh 0vh 2vh 0vh; ">
                <strong style="color:red">Catatan</strong>: 
                <br>
                * Ketika Status "<strong>Draf</strong>" Masih Bisa Diedit
                <br>
                * Kolom "<strong>Catatan</strong>" Keterangan sampai mana 
                <br>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>

</div>
</div>
</div>
</div>

<link
href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"
rel="stylesheet"
type="text/css"/>

<script
src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"
type="text/javascript"></script>
<script
src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"
type="text/javascript"></script>