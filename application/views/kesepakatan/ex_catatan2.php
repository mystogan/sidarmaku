					
                    <!-- CSS dan JS untuk Search -->
					<script src="<?php echo base_url();?>assets/js/bootstrap-select.js" defer></script>
					<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-select.css">
					
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Permohonan External 
                                <small>Input Permohonan Eksternal</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="#">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Permohonan</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">External</span>
                        </li>
                    </ul>
                   
                    <div class="row ">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-tag font-dark"></i>
                                        <span class="caption-subject font-dark bold uppercase">Disposisi Permohonan Kerjasama</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
										<div class="col-md-12">
											<div class="portlet light portlet-fit bordered">
												<div class="portlet-title">
													<div class="row">
														<div class="col-md-6">
															<div class="caption">
																<div class="row">
																	<div class="col-md-12">
																		<span class="bold"> Permohonan :</span>
																	</div>
																	<div class="col-md-12">
																		<span class=""> Pengembangan Sumber Daya Manusia dan Pengabdian Masyarakat</span>
																	</div>
																</div>
																</br>
																<div class="row">
																	<div class="col-md-12">
																		<span class="bold"> Tanggal :</span>
																	</div>
																	<div class="col-md-12">
																		<span class="caption-helper">02 Juli 2018</span>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="actions">
																<div class="btn-group btn-group-devided pull-right" data-toggle="buttons">
																	<label class="btn red btn-sm statusLabel">
																	Status Menunggu</label>
																	<label class="btn blue btn-outline btn-sm  pull-right" data-toggle="modal" href="#basic"><i class="fa fa-search-plus"></i>
																	Lihat</label>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="portlet-body">
													<div class="timeline">
														<!-- TIMELINE ITEM -->
														<div class="timeline-item">
															<div class="timeline-badge">
																<img class="timeline-badge-userpic" src="../assets/img/layout/avatar.png"> </div>
															<div class="timeline-body">
																<div class="timeline-body-arrow"> </div>
																<div class="timeline-body-head">
																	<div class="timeline-body-head-caption">
																		<a href="javascript:;" class="timeline-body-title font-blue-madison">Rektor</a>
																		<span class="timeline-body-time font-grey-cascade">Prof.Masdar Hilmy, S.Ag., MA, Ph.D | at 7:45 PM</span>
																	</div>
																</div>
																<div class="timeline-body-content">
																	<span class="font-grey-cascade"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation
																		ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </span>
																</div>
															</div>
														</div>
														<div class="timeline-item">
															<div class="timeline-badge">
																<img class="timeline-badge-userpic" src="../assets/img/layout/avatar.png"> </div>
															<div class="timeline-body">
																<div class="timeline-body-arrow"> </div>
																<div class="timeline-body-head">
																	<div class="timeline-body-head-caption">
																		<a href="javascript:;" class="timeline-body-title font-blue-madison">Wakil Rektor III</a>
																		<span class="timeline-body-time font-grey-cascade">Pak Warek Tiga | at 7:45 PM</span>
																	</div>
																</div>
																<div class="timeline-body-content">
																	<span class="font-grey-cascade"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation
																		ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </span>
																</div>
															</div>
														</div>
														<div class="timeline-item">
															<div class="timeline-badge">
																<img class="timeline-badge-userpic" src="../assets/img/layout/avatar.png"> </div>
															<div class="timeline-body">
																<div class="timeline-body-arrow"> </div>
																<div class="timeline-body-head">
																	<div class="timeline-body-head-caption">
																		<a href="javascript:;" class="timeline-body-title font-blue-madison">Kabiro AAK</a>
																		<span class="timeline-body-time font-grey-cascade">Pak Kabiro | at 7:45 PM</span>
																	</div>
																</div>
																<div class="timeline-body-content">
																	<span class="font-grey-cascade"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation
																		ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </span>
																</div>
															</div>
														</div>
														<div class="timeline-item">
															<div class="timeline-badge">
																<img class="timeline-badge-userpic" src="../assets/img/layout/avatar.png"> </div>
															<div class="timeline-body">
																<div class="timeline-body-arrow"> </div>
																<div class="timeline-body-head">
																	<div class="timeline-body-head-caption">
																		<a href="javascript:;" class="timeline-body-title font-blue-madison">Kepala Bagian</a>
																		<span class="timeline-body-time font-grey-cascade">Pak Kabag | at 7:45 PM</span>
																	</div>
																</div>
																<div class="timeline-body-content">
																	<span class="font-grey-cascade"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation
																		ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </span>
																</div>
															</div>
														</div>
														<!-- END TIMELINE ITEM -->
													</div>
												</div>
											</div>
										</div>
									</div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
										<div class="col-md-12">
											<div class="portlet light portlet-fit bordered">
												<div class="portlet-title">
													<div class="caption">
														<span class="caption-subject bold font-green uppercase">Catatan</span>
													</div>
												</div>
												<div class="portlet-body">
													<textarea class="wysihtml5 form-control" rows="6"></textarea>
												</div>
												<div class="portlet-body">
													<a href="tabeldisposisikerjasama.html"><button type="submit" class="btn green"></a>
														<i class="fa fa-check"></i> Submit</button>
													<a href="tabeldisposisikerjasama.html"><button type="button" class="btn grey-salsa btn-outline">Cancel</button></a>
												</div>
											</div>
										</div>
									</div>
                                </div>
								
					        </div>
                            <!-- END SAMPLE FORM PORTLET-->
							
                        </div>
						
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
          
        </div>
		
<!-- untuk   -->
<script>
document.getElementById("gantifile").onclick = function() {file()};

function file() {
   // $("#div_file").hide();
   // $("#div_download").show();
}
</script>
