<!-- CSS dan JS untuk Search -->
<script
    src="<?php echo base_url();?>assets/js/bootstrap-select.js"
    defer="defer"></script>
<link
    rel="stylesheet"
    href="<?php echo base_url();?>assets/css/bootstrap-select.css">

<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE --> 
    <div class="page-title">
        <h1>Kesepakatan External
            <small>Detail Kesepakatan Eksternal</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="#">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">Kesepakatan</span>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">External</span>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">Detail</span>

    </li>
</ul>

<div class="row ">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Detail Kesepakatan</span>
                    <a
                        type="button"
                        href="<?php echo base_url();?>kesepakatan"
                        class="btn grey-salsa btn-outline">Kembali
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Kesepahaman</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <div class="form-group">
                            <input
                                class="form-control spinner"
                                value="<?php echo $detail['sph_judul'];?>"
                                readonly />
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Jenis Kesepakatan</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">	
                            <div class="form-group">
                            <input
                                class="form-control spinner"
                                value="<?php echo $detail['jns_kerjasama'];?>"
                                readonly />
                            </div>
                        </div>
                    </div>  
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Unit Kerja</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">	
                            <div class="form-group">
                            <input
                                class="form-control spinner"
                                value="<?php echo $unit['nama_unit'];?>"
                                readonly />
                            </div>
                        </div>
                    </div>  
                </div>	
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Nomor Kesepakatan</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <input
                                class="form-control spinner"
                                value="<?php echo $detail['spk_nomor'];?>"
                                readonly />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Judul Kesepakatan</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <input
                                class="form-control spinner"
                                value="<?php echo $detail['spk_judul'];?>"
                                readonly />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Tanggal Kesepakatan</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <input
                                class="form-control spinner"
                                value="<?php echo $detail['spk_tanggal'];?>"
                                readonly />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Nama Pihak 1</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <input
                                class="form-control spinner"
                                value="<?php echo $detail['spk_namapihak1'];?>"
                                readonly />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Jabatan Pihak 1</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <input
                                class="form-control spinner"
                                value="<?php echo $detail['spk_jabatanpihak1'];?>"
                                readonly />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>SK Pihak 1</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <input
                                class="form-control spinner"
                                value="<?php echo $detail['spk_skpihak1'];?>"
                                readonly />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Nama Pihak 2</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <input
                                class="form-control spinner"
                                value="<?php echo $detail['spk_namapihak2'];?>"
                                readonly />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Jabatan Pihak 2</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <input
                                class="form-control spinner"
                                value="<?php echo $detail['spk_jabatanpihak2'];?>"
                                readonly />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>SK Pihak 2</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <input
                                class="form-control spinner"
                                value="<?php echo $detail['spk_skpihak2'];?>"
                                readonly />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Tujuan Kesepakatan</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <textarea name="spk_tujuan" class="form-control spinner" readonly ><?php echo $detail['spk_tujuan'];?></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Tanggal Mulai</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <input
                                class="form-control spinner"
                                value="<?php echo $detail['spk_mulai'];?>"
                                readonly />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Tanggal Akhir</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <input
                                class="form-control spinner"
                                value="<?php echo $detail['spk_akhir'];?>"
                                readonly />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>File</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">

                            <?php 
										if($detail['spk_file'] == ''){ ?>
                            Tidak ada File
                        <?php	
									}else { ?>
                            <div id="div_download">
                                <a
                                    href="<?php echo base_url();?>assets/upload/<?php echo $detail['spk_file'];?>">Klik disini</a>
                            </div>
                            <?php
									}
									?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->

</div>

</div>
<!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>

</div>

<!-- untuk -->
<script>
document
.getElementById("gantifile")
.onclick = function () {
file()
};

function file() {
// $("#div_file").hide(); $("#div_download").show();
}
</script>