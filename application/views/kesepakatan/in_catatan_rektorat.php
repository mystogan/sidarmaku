<!-- CSS dan JS untuk Search -->
<script
    src="<?php echo base_url();?>assets/js/bootstrap-select.js"
    defer="defer"></script>
<link
    rel="stylesheet"
    href="<?php echo base_url();?>assets/css/bootstrap-select.css">

<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Kesepakatan Internal
            <small>Input Kesepakatan Internal</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="#">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">Kesepakatan</span>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">External</span>
    </li>
</ul>

<div class="row ">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-tag font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Disposisi Kesepakatan Kerjasama</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="caption">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <span class="bold">
                                                    Kesepakatan :</span>
                                                </div>
                                                <div class="col-md-12">
                                                    <span class=""><?php echo $detail['prm_judul'];?></span>
                                                </div>
                                            </div>
                                        </br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span class="bold">
                                                    Tanggal :</span>
                                            </div>
                                            <div class="col-md-12">
                                                <span class="caption-helper"><?php echo $detail['tanggal_permohonan'];?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="timeline">
                                <!-- TIMELINE ITEM -->
                                <div class="timeline-item">
                                    <div class="timeline-badge">
                                        <img
                                            class="timeline-badge-userpic"
                                            src="<?php echo base_url();?>assets/img/layout/avatar.png">
                                    </div>
                                    <div class="timeline-body">
                                        <div class="timeline-body-arrow"></div>
                                        <div class="timeline-body-head">
                                            <div class="timeline-body-head-caption">
                                                <a href="javascript:;" class="timeline-body-title font-blue-madison">Wakil Rektor III</a>
                                                <span class="timeline-body-time font-grey-cascade">
                                                <?php
                                                if($catatan[0]['tanggal_permohonan'] == "00-00-0000 00:00"){
                                                    echo "-";
                                                }else {
                                                    echo $catatan[0]['tanggal_permohonan'];
                                                }
                                                ?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="timeline-body-content">
                                            <span class="font-grey-cascade">
                                            <?php
                                            if($catatan[0]['dis_catatan'] == ''){
                                                echo "-";
                                            }else {
                                                echo $catatan[0]['dis_catatan'];
                                            }
                                            ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-badge">
                                        <img
                                            class="timeline-badge-userpic"
                                            src="<?php echo base_url();?>assets/img/layout/avatar.png">
                                    </div>
                                    <div class="timeline-body">
                                        <div class="timeline-body-arrow"></div>
                                        <div class="timeline-body-head">
                                            <div class="timeline-body-head-caption">
                                                <a href="javascript:;" class="timeline-body-title font-blue-madison">Rektor</a>
                                                <span class="timeline-body-time font-grey-cascade">
                                                <?php
                                                if($catatan[1]['tanggal_permohonan'] == "00-00-0000 00:00"){
                                                    echo "-";
                                                }else {
                                                    echo $catatan[1]['tanggal_permohonan'];
                                                }
                                                ?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="timeline-body-content">
                                            <span class="font-grey-cascade">
                                            <?php
                                            if($catatan[1]['dis_catatan'] == ''){
                                                echo "-";
                                            }else {
                                                echo $catatan[1]['dis_catatan'];
                                            }
                                            ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-badge">
                                        <img
                                            class="timeline-badge-userpic"
                                            src="<?php echo base_url();?>assets/img/layout/avatar.png">
                                    </div>
                                    <div class="timeline-body">
                                        <div class="timeline-body-arrow"></div>
                                        <div class="timeline-body-head">
                                            <div class="timeline-body-head-caption">
                                                <a href="javascript:;" class="timeline-body-title font-blue-madison">Wakil Rektor III</a>
                                                <span class="timeline-body-time font-grey-cascade">
                                                <?php
                                                if($catatan[2]['tanggal_permohonan'] == "00-00-0000 00:00"){
                                                    echo "-";
                                                }else {
                                                    echo $catatan[2]['tanggal_permohonan'];
                                                }
                                                ?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="timeline-body-content">
                                            <span class="font-grey-cascade">
                                            <?php
                                            if($catatan[2]['dis_catatan'] == ''){
                                                echo "-";
                                            }else {
                                                echo $catatan[2]['dis_catatan'];
                                            }
                                            ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-badge">
                                        <img
                                            class="timeline-badge-userpic"
                                            src="<?php echo base_url();?>assets/img/layout/avatar.png">
                                    </div>
                                    <div class="timeline-body">
                                        <div class="timeline-body-arrow"></div>
                                        <div class="timeline-body-head">
                                            <div class="timeline-body-head-caption">
                                                <a href="javascript:;" class="timeline-body-title font-blue-madison">Kabiro AAK</a>
                                                <span class="timeline-body-time font-grey-cascade">
                                                <?php
                                                if($catatan[3]['tanggal_permohonan'] == "00-00-0000 00:00"){
                                                    echo "-";
                                                }else {
                                                    echo $catatan[3]['tanggal_permohonan'];
                                                }
                                                ?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="timeline-body-content">
                                            <span class="font-grey-cascade">
                                            <?php
                                            if($catatan[3]['dis_catatan'] == ''){
                                                echo "-";
                                            }else {
                                                echo $catatan[3]['dis_catatan'];
                                            }
                                            ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-badge">
                                        <img
                                            class="timeline-badge-userpic"
                                            src="<?php echo base_url();?>assets/img/layout/avatar.png">
                                    </div>
                                    <div class="timeline-body">
                                        <div class="timeline-body-arrow"></div>
                                        <div class="timeline-body-head">
                                            <div class="timeline-body-head-caption">
                                                <a href="javascript:;" class="timeline-body-title font-blue-madison">Penanggung Jawab { <?php echo $catatan[4]['nama']; ?> }</a>
                                                <span class="timeline-body-time font-grey-cascade">
                                                <?php
												if($catatan[4]['spk_tanggal'] == "00-00-0000 00:00"){
													echo "-";
												}else {
													echo $catatan[4]['spk_tanggal'];
												}
												?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="timeline-body-content">
                                            <span class="font-grey-cascade">
                                            <?php
											if($catatan[4]['dis_catatan'] == ''){
												echo "-";
											}else {
												echo $catatan[4]['dis_catatan'];
											}
											?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-badge">
                                        <img
                                            class="timeline-badge-userpic"
                                            src="<?php echo base_url();?>assets/img/layout/avatar.png">
                                    </div>
                                    <div class="timeline-body">
                                        <div class="timeline-body-arrow"></div>
                                        <div class="timeline-body-head">
                                            <div class="timeline-body-head-caption">
                                                <a href="javascript:;" class="timeline-body-title font-blue-madison">Person In Charge (Pegawai PIC) { <?php echo $catatan[5]['nama']; ?> }</a>
                                                <span class="timeline-body-time font-grey-cascade">
                                                <?php
												if($catatan[5]['spk_tanggal'] == "00-00-0000 00:00"){
													echo "-";
												}else {
													echo $catatan[5]['spk_tanggal'];
												}
												?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="timeline-body-content">
                                            <span class="font-grey-cascade">
                                            <?php
											if($catatan[5]['dis_catatan'] == ''){
												echo "-";
											}else {
												echo $catatan[5]['dis_catatan'];
											}
											?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-badge">
                                        <img
                                            class="timeline-badge-userpic"
                                            src="<?php echo base_url();?>assets/img/layout/avatar.png">
                                    </div>
                                    <div class="timeline-body">
                                        <div class="timeline-body-arrow"></div>
                                        <div class="timeline-body-head">
                                            <div class="timeline-body-head-caption">
                                                <a href="javascript:;" class="timeline-body-title font-blue-madison">Validasi Penanggung Jawab { <?php echo $catatan[6]['nama']; ?> }</a>
                                                <span class="timeline-body-time font-grey-cascade">
                                                <?php
												if($catatan[6]['spk_tanggal'] == "00-00-0000 00:00"){
													echo "-";
												}else {
													echo $catatan[6]['spk_tanggal'];
												}
												?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="timeline-body-content">
                                            <span class="font-grey-cascade">
                                            <?php
											if($catatan[6]['dis_catatan'] == ''){
												echo "-";
											}else {
												echo $catatan[6]['dis_catatan'];
											}
											?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-badge">
                                        <img
                                            class="timeline-badge-userpic"
                                            src="<?php echo base_url();?>assets/img/layout/avatar.png">
                                    </div>
                                    <div class="timeline-body">
                                        <div class="timeline-body-arrow"></div>
                                        <div class="timeline-body-head">
                                            <div class="timeline-body-head-caption">
                                                <a href="javascript:;" class="timeline-body-title font-blue-madison">Kepala Bagian Kerjasama</a>
                                                <span class="timeline-body-time font-grey-cascade">
                                                <?php
                                                if($catatan[7]['tanggal_permohonan'] == "00-00-0000 00:00"){
                                                    echo "-";
                                                }else {
                                                    echo $catatan[7]['tanggal_permohonan'];
                                                }
                                                ?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="timeline-body-content">
                                            <span class="font-grey-cascade">
                                            <?php
                                            if($catatan[7]['dis_catatan'] == ''){
                                                echo "-";
                                            }else {
                                                echo $catatan[7]['dis_catatan'];
                                            }
                                            ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-badge">
                                        <img
                                            class="timeline-badge-userpic"
                                            src="<?php echo base_url();?>assets/img/layout/avatar.png">
                                    </div>
                                    <div class="timeline-body">
                                        <div class="timeline-body-arrow"></div>
                                        <div class="timeline-body-head">
                                            <div class="timeline-body-head-caption">
                                                <a href="javascript:;" class="timeline-body-title font-blue-madison">Kasubag Bagian Kerjasama</a>
                                                <span class="timeline-body-time font-grey-cascade">
                                                <?php
                                                if($catatan[8]['tanggal_permohonan'] == "00-00-0000 00:00"){
                                                    echo "-";
                                                }else {
                                                    echo $catatan[8]['tanggal_permohonan'];
                                                }
                                                ?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="timeline-body-content">
                                            <span class="font-grey-cascade">
                                            <?php
                                            if($catatan[8]['dis_catatan'] == ''){
                                                echo "-";
                                            }else {
                                                echo $catatan[8]['dis_catatan'];
                                            }
                                            ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!-- END TIMELINE ITEM -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <form
                            method="POST"
                            action="<?php echo base_url();?>kesepakatan/update_catatan_internal">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold font-green uppercase">Catatan</span>
                                </div>
                            </div>
                            <input type="hidden" name="dis_id" id="dis_id" value="<?php echo $dis_id; ?>"/>
                            <input type="hidden" name="spk_internal" id="spk_internal" value="3"/>
                            <input
                                type="hidden"
                                name="spk_id"
                                id="spk_id"
                                value="<?php echo $detail['spk_id']; ?>"/>
                                <?php
                            // cek apakah sudah disposisi apa belum
                            // cek juga hanya warek 3 yang memasukkan id_tugastambahan
							if($detail['dis_catatan'] == null && $id_tugastambahan == 4)
							{
							?>
                            <div class="portlet-body">
                                <label for="">Nama Penanggung Jawab</label>
                                <select
                                    id="spk_pegawaipihak1"
                                    data-live-search="true"
                                    name="spk_pegawaipihak1"
                                    class="form-control select2 selectpicker">

                                    <?php
                                    foreach($pegawai as $value){ ?>
                                    <option
                                        value="<?php echo $value['id'];?>"
                                        <?php if((rand(10,100)) == $value['id']){ echo "selected";}?>><?php echo $value['nama'];?></option>

                                    <?php
                                    }
                                    ?>

                                </select>
                            </div>
                            <div class="portlet-body">
                                <label for="">Nama Rekomendasi PIC</label>
                                <select
                                    id="spk_pegawaipic"
                                    data-live-search="true"
                                    name="spk_pegawaipic"
                                    class="form-control select2 selectpicker">

                                    <?php
                                    foreach($pegawai as $value){ ?>
                                    <option
                                        value="<?php echo $value['id'];?>"
                                        <?php if((rand(10,100)) == $value['id']){ echo "selected";}?>><?php echo $value['nama'];?></option>

                                    <?php
                                    }
                                    ?>

                                </select>
                            </div>
                            <?php
							}else if($detail['dis_catatan'] == null && $catatan[4]['dis_pegawaiasal'] == $this->session->userdata('user_id') && $count_pj == 1){ ?>
                            <div class="portlet-body">
                                <label for="">Nama Pegawai PIC</label>
                                <select
                                    id="spk_pegawaipic"
                                    data-live-search="true"
                                    name="spk_pegawaipic"
                                    class="form-control select2 selectpicker">

                                    <?php
                                    foreach($pegawai as $value){ ?>
                                    <option
                                        value="<?php echo $value['id'];?>"
                                        <?php if((rand(10,100)) == $value['id']){ echo "selected";}?>><?php echo $value['nama'];?></option>

                                    <?php
                                    }
                                    ?>

                                </select>
                            </div>

                            <div class="portlet-body">
                                <label for="">Masukkan Anggaran</label>
                                <input
                                    name="spk_anggaran"
                                    class="form-control spinner"
                                    value="<?php echo $detail['spk_anggaran'];?>"
                                    type="number"
                                    placeholder="Masukkan Anggaran"
                                    required="required"/>
                            </div>
                            <?php
                            }else if($detail['dis_catatan'] == null && $catatan[5]['dis_pegawaiasal'] == $this->session->userdata('user_id'))
                            {
                            ?>
                            <div class="portlet-body">
                                <label for="">Masukkan Anggaran</label>
                                <input
                                    name="spk_anggaran"
                                    class="form-control spinner"
                                    value="<?php echo $detail['spk_anggaran'];?>"
                                    type="number"
                                    placeholder="Masukkan Anggaran"
                                    required="required"/>
                            </div>
                            <?php
                            }
							?>
                            <div class="portlet-body">
                                <textarea class="wysihtml5 form-control" name="dis_catatan" rows="6" required="required" ><?php echo $detail['dis_catatan']; ?></textarea>
                            </div>
                            <?php
							if($detail['dis_catatan'] == null)
							{
							?>
                            <div class="portlet-body">
                                <button type="submit" class="btn green">
                                    <i class="fa fa-check"></i>
                                    <?php
                                    if ($catatan[5]['dis_pegawaiasal'] == $this->session->userdata('user_id')) {
                                      echo "Kirim ke Penanggung Jawab";
                                    }else {
                                      echo "Disposisi";
                                    }
                                     ?>
                                   </button>
                            </form>
                            <a
                                type="button"
                                href="<?php echo base_url();?>kesepakatan"
                                class="btn grey-salsa btn-outline">Kembali</a>
                            <?php
							// event untuk click file ini tapi dibawah sendiri dengan acuan ID
						?>
                            <a class="btn red" id="batal">Tidak Disposisi</a>
                        </div>
                        <?php
						}
						?>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END SAMPLE FORM PORTLET-->

</div>

</div>
<!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>

</div>

<!-- untuk -->
<script>
$('#batal').click(function () {
	let prm_id = $("#sph_id").val();
	let dis_id = $("#dis_id").val();
	if (confirm('Apakah Kamu Benar akan Membatalkan Permohonan ini ? ')) {
		window.location.assign(base_url + "permohonan/setpermohonan/" + prm_id + "/" + dis_id)
	}
});
</script>
