<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Kesepakatan Kerjasama
            <small>Kesepakatan Kerjasama</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="index.html">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">Kesepakatan Kerjasama</span>
    </li>
</ul>
</div>

<div class="row">
<div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-dark">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject bold uppercase">
                    Entri Kesepakatan UIN Sunan Ampel Surabaya (<?php echo $this->session->userdata('user_nama');?>)
                </span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-toolbar">
                <div class="row">
                <div class="col-md-12">
                        <form action="<?php echo base_url();?>kesepakatan" method="post">
                            <div class="col-md-3">
                                <select name="yearPost" id="yearPost" class="form-control">
                                <?php
                                for ($i=0; $i < 5; $i++) { ?>
                                    <option value="<?php echo (date('Y')-$i)?>" <?php if((date('Y')-$i) == $yearPost){ echo "selected";}?> ><?php echo (date('Y')-$i)?></option>
                                <?php
                                }
                                ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select name="internalPost" id="internalPost" class="form-control">
                                    <option value="1" <?php if($internalPost == '1'){ echo "selected"; }?> >Internal</option>
                                    <option value="2" <?php if($internalPost == '2'){ echo "selected"; }?> >Eksternal</option>

                                </select>
                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-primary">Cari</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <table
                class="table table-striped table-bordered table-hover table-checkable order-column"
                id="sample_1">
                <thead>
                    <tr>
                        <th style="text-align:center;">No</th>
                        <th style="text-align:center;">Nama Kesepahaman</th>
                        <th style="text-align:center;">Nomor Kesepakatan</th>
                        <th style="text-align:center;">Judul Kesepakatan</th>
                        <th style="text-align:center;">Tujuan</th>
                        <th style="text-align:center;">Tanggal</th>
                        <th style="text-align:center;">File</th>
                        <?php 
						if($this->session->userdata('id_tugastambahan') != 375){ ?>
                        <th style="text-align:center;">Catatan</th>
                        <th style="text-align:center;">Status</th>
                        <?php
                        }
                        ?>
                        <th style="text-align:center;">
                            Aksi
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $i=1;
                        foreach($disposisi as $Hdisposisi){
                        
                        ?>
                    <tr class="odd gradeX">
                        <td><?php echo $i;?></td>
                        <td><?php echo $Hdisposisi['sph_judul'];?></td>
                        <td><?php echo $Hdisposisi['spk_nomor'];?></td>
                        <td><?php echo $Hdisposisi['spk_judul'];?></td>
                        <td><?php echo $Hdisposisi['spk_tujuan'];?></td>
                        <td><?php echo $Hdisposisi['spk_tanggal'];?></td>

                        <td>
                        <?php
                            if($Hdisposisi['spk_file'] == ""){
                                echo "Tidak Ada File";
                            }else { ?>
                            <a href="<?php echo base_url();?>assets/upload/<?php echo $Hdisposisi['spk_file'];?>">Klik disini</a>
                            <?php													
                            }
                            ?>
                        </td>
                        <td style="text-align:center;">
                        <?php 
                            if($this->session->userdata('id_tugastambahan') != 375){ 
                                if($Hdisposisi['spk_status'] == 3)
                                {
                                }else 
                                {
                                ?>
                            <a href="<?php echo base_url();?>kesepakatan/catatan/<?php echo ($Hdisposisi['dis_id']);?>" class="btn btn-outline blue btn-sm active">
                                Catatan</a>
                                <?php
                                }
                                ?>
                        </td>
                        <td>
                        <?php
                            if($Hdisposisi['spk_status'] == 3)
                            {
                                $pisah = explode(" ",$paksa[($i-1)]['nama_jastruk']);
                                $dekan = $pisah[0]." ".$pisah[1];
                                if($paksa[($i-1)]['nama_jastruk'] == 'Wakil Dekan Bidang Kemahasiswaan dan Kerjasama')
                                {                                    
                                    echo "<span style='color:red'>Wadek 3 Telah Membatalkan Disposisi</span>";
                                }else if($dekan == 'Dekan Fakultas')
                                {                                    
                                    echo "<span style='color:red'>Dekan Telah Membatalkan Disposisi</span>";
                                }else if($paksa[($i-1)]['dis_jastrukasal'] == '1')
                                {
                                    echo "<span style='color:red'>Rektor Telah Membatalkan Disposisi</span>";
                                }else if($paksa[($i-1)]['dis_jastrukasal'] == '4')
                                {
                                    echo "<span style='color:red'>Wakil Rektor 3 Telah Membatalkan Disposisi</span>";
                                
                                }else if($paksa[($i-1)]['dis_jastrukasal'] == '353')
                                {
                                    echo "<span style='color:red'>Biro Administrasi Akademik, Kemahasiswaan dan Kerjasama Telah Membatalkan Disposisi</span>";
                                
                                }else if($paksa[($i-1)]['dis_jastrukasal'] == '369')
                                {
                                    echo "<span style='color:red'>Kepala Bagian Kerjasama Telah Membatalkan Disposisi</span>";
                                
                                }else if($paksa[($i-1)]['dis_jastrukasal'] == '370')
                                {
                                    echo "<span style='color:red'>Kepala Sub Bagian Kerjasama Telah Membatalkan Disposisi</span>";
                                }
                                
                            }else if($Hdisposisi['dis_status'] == 1)
                            {
                                echo '<button type="button" class="btn btn-warning">Belum di Disposisi</button>';
                            }else if($Hdisposisi['dis_status'] == 2)
                            {
                                echo '<button type="button" class="btn btn-success">Sudah di Disposisi</button>';
                            
                            }else if($Hdisposisi['dis_status'] == 3)
                            {
                                echo '<button type="button" class="btn btn-danger">Batal</button>';
                            }else 
                            {
                                echo '<button type="button" class="btn btn-link">Terhapus</button>';
                                
                            }
                        ?>
                        </td>
                        <?php
                        }
                        ?>
                       
                        <td style="text-align:center;">
                            <!-- <a href="viewmou.html" title="Detail" class="btn btn-outline green btn-sm
                            active"> <i class="fa fa-binoculars"></i> </a> -->
                            <?php
                            if($Hdisposisi['spk_pegawaipic'] == $this->session->userdata('user_id')){   ?>
                            <a
                                href="<?php echo base_url();?>kesepakatan/in_add/<?php echo ($Hdisposisi['spk_id']);?>"
                                title="Edit"
                                class="btn btn-outline yellow btn-sm active">
                                <i class="fa fa-edit"></i>
                            </a>
                            <?php
                            }else { ?>
                            <a
                                href="<?php echo base_url();?>kesepakatan/detail/<?php echo ($Hdisposisi['dis_id']);?>"
                                title="Edit"
                                class="btn btn-outline yellow btn-sm active">
                                <i class="fa fa-edit"></i>
                            </a>
                            <?php
                            }
                            ?>
                            
                        </td>

                    </tr>
                    <?php
                    $i++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>

</div>
</div>
</div>
<!-- END QUICK SIDEBAR -->
</div>

<link
href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"
rel="stylesheet"
type="text/css"/>

<script
src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"
type="text/javascript"></script>
<script
src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"
type="text/javascript"></script>