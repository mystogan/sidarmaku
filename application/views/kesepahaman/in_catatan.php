<!-- CSS dan JS untuk Search -->
<script
    src="<?php echo base_url();?>assets/js/bootstrap-select.js"
    defer="defer"></script>
<link
    rel="stylesheet"
    href="<?php echo base_url();?>assets/css/bootstrap-select.css">

<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Kesepahaman Internal
            <small>Input Kesepahaman Internal</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="#">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">Kesepahaman</span>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">External</span>
    </li>
</ul>

<div class="row ">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-tag font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Disposisi Kesepahaman Kerjasama</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="caption">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <span class="bold">
                                                    Kesepahaman :</span>
                                                </div>
                                                <div class="col-md-12">
                                                    <span class=""><?php echo $detail['sph_judul'];?></span>
                                                </div>
                                            </div>
                                        </br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span class="bold">
                                                    Tanggal :</span>
                                            </div>
                                            <div class="col-md-12">
                                                <span class="caption-helper"><?php echo $detail['sph_tanggal'];?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="timeline">
                                <!-- TIMELINE ITEM -->
                                <div class="timeline-item">
                                    <div class="timeline-badge">
                                        <img
                                            class="timeline-badge-userpic"
                                            src="<?php echo base_url();?>assets/img/layout/avatar.png">
                                    </div>
                                    <div class="timeline-body">
                                        <div class="timeline-body-arrow"></div>
                                        <div class="timeline-body-head">
                                            <div class="timeline-body-head-caption">
                                                <a href="javascript:;" class="timeline-body-title font-blue-madison">Wakil Dekan 3</a>
                                                <span class="timeline-body-time font-grey-cascade">
                                                <?php 
                                                    if($catatan[0]['sph_tanggal'] == "00-00-0000 00:00"){
                                                        echo "-";
                                                    }else {
                                                        echo $catatan[0]['sph_tanggal']; 
                                                    }
                                                    ?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="timeline-body-content">
                                            <span class="font-grey-cascade">
                                            <?php
                                                if($catatan[0]['dis_catatan'] == ''){
                                                    echo "-";
                                                }else {
                                                    echo $catatan[0]['dis_catatan'];
                                                }
                                                ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-badge">
                                        <img
                                            class="timeline-badge-userpic"
                                            src="<?php echo base_url();?>assets/img/layout/avatar.png">
                                    </div>
                                    <div class="timeline-body">
                                        <div class="timeline-body-arrow"></div>
                                        <div class="timeline-body-head">
                                            <div class="timeline-body-head-caption">
                                                <a href="javascript:;" class="timeline-body-title font-blue-madison">Dekan</a>
                                                <span class="timeline-body-time font-grey-cascade">
                                                <?php 
												if($catatan[1]['sph_tanggal'] == "00-00-0000 00:00"){
													echo "-";
												}else {
													echo $catatan[1]['sph_tanggal']; 
												}
												?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="timeline-body-content">
                                            <span class="font-grey-cascade">
                                            <?php
											if($catatan[1]['dis_catatan'] == ''){
												echo "-";
											}else {
												echo $catatan[1]['dis_catatan'];
											}
											?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-badge">
                                        <img
                                            class="timeline-badge-userpic"
                                            src="<?php echo base_url();?>assets/img/layout/avatar.png">
                                    </div>
                                    <div class="timeline-body">
                                        <div class="timeline-body-arrow"></div>
                                        <div class="timeline-body-head">
                                            <div class="timeline-body-head-caption">
                                                <a href="javascript:;" class="timeline-body-title font-blue-madison">Rektor</a>
                                                <span class="timeline-body-time font-grey-cascade">
                                                <?php 
												if($catatan[2]['sph_tanggal'] == "00-00-0000 00:00"){
													echo "-";
												}else {
													echo $catatan[2]['sph_tanggal']; 
												}
												?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="timeline-body-content">
                                            <span class="font-grey-cascade">
                                            <?php
											if($catatan[2]['dis_catatan'] == ''){
												echo "-";
											}else {
												echo $catatan[2]['dis_catatan'];
											}
											?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-badge">
                                        <img
                                            class="timeline-badge-userpic"
                                            src="<?php echo base_url();?>assets/img/layout/avatar.png">
                                    </div>
                                    <div class="timeline-body">
                                        <div class="timeline-body-arrow"></div>
                                        <div class="timeline-body-head">
                                            <div class="timeline-body-head-caption">
                                                <a href="javascript:;" class="timeline-body-title font-blue-madison">Wakil Rektor III</a>
                                                <span class="timeline-body-time font-grey-cascade">
                                                <?php 
												if($catatan[3]['sph_tanggal'] == "00-00-0000 00:00"){
													echo "-";
												}else {
													echo $catatan[3]['sph_tanggal']; 
												}
												?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="timeline-body-content">
                                            <span class="font-grey-cascade">
                                            <?php
											if($catatan[3]['dis_catatan'] == ''){
												echo "-";
											}else {
												echo $catatan[3]['dis_catatan'];
											}
											?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-badge">
                                        <img
                                            class="timeline-badge-userpic"
                                            src="<?php echo base_url();?>assets/img/layout/avatar.png">
                                    </div>
                                    <div class="timeline-body">
                                        <div class="timeline-body-arrow"></div>
                                        <div class="timeline-body-head">
                                            <div class="timeline-body-head-caption">
                                                <a href="javascript:;" class="timeline-body-title font-blue-madison">Kabiro AAK</a>
                                                <span class="timeline-body-time font-grey-cascade">
                                                <?php 
												if($catatan[4]['sph_tanggal'] == "00-00-0000 00:00"){
													echo "-";
												}else {
													echo $catatan[4]['sph_tanggal']; 
												}
												?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="timeline-body-content">
                                            <span class="font-grey-cascade">
                                            <?php
											if($catatan[4]['dis_catatan'] == ''){
												echo "-";
											}else {
												echo $catatan[4]['dis_catatan'];
											}
											?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-badge">
                                        <img
                                            class="timeline-badge-userpic"
                                            src="<?php echo base_url();?>assets/img/layout/avatar.png">
                                    </div>
                                    <div class="timeline-body">
                                        <div class="timeline-body-arrow"></div>
                                        <div class="timeline-body-head">
                                            <div class="timeline-body-head-caption">
                                                <a href="javascript:;" class="timeline-body-title font-blue-madison">Kepala Bagian Kerjasama</a>
                                                <span class="timeline-body-time font-grey-cascade">
                                                <?php 
												if($catatan[5]['sph_tanggal'] == "00-00-0000 00:00"){
													echo "-";
												}else {
													echo $catatan[5]['sph_tanggal']; 
												}
												?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="timeline-body-content">
                                            <span class="font-grey-cascade">
                                            <?php
											if($catatan[5]['dis_catatan'] == ''){
												echo "-";
											}else {
												echo $catatan[5]['dis_catatan'];
											}
											?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-badge">
                                        <img
                                            class="timeline-badge-userpic"
                                            src="<?php echo base_url();?>assets/img/layout/avatar.png">
                                    </div>
                                    <div class="timeline-body">
                                        <div class="timeline-body-arrow"></div>
                                        <div class="timeline-body-head">
                                            <div class="timeline-body-head-caption">
                                                <a href="javascript:;" class="timeline-body-title font-blue-madison">Kasubag Bagian Kerjasama</a>
                                                <span class="timeline-body-time font-grey-cascade">
                                                <?php 
													if($catatan[6]['sph_tanggal'] == "00-00-0000 00:00"){
														echo "-";
													}else {
														echo $catatan[6]['sph_tanggal']; 
													}
													?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="timeline-body-content">
                                            <span class="font-grey-cascade">
                                            <?php
												if($catatan[6]['dis_catatan'] == ''){
													echo "-";
												}else {
													echo $catatan[6]['dis_catatan'];
												}
												?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!-- END TIMELINE ITEM -->
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <form
                            method="POST"
                            action="<?php echo base_url();?>kesepahaman/update_catatan_internal">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold font-green uppercase">Catatan</span>
                                </div>
                            </div>
                            <input type="hidden" name="dis_id" id="dis_id" value="<?php echo $dis_id; ?>"/>
                            <input
                                type="hidden"
                                name="sph_id"
                                id="sph_id"
                                value="<?php echo $detail['sph_id']; ?>"/>
                            <div class="portlet-body">
                                <textarea class="wysihtml5 form-control" name="dis_catatan" rows="6" required="required" ><?php echo $detail['dis_catatan']; ?></textarea>
                            </div>
							<?php
							if($detail['dis_catatan'] == null)
							{
							?>
                            <div class="portlet-body">
                                <button type="submit" class="btn green">
                                    <i class="fa fa-check"></i>
                                    Disposisi</button>
                            </form>
                            <a
                                type="button"
                                href="<?php echo base_url();?>kesepahaman"
                                class="btn grey-salsa btn-outline">Kembali</a>
                            <?php
							// event untuk click file ini tapi dibawah sendiri dengan acuan ID
							?>
                            <a class="btn red" id="batal">Tidak Disposisi</a>
                        	</div>
							<?php
							}
							?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>

</div>

<!-- untuk -->
<script>
$('#batal').click(function () {
	let prm_id = $("#sph_id").val();
	let dis_id = $("#dis_id").val();
	if (confirm('Apakah Kamu Benar akan Membatalkan Permohonan ini ? ')) {
		window.location.assign(base_url + "kesepahaman/setpermohonan/" + prm_id + "/" + dis_id)
	}
});
</script>