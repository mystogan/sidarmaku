<!-- CSS dan JS untuk Search -->
<script
    src="<?php echo base_url();?>assets/js/bootstrap-select.js"
    defer="defer"></script>
<link
    rel="stylesheet"
    href="<?php echo base_url();?>assets/css/bootstrap-select.css">

<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Kesepahaman Internal
            <small>Input Kesepahaman Internal</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="#">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">Kesepahaman</span>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">Internal</span>
    </li>
</ul>

<div class="row ">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Input Kesepahaman</span>
                </div>
            </div>
            <div class="portlet-body">
                <form
                    action="<?php echo base_url();?>kesepahaman/addKesepahaman_mitra"
                    method="post"
                    enctype="multipart/form-data">
                    <input
                        type="text"
                        name="sph_id"
                        value="<?php echo $kesepahaman['sph_id'];?>"/>
                    <input type="text" name="sph_internal" value="2"/>
                    
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Permohonan</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <div class="form-group">
                                    <select
                                        id="prm_id"
                                        data-live-search="true"
                                        name="prm_id"
                                        class="form-control select2 selectpicker">

                                        <?php 
										foreach($permohonan as $value){ ?>
                                        <option
                                            value="<?php echo $value['prm_id'];?>"
                                            <?php if($kesepahaman['prm_id'] == $value['prm_id']){ echo "selected";}?>><?php echo $value['prm_judul'];?></option>

                                        <?php
										}
										?>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Nomor Kesepahaman</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="sph_nomor"
                                    class="form-control spinner"
                                    value="<?php echo $kesepahaman['sph_nomor'];?>"
                                    type="number"
                                    placeholder="Nomor Kesepahaman"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Judul Kesepahaman</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="sph_judul"
                                    class="form-control spinner"
                                    value="<?php echo $kesepahaman['sph_judul'];?>"
                                    type="text"
                                    placeholder="Judul Kesepahaman"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Tanggal Kesepahaman</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="sph_tanggal"
                                    class="form-control spinner"
                                    value="<?php echo $kesepahaman['sph_tanggal'];?>"
                                    type="date"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Nama Pihak 1</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="sph_namapihak1"
                                    class="form-control spinner"
                                    value="<?php echo $kesepahaman['sph_namapihak1'];?>"
                                    type="text"
                                    placeholder="Masukkan Nama Pihak 1"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Jabatan Pihak 1</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="sph_jabatanpihak1"
                                    class="form-control spinner"
                                    value="<?php echo $kesepahaman['sph_jabatanpihak1'];?>"
                                    type="text"
                                    placeholder="Masukkan Jabatan Pihak 1"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>SK Pihak 1</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="sph_skpihak1"
                                    class="form-control spinner"
                                    value="<?php echo $kesepahaman['sph_skpihak1'];?>"
                                    type="text"
                                    placeholder="Masukkan SK Pihak 1"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Nama Pegawai</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <div class="form-group">
                                    <select
                                        id="sph_pegawaipihak1"
                                        data-live-search="true"
                                        name="sph_pegawaipihak1"
                                        class="form-control select2 selectpicker">

                                        <?php 
										foreach($pegawai as $value){ ?>
                                        <option
                                            value="<?php echo $value['id'];?>"
                                            <?php if($kesepahaman['sph_pegawaipihak1'] == $value['id']){ echo "selected";}?>><?php echo $value['nama'];?></option>

                                        <?php
										}
										?>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Nama Pihak 2</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="sph_namapihak2"
                                    class="form-control spinner"
                                    value="<?php echo $kesepahaman['sph_namapihak2'];?>"
                                    type="text"
                                    placeholder="Masukkan Nama Pihak 2"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Jabatan Pihak 2</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="sph_jabatanpihak2"
                                    class="form-control spinner"
                                    value="<?php echo $kesepahaman['sph_jabatanpihak2'];?>"
                                    type="text"
                                    placeholder="Masukkan Jabatan Pihak 2"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>SK Pihak 2</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="sph_skpihak2"
                                    class="form-control spinner"
                                    value="<?php echo $kesepahaman['sph_skpihak2'];?>"
                                    type="text"
                                    placeholder="Masukkan SK Pihak 2"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Tujuan Kesepahaman</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <textarea name="sph_tujuan" class="form-control spinner" required="required"><?php echo $kesepahaman['sph_tujuan'];?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Tanggal Mulai</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="sph_mulai"
                                    class="form-control spinner"
                                    value="<?php echo $kesepahaman['sph_mulai'];?>"
                                    type="date"
                                    placeholder="Judul Permohonan"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>Tanggal Akhir</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <input
                                    name="sph_akhir"
                                    class="form-control spinner"
                                    value="<?php echo $kesepahaman['sph_akhir'];?>"
                                    type="date"
                                    placeholder="Judul Permohonan"
                                    required="required"/>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row ">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <h4>File</h4>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9">
                                <div id="div_file">
                                    <input name="sph_file" type="file" required />
                                </div>
                                	<?php 
										if($kesepahaman['sph_file'] == ''){ ?>

                           			 <?php
									}else { ?>
									<!-- <div id="div_download"> <a href="<?php echo
									base_url();?>assets/upload/<?php echo $kesepahaman['sph_file'];?>">Klik
									disini</a> <button type="button" id="gantifile" class="btn green">Ganti
									File</button> </div> -->
									<?php
									}
									?>
                            </div>
                            
                        </div>
                        <div style="#36c6d3;width:100%;padding:2vh 0vh 2vh 0vh; ">
                                <strong style="color:red">Catatan</strong>: Ketika "<strong>Edit</strong>" Silahkan Upload Kembali
                                <br>
                                Jika Tidak File akan terhapus
                            </div>
                    </div>
                    
                </div>
                <div class="portlet-body">
                    <div class="modal-footer">
                        <a href="<?php echo base_url();?>kesepahaman">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Batal</button>
                        </a>
                        <button type="submit" class="btn green">Simpan</button>
                    </div>
                </div>
            </form>
            
        </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->

</div>

</div>
<!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>

</div>

<!-- untuk -->
<script>
document
.getElementById("gantifile")
.onclick = function () {
file()
};

function file() {
// $("#div_file").hide(); $("#div_download").show();
}
</script>