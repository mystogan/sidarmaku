<!-- CSS dan JS untuk Search -->
<script
    src="<?php echo base_url();?>assets/js/bootstrap-select.js"
    defer="defer"></script>
<link
    rel="stylesheet"
    href="<?php echo base_url();?>assets/css/bootstrap-select.css">

<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Kesepahaman External
            <small>Detail Kesepahaman Eksternal</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="#">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">Kesepahaman</span>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">External</span>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">Detail</span>

    </li>
</ul>

<div class="row ">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Detail Kesepahaman</span>
                    <a
                        type="button"
                        href="<?php echo base_url();?>kesepahaman"
                        class="btn grey-salsa btn-outline">Kembali
                    </a>

                </div>
            </div>
            <div class="portlet-body">

                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Mitra</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <div class="form-group">
                                <input
                                    class="form-control spinner"
                                    value="<?php echo $detail['mtr_namainstansi'];?>"
                                    type="text"
                                    readonly="readonly"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Permohonan</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <div class="form-group">
                                <input
                                    class="form-control spinner"
                                    value="<?php echo $detail['prm_judul'];?>"
                                    type="text"
                                    readonly="readonly"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Nomor Kesepahaman</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <div class="form-group">
                                <input
                                    class="form-control spinner"
                                    value="<?php echo $detail['sph_nomor'];?>"
                                    type="text"
                                    readonly="readonly"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Judul Kesepahaman</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <div class="form-group">
                                <input
                                    class="form-control spinner"
                                    value="<?php echo $detail['sph_judul'];?>"
                                    type="text"
                                    readonly="readonly"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Tanggal Kesepahaman</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <div class="form-group">
                                <input
                                    class="form-control spinner"
                                    value="<?php echo $detail['sph_tanggal'];?>"
                                    type="text"
                                    readonly="readonly"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Nama Pihak 1</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <div class="form-group">
                                <input
                                    class="form-control spinner"
                                    value="<?php echo $detail['sph_namapihak1'];?>"
                                    type="text"
                                    readonly="readonly"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Jabatan Pihak 1</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <div class="form-group">
                                <input
                                    class="form-control spinner"
                                    value="<?php echo $detail['sph_jabatanpihak1'];?>"
                                    type="text"
                                    readonly="readonly"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>SK Pihak 1</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <div class="form-group">
                                <input
                                    class="form-control spinner"
                                    value="<?php echo $detail['sph_skpihak1'];?>"
                                    type="text"
                                    readonly="readonly"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Nama Pihak 2</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <div class="form-group">
                                <input
                                    class="form-control spinner"
                                    value="<?php echo $detail['sph_namapihak2'];?>"
                                    type="text"
                                    readonly="readonly"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Jabatan Pihak 2</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <div class="form-group">
                                <input
                                    class="form-control spinner"
                                    value="<?php echo $detail['sph_jabatanpihak2'];?>"
                                    type="text"
                                    readonly="readonly"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>SK Pihak 2</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <div class="form-group">
                                <input
                                    class="form-control spinner"
                                    value="<?php echo $detail['sph_skpihak2'];?>"
                                    type="text"
                                    readonly="readonly"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Tujuan Kesepahaman</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <textarea
                                disabled="disabled"
                                name="sph_tujuan"
                                class="form-control spinner"
                                required="required"><?php echo $detail['sph_tujuan'];?></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Tanggal Mulai</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <div class="form-group">
                                <input
                                    class="form-control spinner"
                                    value="<?php echo $detail['sph_mulai'];?>"
                                    type="text"
                                    readonly="readonly"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>Tanggal Akhir</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                            <div class="form-group">
                                <input
                                    class="form-control spinner"
                                    value="<?php echo $detail['sph_akhir'];?>"
                                    type="text"
                                    readonly="readonly"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <h4>File</h4>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">

                            <?php 
										if($detail['sph_file'] == ''){ ?>
                            Tidak ada File
                        <?php	
									}else { ?>
                            <div id="div_download">
                                <a
                                    href="<?php echo base_url();?>assets/upload/<?php echo $detail['sph_file'];?>">Klik disini</a>
                            </div>
                            <?php
									}
									?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->

</div>

</div>
<!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>

</div>

<!-- untuk -->
<script>
document
.getElementById("gantifile")
.onclick = function () {
file()
};

function file() {
// $("#div_file").hide(); $("#div_download").show();
}
</script>