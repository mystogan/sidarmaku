<!-- BEGIN PAGE HEAD-->
<div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Permohonan MoU Kerjasama
                                <small>MoU Kerjasama</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">MoU Kerjasama</span>
                        </li>
                    </ul>
                    </div>
                    
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Permohonan MOU Kerjasama (<?php echo $this->session->userdata('user_nama');?>)</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="btn-group">
                                                <a href="<?php echo base_url();?>kesepahaman/addMouIn"><button id="sample_editable_1_new" class="btn btn-success btn-sm"> Tambah Data 
                                                            <i class="fa fa-plus"></i>
                                                    </button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
										  <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center;"> Nomor MoU </th>
                                                <th style="text-align:center;"> Judul MoU </th>
                                                <th style="text-align:center;"> Tanggal MoU </th>
                                                <th style="text-align:center;"> Tanggal Mulai </th>
                                                <th style="text-align:center;"> Tanggal Akhir</th>
                                                <th style="text-align:center;"> Aksi </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
											$i=1;
											foreach($disposisi as $Hdisposisi){
											
											?>
                                            <tr class="odd gradeX">
                                                
                                                <td>
                                                <?php echo $Hdisposisi['sph_nomor'];?>
                                                </td>
												<td style="text-align:center;"><?php echo $Hdisposisi['sph_judul'];?> </td>
												   <td style="text-align:center;"> <?php echo $Hdisposisi['sph_tanggal'];?> </td>
												   <td style="text-align:center;"> <?php echo $Hdisposisi['sph_awal'];?> </td>
												   <td style="text-align:center;"> <?php echo $Hdisposisi['sph_akhir'];?> </td>
                                               
												 <td style="text-align:center;">	
													<a href="<?php echo base_url();?>kesepahaman/detail/<?php echo ($Hdisposisi['dis_id']);?>" title="Detail" class="btn btn-outline green btn-sm active">
                                                        <i class="fa fa-binoculars"></i> </a>
													<a href="" title="Print" class="btn btn-outline btn-info btn-sm active">
                                                        <i class="fa fa-print"></i> </a>
                                                 </td>
												
                                            </tr>
                                            <?php
											$i++;
											}
											?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->