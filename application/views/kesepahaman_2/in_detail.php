<div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>MoU 
                                <small>Nota Kesepemahaman</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">MoU</span>
                        </li>
                    </ul>
                   
                    <div class="row ">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-share font-dark"></i>
                                        <span class="caption-subject font-dark bold uppercase">Permohonan Nota Kesepemahaman</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
								
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Mitra Kerja	</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="<?php echo $detail['mtr_namainstansi'];?>" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Judul Permohonan	</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="<?php echo $detail['sph_judul'];?>" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Nomor Permohonan MoU</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="<?php echo $detail['sph_nomor'];?>" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Tanggal</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="<?php echo $detail['sph_tanggal'];?>" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Nama Pihak 1</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="<?php echo $detail['sph_namapihak1'];?>" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Jabatan Pihak 1</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="<?php echo $detail['sph_jabatanpihak1'];?>" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>SK Pihak 1</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="<?php echo $detail['sph_skpihak1'];?>" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Nama Pihak 2</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="<?php echo $detail['sph_namapihak2'];?>" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Jabatan Pihak 1</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="<?php echo $detail['sph_jabatanpihak1'];?>" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>SK Pihak 2</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="<?php echo $detail['sph_skpihak2'];?>" disabled>
												</div>
										</div>  
									</div>
									
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Tanggal Mulai Permohonan</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="<?php echo $detail['sph_mulai'];?>" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Tanggal Akhir Permohonan</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="<?php echo $detail['sph_akhir'];?>" disabled>
												</div>
										</div>  
									</div>
									
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Tujuan</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="<?php echo $detail['sph_tujuan'];?>" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4> File</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												<a href="<?php echo base_url();?>assets/upload/kesepahaman/<?php echo $detail['sph_file'];?>">Klik disini</a>
												</div>
										</div>  
									</div>
									<div class="portlet-body">
										<div class="modal-footer">
											<a href="<?php echo base_url();?>kesepahaman"><button type="button" class="btn green">Kembali</button></a>
										</div>
									</div>
                                   
                                </div>
                            </div>