  <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Permohonan MoU Kerjasama
                                <small>Mou Kerjasama</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Permohonan Kerjasama</span>
                        </li>
                    </ul>
                    </div>
                    
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Permohonan Mou Kerjasama (<?php echo $this->session->userdata('user_nama');?>) </span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="btn-group">
                                                    <?php 
                                                    $userj = $this->session->userdata("user_jenis");
                                                    if($userj==1){?>
                                                        <a href="<?php echo base_url();?>kesepahaman/addMouEx"><button id="sample_editable_1_new" class="btn btn-success btn-sm"> Tambah Data 
                                                            <i class="fa fa-plus"></i>
                                                        </button></a>
                                                    <?php }else{?>
                                                        <a href="<?php echo base_url();?>kesepahaman/addMouIn"><button id="sample_editable_1_new" class="btn btn-success btn-sm"> Tambah Data 
                                                            <i class="fa fa-plus"></i>
                                                        </button></a>
                                                    <?php }?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
									<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
						<th>
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                                        <span></span>
                                                    </label>
                                                </th>
                                                <th style="text-align:center;">No MoU</th>
                                                <th style="text-align:center;">Judul MoU</th>
                                                <th style="text-align:center;">Tanggal Mou</th>
                                                <th style="text-align:center;">Tanggal Mulai</th>
                                                <th style="text-align:center;">Tanggal Akhir</th>
                                                <th style="text-align:center;"> Aksi </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
											$i=1;
											foreach($kesepahaman as $ksph){
											
											?>
                                            <tr class="odd gradeX">
						<td>
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="checkboxes" value="1" />
                                                        <span></span>
                                                    </label>
                                                </td>                                           
                                                
												<td style="text-align:center;"><?php echo $ksph['sph_nomor'];?></td>
												<td style="text-align:center;"><?php echo $ksph['sph_judul'];?></td>
												<td style="text-align:center;"><?php echo $ksph['sph_tanggal'];?></td>
												<td style="text-align:center;"><?php echo $ksph['sph_mulai'];?></td>
												<td style="text-align:center;"><?php echo $ksph['sph_akhir']?></td>
												<!-- <td><?php 
													if($ksph['sph_status'] == 1){
														echo "Draf";
													}else if($ksph['sph_status'] == 2){
														echo "Proses";
													}else if($ksph['sph_status'] == 3){
														echo "Accept";
													}else {
														echo "Terhapus";
													}
												?></td> -->
												<td style="text-align:center;">	
													<a href="<?php echo base_url();?>kesepahaman/detail" title="Detail" class="btn btn-outline green btn-sm active">
                                                        <i class="fa fa-binoculars"></i> </a>
                                                    <a href="formmou.html" title="Edit" class="btn btn-outline yellow btn-sm active">
                                                        <i class="fa fa-edit"></i>  </a>	
													<a href="javascript:;" title="Hapus" class="btn btn-outline red btn-sm active">
                                                        <i class="fa fa-trash"></i> </a>
                                                 </td>                                                  </td>												
                                            </tr>
                                            <?php
											$i++;
											}
											?>
                                        </tbody>
                                    </table>
									
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                    </div>
                </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>