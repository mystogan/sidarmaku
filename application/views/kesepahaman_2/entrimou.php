<!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>MoU 
                                <small>Nota Kesepemahaman</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">MoU</span>
                        </li>
                    </ul>
                   
                    <div class="row ">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-share font-dark"></i>
                                        <span class="caption-subject font-dark bold uppercase">Permohonan Nota Kesepemahaman</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
								
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Nomor Permohonan MoU</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" type="text" placeholder="Nomor Permohonan MoU" /> 
											</div>
										</div>  
									</div>
									
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Mitra Kerja</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<div class="form-group">
													<label for="single" class="control-label">Pilih Mitra Kerja</label>
													<select id="single" class="form-control select2">
														<option></option>
														<optgroup label="Internal">
															<option value="AK">Fakultas xxx</option>
															<option value="HI">Dosen</option>
															<option value="HI">Unit</option>
														</optgroup>
														<optgroup label="Eksternal">
															<option value="CA">Perusahaan xxx</option>
															<option value="NV">Universitas xxx</option>
															<option value="OR">Instansi Pemerintah xxx</option>
														</optgroup>
													</select>
												</div>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Surat Permohonan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<div class="form-group">
													<label for="single" class="control-label">Pilih Surat Permohonan</label>
													<select id="single" class="form-control select2">
														<option></option>
														<optgroup label="Internal">
															<option value="AK">Fakultas xxx</option>
															<option value="HI">Dosen</option>
															<option value="HI">Unit</option>
														</optgroup>
														<optgroup label="Eksternal">
															<option value="CA">Perusahaan xxx</option>
															<option value="NV">Universitas xxx</option>
															<option value="OR">Instansi Pemerintah xxx</option>
														</optgroup>
													</select>
												</div>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Judul Permohonan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" type="text" placeholder="Judul Permohonan" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Nama Pihak 1</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" type="text" placeholder="Masukkan Nama Pihak 1" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Jabatan Pihak 1</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" type="text" placeholder="Masukkan Jabatan Pihak 1" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>SK Pihak 1</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" type="text" placeholder="Masukkan No SK Pihak 1" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Pegawai Pihak 1</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" type="number" placeholder="Masukkan Pegawai Pihak 1" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Nama Pihak 2</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" type="text" placeholder="Masukkan Nama Pihak 2" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Jabatan Pihak 2</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" type="text" placeholder="Masukkan Jabatan Pihak 2" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>SK Pihak 2</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" type="text" placeholder="Masukkan No SK Pihak 2" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Tujuan Permohonan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<textarea id="summernote" name="editordata"></textarea>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Tanggal Mulai Permohonan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                                        <input type="text" class="form-control" readonly name="datepicker">
                                                        <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                                    </div>
                                                    <!-- /input-group -->
                                                    <span class="help-block"> select a date </span>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Tanggal Akhir Permohonan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                                        <input type="text" class="form-control" readonly name="datepicker">
                                                        <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                                    </div>
                                                    <!-- /input-group -->
                                                    <span class="help-block"> select a date </span>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Unit</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" type="text" placeholder="Unit" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Anggaran</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" type="text" placeholder="Anggaran" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
									<div class="row ">
										<div class="col-xs-12 col-sm-12 col-md-3">
											<h4>File </h4>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-9">	
										<div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
													<span class="fileinput-filename"> </span>
                                                </div>
                                                <span class="input-group-addon btn default btn-file">
                                                <span class="fileinput-new"> Select file </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="..."> </span>
												<a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
										</div>
									</div>  
									</div>
									<div class="portlet-body">
										<div class="modal-footer">
											<a href="entrimou.html"><button type="button" class="btn dark btn-outline" data-dismiss="modal">Batal</button></a>
											<a href="entrimou.html"><button type="button" class="btn green">Simpan</button></a>
										</div>
									</div>
                                   
                                </div>
                            </div>
                            <!-- END SAMPLE FORM PORTLET-->
							
                        </div>
						
                    </div>
                    <!-- END PAGE BASE CONTENT -->