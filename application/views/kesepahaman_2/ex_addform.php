<script>
 $(document).ready(function(){
 
        $('#submit').submit(function(e){
            e.preventDefault(); 
                 $.ajax({
                     url:'<?php echo base_url();?>index.php/upload/do_upload',
                     type:"post",
                     data:new FormData(this),
                     processData:false,
                     contentType:false,
                     cache:false,
                     async:false,
                      success: function(data){
                          alert("Upload Image Berhasil.");
                   }
                 });
            });
         
 
    });

function getTanggalAwal(){
		$("#tanggal_awal").datepicker().datepicker( "show" );
}

function getTanggalAkhir(){
        $("#tanggal_akhir").datepicker().datepicker( "show" )
}
</script>
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>MoU 
                                <small>Nota Kesepemahaman</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">MoU</span>
                        </li>
                    </ul>
                   
                    <div class="row ">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-share font-dark"></i>
                                        <span class="caption-subject font-dark bold uppercase">Permohonan Nota Kesepemahaman (<?php echo $this->session->userdata('user_nama');?>)</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
								<form action="<?php echo base_url();?>kesepahaman/addMou_ex" method="post"enctype="multipart/form-data">
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Nomor MoU</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input name="sph_nomor" class="form-control spinner" type="text" placeholder="Nomor Permohonan MoU" /> 
											</div>
										</div>  
									</div>
									
									<!--<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Mitra Kerja</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<div class="form-group">
													<label for="single" class="control-label">Pilih Mitra Kerja</label>
													<select id="single" class="form-control select2">
														<option></option>
														<optgroup label="Internal">
															<option value="AK">Fakultas xxx</option>
															<option value="HI">Dosen</option>
															<option value="HI">Unit</option>
														</optgroup>
														<optgroup label="Eksternal">
															<option value="CA">Perusahaan xxx</option>
															<option value="NV">Universitas xxx</option>
															<option value="OR">Instansi Pemerintah xxx</option>
														</optgroup>
													</select>
												</div>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Surat Permohonan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<div class="form-group">
													<label for="single" class="control-label">Pilih Surat Permohonan</label>
													<select id="single" class="form-control select2">
														<option></option>
														<optgroup label="Internal">
															<option value="AK">Fakultas xxx</option>
															<option value="HI">Dosen</option>
															<option value="HI">Unit</option>
														</optgroup>
														<optgroup label="Eksternal">
															<option value="CA">Perusahaan xxx</option>
															<option value="NV">Universitas xxx</option>
															<option value="OR">Instansi Pemerintah xxx</option>
														</optgroup>
													</select>
												</div>
											</div>
										</div>  
									</div>-->
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Judul MoU</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" name="sph_judul" type="text" placeholder="Judul Permohonan" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">											
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Tujuan Permohonan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<textarea name="sph_tujuan" class="form-control spinner" required ><?php echo $detilpermohonan['prm_deskripsi'];?></textarea>
											</div>
										</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Nama Pihak 1</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" type="text" name=" sph_namapihak1" placeholder="Masukkan Nama Pihak 1" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Jabatan Pihak 1</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" type="text" name="sph_jabatanpihak1" placeholder="Masukkan Jabatan Pihak 1" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>SK Pihak 1</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" type="text" name="sph_skpihak1" placeholder="Masukkan No SK Pihak 1" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Pegawai Pihak 1</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" type="text" name="sph_pegawaipihak1" placeholder="Masukkan Pegawai Pihak 1" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Nama Pihak 2</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" type="text" name="sph_namapihak2" placeholder="Masukkan Nama Pihak 2" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Jabatan Pihak 2</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" type="text" name="sph_jabatanpihak2" placeholder="Masukkan Jabatan Pihak 2" /> 
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>SK Pihak 2</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input class="form-control spinner" type="text" name="sph_skpihak2" placeholder="Masukkan No SK Pihak 2" /> 
											</div>
										</div>  
									</div>								
									
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Tanggal Mulai Permohonan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<div class="input-group date date-picker" >
													<input id="tanggal_awal" type="text" date-format="yyyy-mm-dd" class="form-control" readonly name="tanggal_awal" >
													<span class="input-group-btn">
														<button class="btn default tanggal" type="button" onclick="javascript:getTanggalAwal()" >
															<i class="fa fa-calendar" ></i>
														</button>
													</span>
												</div>
													<!-- /input-group -->
													<span class="help-block"> select a date </span>
											</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Tanggal Mulai Permohonan</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
													<input id="tanggal_akhir" name="sph_akhir"  type="text" class="form-control" readonly name="datepicker">
													<span class="input-group-btn">
														<button class="btn default tanggal" type="button" onclick="javascript:getTanggalAkhir()" >
															<i class="fa fa-calendar" ></i>
														</button>
													</span>
												</div>
													<!-- /input-group -->
													<span class="help-block"> select a date </span>
											</div>
										</div>  
									</div>
									
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Unit</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">
													<select id="prm_unit" data-live-search="true" name="prm_unit" class="form-control select2 selectpicker">
														<?php 
														foreach($unit as $Hunit){ ?>
														<option value="<?php echo $Hunit['id'];?>" <?php if($detilpermohonan['prm_unit'] == $Hunit['id']){ echo "selected";}?>><?php echo $Hunit['nama_unit'];?></option>
														
														<?php
														}
														?>
														
													</select>
											</div>
										</div>  
									</div>
									<div class="form-group">
									<div class="row ">
										<div class="col-xs-12 col-sm-12 col-md-3">
											<h4>File </h4>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-9">	
												<div id="div_file">
													<input name="sph_file" type="file" id = "upload"/> 
												</div>
												<?php 
												if($detilpermohonan['sph_file'] == ''){ ?>
													<!--echo "tidak ada file";
												<?php
												}else { ?>
												
													<div id="div_download">
														<a href="<?php echo base_url();?>assets/upload/<?php echo $detilpermohonan['sph_file'];?>">Klik disini</a>
														<button type="button" id="gantifile" class="btn green">Ganti File</button>
													</div>
													-->
												<?php
												}
												?>
											</div>
									</div>  
									</div>
									<div class="portlet-body">
										<div class="modal-footer">
											<!-- <a href="<?php echo base_url();?>kesepahaman"><button type="button" class="btn dark btn-outline" data-dismiss="modal">Batal</button></a> -->
											<button type="submit" class="btn green">Simpan</button>
										</div>
									</div>
                                   </form>
                                </div>
                            </div>
                            <!-- END SAMPLE FORM PORTLET-->
							
                        </div>
						
                    </div>
                    <!-- END PAGE BASE CONTENT -->
