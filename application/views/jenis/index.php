  <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Master Jenis Kerjasama 
                                <small>Master Jenis Kerjasama </small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Master Jenis Kerjasama </span>
                        </li>
                    </ul>
                    </div>
                    
                   
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Daftar Jenis Kerjasama  </span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="btn-group">
                                                    <a href="<?php echo base_url();?>m_jenis/add"><button id="sample_editable_1_new" class="btn btn-success btn-sm"> Tambah Data 
														<i class="fa fa-plus"></i>
                                                    </button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
										  <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center;"> No </th>
                                                <th style="text-align:center;"> Nama  </th>
                                                <th style="text-align:center;"> Aksi </th>
                                            </tr>
                                        </thead>
                                        <tbody>
											<?php 
											$i=1;
											foreach($jenis as $Hjenis){
											
											?>
                                            <tr class="odd gradeX">                                           
                                                <td><?php echo $i;?></td>
												<td><?php echo $Hjenis['jns_kerjasama'];?></td>
												
												 <td style="text-align:center;">	
                                                    <a href="<?php echo base_url();?>m_jenis/add/<?php echo ($Hjenis['jns_id']);?>" title="Edit" class="btn btn-outline yellow btn-sm active">
                                                        <i class="fa fa-edit"></i>  </a>
													<a href="<?php echo base_url();?>m_jenis/hapus/<?php echo ($Hjenis['jns_id']);?>" title="Hapus" class="btn btn-outline red btn-sm active">
                                                        <i class="fa fa-trash"></i> </a>
                                            
                                                 </td>
												
                                            </tr>
                                            <?php
											$i++;
											}
											?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                    </div>
                </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        </div>