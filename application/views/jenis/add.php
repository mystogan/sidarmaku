
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Master Jenis Kerjasama
                                <small>Input Jenis Kerjasama</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Jenis Kerjasama</span>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Add</span>
                        </li>
                    </ul>
                   
                    <div class="row ">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-share font-dark"></i>
                                        <span class="caption-subject font-dark bold uppercase">Input Jenis Kerjasama</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
									<form action="<?php echo base_url();?>m_jenis/save" method="post">
									<input type="hidden" name="jns_id" value="<?php echo $detiljenis['jns_id'];?>" class="form-control spinner"  /> 
											
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Jenis Kerja Sama</h4>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9">	
												<input name="jns_kerjasama" class="form-control spinner" value="<?php echo $detiljenis['jns_kerjasama'];?>" type="text" placeholder="Masukkan Jenis Kerjasama" /> 
											</div>
										</div>  
									</div>
								</div>
									<div class="portlet-body">
										<div class="modal-footer">
											<a href="<?php echo base_url();?>m_mitra"><button type="button" class="btn dark btn-outline" data-dismiss="modal">Batal</button></a>
											<button type="submit" class="btn green">Simpan</button>
										</div>
									</div>
                                   </form>
                                </div>
                            </div>
                            <!-- END SAMPLE FORM PORTLET-->
							
                        </div>
						
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
          
        </div>
		
<!-- untuk load Kabupaten  -->
<script>
function pilih_kabupaten(){
	var id_propinsi = $('#mtr_propinsi').val();
	$('#mtr_kota').load("<?php echo base_url(); ?>m_mitra/pilih_kabupaten/"+id_propinsi);
	$('#mtr_kecamatan').load("<?php echo base_url(); ?>m_mitra/reset_kecamatan/");

}
function pilih_kecamatan(){
	var mtr_kota = $('#mtr_kota').val();
	$('#mtr_kecamatan').load("<?php echo base_url(); ?>m_mitra/pilih_kecamatan/"+mtr_kota);
}
</script>
