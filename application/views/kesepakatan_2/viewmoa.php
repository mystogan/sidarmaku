
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Daftar Nota Kesepakatan
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Nota Kesepakatan</span>
                        </li>
                    </ul>
                   
                    <div class="row ">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-share font-dark"></i>
                                        <span class="caption-subject font-dark bold uppercase">Permohonan Nota Kesepakatan</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
								
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Mitra Kerja	</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="Mitra Kerjasama Eksternal" disabled>
												</div>
										</div>  
									</div>
									
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Nomor Permohonan MoA</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="287924992.398193939" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Judul Permohonan	</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="Kerjasama Internal Universitas XXX" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Tanggal</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="10-12-2011" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Nama Pihak 1</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="Ina Yurati" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Jabatan Pihak 1</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="Kepala Bagian Keuangan" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>SK Pihak 1</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="287924992.398193939" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Nama Pihak 2</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="Intan Trianti" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Jabatan Pihak 1</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="Kasubag Keuangan" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>SK Pihak 2</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="03023028.398928392" disabled>
												</div>
										</div>  
									</div>
									
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Tanggal Mulai Permohonan</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="13-12-2011" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Tanggal Akhir Permohonan</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="13-12-2012" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Unit	</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="Fakultas XXX" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Anggaran</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="30.000.0000" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4>Pegawai PIC</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												  <input class="form-control" id="disabledInput" type="text" value="3" disabled>
												</div>
										</div>  
									</div>
									<div class="form-group">
										<div class="row ">
											<div class="col-xs-12 col-sm-12 col-md-3">
												<h4> File</h4>
											</div>
												<label for="disabledInput" class="col-xs-12 col-sm-12 col-md-3"></label>
												<div class="col-xs-12 col-sm-12 col-md-9">
												   <a href="DokumenMoU.pdf"> Dokumen MoA.pdf</a>
												</div>
										</div>  
									</div>
									<div class="portlet-body">
										<div class="modal-footer">
											<a href="entrimoa.html"><button type="button" class="btn green">Kembali</button></a>
										</div>
									</div>
                                   
                                </div>
                            </div>
                            <!-- END SAMPLE FORM PORTLET-->
							
                        </div>
						
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
