<script>
$( document ).ready(function() {
	var jastruk = $("#jastruk").val();
	// var x = document.getElementById("tombol_tambah");
    if(jastruk == 28 || jastruk == 74 || jastruk ==117 || jastruk == 155 || jastruk == 183 || jastruk == 216 || jastruk == 238 || jastruk == 251 || jastruk == 267 ){
		$("#tombol_tambah").show();
	}else{
		$("#tombol_tambah").hide();
	}
	
	var jenis_user = $("#jenis_user").val();
	if(jenis_user == 1){
		
	}else{
		
	}
});
</script>
<input type="text" id="jastruk" value="<?php echo $this->session->userdata('id_tugastambahan') ?>" hidden>
<input type="text" id="jenis_user" value="<?php echo $jenis_user?>" hidden>
<div class="page-head">
	<!-- BEGIN PAGE TITLE -->
   
	<!-- END PAGE TITLE -->
	<!-- BEGIN PAGE TOOLBAR -->
	


<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-dark">
					<i class="icon-settings font-dark"></i>
					<span class="caption-subject bold uppercase"> Permohonan MoA Kerjasama (<?php echo $this->session->userdata('user_nama')?>)</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-6">
							<div class="btn-group" id="tombol_tambah">
								<a href="<?php echo base_url()?>/kesepakatan/entri_moa_internal"><button id="sample_editable_1_new" class="btn btn-success btn-sm"> Tambah Data 
									<i class="fa fa-plus"></i>
								</button></a>
							</div>
						</div>
					</div>
				</div>
				<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
					<thead>
						<tr>
							<!--<th>
								<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
									<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
									<span></span>
								</label>
							</th> -->
							<th style="text-align:center;"> No </th>
							<th style="text-align:center;"> Nomor MoA </th>
							<th style="text-align:center;"> Judul MoA </th>
							<!--<th style="text-align:center;"> Tanggal MoA </th>-->
							<th style="text-align:center;"> Tanggal</th>
							<th style="text-align:center;"> Anggaran</th>
							<th style="text-align:center;"> Tanggal Akhir</th>
							<th style="text-align:center;" colspan="3"> Aksi </th>
						</tr>
					</thead>
					<tbody>
						<?php 
						for($i = 0; $i < count($dataMoA);  $i++){
						?>                                        
							<tr>
								<td><?php echo $i+1;?></td>
								<td><?php echo $dataMoA[$i]['spk_nomor']?></td>
								<td><?php echo $dataMoA[$i]['spk_judul']?></td>
								<td><?php echo $dataMoA[$i]['spk_tanggal']?></td>
								<td><?php echo $dataMoA[$i]['spk_anggaran']?></td>
							 
								 <td style="text-align:center;">	
									<a href="viewmoa.html" title="Detail" class="btn btn-outline green btn-sm active" id="detail">
										<i class="fa fa-binoculars" ></i> </a>
									<a href="formmoa.html" title="Edit" class="btn btn-outline yellow btn-sm active" id="edit">
										<i class="fa fa-edit" ></i>  </a>	
									<a href="javascript:;" title="Hapus" class="btn btn-outline red btn-sm active"  id="hapus">
										<i class="fa fa-trash"></i> </a>
								 </td>
							</tr>
						<?php 
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>

</div>