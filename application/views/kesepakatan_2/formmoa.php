<script type="text/javascript">
var id_pegawai=[];
var nama=[];
var jabatan=[];
var tugastambahan=[];
var index="";

$( document ).ready(function() {
    $.get(base_url+"kesepakatan/getDataPegawai/", function( data ) {
		for(i=0; i < data.length; i++){
			id_pegawai[i] = data[i].id;
			nama[i] = data[i].nama;
			jabatan[i] = data[i].jabatan;
			tugastambahan[i] = data[i].jabatan_tambahan;
		}
	// console.log(data);
	});
});

function getNomorMoa(){
	var xxx = $("#selectMitra").val();
	$.get(base_url+"kesepakatan/getMou/"+xxx, function( data ) {
		// console.log(data.length);
		$("#selectMoU").empty();
		$("#selectMoU").append("<option>-- Pilih Mou --</option>");
		for(var i=0;i<data.length; i++){
		$("#selectMoU").append("<option value="+data[i].sph_id+">"+data[i].sph_nomor+"</option>");
		}
	});
}

function getTanggal(){
        $("#tanggal_surat").datepicker().datepicker( "show" )
}

function getTanggalAwal(){
        $("#tanggal_awal").datepicker().datepicker( "show" )
}

function getTanggalAkhir(){
        $("#tanggal_akhir").datepicker().datepicker( "show" )
}
	
function getNamaPegawai(){
	$( "#spk_namapihak1" ).autocomplete({
		source: nama
    });
	// getJabatan();
}

function getNamaPIC(){
	$( "#spk_pegawaipic" ).autocomplete({
		source: nama
    });
	// getJabatan();
}

function getJabatan(){
	index = nama.indexOf($("#spk_namapihak1").val());
	$("#id_pegawai").val(id_pegawai[index]);
	if(tugastambahan[index] != null){
		$("#spk_jabatanpihak1").val(tugastambahan[index]);	
	}else{
		$("#spk_jabatanpihak1").val(jabatan[index]);
	}
}

function getIdPegawaiPIC(){
	index = nama.indexOf($("#spk_pegawaipic").val());
	$("#id_pegawai_pihak2").val(id_pegawai[index]);
}
</script>

	<!-- BEGIN PAGE HEAD-->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<h1>MoA 
				<small>Nota Kesepakatan</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
	<!-- END PAGE HEAD-->
	<!-- BEGIN PAGE BREADCRUMB -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="index.html">Home</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span class="active">MoA</span>
		</li>
	</ul>
   
	<div class="row ">
		<div class="col-md-12">
			<!-- BEGIN SAMPLE FORM PORTLET-->
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-share font-dark"></i>
						<span class="caption-subject font-dark bold uppercase">Permohonan Nota Kesepakatan</span>
					</div>
				</div>
				<div class="portlet-body">
				<!--<form action="<?php echo base_url();?>permohonan/addPermohonan_mitra" method="post" enctype="multipart/form-data">-->
				<form action="<?php echo base_url();?>kesepakatan/add_kesepakatan" method="post" enctype="multipart/form-data">
				<?php echo form_open_multipart('kesepakatan/add_kesepakatan');?>
					<div class="form-group">
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<h4>Mitra Kerja</h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-9">	
								<div class="form-group">
									<label for="single" class="control-label">Pilih Mitra Kerja</label>
									<select id="selectMitra" name="selectMitra"  class="form-control select2" onchange="javascript:getNomorMoa()" >
										<option>-- Pilih Mitra --</option>
										<?php 
										$i=0;
										
										foreach($mitra as $data){?>
										<option value="<?php echo $data['mtr_id']?>"><?php echo $data['mtr_namainstansi']?></option>
										<?php 
											$i++;
										}
										?>
									</select>
								</div>
							</div>
						</div>  
					</div>
					<div class="form-group">
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<h4>Nomor Permohonan MoU</h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-9">	
								<select id="selectMoU" name="selectMoU" class="form-control select2">
										<option>-- Pilih Mitra Terlebih Dahulu --</option>
								</select> 
							</div>
						</div>  
					</div>
					<div class="form-group">
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<h4>Jenis Permohonan</h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-9">	
								<div class="form-group">
									<select id="jns_id" data-live-search="true" name="jns_id" class="form-control select2 selectpicker">
									
										<?php 
										foreach($jenis as $Hjenis){ ?>
										<option value="<?php echo $Hjenis['jns_id'];?>" <?php if($detilpermohonan['jns_id'] == $Hjenis['jns_id']){ echo "selected";}?>><?php echo $Hjenis['jns_kerjasama'];?></option>
										
										<?php
										}
										?>
										
									</select>
								</div>
							</div>
						</div>  
									</div>
					<div class="form-group">
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<h4>Nomor Kesepakatan</h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-9">	
								<input id="spk_nomor" name="spk_nomor" class="form-control spinner"  type="text" placeholder="Nomor Kesepakatan" /> 
							</div>
						</div>  
					</div>
					<div class="form-group">
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<h4>Judul Permohonan</h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-9">	
								<input id="spk_judul" name="spk_judul" class="form-control spinner"  type="text" placeholder="Judul Permohonan" required /> 
							</div>
						</div>  
					</div>
					<div class="form-group">
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<h4>Tanggal Surat</h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-9">	
								<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
									<input id="tanggal_surat" name="tanggal_surat"  type="text" class="form-control" readonly>
									<span class="input-group-btn">
										<button class="btn default tanggal" type="button" onclick="javascript:getTanggal()" >
											<i class="fa fa-calendar" ></i>
										</button>
									</span>
								</div>
									<!-- /input-group -->
									<span class="help-block"> select a date </span>
							</div>
						</div>  
					</div>
					<div class="form-group">
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<h4>Nama Pihak 1</h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-9">	
								<input id="spk_namapihak1" name="spk_namapihak1" class="form-control spinner" type="text" placeholder="Masukkan Nama Pihak 1" onkeypress="javascript:getNamaPegawai()" onchange="javascript:getJabatan()" required /> 
							</div>
						</div>  
					</div>
					<div class="form-group">
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<h4>Jabatan Pihak 1</h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-9">	
								<input id="spk_jabatanpihak1" name="spk_jabatanpihak1" class="form-control spinner" type="text" placeholder="Masukkan Jabatan Pihak 1" required /> 
							</div>
						</div>  
					</div>
					<div class="form-group">
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<h4>SK Pihak 1</h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-9">	
								<input id="spk_skpihak1" name="spk_skpihak1" class="form-control spinner" type="text" placeholder="Masukkan No SK Pihak 1" required /> 
							</div>
						</div>  
					</div>
					<div class="form-group">
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<h4>Nama Pihak 2</h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-9">	
								<input  id="spk_namapihak2" name="spk_namapihak2" class="form-control spinner" type="text" placeholder="Masukkan Nama Pihak 2" required /> 
							</div>
						</div>  
					</div>
					<div class="form-group">
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<h4>Jabatan Pihak 2</h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-9">	
								<input id="spk_jabatanpihak2" name="spk_jabatanpihak2" class="form-control spinner" type="text" placeholder="Masukkan Jabatan Pihak 2" required /> 
							</div>
						</div>  
					</div>
					<div class="form-group">
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<h4>SK Pihak 2</h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-9">	
								<input id="spk_skpihak2" name="spk_skpihak2" class="form-control spinner" type="text" placeholder="Masukkan No SK Pihak 2" required /> 
							</div>
						</div>  
					</div>
					<div class="form-group">
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<h4>Tujuan</h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-9">	
								<textarea name="spk_tujuan" class="form-control spinner" required ></textarea>
							</div>
						</div>  
					</div>
					<div class="form-group">
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<h4>Tanggal Mulai Permohonan</h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-9">	
								<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
									<input id="tanggal_awal" name="tanggal_awal"  type="text" class="form-control" readonly name="datepicker" required>
									<span class="input-group-btn">
										<button class="btn default tanggal" type="button" onclick="javascript:getTanggalAwal()" >
											<i class="fa fa-calendar" ></i>
										</button>
									</span>
								</div>
									<!-- /input-group -->
									<span class="help-block"> select a date </span>
							</div>
						</div>  
					</div>
					<div class="form-group">
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<h4>Tanggal Akhir Permohonan</h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-9">	
								<div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
										<input id="tanggal_akhir" name="tanggal_akhir" type="text" class="form-control" readonly name="datepicker">
										<span class="input-group-btn">
											<button class="btn default" type="button">
												<i class="fa fa-calendar"  onclick="javascript:getTanggalAkhir()"></i>
											</button>
										</span>
									</div>
									<!-- /input-group -->
									<span class="help-block"> select a date </span>
							</div>
						</div>  
					</div>
					<div class="form-group">
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<h4>Unit</h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-9">	
								<input id="spk_unit" name="spk_unit" class="form-control spinner" type="text" placeholder="Unit" required /> 
							</div>
						</div>  
					</div>
					<div class="form-group">
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<h4>Anggaran</h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-9">	
								<input id="spk_anggaran" name="spk_anggaran" class="form-control spinner" type="text" placeholder="Anggaran" required /> 
							</div>
						</div>  
					</div>
					<div class="form-group">
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<h4>Pegawai PIC</h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-9">	
								<input id="spk_pegawaipic" name="spk_pegawaipic"  class="form-control spinner" type="text" placeholder="Masukkan Nama PIC" onkeypress="javascript:getNamaPIC()" onkeyup ="javascript:getIdPegawaiPIC()" required /> 
							</div>
						</div>  
					</div>
					
					
					<!--TAMPUNGAN-->
					<input type="text" id="id_pegawai" name="id_pegawai" hidden>
					<input type="text" id="id_pegawai_pihak2" name="id_pegawai_pihak2" hidden>
					<!--TAMPUNGAN-->
					
					
					<div class="form-group">
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<h4>File </h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-9">	
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="input-group input-large">
										<div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
											<i class="fa fa-file fileinput-exists"></i>&nbsp;
											<span class="fileinput-filename"> </span>
										</div>
										<span class="input-group-addon btn default btn-file">
										<span class="fileinput-new"> Select file </span>
										<span class="fileinput-exists"> Change </span>
										<input id="spk_file" name="spk_file" type="file" > </span>
										<a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
									</div>
								</div>
							</div>
						</div>  
					</div>
					<div class="portlet-body">
						<div class="modal-footer">
							<a href="entrimoa.html"><button type="button" class="btn dark btn-outline" data-dismiss="modal">Batal</button></a>
							<button type="submit" class="btn green">Simpan</button>
						</div>
					</div>
					</form>
				</div>
			</div>
			<!-- END SAMPLE FORM PORTLET-->
			
		</div>
		
	</div>
	<!-- END PAGE BASE CONTENT -->

			