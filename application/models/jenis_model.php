<?php
class Jenis_model extends CI_Model {
	function __construct(){
		parent::__construct();
		// $this->db_simpeg = $this->load->database('simpeg', TRUE);
	}
		
	function getJenis(){
		$this->db->select('*');
		$this->db->from("jenis");
		$this->db->where("jns_delete = 1");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getDetJenis($id){
		$this->db->select('*');
		$this->db->from("jenis");
		$this->db->where("jns_id = $id");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function save($data){
		$this->db->insert('jenis', $data); 
	}
	function update($data,$id){
		$this->db->where('jns_id', $id);
		$this->db->update('jenis', $data); 
	}
	


}
?>
