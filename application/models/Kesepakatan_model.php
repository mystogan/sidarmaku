<?php
class Kesepakatan_model extends CI_Model {
	function __construct(){
		parent::__construct();
		// $this->db_simpeg = $this->load->database('simpeg', TRUE);
	}
	
	 
	function getKesepahaman(){
		$this->db->select('*');
		$this->db->from("kesepahaman");
		$this->db->where("sph_status = 2");
		$query = $this->db->get();
		$data = $query->result_array(); 
		return $data;
	} 
	function getKesepahamanFix(){
		$this->db->select('*');
		$this->db->from("kesepahaman");
		$this->db->where("sph_status = 3");
		$query = $this->db->get();
		$data = $query->result_array(); 
		return $data;
	} 
	function getDetilkesepakatan($spk_id){
		$this->db->select('*');
		$this->db->from("kesepakatan"); 
		$this->db->where("spk_id = '$spk_id'");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getKesepakatan_ex($id_mitra, $yearPost, $internalPost){
		$this->db->select('a.*,e.sph_judul,b.mtr_id,b.mtr_namainstansi,jns_kerjasama,dis_jastrukasal,dis_jastruktujuan,dis_status');
		$this->db->select("DATE_FORMAT(spk_tanggal, '%d-%m-%Y') AS spk_tanggal",false);
		$this->db->from("kesepakatan a");
		$this->db->join('kesepahaman e', 'e.sph_id = a.sph_id','left');
		$this->db->join('mitra b', 'a.mtr_id = b.mtr_id','left');
		$this->db->join('disposisi c', 'dis_entitas = a.spk_id','left');
		$this->db->join('jenis d', 'a.jns_id = d.jns_id','left');
		$this->db->where("a.mtr_id = $id_mitra");
		$this->db->where("spk_internal = $internalPost");
		$this->db->where("DATE_FORMAT(spk_tanggal, '%Y') = '$yearPost'");
		$this->db->where("dis_jenis = 3");
		$this->db->where("(dis_status = 1 or dis_status = 3)");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getkesepakatan_in($id_unit, $yearPost, $internalPost){
		$this->db->select('f.nama as namaPegawai,a.*,e.sph_judul,b.mtr_id,b.mtr_namainstansi,jns_kerjasama,dis_jastrukasal,dis_jastruktujuan,dis_pegawaiasal,dis_status');
		$this->db->select("DATE_FORMAT(spk_tanggal, '%d-%m-%Y') AS spk_tanggal",false);
		$this->db->from("kesepakatan a");
		$this->db->join('kesepahaman e', 'e.sph_id = a.sph_id','left');
		$this->db->join('mitra b', 'a.mtr_id = b.mtr_id','left');
		$this->db->join('disposisi c', 'dis_entitas = a.spk_id','left');
		$this->db->join('jenis d', 'a.jns_id = d.jns_id','left');
		$this->db->join(SIMPEG.'.tbpegawai f', 'f.id = dis_pegawaiasal','left');
		$this->db->where("a.spk_unit = $id_unit");
		$this->db->where("spk_internal = $internalPost");
		$this->db->where("DATE_FORMAT(spk_tanggal, '%Y') = '$yearPost'");
		$this->db->where("dis_jenis = 3");
		$this->db->where("(dis_status = 1 or dis_status = 3)");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getkesepakatan_rek($yearPost){
		$this->db->select('f.nama as namaPegawai,a.*,e.sph_judul,b.mtr_id,b.mtr_namainstansi,jns_kerjasama,dis_jastrukasal,dis_jastruktujuan,dis_pegawaiasal,dis_status');
		$this->db->select("DATE_FORMAT(spk_tanggal, '%d-%m-%Y') AS spk_tanggal",false);
		$this->db->from("kesepakatan a");
		$this->db->join('kesepahaman e', 'e.sph_id = a.sph_id','left');
		$this->db->join('mitra b', 'a.mtr_id = b.mtr_id','left');
		$this->db->join('disposisi c', 'dis_entitas = a.spk_id','left');
		$this->db->join('jenis d', 'a.jns_id = d.jns_id','left');
		$this->db->join(SIMPEG.'.tbpegawai f', 'f.id = dis_pegawaiasal','left');
		$this->db->where("sph_internal = '3'");
		$this->db->where("DATE_FORMAT(sph_tanggal, '%Y') = '$yearPost'");
		$this->db->where("dis_jenis = 3");
		$this->db->where("(dis_status = 1 or dis_status = 3)");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function get_dis_catatan($id_kesepakatan){
		$this->db->select('dis_jastrukasal,dis_catatan,nama,dis_pegawaiasal');
		$this->db->select("DATE_FORMAT(dis_updated, '%d-%m-%Y %H:%i') AS tanggal_permohonan",false);
		$this->db->from("disposisi");
		$this->db->join(SIMPEG.'.tbpegawai', 'dis_pegawaiasal = id','left');
		$this->db->where("dis_jenis = 3");
		$this->db->where("dis_entitas = $id_kesepakatan");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getdisposisi($id_tugastambahan = 0, $yearPost, $internalPost){
		$this->db->select('e.*,a.*,a.dis_id,c.mtr_namainstansi,a.dis_status,sph_judul,e.spk_id');
		$this->db->select("DATE_FORMAT(spk_tanggal, '%d-%m-%Y') AS tanggal_permohonan",false);
		$this->db->from("disposisi a");
		$this->db->join('kesepakatan e', 'a.dis_entitas = e.spk_id');
		$this->db->join('kesepahaman d', 'e.sph_id = d.sph_id');
		$this->db->join('mitra c ', 'e.mtr_id = c.mtr_id','left');
		$this->db->where("a.dis_jastrukasal = $id_tugastambahan");
		if($internalPost == 1)
		{
			$this->db->where("(spk_internal = '1' or spk_internal = '3')");
		}else 
		{
			$this->db->where("spk_internal = $internalPost");
		}
		$this->db->where("DATE_FORMAT(spk_tanggal, '%Y') = '$yearPost'");
		$this->db->where("a.dis_jenis = 3");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getDisposisiLast($prm_id){
		$this->db->select('disposisi.*,nama_jastruk');
		$this->db->from("disposisi");
		$this->db->join(SIMPEG.'.m_jastruk', 'dis_jastrukasal = id');
		$this->db->where("dis_jenis = 3");
		$this->db->where("dis_entitas = $prm_id");
		$this->db->order_by("dis_id", "desc"); 
		$this->db->limit(1);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function cek_disposisi2($disposisi){
		$this->db->select('*');
		$this->db->from("disposisi");
		$this->db->where("dis_jenis",$disposisi['dis_jenis']);
		$this->db->where("dis_entitas",$disposisi['dis_entitas']);
		$this->db->where("dis_pegawaiasal",$disposisi['dis_pegawaiasal']);
		$this->db->where("dis_pegawaitujuan",$disposisi['dis_pegawaitujuan']);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getdisposisiDetail($dis_id = 0){
		$this->db->select('a.*,e.*,h.sph_judul,e.spk_unit,a.dis_catatan,a.dis_id,a.dis_catatan,c.mtr_namainstansi, b.prm_judul,b.prm_tanggal, b.prm_deskripsi, d.jns_kerjasama, b.prm_unit, b.prm_file,
							c.mtr_email,c.mtr_namainstansi');
		$this->db->select("DATE_FORMAT(spk_tanggal, '%d-%m-%Y') AS spk_tanggal",false);
		$this->db->from("disposisi a");
		$this->db->join('kesepakatan e', 'a.dis_entitas = e.spk_id','left');
		$this->db->join('kesepahaman h', 'e.sph_id = h.sph_id','left');
		$this->db->join('permohonan b', 'h.prm_id = b.prm_id','left');
		$this->db->join('mitra c ', 'e.mtr_id = c.mtr_id','left');
		$this->db->join('jenis d ', 'e.jns_id = b.jns_id','left');
		$this->db->where("a.dis_id = $dis_id");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getunit_single($nip){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		
		$this->db_simpeg->select('b.kode_unit');
		$this->db_simpeg->from("tbpegawai a");
		$this->db_simpeg->join('m_jastruk b', 'a.id_jastruk = b.id','left');
		$this->db_simpeg->where("a.nip = $nip");
		$query = $this->db_simpeg->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getFakultas($idUnit){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		
		$this->db_simpeg->select('nama_unit');
		$this->db_simpeg->from("m_unit");
		$this->db_simpeg->where("id = $idUnit");
		$query = $this->db_simpeg->get();
		$data = $query->result_array();
		return $data[0];
	}
	
	function getAllPegawai(){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		
		$this->db_simpeg->select('id,nama');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id_jastruk is not null");
		$this->db_simpeg->where("id_jastruk != '0' ");
		$query = $this->db_simpeg->get();
		$data = $query->result_array();
		return $data;
	}
	function cek_pangkat($id_tugastambahan){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		$this->db_simpeg->select('*');
		$this->db_simpeg->from("m_jastruk");
		$this->db_simpeg->where("id = $id_tugastambahan");
		$query = $this->db_simpeg->get();
		$data = $query->result_array();
		return $data[0];
	}
	function get_jastrukwadek3($id_kodeunit){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		$this->db_simpeg->select('*');
		$this->db_simpeg->from("m_jastruk");
		$this->db_simpeg->where("nama_jastruk = 'Wakil Dekan Bidang Kemahasiswaan dan Kerjasama'");
		$this->db_simpeg->where("kode_unit = $id_kodeunit");
		$query = $this->db_simpeg->get();
		$data = $query->result_array();
		return $data[0];
	}
	function get_jastrukdekan($id_kodeunit){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		$this->db_simpeg->select('*');
		$this->db_simpeg->select("'Dekan Fakultas' as nama_dekan",false);
		$this->db_simpeg->from("m_jastruk");
		$this->db_simpeg->where("nama_jastruk LIKE '%Dekan Fakultas%'");
		$this->db_simpeg->where("kode_unit = $id_kodeunit");
		$query = $this->db_simpeg->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getunit(){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		
		$this->db_simpeg->select('id,nama_unit');
		$this->db_simpeg->from("m_unit");
		$query = $this->db_simpeg->get();
		$data = $query->result_array();
		return $data;
	}
	function getmitra(){
		$this->db->select('mtr_id,mtr_namainstansi');
		$this->db->from("mitra");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function count_kesepakatan($prm_id = '0'){
		$this->db->select('*');
		$this->db->from("disposisi");
		$this->db->where("dis_jenis = 3");
		$this->db->where("dis_entitas = $prm_id");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function count_pj($prm_id,$pjKesepakatan){
		$this->db->select('*');
		$this->db->from("disposisi");
		$this->db->where("dis_jenis = 3");
		$this->db->where("dis_entitas = $prm_id");
		$this->db->where("dis_pegawaiasal = $pjKesepakatan");
		$query = $this->db->get();
		$data = $query->num_rows();
		return $data;
	}
	function cek_wadek3_dekan($kode_unit){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		
		$this->db_simpeg->select('b.id,b.id_tugastambahan');
		$this->db_simpeg->from("m_jastruk a");
		$this->db_simpeg->join('tbpegawai b', 'a.id = b.id_tugastambahan','left');
		$this->db_simpeg->where("nama_jastruk LIKE '%Wakil Dekan Bidang Kemahasiswaan dan Kerjasama%'  ");
		$this->db_simpeg->where("kode_unit = $kode_unit");
		$this->db_simpeg->limit(1);
		$query1 = $this->db_simpeg->get()->result();
		
		$this->db_simpeg->select('b.id,b.id_tugastambahan');
		$this->db_simpeg->from("m_jastruk a");
		$this->db_simpeg->join('tbpegawai b', 'a.id = b.id_tugastambahan','left');
		$this->db_simpeg->where("nama_jastruk LIKE '%Dekan Fakultas%'  ");
		$this->db_simpeg->where("kode_unit = $kode_unit");
		$this->db_simpeg->limit(1);
		$query2 = $this->db_simpeg->get()->result();
		
		$query = array_merge($query1, $query2);
		
		return $query;
	}
	function cek_rek_wa3(){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		
		$this->db_simpeg->select('id,id_tugastambahan');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id_tugastambahan = 1");
		$this->db_simpeg->limit(1);
		$query1 = $this->db_simpeg->get()->result();
		
		$this->db_simpeg->select('id,id_tugastambahan');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id_tugastambahan = 4");
		$this->db_simpeg->limit(1);
		$query2 = $this->db_simpeg->get()->result();
		
		$query = array_merge($query1, $query2);
		
		return $query;
	}
	function cek_wa3_biro(){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		
		$this->db_simpeg->select('id,id_tugastambahan');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id_tugastambahan = 4");
		$this->db_simpeg->limit(1);
		$query1 = $this->db_simpeg->get()->result();
		
		$this->db_simpeg->select('id,id_jastruk');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id_jastruk = 353");
		$this->db_simpeg->limit(1);
		$query2 = $this->db_simpeg->get()->result();
		
		$query = array_merge($query1, $query2);
		
		return $query;
	}
	function cek_disposisi_in($pertama, $kedua){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		
		$this->db_simpeg->select('id,id_tugastambahan');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id_tugastambahan = $pertama");
		$this->db_simpeg->limit(1);
		$query1 = $this->db_simpeg->get()->result();
		
		$this->db_simpeg->select('id,id_tugastambahan');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id_tugastambahan = $kedua");
		$this->db_simpeg->limit(1);
		$query2 = $this->db_simpeg->get()->result();
		
		$query = array_merge($query1, $query2);
		
		return $query;
	}
	function cek_disposisi($pertama, $kedua){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		
		$this->db_simpeg->select('id,id_jastruk');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id_jastruk = $pertama");
		$this->db_simpeg->limit(1);
		$query1 = $this->db_simpeg->get()->result();
		
		$this->db_simpeg->select('id,id_jastruk');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id_jastruk = $kedua");
		$this->db_simpeg->limit(1);
		$query2 = $this->db_simpeg->get()->result();
		
		$query = array_merge($query1, $query2);
		
		return $query;
	}
	function cek_disposisiForSPK($pertama, $kedua){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		
		$this->db_simpeg->select('id,id_jastruk,id_tugastambahan');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id = $pertama");
		$this->db->limit(1);
		$query1 = $this->db_simpeg->get()->result();
		
		$this->db_simpeg->select('id,id_jastruk,id_tugastambahan');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id = $kedua");
		$this->db->limit(1);
		$query2 = $this->db_simpeg->get()->result();
		
		$query = array_merge($query1, $query2);
		
		return $query;
	}
	function getDetPermohonan($id){
		$this->db->select('*');
		$this->db->from("permohonan");
		$this->db->where("prm_id = $id");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function add_disposisi($data) {
		$this->db->insert('disposisi',$data);
		return $this->db->insert_id();
	}
	function update_disposisi($data,$id = 0){
		$this->db->where('dis_id', $id);
		$this->db->update('disposisi', $data); 
	}
	function add_kesepakatan($data) {
		$this->db->insert('kesepakatan',$data);
		return $this->db->insert_id();
	}
	
	function update_kesepakatan($data,$id = 0){
		$this->db->where('spk_id', $id);
		$this->db->update('kesepakatan', $data); 
	}
	
	


}
?>
