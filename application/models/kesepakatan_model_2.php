<?php
class Kesepakatan_model extends CI_Model {
	function __construct(){
		parent::__construct();
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		$this->db_sidarmas = $this->load->database('default', TRUE);
	}

	function add_kesepakatan($data){ 
		$strSql = "INSERT INTO kesepakatan VALUES('', 
		'$data[sph_id]',
		'$data[mtr_id]',
		'$data[jns_id]',
		'$data[spk_nomor]',
		'$data[spk_judul]',
		'$data[spk_tanggal]',
		'$data[spk_namapihak1]',
		'$data[spk_jabatanpihak1]',
		'$data[spk_skpihak1]',
		'$data[spk_pegawaipihak1]',
		'$data[spk_namapihak2]',
		'$data[spk_jabatanpihak2]',
		'$data[spk_skpihak2]',
		'$data[spk_tujuan]',
		'$data[spk_mulai]',
		'$data[spk_akhir]',
		'$data[spk_unit]',
		'$data[spk_pegawaipic]',
		'$data[spk_anggaran]',
		'$data[spk_status]',
		'$data[spk_file]',CURRENT_TIMESTAMP);";
		// print $strSql;
		$query = $this->db_sidarmas->query($strSql);
	}
	
	// function getIdMoa(){
		// $strSql = "SELECT MAX(spk_id) as id FROM kesepakatan";
		// $query = $this->db_sidarmas->query($strSql);
		// $data = $query->result_array();
		// return $data;
	// }
	
	function getMoA($kode_unit){
		$strSql = "SELECT * from kesepakatan where spk_unit = $kode_unit order by spk_id DESC";
		$query = $this->db_sidarmas->query($strSql);
		$data = $query->result_array();
		return $data;
	}
	
	function getMoaPenanggungJawab($id_pegawai){
		$strSql = "SELECT * from kesepakatan where spk_pegawaipihak1 = $id_pegawai";
		$query = $this->db_sidarmas->query($strSql);
		$data = $query->result_array();
		return $data;
	}
	
	function getMou($id_mitra){
		$strSql = "SELECT sph_id,sph_nomor FROM kesepahaman WHERE mtr_id = $id_mitra";
		$query = $this->db_sidarmas->query($strSql);
		$data = $query->result_array();
		return $data;
	}
	
	function getDataPegawai(){
		$strSql = "SELECT a.id, a.nama, b.nama_jastruk 'jabatan', c.nama_jastruk 'jabatan_tambahan' FROM tbpegawai a
					LEFT JOIN m_jastruk b ON a.id_jastruk = b.id
					LEFT JOIN m_jastruk c ON a.id_tugastambahan = c.id
					WHERE a.keterangan_aktif = 1";
		$query = $this->db_simpeg->query($strSql);
		$data = $query->result_array();
		return $data;
	}
	
	
	function getMitra(){
		$strSql = "SELECT * FROM mitra";
		$query = $this->db_sidarmas->query($strSql);
		$data = $query->result_array();
		return $data;
	}
	
	function getWadekIII(){
		$strSql = "SELECT id, kode_unit FROM m_jastruk WHERE nama_jastruk LIKE 'Wakil Dekan Bidang Kemahasiswaan dan Kerjasama'";
		$query = $this->db_simpeg->query($strSql);
		$data = $query->result_array();
		return $data;
	}
	
	function getDekan(){
		$strSql = "SELECT id, kode_unit  FROM m_jastruk WHERE nama_jastruk LIKE 'Dekan Fakultas%'";
		$query = $this->db_simpeg->query($strSql);
		$data = $query->result_array();
		return $data;
	}
	
	function getWarekIII(){
		$strSql = "SELECT id,kode_unit  FROM m_jastruk WHERE nama_jastruk LIKE 'Wakil Rektor Bidang Kemahasiswaan dan Kerjasama'";
		$query = $this->db_simpeg->query($strSql);
		$data = $query->result_array();
		return $data;
	}
	
	function getPegawaiTujuan($id){
		$strSql = "SELECT id FROM tbpegawai WHERE id_jastruk = $id OR id_tugastambahan = $id";
		$query = $this->db_simpeg->query($strSql);
		$data = $query->result_array();
		return $data;
	}
	
	function getId_Kesepakatan_After_Insert($kode_unit){
		$strSql = "SELECT MAX(spk_id) as spk_id FROM kesepakatan WHERE spk_unit = $kode_unit";
		$query = $this->db_sidarmas->query($strSql);
		$data = $query->result_array();
		return $data;
	}
	// function getFakultas(){
		// $strSql = "SELECT * FROM m_unit WHERE nama_unit LIKE 'Fakultas%'";
		// $query = $this->db_simpeg->query($strSql);
		// $data = $query->result_array();
		// return $data;
	// }
	
	// function getUnit(){
		// $strSql = "SELECT * FROM m_unit WHERE jenis_unit = 'non akademik'";
		// $query = $this->db_simpeg->query($strSql);
		// $data = $query->result_array();
		// return $data;
	// }
	
	function add_disposisi($data){ 
		$strSql = "INSERT INTO disposisi VALUES(
		'$data[dis_idint]',
		'$data[dis_jenisint]',
		'$data[dis_entitasint]',
		'$data[dis_pegawaiasalint]',
		'$data[dis_jastrukasalint]',
		'$data[dis_pegawaitujuanint]',
		'$data[dis_jastruktujuanint]',
		'$data[dis_catatantext]',
		'$data[dis_statussmallint]',
		$data[dis_createdtimestamp],
		'$data[dis_updatedtimestamp]')
		";
		// print $strSql;
		$query = $this->db_sidarmas->query($strSql);
	}
}
?>
