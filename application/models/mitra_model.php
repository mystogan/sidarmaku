<?php
class Mitra_model extends CI_Model {
	function __construct(){
		parent::__construct();
		// $this->db_simpeg = $this->load->database('simpeg', TRUE);
	}
	
	function getMitra(){
		$this->db->select('*');
		$this->db->from("mitra");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getDetMitra($id){
		$this->db->select('*, Nama_Kab as nama_kabupaten,Nama_Kec as nama_kecamatan');
		$this->db->from("mitra");
		$this->db->join('m_kabupaten b', 'mtr_kota = b.id_kabupaten','left');
		$this->db->join('m_kecamatan c', 'mtr_kecamatan = c.id_kecamatan','left');
		$this->db->where("mtr_id = $id");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getProvinsi(){
		$this->db->select('*');
		$this->db->from("m_propinsi");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getKabupaten($id_propinsi){
		$this->db->select('id_kabupaten, Nama_kab');
		$this->db->from("m_kabupaten");
		$this->db->where("id_propinsi = $id_propinsi");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getKecamatan($id_kota){
		$this->db->select('id_kecamatan, Nama_kec');
		$this->db->from("m_kecamatan");
		$this->db->where("id_kabupaten = $id_kota");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function save($data){
		$this->db->insert('mitra', $data); 
	}
	function update($data,$id){
		$this->db->where('mtr_id', $id);
		$this->db->update('mitra', $data); 
	}
	


}
?>
