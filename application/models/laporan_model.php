<?php
class Laporan_model extends CI_Model {
	function __construct(){
		parent::__construct();
		// $this->db_simpeg = $this->load->database('simpeg', TRUE);
	}

	function getkesepakatan($user_id){
		$this->db->select('a.*,b.mtr_namainstansi, c.jns_kerjasama');
		// $this->db->select("DATE_FORMAT(a.prm_tanggal, '%d-%m-%Y') AS tanggal_permohonan",false);
		$this->db->from("kesepakatan a");
		$this->db->join('mitra b', 'a.mtr_id = b.mtr_id','left');
		$this->db->join('jenis c', 'c.jns_id = a.jns_id','left');
		$this->db->where("a.spk_pegawaipic = $user_id");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getkesepakatan_pj($user_id){
		$this->db->select('a.*,b.mtr_namainstansi, c.jns_kerjasama');
		// $this->db->select("DATE_FORMAT(a.prm_tanggal, '%d-%m-%Y') AS tanggal_permohonan",false);
		$this->db->from("kesepakatan a");
		$this->db->join('mitra b', 'a.mtr_id = b.mtr_id','left');
		$this->db->join('jenis c', 'c.jns_id = a.jns_id','left');
		$this->db->where("a.spk_pegawaipihak1 = $user_id");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}

	function get_dis_catatan($id_laporan = 0){
		$this->db->select('dis_jastrukasal,dis_catatan,nama');
		$this->db->select("DATE_FORMAT(dis_updated, '%d-%m-%Y %H:%i') AS tanggal_permohonan",false);
		$this->db->from("disposisi");
		$this->db->join(SIMPEG.'.tbpegawai', 'dis_pegawaiasal = id','left');
		$this->db->where("dis_jenis = 4");
		$this->db->where("dis_entitas = $id_laporan");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getLaporanDisposisi($id_tugastambahan = ''){
		$this->db->select('b.lak_jenis,c.spk_id,b.lak_id,a.dis_id,c.spk_judul, b.lak_tanggal,b.lak_periode,a.dis_status,b.lak_penandatangan');
		$this->db->select("DATE_FORMAT(b.lak_tanggal, '%d-%m-%Y') AS tanggal_permohonan",false);
		$this->db->from("disposisi a");
		$this->db->join('laporan b', 'a.dis_entitas = b.lak_id','left');
		$this->db->join('kesepakatan c ', 'b.spk_id = c.spk_id','left');
		$this->db->where("a.dis_jastrukasal = $id_tugastambahan");
		$this->db->where("a.dis_jenis = 4");
		$this->db->group_by("c.spk_id");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getdisposisi($id_tugastambahan = 0,$id_kesepakatan = 0){
		$this->db->select('a.dis_id,a.dis_status,b.*,c.spk_judul,c.spk_pegawaipic');
		$this->db->select("DATE_FORMAT(b.lak_tanggal, '%d-%m-%Y') AS tanggal",false);
		$this->db->from("disposisi a");
		$this->db->join('laporan b', 'a.dis_entitas = b.lak_id','left');
		$this->db->join('kesepakatan c', 'b.spk_id = c.spk_id','left');
		$this->db->where("a.dis_jastrukasal = $id_tugastambahan");
		$this->db->where("a.dis_jenis = 4");
		$this->db->where("b.spk_id = $id_kesepakatan");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getAllkesepakatan($user_id = ''){
		$this->db->select('*');
		$this->db->from("kesepakatan");
		if($user_id != ''){
			$this->db->where("spk_pegawaipic = $user_id");
		}
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getlaporanDetail($id_laporan){
		$this->db->select('*');
		$this->db->from("laporan");
		$this->db->where("lak_id = $id_laporan");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getPJ($spk_id = 0){
		$this->db->select('*');
		$this->db->from("kesepakatan");
		$this->db->where("spk_id = $spk_id");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getPJ_id($id_pegawai = 0){
		$this->db->select('*');
		$this->db->from("kesepakatan");
		$this->db->where("spk_pegawaipihak1 = $id_pegawai");
		$query = $this->db->get();
		$data['sum'] = $query->num_rows();
		$data['data'] = $query->result_array();
		return $data;
	}

	function update_laporan($data,$id = 0){
		$this->db->where('lak_id', $id);
		$this->db->update('laporan', $data);
	}
	function add_laporan($data) {
		$this->db->insert('laporan',$data);
		return $this->db->insert_id();
	}
	function cek_wa3_biro(){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		$this->db_simpeg->select('id,id_tugastambahan');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id_tugastambahan = 4");
		$query1 = $this->db_simpeg->get()->result();
		$this->db_simpeg->select('id,id_jastruk');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id_jastruk = 353");
		$query2 = $this->db_simpeg->get()->result();

		$query = array_merge($query1, $query2);

		return $query;
	}
	function add_disposisi($data) {
		$this->db->insert('disposisi',$data);
		return $this->db->insert_id();
	}
	function getlaporan($id_kesepakatan){
		$this->db->select('f.nama as namaPegawai,a.*,b.spk_judul,d.dis_jastrukasal,d.dis_jastruktujuan,d.dis_status');
		$this->db->from("laporan a");
		$this->db->join('kesepakatan b', 'a.spk_id = b.spk_id','left');
		$this->db->join('disposisi d', 'a.lak_id = d.dis_entitas','left');
		$this->db->join(SIMPEG.'.tbpegawai f', 'f.id = d.dis_pegawaiasal','left');
		$this->db->where("a.spk_id = $id_kesepakatan");
		$this->db->where("d.dis_jenis = 4");
		$this->db->where("d.dis_status = 1");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getid_laporan($id_disposisi){
		$this->db->select('a.*,b.spk_judul,c.dis_catatan');
		$this->db->from("laporan a");
		$this->db->join('kesepakatan b', 'a.spk_id = b.spk_id','left');
		$this->db->join('disposisi c', 'a.lak_id = c.dis_entitas','left');
		$this->db->where("c.dis_jenis = 4");
		$this->db->where("c.dis_id = $id_disposisi");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}





	function getpermohonan_in($id_mitra){
		$this->db->select('a.*,c.*,d.dis_jastrukasal,d.dis_jastruktujuan,d.dis_status');
		$this->db->select("DATE_FORMAT(a.prm_tanggal, '%d-%m-%Y') AS tanggal_permohonan",false);
		$this->db->from("permohonan a");
		$this->db->join('mitra b', 'a.mtr_id = b.mtr_id','left');
		$this->db->join('jenis c', 'c.jns_id = a.jns_id','left');
		$this->db->join('disposisi d', 'a.prm_id = d.dis_entitas','left');
		$this->db->where("a.prm_unit = $id_mitra");
		$this->db->where("d.dis_jenis = 1");
		$this->db->where("d.dis_status = 1");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function cek_disposisi2($disposisi){
		$this->db->select('*');
		$this->db->from("disposisi");
		$this->db->where("dis_jenis",$disposisi['dis_jenis']);
		$this->db->where("dis_entitas",$disposisi['dis_entitas']);
		$this->db->where("dis_pegawaiasal",$disposisi['dis_pegawaiasal']);
		$this->db->where("dis_pegawaitujuan",$disposisi['dis_pegawaitujuan']);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getdisposisiDetail($dis_id = 0){
		$this->db->select('a.dis_catatan,a.dis_id,a.dis_catatan,c.mtr_namainstansi, b.prm_judul,b.prm_tanggal, b.prm_deskripsi, d.jns_kerjasama, b.prm_unit, b.prm_file
							, c.mtr_alamat, e.Nama_Kec AS nama_kec, f.Nama_prop AS nama_prov, g.Nama_Kab AS nama_kab, c.mtr_telepon, c.mtr_handphone,
							c.mtr_email,b.prm_id,b.prm_internal');
		$this->db->select("DATE_FORMAT(b.prm_tanggal, '%d-%m-%Y') AS tanggal_permohonan",false);
		$this->db->from("disposisi a");
		$this->db->join('permohonan b', 'a.dis_entitas = b.prm_id','left');
		$this->db->join('mitra c ', 'b.mtr_id = c.mtr_id','left');
		$this->db->join('jenis d ', 'd.jns_id = b.jns_id','left');
		$this->db->join('m_kecamatan e ', 'e.id_kecamatan = c.mtr_kecamatan','left');
		$this->db->join('m_propinsi f', 'f.id_propinsi = c.mtr_propinsi','left');
		$this->db->join('m_kabupaten g ', 'g.id_kabupaten = mtr_kota','left');
		$this->db->where("a.dis_id = $dis_id");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getunit_single($nip){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);

		$this->db_simpeg->select('b.kode_unit');
		$this->db_simpeg->from("tbpegawai a");
		$this->db_simpeg->join('m_jastruk b', 'a.id_jastruk = b.id','left');
		$this->db_simpeg->where("a.nip = $nip");
		$query = $this->db_simpeg->get();
		$data = $query->result_array();
		return $data[0];
	}
	function cek_pangkat($id_tugastambahan){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		$this->db_simpeg->select('*');
		$this->db_simpeg->from("m_jastruk");
		$this->db_simpeg->where("id = $id_tugastambahan");
		$query = $this->db_simpeg->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getunit(){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);

		$this->db_simpeg->select('id,nama_unit');
		$this->db_simpeg->from("m_unit");
		$query = $this->db_simpeg->get();
		$data = $query->result_array();
		return $data;
	}
	function getmitra(){
		$this->db->select('mtr_id,mtr_namainstansi');
		$this->db->from("mitra");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function cek_wadek3_dekan($kode_unit){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);

		$this->db_simpeg->select('b.id,b.id_tugastambahan');
		$this->db_simpeg->from("m_jastruk a");
		$this->db_simpeg->join('tbpegawai b', 'a.id = b.id_tugastambahan','left');
		$this->db_simpeg->where("nama_jastruk LIKE '%Wakil Dekan Bidang Kemahasiswaan dan Kerjasama%'  ");
		$this->db_simpeg->where("kode_unit = $kode_unit");
		$query1 = $this->db_simpeg->get()->result();

		$this->db_simpeg->select('b.id,b.id_tugastambahan');
		$this->db_simpeg->from("m_jastruk a");
		$this->db_simpeg->join('tbpegawai b', 'a.id = b.id_tugastambahan','left');
		$this->db_simpeg->where("nama_jastruk LIKE '%Dekan Fakultas%'  ");
		$this->db_simpeg->where("kode_unit = $kode_unit");
		$query2 = $this->db_simpeg->get()->result();

		$query = array_merge($query1, $query2);

		return $query;
	}
	function cek_rek_wa3(){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);

		$this->db_simpeg->select('id,id_tugastambahan');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id_tugastambahan = 1");
		$query1 = $this->db_simpeg->get()->result();

		$this->db_simpeg->select('id,id_tugastambahan');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id_tugastambahan = 4");
		$query2 = $this->db_simpeg->get()->result();

		$query = array_merge($query1, $query2);

		return $query;
	}

	function cek_disposisi_in($pertama, $kedua){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);

		$this->db_simpeg->select('id,id_tugastambahan');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id_tugastambahan = $pertama");
		$query1 = $this->db_simpeg->get()->result();

		$this->db_simpeg->select('id,id_tugastambahan');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id_tugastambahan = $kedua");
		$query2 = $this->db_simpeg->get()->result();

		$query = array_merge($query1, $query2);

		return $query;
	}
	function cek_disposisi($pertama, $kedua){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);

		$this->db_simpeg->select('id,id_jastruk');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id_jastruk = $pertama");
		$query1 = $this->db_simpeg->get()->result();

		$this->db_simpeg->select('id,id_jastruk');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id_jastruk = $kedua");
		$query2 = $this->db_simpeg->get()->result();

		$query = array_merge($query1, $query2);

		return $query;
	}
	function cek_disposisi_khusus($pertama, $kedua){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		
		$this->db_simpeg->select('id,id_jastruk,id_tugastambahan');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id = $pertama");
		$this->db_simpeg->limit(1);
		$query1 = $this->db_simpeg->get()->result();
		
		$this->db_simpeg->select('id,id_jastruk,id_tugastambahan');
		$this->db_simpeg->from("tbpegawai");
		$this->db_simpeg->where("id_jastruk = $kedua");
		$this->db_simpeg->or_where("id_tugastambahan = $kedua");
		$this->db_simpeg->limit(1);
		$query2 = $this->db_simpeg->get()->result();
		
		$query = array_merge($query1, $query2);
		
		return $query;
	}
	function getDetPermohonan($id){
		$this->db->select('*');
		$this->db->from("permohonan");
		$this->db->where("prm_id = $id");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}

	function update_disposisi($data,$id = 0){
		$this->db->where('dis_id', $id);
		$this->db->update('disposisi', $data);
	}





}
?>
