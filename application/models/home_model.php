<?php
class Home_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	function login_mitra($username,$password){
		$this->db->select('*');
		$this->db->from("mitra");
		$this->db->where('mtr_username =', $username);
		$this->db->where('mtr_password =', $password);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function login_uin($username,$password){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		
		$this->db_simpeg->select('a.id,a.nama,a.nip,a.id_tugastambahan,a.id_jastruk,a.id_unitkerja,
								  a.id_unittugastambahan, c.kode_unit,d.nama_jastruk,d.kode_unit as id_kode_unit');
		$this->db_simpeg->from("tbpegawai a");
		$this->db_simpeg->join('tbsimpeguser b', 'b.username = a.nip','left');
		$this->db_simpeg->join('m_jastruk c', 'a.id_jastruk = c.kode_unit','left');
		$this->db_simpeg->join('m_jastruk d', 'a.id_jastruk = d.id','left');
		$this->db_simpeg->where('b.username =', $username);
		$this->db_simpeg->where('b.password =', $password);
		$query = $this->db_simpeg->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getUnit($id_tugastambahan){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		
		$this->db_simpeg->select('*');
		$this->db_simpeg->from("m_jastruk");
		$this->db_simpeg->where('id =', $id_tugastambahan);
		$query = $this->db_simpeg->get();
		$data = $query->result_array();
		return $data[0];
	}
	function cekPICPJ($idUser){
		$this->db->select('*');
		$this->db->from("kesepakatan");
		$this->db->where("spk_status != '3' ");
		$this->db->where(" (spk_pegawaipihak1 = $idUser or spk_pegawaipic = $idUser) ",null,false);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function cekPIC($idUser){
		$this->db->select('*');
		$this->db->from("kesepakatan");
		$this->db->where("spk_status != '3' ");
		$this->db->where("spk_pegawaipic = $idUser ");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}


}
?>
