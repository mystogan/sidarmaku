<?php
class Kesepahaman_model extends CI_Model {
	function __construct(){
		parent::__construct();
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		$this->db_sidarmas = $this->load->database('default', TRUE);
	}

	function getMouEx($id_mitra){		
		$this->db->select('*');		
		$this->db->from("kesepahaman a");
		$this->db->join('mitra b', 'a.mtr_id = b.mtr_id','left');
		$this->db->where("a.mtr_id = $id_mitra");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}

	function getMouIn($id_mitra){	
		$this->db->select('*');
		$this->db->from("kesepahaman a");
		$this->db->where("a.sph_unit = $id_mitra");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}

	function getDetKesepahaman($id){
		$this->db->select('*');
		$this->db->from("kesepahaman");
		$this->db->where("sph_id = $id");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}

	function getMoulist($limit, $start){
        $query = $this->db->get('kesepahaman', $limit, $start);
        return $query;
	}

	function getunit_single($nip){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		
		$this->db_simpeg->select('b.kode_unit');
		$this->db_simpeg->from("tbpegawai a");
		$this->db_simpeg->join('m_jastruk b', 'a.id_jastruk = b.id','left');
		$this->db_simpeg->where("a.nip = $nip");
		$query = $this->db_simpeg->get();
		$data = $query->result_array();
		return $data[0];
	}

	function getpermohonan($id_mitra){
		$this->db->select('a.*,c.*,d.dis_jastrukasal,d.dis_jastruktujuan,d.dis_status');
		$this->db->select("DATE_FORMAT(a.prm_tanggal, '%d-%m-%Y') AS tanggal_permohonan",false);
		$this->db->from("permohonan a");
		$this->db->join('mitra b', 'a.mtr_id = b.mtr_id','left');
		$this->db->join('jenis c', 'c.jns_id = a.jns_id','left');
		$this->db->join('disposisi d', 'a.prm_id = d.dis_entitas','left');
		$this->db->where("a.mtr_id = $id_mitra");
		$this->db->where("d.dis_jenis = 1");
		$this->db->where("d.dis_status = 1");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	
	
	function getkesepahaman_in($id_mitra){
		$this->db->select('a.*, d.dis_jastrukasal,d.dis_jastruktujuan,d.dis_status');
		$this->db->from("kesepahaman a");
//		$this->db->join('mitra b', 'a.mtr_id = b.mtr_id','left');
		$this->db->join('disposisi d', 'a.sph_id = d.dis_entitas','left');
		$this->db->where("a.sph_unit = $id_mitra");
		$this->db->where("d.dis_jenis = 1");
		$this->db->where("d.dis_status = 1");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}

	function get_jastrukwadek3($id_kodeunit){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		$this->db_simpeg->select('*');
		$this->db_simpeg->from("m_jastruk");
		$this->db_simpeg->where("nama_jastruk = 'Wakil Dekan Bidang Kemahasiswaan dan Kerjasama'");
		$this->db_simpeg->where("kode_unit = $id_kodeunit");
		$query = $this->db_simpeg->get();
		$data = $query->result_array();
		return $data[0];
	}
	function get_jastrukdekan($id_kodeunit){
		$this->db_simpeg = $this->load->database('simpeg', TRUE);
		$this->db_simpeg->select('*');
		$this->db_simpeg->select("'Dekan Fakultas' as nama_dekan",false);
		$this->db_simpeg->from("m_jastruk");
		$this->db_simpeg->where("nama_jastruk LIKE '%Dekan Fakultas%'");
		$this->db_simpeg->where("kode_unit = $id_kodeunit");
		$query = $this->db_simpeg->get();
		$data = $query->result_array();
		return $data[0];
	}
	
	function getMouIn1($id_mitra){
		$this->db->select('*');
		$this->db->from("permohonan a");
		$this->db->join('mitra b', 'a.mtr_id = b.mtr_id','left');
		$this->db->where("a.prm_unit = $id_mitra");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}

	function getDetMou($id){
		$this->db->select('*');
		$this->db->from("kesepahaman");
		$this->db->where("sph_id = $id");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	
	//membuat id baru dan mengambil id 
	function ambilID($data){
		$this->db->insert('kesepahaman',$data);
		return $this->db->insert_id();
	}
	function add_kesepahaman($data){
		$this->db->insert('kesepahaman',$data);
		return $this->db->insert_id();
	}

	function add_kesepahamanCoba($data){
		//function add_kesepakatan($data){ 
			// $strSql = "INSERT INTO kesepakatan VALUES('', 
			// '$data[sph_id]',
			// '$data[mtr_id]',
			// '$data[jenis_id]',
			// '$data[spk_nomor]',
			// '$data[spk_judul]',
			// '$data[spk_tanggal]',
			// '$data[spk_namapihak1]',
			// '$data[spk_jabatanpihak1]',
			// '$data[spk_skpihak1]',
			// '$data[spk_pegawaipihak1]',
			// '$data[spk_namapihak2]',
			// '$data[spk_jabatanpihak2]',
			// '$data[spk_skpihak2]',
			// '$data[spk_tujuan]',
			// '$data[spk_mulai]',
			// '$data[spk_akhir]',
			// '$data[spk_unit]',
			// '$data[spk_pegawaipic]',
			// '$data[spk_anggaran]',
			// '$data[spk_status]',
			// '$data[spk_file]',CURRENT_TIMESTAMP);";
			// // print $strSql;
			// $query = $this->db_sidarmas->query($strSql);
		// }
	}

	function simpan_file($judul,$image){
        $data = array(
                'judul' => $judul,
                'gambar' => $image
            );  
        $result= $this->db->insert('tbl_galeri',$data);
        return $result;
	}
	
	function update_disposisi($data,$id = 0){
		$this->db->where('dis_id', $id);
		$this->db->update('disposisi', $data); 
	}                                                          

	function update_kesepahaman($data,$id = 0){
		$this->db->where('sph_id', $id);
		$this->db->update('kesepahaman', $data); 
	}

	function getdisposisi($id_tugastambahan = 0){
		$this->db->select('b.sph_internal,a.dis_id,c.mtr_namainstansi, b.sph_judul,b.sph_tanggal,a.dis_status,b.sph_file');
		//$this->db->select("DATE_FORMAT(b.prm_tanggal, '%d-%m-%Y') AS tanggal_permohonan",false);
		$this->db->from("disposisi a");
		$this->db->join('kesepahaman b', 'a.dis_entitas = b.sph_id');
		$this->db->join('mitra c ', 'b.mtr_id = c.mtr_id','left');
		$this->db->where("a.dis_jastrukasal = $id_tugastambahan");
		$this->db->where("a.dis_jenis = 1");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}

	function count_kesepahaman($sph_id){
		$this->db->select('*');
		$this->db->from("disposisi");
		$this->db->where("dis_jenis = 1");
		$this->db->where("dis_entitas = $sph_id");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}

	function getUnit(){
		$strSql = "SELECT * FROM m_unit WHERE jenis_unit = 'non akademik'";
		$query = $this->db_simpeg->query($strSql);
		$data = $query->result_array();
		return $data;
	}

	function getdisposisiDetail($dis_id = 0){
		$this->db->select('b.sph_unit,a.dis_catatan,a.dis_id,a.dis_catatan,a.dis_status,c.mtr_namainstansi, b.sph_judul,b.sph_tanggal,b.sph_unit, b.sph_file
							, c.mtr_alamat,  c.mtr_handphone,
							c.mtr_email,b.sph_id,b.sph_internal');
		//$this->db->select("DATE_FORMAT(b.prm_tanggal, '%d-%m-%Y') AS tanggal_permohonan",false);
		$this->db->from("disposisi a");
		$this->db->join('kesepahaman b', 'a.dis_entitas = b.sph_id','left');
		$this->db->join('mitra c ', 'b.mtr_id = c.mtr_id','left');
		//$this->db->join('jenis d ', 'd.jns_id = b.jns_id','left');
		//$this->db->join('m_kecamatan e ', 'e.id_kecamatan = c.mtr_kecamatan','left');
		//$this->db->join('m_propinsi f', 'f.id_propinsi = c.mtr_propinsi','left');
		//$this->db->join('m_kabupaten g ', 'g.id_kabupaten = mtr_kota','left');
		$this->db->where("a.dis_id = $dis_id");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	
	function getmitra(){
		$this->db->select('mtr_id,mtr_namainstansi');
		$this->db->from("mitra");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}

}
?>
