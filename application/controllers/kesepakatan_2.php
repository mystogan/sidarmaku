<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kesepakatan extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('Kesepakatan_model');
		$this->load->model('Kesepakatan_model');
		$this->load->library('upload');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		$user = $this->session->userdata('user_jenis');
		
		if ($user == '') {
			header("location:".base_url()."login");
		}

	}
	public function index(){		
		//echo $id_tugastambahan = $this->session->userdata('id_tugastambahan');
		
		$user_id = $this->session->userdata('user_id');
		$kode_unit	 = $this->session->userdata('kode_unit');
		$user_jenis = $this->session->userdata('user_jenis');
		$jastruk = $this->session->userdata('id_tugastambahan');
		$jastruk_kbtu = array("28", "74", "117", "155", "183", "216", "238", "251", "267");
		if(in_array($jastruk, $jastruk_kbtu, true )){
			$data['dataMoA'] = $this->Kesepakatan_model->getMoA($kode_unit);
			print_r($data['dataMoA']);
		}else{
			$data['dataMoA'] = $this->Kesepakatan_model->getMoaPenanggungJawab($user_id);
		}
		
		$this->load->view('base/header',$data);
		$this->load->view('kesepakatan/daftarmoa',$data);
		$this->load->view('base/footer',$data);
		
		
	}
	
	public function entri_moa_internal(){
		$this->load->model('Jenis_model');
		$data['jenis'] = $this->Jenis_model->getJenis();
		$data['mitra'] = $this->Kesepakatan_model->getMitra();
		// $data['mitra'] = $this->Kesepakatan_model->getMitra();
		$this->load->view('base/header',$data);
		$this->load->view('kesepakatan/formmoa',$data);
		$this->load->view('base/footer',$data);
	}
	
	public function getDataPegawai(){
		header('Content-Type: application/json');
		echo json_encode($this->Kesepakatan_model->getDataPegawai());
		
	}
	
	public function getMou($id_mitra){
		header('Content-Type: application/json');
		echo json_encode($this->Kesepakatan_model->getMou($id_mitra));
	}
	
	public function add_kesepakatan(){
		$data['spk_id'] = $this->input->post('spk_id');
		$data['sph_id'] = $this->input->post('selectMoU');
		$data['mtr_id'] = $this->input->post('selectMitra');
		$data['jns_id'] = $this->input->post('jns_id');
		$data['spk_nomor'] = $this->input->post('spk_nomor');
		$data['spk_judul'] = $this->input->post('spk_judul');
		$data['spk_tanggal'] = date('Y-m-d', strtotime($this->input->post('tanggal_surat')));//date_format($this->input->post('tanggal_surat'),"Y-m-d");
		$data['spk_namapihak1'] = $this->input->post('spk_namapihak1');
		$data['spk_jabatanpihak1'] = $this->input->post('spk_jabatanpihak1');
		$data['spk_skpihak1'] = $this->input->post('spk_skpihak1');
		$data['spk_pegawaipihak1'] = $this->input->post('id_pegawai');
		$data['spk_namapihak2'] = $this->input->post('spk_namapihak2');
		$data['spk_jabatanpihak2'] = $this->input->post('spk_jabatanpihak2');
		$data['spk_skpihak2'] = $this->input->post('spk_skpihak2');
		$data['spk_tujuan'] = $this->input->post('spk_tujuan');
		$data['spk_mulai'] =  date('Y-m-d', strtotime($this->input->post('tanggal_awal')));//date_format($this->input->post('tanggal_awal'),"Y/m/d");
		$data['spk_akhir'] =  date('Y-m-d', strtotime($this->input->post('tanggal_akhir')));//date_format($this->input->post('tanggal_akhir'),"Y/m/d");
		$data['spk_unit'] = $this->session->userdata('kode_unit');//$this->input->post('spk_unit');
		$data['spk_pegawaipic'] = $this->input->post('id_pegawai_pihak2');
		$data['spk_anggaran'] = $this->input->post('spk_anggaran');
		$data['spk_status'] = 1;//isi 1
		$data['spk_created'] = ""; //isi tanggal sekarang waktu upload

		$file_upload = $_FILES["spk_file"]["name"];
		if($file_upload != "" || $file_upload != null){
			$terima = $this->loop($file_upload,"spk_file");
		}
		$data['spk_file'] = $terima['nama_file'];
		// print_r($data);
		$this->Kesepakatan_model->add_kesepakatan($data);
		disposisi();
		header('Location: '.base_url().'/kesepakatan');
	}
	
	function loop($simpan,$nama){
		
		$sub = substr($simpan,-5);
		$string = uniqid().$sub;


		$target_dir = "assets/upload/kesepakatan/";
		$target_file = $target_dir . $string;
		$data['cek'] = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

		if(isset($_POST["submit"])) {
			$check = getimagesize($_FILES[$nama]["tmp_name"]);
			if($check !== false) {
				$data['cek'] = 1;
			} else {
				$data['cek'] = 0;
			}
		}

		if($imageFileType != "jpg" && $imageFileType != "png" &&
	 			$imageFileType != "JPG" && $imageFileType != "PNG" &&
				$imageFileType != "jpeg" && $imageFileType != "JPEG" &&
				$imageFileType != "gif" && $imageFileType != "GIF"  ) {
			$data['error'] = 'Format Harus JPG, JPEG, PNG & GIF !';
			$data['cek'] = 0;
		}
		if ($data['cek'] != 0) {
			if (move_uploaded_file($_FILES[$nama]["tmp_name"], $target_file)) {
				// $this->Home_model->saveupload($kolom,$string);
				$data['nama_file'] = $string;
				$data['cek'] = 1;
			} else {
				$data['error'] = 'Ada Kesalahan Waktu Upload silahkan data diisi Kembali ';
				$data['cek'] = 0;
				
			}
		} 
		
		return $data;
	}
	
	public function disposisi(){
		$unit_asal = $this->session->userdata('id_kode_unit');
		$jastruk_kbtu = array("28", "74", "117", "155", "183", "216", "238", "251", "267");
		$wadek3 = $this->Kesepakatan_model->getWadekIII();
		
		// print_r($wadek3);
		$data['dis_idint'] = ''; // auto increment
		$data['dis_jenisint'] = 2;
		$spk_id = $this->Kesepakatan_model->getId_Kesepakatan_After_Insert($unit_asal);
		$data['dis_entitasint'] = $spk_id[0]['spk_id']; // ini id di kesepakatan
		$data['dis_pegawaiasalint'] = $this->session->userdata('user_id'); // ini siapa yang input kesepakatan
		$data['dis_jastrukasalint'] = $this->session->userdata('id_tugastambahan');// ini jastruk yang menginputkan
		
		
		//CORE DISPOSISI
		
		if(in_array($data['dis_jastrukasalint'], $jastruk_kbtu, true )){ // CEK JASTRUK KABAG TU FAKULTAS ATAU BUKAN
			foreach($wadek3 as $key => $tujuan){
				if($tujuan['kode_unit'] === $unit_asal){
					$jastruk_tujuan = $tujuan['id'];
				}
			}
		
		}else{ // KALAU BUKAN JASTRUK KABAG TU
		
		}
		
		//CORE DISPOSISI
		$pegawai_tujuan = $this->Kesepakatan_model->getPegawaiTujuan($jastruk_tujuan);
		// print_r($pegawai_tujuan); 
		$data['dis_pegawaitujuanint'] = $pegawai_tujuan[0]['id']; // ini id pegawai tujuan disposisi
		$data['dis_jastruktujuanint'] = $jastruk_tujuan; // ini jabatan disposisi
		$data['dis_catatantext'] = ''; // ini inputan update
		$data['dis_statussmallint'] = ''; // ngga tau
		$data['dis_createdtimestamp'] = 'CURRENT_TIMESTAMP';// tanggal dibuat
		$data['dis_updatedtimestamp'] = '';// tanggal diupdate
		
		$this->Kesepakatan_model->add_disposisi($data);
	}
	
	public function after_update_disposisi(){
	
	}
	
	// public function addMouEx(){
// //		$data['kesepahaman'] = $this->kesepahaman_model->getMou($user_id);
		// $this->load->model('Jenis_model');
		// if($id != 0){
			// $data['detilkesepahaman'] = $this->Kesepahaman_model->getMouAll($id);
		// }
		// // print_r ($data['detilpermohonan']);
		// $data['unit'] = $this->Kesepahaman_model->getunit();
		// $this->load->view('base/header',$data);
		// $this->load->view('kesepahaman/formmou',$data);
		// $this->load->view('base/footer',$data);
	// }
	// public function addMou_ex(){		
		// $data['sph_nomor'] = $this->input->post('sph_nomor');
		// $data['sph_judul'] = $this->input->post('sph_judul');  
		// $data['sph_tanggal'] = $this->input->post('sph_tanggal');  
		// $data['sph_namapihak1'] = $this->input->post('sph_namapihak1');  
		// $data['sph_jabatanpihak1'] = $this->input->post('sph_jabatanpihak1');  
		// $data['sph_skpihak1'] = $this->input->post('sph_skpihak1');  
		// $data['sph_pegawaipihak1'] = $this->input->post('sph_pegawaipihak1');  
		// $data['sph_namapihak2'] = $this->input->post('sph_namapihak2');  
		// $data['sph_jabatanpihak2'] = $this->input->post('sph_jabatanpihak2');  
		// $data['sph_skpihak2'] = $this->input->post('sph_skpihak2');  
		// $data['sph_pegawaipihak2'] = $this->input->post('sph_pegawaipihak2');  
		// $data['sph_tujuan'] = $this->input->post('sph_tujuan');
		// $data['sph_mulai'] = $this->input->post('sph_mulai');
		// $data['sph_akhir'] = $this->input->post('sph_akhir');		
		// $data['sph_id'] = $this->input->post('sph_id'); // cek untuk id update 
		// $data['mtr_id'] = $this->session->userdata('user_id'); // id untuk mitra 
		
		// if($data['sph_id'] == ""){
				// $data['sph_tanggal'] = date('Y-m-d');
				// $data['sph_status'] = "1";
				// $id_permohonan = $this->Permohonan_model->add_permohonan($data);
				
				// $id_pegawai = $this->Permohonan_model->cek_rek_wa3();
				
				// $disposisi['dis_jenis'] = 1; // 1 karena permohonan 1
				// $disposisi['dis_entitas'] = $id_permohonan; // 
				// $disposisi['dis_pegawaiasal'] = $id_pegawai[0]->id; // 
				// $disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; // 
				// $disposisi['dis_pegawaitujuan'] = $id_pegawai[1]->id; // 
				// $disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; // 
				// $disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; // 
				// $disposisi['dis_status'] = 1; 
				
				// // insert disposisi rektor dan warek 3
				// $id_disposisi = $this->Permohonan_model->add_disposisi($disposisi);
				 
			// }else {
				// $this->Permohonan_model->update_permohonan($data,$data['prm_id']);
			// }
	// }
	
	// public function detail(){
		// //$data['detail'] = $this->Permohonan_model->getdisposisiDetail($dis_id);	
		// // print_r ($data['detail']);
		// $this->load->view('base/header',$data);
		// $this->load->view('kesepahaman/formmou',$data);
		// $this->load->view('base/footer',$data);
	// }

	
}
