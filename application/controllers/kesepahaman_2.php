<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kesepahaman extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('Kesepahaman_model');
		$this->load->model('Permohonan_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		$user = $this->session->userdata('user_jenis');
		if ($user == '') {
			header("location:".base_url()."login");
		}

	}
	public function index(){		
		//echo $id_tugastambahan = $this->session->userdata('id_tugastambahan');
		$id_tugastambahan = $this->session->userdata('id_tugastambahan');
		$user_id = $this->session->userdata('user_id');
		$user_jenis = $this->session->userdata('user_jenis');
		echo $user_jenis;	
		if($user_jenis == 1){
			$data['kesepahaman'] = $this->Kesepahaman_model->getMouEx($user_id);
			print_r ($data);
			$form = 'ex_index';
		}elseif($user_jenis == 2){
			echo $this->session->userdata('nama_jastruk');
			if($this->session->userdata('nama_jastruk') == "Kepala Bagian Tata Usaha"){
			 	$data['kesepahaman'] = $this->Kesepahaman_model->getMouIn($this->session->userdata('id_kode_unit'));			
			 	$data['wadek3'] = $this->Kesepahaman_model->get_jastrukwadek3($this->session->userdata('id_kode_unit'));				
			 	$data['dekan'] = $this->Kesepahaman_model->get_jastrukdekan($this->session->userdata('id_kode_unit'));
				//echo $this->session->userdata('id_kode_unit');
				//print_r ($data);
				 $form = 'in_index';
				 //echo $data['kesepahaman'];
				 //echo $this->session->userdata('id_kode_unit');

			}else if($id_tugastambahan == 374){
				$data['kesepahaman'] = $this->Kesepahaman_model->getkesepahaman_in($this->session->userdata('id_kode_unit'));				
				echo $this->session->userdata('id_kode_unit');
				print_r ($data);
			 	$form = 'in_index';
			 }else {
				 $data['disposisi'] = $this->Kesepahaman_model->getdisposisi($id_tugastambahan);
				 //echo $this->Permohonan_model->getdisposisi($id_tugastambahan);
				 print_r($data);
			 	$form = 'ex_uin';
			}
			//$data['kesepahaman'] = $this->Kesepahaman_model->getMouIn($user_id);
			//$form = 'in_index';

		}else {
			header("location:".base_url()."login");
		}
		//echo $form;
		$this->load->view('base/header',$data);
		$this->load->view('kesepahaman/'.$form,$data);
		$this->load->view('base/footer',$data);
		
	}

	public function add($id = 0){
		$this->load->model('Jenis_model');
		if($id != 0){
			$data['detilkesepahaman'] = $this->Kesepahaman_model->getDetMou($id);
		}
		// print_r ($data['detilpermohonan']);
		if($user_jenis == 1){
			$data['kesepahaman'] = $this->Kesepahaman_model->getMouEx($user_id);
						
		}elseif($user_jenis == 2){
			$data['kesepahaman'] = $this->Kesepahaman_model->getMouIn($user_id);
		}
		//$data['unit'] = $this->Permohonan_model->getunit();
		$data['jenis'] = $this->Jenis_model->getJenis();
		$this->load->view('base/header',$data);
		$this->load->view('permohonan/ex_add',$data);
		$this->load->view('base/footer',$data);
	}

	public function in_add($id = 0){
		$this->load->model('Jenis_model');
				if($id != 0){
					$data['detilkesepahaman'] = $this->Kesepahaman_model->getMouAll($id);
				}
				// print_r ($data['detilpermohonan']);
		if($this->session->userdata('id_tugastambahan') == 374){
			$form = 'in_add_rektorat';
		}else {
			$form = 'in_add';
		}
		// print_r ($data['detilpermohonan']);
		$data['mitra'] = $this->Kesepahaman_model->getmitra();
		$data['unitsingle'] = $this->Kesepahaman_model->getunit_single($this->session->userdata('user_username'));
		$data['unit'] = $this->Kesepahaman_model->getunit();
		// print_r ($data['unit']);
		$data['jenis'] = $this->Jenis_model->getJenis();		
		$this->load->view('base/header',$data);
		$this->load->view('kesepahaman/'.$form,$data);
		$this->load->view('base/footer',$data);
	}

	public function addMouEx(){
//		$data['kesepahaman'] = $this->kesepahaman_model->getMou($user_id);
		$this->load->model('Jenis_model');
		if($id != 0){
			$data['detilkesepahaman'] = $this->Kesepahaman_model->getMouAll($id);
		}
		// print_r ($data['detilpermohonan']);
		$data['unit'] = $this->Kesepahaman_model->getunit();
		$this->load->view('base/header',$data);
		$this->load->view('kesepahaman/ex_addform',$data);
		$this->load->view('base/footer',$data);
	}
	
	public function addMouIn(){
		//		$data['kesepahaman'] = $this->kesepahaman_model->getMou($user_id);
				$this->load->model('Jenis_model');
				if($id != 0){
					$data['detilkesepahaman'] = $this->Kesepahaman_model->getMouAll($id);
				}
				// print_r ($data['detilpermohonan']);
				$data['unit'] = $this->Kesepahaman_model->getunit();
				$this->load->view('base/header',$data);
				$this->load->view('kesepahaman/in_addform',$data);
				$this->load->view('base/footer',$data);
	}

	public function addMou_ex(){
		$data['sph_nomor'] = $this->input->post('sph_nomor');
		$data['sph_judul'] = $this->input->post('sph_judul');  
		$data['sph_tanggal'] = $this->input->post('sph_tanggal');  
		$data['sph_namapihak1'] = $this->input->post('sph_namapihak1');  
		$data['sph_jabatanpihak1'] = $this->input->post('sph_jabatanpihak1');  
		$data['sph_skpihak1'] = $this->input->post('sph_skpihak1');  
		$data['sph_pegawaipihak1'] = $this->input->post('sph_pegawaipihak1');  
		$data['sph_namapihak2'] = $this->input->post('sph_namapihak2');  
		$data['sph_jabatanpihak2'] = $this->input->post('sph_jabatanpihak2');  
		$data['sph_skpihak2'] = $this->input->post('sph_skpihak2');  
		$data['sph_tujuan'] = $this->input->post('sph_tujuan');
		$date_awal=date_create($this->input->post('tanggal_awal'));		
		$data['sph_mulai'] = date_format($date_awal,"Y-m-d");
		$date_akhir = date_create($this->input->post('sph_akhir'));
		$data['sph_akhir'] = date_format($date_akhir,"Y-m-d");
		$data['sph_id'] = $this->input->post('sph_id'); // cek untuk id update 
		$data['mtr_id'] = $this->session->userdata('user_id'); // id untuk mitra 
		$data['sph_internal'] = "2";

		$file_upload = $_FILES["sph_file"]["name"];
		if($file_upload != "" || $file_upload != null){
			$terima = $this->kesepahaman->loop($file_upload,"sph_file");
		}
		$data['sph_file'] = $terima['nama_file'];
		if($terima['cek'] == 0  && $file_upload != '' ){
			echo "<script> alert ('".$terima['error']."')</script>";
		}else {
			// if untuk mengecek update atau insert 
			if($data['sph_id'] == ""){
				$data['sph_tanggal'] = date('Y-m-d');
				$data['sph_status'] = "1";
				//$id_kesepahaman = $this->Kesepahaman_model->add_kesepahaman($data);
				
				// untuk mendapatkan id pegawai dan id_tugastambahan pada rektor dan warek 3
				$id_pegawai = $this->Permohonan_model->cek_rek_wa3();
				
				$disposisi['dis_jenis'] = 2; // 2 kesepahaman
				//$disposisi['dis_entitas'] = $id_kesepahaman; // 
				$disposisi['dis_entitas'] = $this->Kesepahaman_model->ambilID($data);
				$disposisi['dis_pegawaiasal'] = $id_pegawai[0]->id; // 
				$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; // 
				$disposisi['dis_pegawaitujuan'] = $id_pegawai[1]->id; // 
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; // 
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; // 
				$disposisi['dis_status'] = 1; 
				
				// insert disposisi rektor dan warek 3
				$id_disposisi = $this->Permohonan_model->add_disposisi($disposisi);
				 
			}else {
				$this->Permohonan_model->update_permohonan($data,$data['sph_id']);
			}
			
		}
		echo "<script>window.location.href = '".base_url()."kesepahaman';</script>";
	}
	
	public function addMou_in(){		
		//$data['mtr_id'] = $this->session->userdata('user_id');
		$data['sph_nomor'] = $this->input->post('sph_nomor');
		$data['sph_judul'] = $this->input->post('sph_judul');  
		$data['sph_tanggal'] = $this->input->post('sph_tanggal');  
		$data['sph_namapihak1'] = $this->input->post('sph_namapihak1');  
		$data['sph_jabatanpihak1'] = $this->input->post('sph_jabatanpihak1');  
		$data['sph_skpihak1'] = $this->input->post('sph_skpihak1');  
		$data['sph_pegawaipihak1'] = $this->input->post('sph_pegawaipihak1');  
		$data['sph_namapihak2'] = $this->input->post('sph_namapihak2');  
		$data['sph_jabatanpihak2'] = $this->input->post('sph_jabatanpihak2');  
		$data['sph_skpihak2'] = $this->input->post('sph_skpihak2');  
		//$data['sph_pegawaipihak2'] = $this->input->post('sph_pegawaipihak2');  
		$data['sph_tujuan'] = $this->input->post('sph_tujuan');
		$date_awal=date_create($this->input->post('tanggal_awal'));		
		$data['sph_mulai'] = date_format($date_awal,"Y-m-d");
		$date_akhir = date_create($this->input->post('sph_akhir'));
		$data['sph_akhir'] = date_format($date_akhir,"Y-m-d");
		$data['sph_id'] = $this->input->post('sph_id'); // cek untuk id update 
		//$data['mtr_id'] = $this->session->userdata('user_id'); // id untuk mitra 
		$data['sph_unit'] = $this->input->post('sph_unit');
		//do_upload("sph_file");
		$data['sph_internal'] = "1";		

		
			// if untuk mengecek update atau insert 
			if($data['sph_id'] == ""){
				$data['sph_tanggal'] = date('Y-m-d');
				$data['sph_status'] = "1";
				$id_permohonan = $this->Kesepahaman_model->add_kesepahaman($data);
				
				// untuk mendapatkan id pegawai dan id_tugastambahan pada rektor dan warek 3
				$id_pegawai = $this->Permohonan_model->cek_rek_wa3();
				
				$disposisi['dis_jenis'] = 2; // 2 kesepahaman
				$disposisi['dis_entitas'] = $this->Kesepahaman_model->ambilID($data); // 
				$disposisi['dis_pegawaiasal'] = $id_pegawai[0]->id; // 
				$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; // 
				$disposisi['dis_pegawaitujuan'] = $id_pegawai[1]->id; // 
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; // 
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; // 
				$disposisi['dis_status'] = 1; 
				
				// insert disposisi rektor dan warek 3
				$id_disposisi = $this->Permohonan_model->add_disposisi($disposisi);
				 
			}else {
				$this->Kesepahaman_model->update_kesepahaman($data,$data['sph_id']);
			}
			
		
		echo "<script>window.location.href = '".base_url()."kesepahaman';</script>";
	}

	function do_upload($fileinput){
		$config['upload_path']="./assets/upload/kesepahaman";
        //$config['allowed_types']='gif|jpg|png';
		$config['encrypt_name'] = TRUE;
		         
        $this->load->library('upload',$config);
        if($this->upload->do_upload($fileinput)){
			$data = array('upload_data' => $this->upload->data());
			
            $file = $data['upload_data']['file_name']; 
             
            $result= $this->kesepahaman->simpan_file($judul,$image);
            echo json_decode($result);
        }
 
	 }
	 
	
	function loop($simpan,$nama){
		$sub = substr($simpan,-5);
		$string = uniqid().$sub;


		$target_dir = "assets/upload/";
		$target_file = $target_dir . $string;
		$data['cek'] = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

		if(isset($_POST["submit"])) {
			$check = getimagesize($_FILES[$nama]["tmp_name"]);
			if($check !== false) {
				$data['cek'] = 1;
			} else {
				$data['cek'] = 0;
			}
		}

		if($imageFileType != "jpg" && $imageFileType != "png" &&
	 			$imageFileType != "JPG" && $imageFileType != "PNG" &&
				$imageFileType != "jpeg" && $imageFileType != "JPEG" &&
				$imageFileType != "gif" && $imageFileType != "GIF"  ) {
			$data['error'] = 'Format Harus JPG, JPEG, PNG & GIF !';
			$data['cek'] = 0;
		}
		if ($data['cek'] != 0) {
			if (move_uploaded_file($_FILES[$nama]["tmp_name"], $target_file)) {
				// $this->Home_model->saveupload($kolom,$string);
				$data['nama_file'] = $string;
				$data['cek'] = 1;
			} else {
				$data['error'] = 'Ada Kesalahan Waktu Upload silahkan data diisi Kembali ';
				$data['cek'] = 0;
				
			}
		} 
		
		return $data;
	}

	public function catatan($dis_id = 0){
		$id_tugastambahan = $this->session->userdata('id_tugastambahan');
		$data['detail'] = $this->Kesepahaman_model->getdisposisiDetail($dis_id);	
		$data['catatan'] = $this->Kesepahaman_model->get_dis_catatan($data['detail']['sph_id']);	
		$data['dis_id'] = $dis_id;
		// print_r ();
		
		if($data['detail']['sph_unit'] == 76){
			$view = 'in_catatan_rektorat';		
		}else if($data['detail']['sph_internal'] == 1){
			$view = 'in_catatan';		
		}else {
			$view = 'ex_catatan';
		}
		
		$this->load->view('base/header',$data);
		$this->load->view('permohonan/'.$view,$data);
		$this->load->view('base/footer',$data);
	}

	function get_dis_catatan($id_kesepahaman){
		$this->db->select('dis_jastrukasal,dis_catatan');
//		$this->db->select("DATE_FORMAT(dis_updated, '%d-%m-%Y %H:%i') AS tanggal_permohonan",false);
		$this->db->from("disposisi");
		$this->db->where("dis_jenis = 2");
		$this->db->where("dis_entitas = $id_kesepahaman");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}

	public function update_catatan_internal(){
		date_default_timezone_set('Asia/Jakarta');
		$data['dis_catatan'] = $this->input->post('dis_catatan');
		$sph_id = $this->input->post('sph_id');
		$dis_id = $this->input->post('dis_id');
		$data['dis_status'] = '2';
		$data['dis_updated'] = date('Y-m-d H:i:s');
		$data_kesepahaman['sph_status'] = '2';
		
		echo $id_tugastambahan = $this->session->userdata('id_tugastambahan');
		
		// untuk mengecek apakah dia wadek atau dekan 
		$cek_pangkat = $this->Permohonan_model->cek_pangkat($id_tugastambahan);
		// query dibawah untuk mengecek apakah warek 3 disposisi ke 1 atau yang ke 2
		$count_permohonan = $this->Kesepahaman_model->count_permohonan($sph_id);
		// echo "count hasil".count($count_permohonan);
		
		
		$sub_kalimat = substr($cek_pangkat['nama_jastruk'],0,5);
		
		if($id_tugastambahan == 4 && $this->input->post('sph_unit') == 76 
			&& count($count_permohonan) == 1){
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			$this->Kesepahaman_model->update_kesepahaman($data_kesepahaman,$sph_id);
			/////////// insert warek 3 dan rektor 
			$id_pegawai = $this->Permohonan_model->cek_disposisi_in(1, 4);
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; // 

		}else if($id_tugastambahan == 1){
			///////////kondisi dimana acc disposisi untuk warek 3 ke biro
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			$this->Kesepahaman_model->update_kesepahaman($data_kesepahaman,$sph_id);			
			$id_pegawai = $this->Permohonan_model->cek_wa3_biro();
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; // 

		}else if($id_tugastambahan == 4){
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			/////////// insert biro dan kabag 
			$id_pegawai = $this->Permohonan_model->cek_disposisi(353, 369);
			
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; // 

		}else if($id_tugastambahan == 353){
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			///////////insert kabag dan kasubag
			$id_pegawai = $this->Permohonan_model->cek_disposisi(369, 370);
			
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; // 

		}else if($id_tugastambahan == 369){
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			$id_pegawai = $this->Permohonan_model->cek_disposisi(370, 375);
			
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; // 

		}else if($id_tugastambahan == 370){
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			$id_pegawai = $this->Permohonan_model->cek_disposisi(375, 375);
			
			
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; // 

		}else if($sub_kalimat == "Wakil"){
			///////////kondisi dimana acc disposisi untuk dekan ke rektor
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			$this->Kesepahaman_model->update_kesepahaman($data_kesepahaman,$sph_id);		
			$id_pegawai = $this->Permohonan_model->cek_disposisi_in($cek_pangkat['id_pimpinan'], 1);
			// print_r ($id_pegawai);
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; // 

		}else if($sub_kalimat == "Dekan"){
			///////////kondisi dimana acc disposisi untuk warek 3 ke biro
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			$this->Kesepahaman_model->update_kesepahaman($data_kesepahaman,$sph_id);			
			$id_pegawai = $this->Permohonan_model->cek_disposisi_in(1, 4);
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; // 

		}
		
		$disposisi['dis_jenis'] = 2; // 1 karena permohonan 1
		$disposisi['dis_entitas'] = $sph_id; // 
		$disposisi['dis_pegawaiasal'] = $id_pegawai[0]->id; // 
		$disposisi['dis_pegawaitujuan'] = $id_pegawai[1]->id; // 
		$disposisi['dis_status'] = 1; 
		
		///////cek disposisi apakah sudah ada atau tidak jika belum ada isi kalau sudah ada tidak perlu di isi 
		$cek_disposisi = $this->Permohonan_model->cek_disposisi2($disposisi);
		if($cek_disposisi['dis_id'] == ''){
			$id_disposisi = $this->Permohonan_model->add_disposisi($disposisi);			
		}
		
		
		echo "<script>window.location.href = '".base_url()."permohonan';</script>";
		
		
	}
	public function update_catatan(){
		date_default_timezone_set('Asia/Jakarta');
		$data['dis_catatan'] = $this->input->post('dis_catatan');
		$prm_id = $this->input->post('prm_id');
		$dis_id = $this->input->post('dis_id');
		$data['dis_status'] = '2';
		$data['dis_updated'] = date('Y-m-d H:i:s');
		$data_permohonan['prm_status'] = '2';
		
		$id_tugastambahan = $this->session->userdata('id_tugastambahan');
		
		if($id_tugastambahan == 1){
			// kondisi dimana acc disposisi untuk warek 3 ke biro
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			$this->Permohonan_model->update_permohonan($data_permohonan,$prm_id);			
			$id_pegawai = $this->Permohonan_model->cek_wa3_biro();
			
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; // 

		}else if($id_tugastambahan == 4){
			// 
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			// insert biro dan kabag 
			$id_pegawai = $this->Permohonan_model->cek_disposisi(353, 369);
			
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; // 

		}else if($id_tugastambahan == 353){
			
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			// insert kabag dan kasubag
			$id_pegawai = $this->Permohonan_model->cek_disposisi(369, 370);
			
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; // 

		}else if($id_tugastambahan == 369){
			
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			$id_pegawai = $this->Permohonan_model->cek_disposisi(370, 375);
			
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; // 

		}else if($id_tugastambahan == 370){
			
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			$id_pegawai = $this->Permohonan_model->cek_disposisi(375, 375);
			
			
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; // 

		}
		
		
		$disposisi['dis_jenis'] = 1; // 1 karena permohonan 1
		$disposisi['dis_entitas'] = $prm_id; // 
		$disposisi['dis_pegawaiasal'] = $id_pegawai[0]->id; // 
		$disposisi['dis_pegawaitujuan'] = $id_pegawai[1]->id; // 
		$disposisi['dis_status'] = 1; 
		
		// cek disposisi apakah sudah ada atau tidak jika belum ada isi kalau sudah ada tidak perlu di isi 
		$cek_disposisi = $this->Permohonan_model->cek_disposisi2($disposisi);
		if($cek_disposisi['dis_id'] == ''){
			$id_disposisi = $this->Permohonan_model->add_disposisi($disposisi);			
		}
		
		
		echo "<script>window.location.href = '".base_url()."permohonan';</script>";
	}
	public function detail($dis_id = 0){
		$data['detail'] = $this->Kesepahaman_model->getdisposisiDetail($dis_id);	
		// print_r ($data['detail']);
		$this->load->view('base/header',$data);
		$this->load->view('kesepahaman/ex_detail',$data);
		$this->load->view('base/footer',$data);
	}

	
}
