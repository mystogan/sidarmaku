<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kesepakatan extends CI_Controller {
	function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('Kesepakatan_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		$menu = $this->session->userdata('menu');
		$id = $this->session->userdata('user_id');
		if ($id == '' || $id == null) {
			header("location:".base_url()."login");
		}else if ($menu['kesepakatan'] == 0 || $menu['kesepakatan'] == '' ) {
			// echo "<script>alert ('Menu Laporan tidak untuk diakses untuk user ini');
			// window.location.href = '".base_url()."home/notfound';</script>";
			header("location:".base_url()."home/notfound");
		}
	}

	public function index()
	{

		// get session dan disimpan dalam variabel
		$id_tugastambahan = $this->session->userdata('id_tugastambahan');
		$user_id = $this->session->userdata('user_id');
		$user_jenis = $this->session->userdata('user_jenis');
		// menangkap post year dan internal post
		$data['yearPost'] = $this->input->post('yearPost');
		$data['internalPost'] = $this->input->post('internalPost');
		// kondisi dimana ketika tidak ada yang dipost maka akan didefault karena akan dikirim ke parameter
		if($data['yearPost'] == null)
		{
			$data['yearPost'] = date('Y');
		}
		if($data['internalPost'] == null)
		{
			$data['internalPost'] = 1;
		}
		// user_jenis 1 untuk external dan user_jenis 2 untuk internal
		if($user_jenis == 1 ){
			// default untuk parameter external
			$data['internalPost'] = 2;
			// get models kesepakatan external
			$data['kesepakatan'] = $this->Kesepakatan_model->getKesepakatan_ex($user_id,$data['yearPost'],$data['internalPost']);
			$form = 'ex_index';
		}else if($user_jenis == 2){ // kondisi untuk internal karena user_jenis 2
			// kondisi dimana untuk Kabag Fakultas contohnya disaintek adlah bu hasmi
			if($this->session->userdata('nama_jastruk') == "Kepala Bagian Tata Usaha"){
				// get kesepakatan internal pada kabag ada 3 parameter kode unit, tahun, dan internal
				$data['kesepakatan'] = $this->Kesepakatan_model->getkesepakatan_in($this->session->userdata('id_kode_unit'),$data['yearPost'],$data['internalPost']);
				// get wakil dekan untuk if else ditampilan  karena setiap fakultas mempunyai wakil dekan yang tidak sama
				$data['wadek3'] = $this->Kesepakatan_model->get_jastrukwadek3($this->session->userdata('id_kode_unit'));
				// get dekan untuk if else ketika ditampilkan di view
				$data['dekan'] = $this->Kesepakatan_model->get_jastrukdekan($this->session->userdata('id_kode_unit'));
				// print_r ($data['kesepakatan']);
				$form = 'in_index';
			// khusus untuk jastruk Pengadministrasi pada kerjasama , sbg input kerja sama dengan bagian yang lain
			}else if($id_tugastambahan == 374){ // khusus untuk jastruk Pengadministrasi pada kerjasama
				// model khusu yang digunakan untuk menampilkan semua permintaan dari masing-masing
				// 			bagian atau unit dalam kampur uin sunan ampel surabaya
				$data['kesepakatan'] = $this->Kesepakatan_model->getkesepakatan_rek($data['yearPost']);
				$form = 'in_index_rektorat';
			}else {
				// kondisi terakhir adalah untuk user yang ada di tabel disposisi yaitu pegawai asal
				// get dibawah untuk mengambil disposisi masing-masing pejabat
				$data['disposisi'] = $this->Kesepakatan_model->getdisposisi($id_tugastambahan,$data['yearPost'],$data['internalPost']);
				$j = 0;
				// perulangan ini digunakan untuk mendapatkan disposisi terakhir ketika ada yang digagalkan pengajuannya
				foreach($data['disposisi'] as $value){
					// get disposisi terakhir dan join ke tabel m_jastruk di simpeg karena untuk mengambil
					// 			nama jastruk dari tabel jastruk digunakan untuk if else di view
					$data['paksa'][$j++] = $this->Kesepakatan_model->getDisposisiLast($value['spk_id']);
				}
				$form = 'ex_uin';
			}

		}else {
			// redirect ketika sessionnya hilang atau habis
			header("location:".base_url()."login");
		}

		$this->load->view('base/header',$data);
		$this->load->view('kesepakatan/'.$form,$data);
		$this->load->view('base/footer',$data);

	}

	// fungsi digunakan untuk add dan update kesepakatan external
	public function add($id = 0)
	{
		// deklarasi model jenis model digunakan untuk get master jenis
		$this->load->model('Jenis_model');
		if($id != 0){
			// digunakan ketika edit akan diselect berdasarkan id kesepakatan
			$data['kesepakatan'] = $this->Kesepakatan_model->getDetilkesepakatan($id);
		}
		// get untuk mendapatkan seluruh unit dalam kampus
		$data['unit'] = $this->Kesepakatan_model->getunit();

		// get seluruh pegawai di uin sunan ampel surabaya
		$data['pegawai'] = $this->Kesepakatan_model->getAllPegawai();
		$data['jenis'] = $this->Jenis_model->getJenis();
		// get untuk mendapatkan seluruh unit dalam kampus
		$data['unit'] = $this->Kesepakatan_model->getunit();
		// get model kesepahaman untuk dropdown
		$data['kesepahaman'] = $this->Kesepakatan_model->getKesepahaman();
		// print_r ($data['kesepakatan']);
		$this->load->view('base/header',$data);
		$this->load->view('kesepakatan/ex_add',$data);
		$this->load->view('base/footer',$data);
	}

	// fungsi digunakan untuk menghapus kesepakatan
	public function setkesepakatan($spk_id,$dis_id)
	{
		// cek jika parameter tidak ada akan diredirect kembali ke kesepakatan
		if($spk_id != null)
		{
			// untuk pertama update disposisi terakhir karena pejabat terakhir yang membatalkan
			// tau terakhir dari di_id karena yang dikirim adalah dis_id terakhir
			$datas['dis_status'] = 3;
			// model update disposisi
			$this->Kesepakatan_model->update_disposisi($datas,$dis_id);
			// kemudian update pada kesepakatan dengan status 3 karena untuk memastikan bahwa kesepakatan tersebut dibatalkan
			$data['spk_status'] = 3;
			// model update kesepakatan
			$this->Kesepakatan_model->update_kesepakatan($data,$spk_id);
			echo "<script>alert ('Pembatalan kesepakatan Selesai');
			window.location.href = '".base_url()."kesepakatan';</script>";
		}else
		{
			echo "<script>alert ('Failed Silahkan Ulangi Kembali');
			window.location.href = '".base_url()."kesepakatan';</script>";
		}
	}

	// fungsi yang untuk add dan update kesepakatan internal
	public function in_add($id = 0)
	{
		// deklarasi model jenis model digunakan untuk get master jenis
		$this->load->model('Jenis_model');
		if($id != 0){
			// digunakan ketika edit akan diselect berdasarkan id kesepakatan
			// $data['detilkesepahaman'] = $this->Kesepakatan_model->getDetPermohonan($id);
		}
		// kondisi untuk pegawai input
		if($this->session->userdata('id_tugastambahan') == 374){
			$form = 'in_add_rektorat';
		}else {
			$form = 'in_add';
		}
		// get seluruh pegawai di uin sunan ampel surabaya
		$data['pegawai'] = $this->Kesepakatan_model->getAllPegawai();
		// get model untuk mengambil mitra
		$data['mitra'] = $this->Kesepakatan_model->getmitra();
		// get untuk mendapatkan unit orangnya
		$data['units'] = $this->Kesepakatan_model->getunit();
		$data['unit'] = $this->Kesepakatan_model->getunit_single($this->session->userdata('user_username'));
		// // get model untuk mengambil kesepakatan
		$data['kesepakatan'] = $this->Kesepakatan_model->getDetilkesepakatan($id);
		$data['jenis'] = $this->Jenis_model->getJenis();
		// get model permohonan untuk dropdown
		$data['kesepahaman'] = $this->Kesepakatan_model->getKesepahamanFix();
		$this->load->view('base/header',$data);
		$this->load->view('kesepakatan/'.$form,$data);
		$this->load->view('base/footer',$data);
	}

	// fungsi yang digunakan untuk menambahkan dan mengupdate kesepakatan external
	public function addKesepakatan_mitra($id = 0)
	{
		$data['spk_id'] = $this->input->post('spk_id'); // cek untuk id update
		$data['mtr_id'] = $this->session->userdata('user_id'); // cek session untuk mitra
		$data['jns_id'] = $this->input->post('jns_id'); //id untuk jenis
		$data['sph_id'] = $this->input->post('sph_id'); //id untuk kepepahaman
		$data['spk_nomor'] = $this->input->post('spk_nomor'); // id untuk nomor
		$data['spk_judul'] = $this->input->post('spk_judul');
		$data['spk_tanggal'] = $this->input->post('spk_tanggal');
		$data['spk_namapihak1'] = $this->input->post('spk_namapihak1');
		$data['spk_jabatanpihak1'] = $this->input->post('spk_jabatanpihak1');
		$data['spk_skpihak1'] = $this->input->post('spk_skpihak1');
		$data['spk_pegawaipihak1'] = $this->input->post('spk_pegawaipihak1');
		$data['spk_namapihak2'] = $this->input->post('spk_namapihak2');
		$data['spk_jabatanpihak2'] = $this->input->post('spk_jabatanpihak2');
		$data['spk_skpihak2'] = $this->input->post('spk_skpihak2');
		$data['spk_tujuan'] = $this->input->post('spk_tujuan');
		$data['spk_unit'] = $this->input->post('spk_unit');
		$data['spk_mulai'] = $this->input->post('spk_mulai');
		$data['spk_akhir'] = $this->input->post('spk_akhir');
		$data['spk_internal'] = $this->input->post('spk_internal'); // set internal 1
		$data['spk_status'] = "1"; // set

		//print_r ($data);
		// berfungsi untuk get format file
		$file_upload = $_FILES["spk_file"]["name"];
		if($file_upload != "" || $file_upload != null){
			// memanggil fungsi untuk looping
			$terima = $this->loop("spk_file");
		}
		// menyimpan nama file untuk disimpan
		$data['spk_file'] = $terima['nama'];

		if($terima['cek'] == 0 ){
			// cek ketika error dan menampilkan pesan error
			echo "<script> alert ('asd ".$terima['pesan']."')</script>";
		}else {
			// if untuk mengecek update atau insert
			if($data['spk_id'] == ""){
				// untuk mnyimpan kesepakatan
				$id_permohonan = $this->Kesepakatan_model->add_kesepakatan($data);
				// untuk mendapatkan id pegawai dan id_tugastambahan pada warek 3 ke biro
				$id_pegawai = $this->Kesepakatan_model->cek_wa3_biro();
				$disposisi['dis_jenis'] = 3; // 3 karena kesepakatan 3
				$disposisi['dis_entitas'] = $id_permohonan; //
				$disposisi['dis_pegawaiasal'] = $id_pegawai[0]->id; //
				$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; //
				$disposisi['dis_pegawaitujuan'] = $id_pegawai[1]->id; //
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; //
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //
				$disposisi['dis_status'] = 1;
				// insert disposisi warek 3 ke biro
				$id_disposisi = $this->Kesepakatan_model->add_disposisi($disposisi);

			}else {
				$this->Kesepakatan_model->update_kesepakatan($data,$data['spk_id']);
			}

		}
		echo "<script>window.location.href = '".base_url()."kesepakatan';</script>";

	}

	// fungsi yang digunakan untuk menambahkan dan mengupdate kesepakatan internal dan external
	// khusus untuk kesepakatan input external dan internal sama
	public function addKesepakatan_unit($id = 0)
	{
		$data['spk_id'] = $this->input->post('spk_id'); // cek untuk id update
		$data['mtr_id'] = $this->input->post('mtr_id'); // get untuk mitra
		$data['jns_id'] = $this->input->post('jns_id'); //id untuk jenis
		$data['sph_id'] = $this->input->post('sph_id'); //id untuk kepepahaman
		$data['spk_nomor'] = $this->input->post('spk_nomor'); // id untuk nomor
		$data['spk_judul'] = $this->input->post('spk_judul');
		$data['spk_tanggal'] = $this->input->post('spk_tanggal');
		$data['spk_namapihak1'] = $this->input->post('spk_namapihak1');
		$data['spk_jabatanpihak1'] = $this->input->post('spk_jabatanpihak1');
		$data['spk_skpihak1'] = $this->input->post('spk_skpihak1');
		$data['spk_pegawaipihak1'] = $this->input->post('spk_pegawaipihak1');
		$data['spk_namapihak2'] = $this->input->post('spk_namapihak2');
		$data['spk_jabatanpihak2'] = $this->input->post('spk_jabatanpihak2');
		$data['spk_skpihak2'] = $this->input->post('spk_skpihak2');
		$data['spk_tujuan'] = $this->input->post('spk_tujuan');
		$data['spk_unit'] = $this->input->post('spk_unit');
		$data['spk_mulai'] = $this->input->post('spk_mulai');
		$data['spk_akhir'] = $this->input->post('spk_akhir');
		$data['spk_internal'] = $this->input->post('spk_internal'); // set internal 1
		$data['spk_status'] = "1"; // set

		$unit = $data['spk_unit'];

		// berfungsi untuk get format file
		$file_upload = $_FILES["spk_file"]["name"];
		if($file_upload != "" || $file_upload != null){
			// memanggil fungsi untuk looping
			$terima = $this->loop("spk_file");
		}
		// untuk menyimpan pada array dan yang akan disimpan
		$data['spk_file'] = $terima['nama'];
		if($terima['cek'] == 0 ){
			// cek ketika error dan menampilkan pesan error
			echo "<script> alert ('".$terima['pesan']."')</script>";
		}else {
			// if untuk mengecek update atau insert
			if($data['spk_id'] == ""){
				// untuk mnyimpan kesepakatan
				$id_kesepahaman = $this->Kesepakatan_model->add_kesepakatan($data);
				// untuk mendapatkan id pegawai dan id_tugastambahan wadek 3 dan dekan
				$id_pegawai = $this->Kesepakatan_model->cek_wadek3_dekan($unit);

				$disposisi['dis_jenis'] = 3; // 3 karena kesepakatan 3
				$disposisi['dis_entitas'] = $id_kesepahaman; //
				$disposisi['dis_pegawaiasal'] = $id_pegawai[0]->id; //
				$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; //
				$disposisi['dis_pegawaitujuan'] = $id_pegawai[1]->id; //
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; //
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; //
				$disposisi['dis_status'] = 1;
				// print_r ($id_pegawai);
				// insert disposisi wadek 3 dan dekan
				$id_disposisi = $this->Kesepakatan_model->add_disposisi($disposisi);

			}else {
				// update kesepakatan
				$this->Kesepakatan_model->update_kesepakatan($data,$data['spk_id']);
			}

		}
		echo "<script>window.location.href = '".base_url()."kesepakatan';</script>";

	}

	// fungsi yang digunakan untuk menambahkan kesepahaman rektorat
	public function addKesepakatan_rektorat($id = 0)
	{
		$data['spk_id'] = $this->input->post('spk_id'); // cek untuk id update
		$data['mtr_id'] = $this->input->post('mtr_id'); // get untuk mitra
		$data['jns_id'] = $this->input->post('jns_id'); //id untuk jenis
		$data['sph_id'] = $this->input->post('sph_id'); //id untuk kepepahaman
		$data['spk_nomor'] = $this->input->post('spk_nomor'); // id untuk nomor
		$data['spk_judul'] = $this->input->post('spk_judul');
		$data['spk_tanggal'] = $this->input->post('spk_tanggal');
		$data['spk_namapihak1'] = $this->input->post('spk_namapihak1');
		$data['spk_jabatanpihak1'] = $this->input->post('spk_jabatanpihak1');
		$data['spk_skpihak1'] = $this->input->post('spk_skpihak1');
		$data['spk_pegawaipihak1'] = $this->input->post('spk_pegawaipihak1');
		$data['spk_namapihak2'] = $this->input->post('spk_namapihak2');
		$data['spk_jabatanpihak2'] = $this->input->post('spk_jabatanpihak2');
		$data['spk_skpihak2'] = $this->input->post('spk_skpihak2');
		$data['spk_tujuan'] = $this->input->post('spk_tujuan');
		$data['spk_unit'] = $this->input->post('spk_unit');
		$data['spk_mulai'] = $this->input->post('spk_mulai');
		$data['spk_akhir'] = $this->input->post('spk_akhir');
		$data['spk_internal'] = 3; // set internal 3
		$data['spk_status'] = "1"; // set



		// berfungsi untuk get format file
		$file_upload = $_FILES["spk_file"]["name"];
		if($file_upload != "" || $file_upload != null){
			// memanggil fungsi untuk looping
			$terima = $this->loop("spk_file");
		}
		// untuk menyimpan pada array dan yang akan disimpan
		$data['spk_file'] = $terima['nama'];

		if($terima['cek'] == 0  ){
			// cek ketika error dan menampilkan pesan error
			echo "<script> alert ('".$terima['error']."')</script>";
		}else {
			// if untuk mengecek update atau insert
			if($data['spk_id'] == ""){
				$id_permohonan = $this->Kesepakatan_model->add_kesepakatan($data);
				// untuk mendapatkan id pegawai dan id_tugastambahan warek 3 dan rektor
				$id_pegawai = $this->Kesepakatan_model->cek_disposisi_in(4,1);
				$disposisi['dis_jenis'] = 3; // 3 kesepakatan
				$disposisi['dis_entitas'] = $id_permohonan; //
				$disposisi['dis_pegawaiasal'] = $id_pegawai[0]->id; //
				$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; //
				$disposisi['dis_pegawaitujuan'] = $id_pegawai[1]->id; //
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan;
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan;
				$disposisi['dis_status'] = 1;

				// insert disposisi wadek 3 dan dekan
				$id_disposisi = $this->Kesepakatan_model->add_disposisi($disposisi);

			}else {
				$this->Kesepakatan_model->update_kesepakatan($data,$data['spk_id']);
			}

		}
		echo "<script>window.location.href = '".base_url()."kesepakatan';</script>";

	}

	// fungsi yang digunakan untuk proses upload menggunakan plugins codeigniter
	function loop($nama)
	{

		$config['upload_path'] = './assets/upload/';
		$config['allowed_types'] = 'gif|jpg|png|exe|xls|doc|docx|xlsx|jpeg';
		$config['max_size']      = '10000000';
		$config['remove_spaces']=TRUE;  //it will remove all spaces
		$config['encrypt_name']=TRUE;   //it wil encrypte the original file name
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload($nama))
		{
			// set variabel error pesan
			$pesan = $this->upload->display_errors();
			$cek = 0;
		}
		else
		{
			// proses upload file dan akan disimpan pada file nama untuk nama file
		   $data = $this->upload->data();
		   $pesan = "Upload Sukses";
		   $nama = $data['file_name'];
		   $cek = 1;
		}
		$datas['cek'] = $cek ; 
		$datas['pesan'] = $pesan ;
		$datas['nama'] = $nama ;
		// mengembalikan nilai cek,pesan dan nama untuk dicek selanjutnya
		return $datas;
	}

	// fungsi yang digunakan untuk menampilkan catatan pada internal dan external akan tetapi dipisahkan
	// 			dengan kondisi
	public function catatan($dis_id = 0)
	{
		// get session id_tugastambahan
		$id_tugastambahan = $this->session->userdata('id_tugastambahan');
		// get disposisi detail pada id disposisi
		$data['detail'] = $this->Kesepakatan_model->getdisposisiDetail($dis_id);
		// get semua disposisi pada permohonan untuk mendapatkan catatan
		$data['catatan'] = $this->Kesepakatan_model->get_dis_catatan($data['detail']['spk_id']);
		$data['dis_id'] = $dis_id;
		$data['count_pj'] = $this->Kesepakatan_model->count_pj($data['detail']['spk_id'],$this->session->userdata('user_id'));
		// kondisi dimana untuk membedakan tampilan antara external dan internal
		if($data['detail']['spk_internal'] == 3){
			$view = 'in_catatan_rektorat';
		}else if($data['detail']['spk_internal'] == 1){
			$view = 'in_catatan';
		}else {
			$view = 'ex_catatan';
		}
		$data['id_tugastambahan'] = $id_tugastambahan; 
		// get seluruh pegawai di uin sunan ampel surabaya
		$data['pegawai'] = $this->Kesepakatan_model->getAllPegawai();
		$this->load->view('base/header',$data);
		$this->load->view('kesepakatan/'.$view,$data);
		$this->load->view('base/footer',$data);
	}

	// fungsi yang digunakan utnuk update catatan internal pada pejabat
	public function update_catatan_internal()
	{
		date_default_timezone_set('Asia/Jakarta');
		// ini kondisi untuk ketika inputan yang dimasukkan adalah kosong atau null dan menghapus spasi
		if (trim($this->input->post('dis_catatan')) == '' || trim($this->input->post('dis_catatan')) == null) {
			$data['dis_catatan'] = "Catatan yang dikirim sebelumnya kosong :)";
		}else {
			$data['dis_catatan'] = trim($this->input->post('dis_catatan'));
		}
		$prm_id = $this->input->post('spk_id');
		$dis_id = $this->input->post('dis_id');
		$data['dis_status'] = '2'; // set status karena sudah didisposisi
		$data['dis_updated'] = date('Y-m-d H:i:s');
		$data_permohonan['spk_status'] = '2'; // set status kesepahaman sudah didisposisi set 2
		$id_tugastambahan = $this->session->userdata('id_tugastambahan');

		// untuk mengecek apakah dia wadek atau dekan
		$cek_pangkat = $this->Kesepakatan_model->cek_pangkat($id_tugastambahan);
		// query dibawah untuk mengecek apakah warek 3 disposisi ke 1 atau yang ke 2
		$count_kesepahaman = $this->Kesepakatan_model->count_kesepakatan($prm_id);
		// get untuk nama penanggungjawab dan pic ditabel kesepakatan
		$detil = $this->Kesepakatan_model->getDetilkesepakatan($prm_id);
		// get pickesepakatan pada tabel kesepakatan dimodel atas
		$picKesepakatan = $detil['spk_pegawaipic'];
		// get pjkesepakatan pada tabel kesepakatan dimodel atas
		$pjKesepakatan = $detil['spk_pegawaipihak1'];
		// get session id pada pegawai
		$user_id = $this->session->userdata('user_id');
		// fungsi substring digunakan untuk mendapatkan kata wakil dan dekan
		$sub_kalimat = substr($cek_pangkat['nama_jastruk'],0,5);
		$count_pj = $this->Kesepakatan_model->count_pj($prm_id,$pjKesepakatan);

		// ini untuk pertanggungjawaban yang kedua validasi
		if($user_id == $pjKesepakatan && $count_pj >= 2 ){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Kesepakatan_model->update_disposisi($data,$dis_id);
			// insert kabag dan kasubag
			$id_pegawai = $this->Kesepakatan_model->cek_disposisi(369, 370);
			// kondisi dimana mengecek apakah dia dosen atau tidak jika dia dosen maka akan diambil
			// 		id_tugastambahan dan jika dia pegawai biasa akan diambil id_jastruk
			if($id_pegawai[0]->id_tugastambahan == 0){
				$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk;
			}else {
				$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan;
			}
			// kondisi yang sama akan tetapi untuk disposisi tujuan
			if($id_pegawai[1]->id_tugastambahan == 0){
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk;
			}else {
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan;
			}

		}else
		//ini adalah input PIC dengan mencocokan session dengan value yang ada dikesepakatan
		if($user_id == $picKesepakatan && $count_pj <= 1){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Kesepakatan_model->update_disposisi($data,$dis_id);

			$data_permohonan['spk_anggaran'] = $this->input->post('spk_anggaran');
			$this->Kesepakatan_model->update_kesepakatan($data_permohonan,$prm_id);
			/////////// insert penanggung jawab kerjasama dan kabag kerjasama
			$id_pegawai = $this->Kesepakatan_model->cek_disposisiForSPK($pjKesepakatan, 369);
			// kondisi dimana mengecek apakah dia dosen atau tidak jika dia dosen maka akan diambil
			// 		id_tugastambahan dan jika dia pegawai biasa akan diambil id_jastruk
			if($id_pegawai[0]->id_tugastambahan == 0){
				$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; //
			}else {
				$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; //
			}
			// kondisi yang sama akan tetapi untuk disposisi tujuan
			if($id_pegawai[1]->id_tugastambahan == 0){
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //
			}else {
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; //
			}

		// kondisi kedua adalah untuk ketika disposisi wakil rektor 3
		}else if($user_id == $pjKesepakatan && $count_pj <= 1){ //ini adalah input penanggung jawab yang pertama kali
			// post pegawai untuk menentukan penanggung jawab
			$data_permohonan['spk_pegawaipic'] = $this->input->post('spk_pegawaipic');
			$data_permohonan['spk_anggaran'] = $this->input->post('spk_anggaran');
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Kesepakatan_model->update_disposisi($data,$dis_id);
			/////////// insert warek 3 dan rektor
			$id_pegawai = $this->Kesepakatan_model->cek_disposisiForSPK($this->input->post('spk_pegawaipic'), $pjKesepakatan);
			$this->Kesepakatan_model->update_kesepakatan($data_permohonan,$prm_id);
			// kondisi dimana mengecek apakah dia dosen atau tidak jika dia dosen maka akan diambil
			// 		id_tugastambahan dan jika dia pegawai biasa akan diambil id_jastruk
			if($id_pegawai[0]->id_tugastambahan == 0){
				$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; //
			}else {
				$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; //
			}
			// kondisi yang sama akan tetapi untuk disposisi tujuan
			if($id_pegawai[1]->id_tugastambahan == 0){
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //
			}else {
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; //
			}

		// kondisi kedua adalah untuk ketika disposisi wakil rektor 3
		}else if($id_tugastambahan == 4 && $this->input->post('spk_internal') == 3
			&& count($count_kesepahaman) == 1){
			// post pegawai untuk menentukan penanggung jawab
			$data_permohonan['spk_pegawaipihak1'] = $this->input->post('spk_pegawaipihak1');
			$data_permohonan['spk_pegawaipic'] = $this->input->post('spk_pegawaipic');
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Kesepakatan_model->update_disposisi($data,$dis_id);
			// model yang dipanggil adalah untuk mengupdate kesepakatan berupa catatan dan status
			$this->Kesepakatan_model->update_kesepakatan($data_permohonan,$prm_id);
			/////////// insert warek 3 dan rektor
			$id_pegawai = $this->Kesepakatan_model->cek_disposisi_in(1, 4);
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; //

		// kondisi kedua adalah untuk ketika disposisi rektor
		}else if($id_tugastambahan == 1){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Kesepakatan_model->update_disposisi($data,$dis_id);
			// model yang dipanggil adalah untuk mengupdate kesepahaman berupa catatan dan status
			$this->Kesepakatan_model->update_kesepakatan($data_permohonan,$prm_id);
			///////////kondisi dimana acc disposisi untuk warek 3 ke biro
			$id_pegawai = $this->Kesepakatan_model->cek_wa3_biro();
			// kondisi dimana mengecek apakah dia dosen atau tidak jika dia dosen maka akan diambil
			// 		id_tugastambahan dan jika dia pegawai biasa akan diambil id_jastruk
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //

		// kondisi selanjutnya adalah untuk wakil rektor 3
		}else if($id_tugastambahan == 4){
			// post pegawai untuk menentukan penanggung jawab
			$data_permohonan['spk_pegawaipihak1'] = $this->input->post('spk_pegawaipihak1');
			$data_permohonan['spk_pegawaipic'] = $this->input->post('spk_pegawaipic');
			$this->Kesepakatan_model->update_kesepakatan($data_permohonan,$prm_id);
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Kesepakatan_model->update_disposisi($data,$dis_id);
			/////////// insert biro dan kabag
			$id_pegawai = $this->Kesepakatan_model->cek_disposisi(353, 369);
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //

		// kondisi selanjutnya adalah digunakan untuk biro AAK
		}else if($id_tugastambahan == 353){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Kesepakatan_model->update_disposisi($data,$dis_id);
			///insert pj dan pic kesepakatan yang didapat dari query diatas yang menggunakan tabel kesepakatan
			// 			untuk lebih jelasnya lihat alur dan dokumentasi
			$id_pegawai = $this->Kesepakatan_model->cek_disposisiForSPK($pjKesepakatan, $picKesepakatan);
			// kondisi dimana mengecek apakah dia dosen atau tidak jika dia dosen maka akan diambil
			// 		id_tugastambahan dan jika dia pegawai biasa akan diambil id_jastruk
			if($id_pegawai[0]->id_tugastambahan == 0){
				$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; //
			}else {
				$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; //
			}
			// kondisi yang sama akan tetapi untuk disposisi tujuan
			if($id_pegawai[1]->id_tugastambahan == 0){
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //
			}else {
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; //
			}
		// kondisi dimana untuk kepala bagian kerjasama
		}else if($id_tugastambahan == 369){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Kesepakatan_model->update_disposisi($data,$dis_id);
			// insert kasubag ke pegawai
			$id_pegawai = $this->Kesepakatan_model->cek_disposisi(370, 375);
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //

		// kondisi dimana untuk kepala sub bagian
		}else if($id_tugastambahan == 370){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Kesepakatan_model->update_disposisi($data,$dis_id);
			// insert pegawai to pegawai kerjasama
			$id_pegawai = $this->Kesepakatan_model->cek_disposisi(375, 375);
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //

		// kondisi dimana digunakan untuk wakil dekan masing-masing fakultas
		}else if($sub_kalimat == "Wakil"){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Kesepakatan_model->update_disposisi($data,$dis_id);
			// model yang dipanggil adalah untuk mengupdate permohonan berupa catatan dan status
			$this->Kesepakatan_model->update_kesepakatan($data_permohonan,$prm_id);
			// kondisi dimana insert dekan dan rektor
			$id_pegawai = $this->Kesepakatan_model->cek_disposisi_in($cek_pangkat['id_pimpinan'], 1);
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; //

		// kondisi dimana digunakan untuk dekan masing-masing fakultas
		}else if($sub_kalimat == "Dekan"){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Kesepakatan_model->update_disposisi($data,$dis_id);
			// model yang dipanggil adalah untuk mengupdate permohonan berupa catatan dan status
			$this->Kesepakatan_model->update_kesepakatan($data_permohonan,$prm_id);
			///////////kondisi dimana acc disposisi untuk warek 3 ke biro
			$id_pegawai = $this->Kesepakatan_model->cek_wa3_biro();
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //

		}

		// proses menyimpan ke array untuk menyimpan keselanjutnya
		$disposisi['dis_jenis'] = 3; // 1 karena kesepahaman adalah 1
		$disposisi['dis_entitas'] = $prm_id;
		$disposisi['dis_pegawaiasal'] = $id_pegawai[0]->id; //
		$disposisi['dis_pegawaitujuan'] = $id_pegawai[1]->id; //
		$disposisi['dis_status'] = 1; // 1 karena insert dan belum didisposisi

		///////cek disposisi apakah sudah ada atau tidak jika belum ada isi kalau sudah ada tidak perlu di isi
		$cek_disposisi = $this->Kesepakatan_model->cek_disposisi2($disposisi);
		if($cek_disposisi['dis_id'] == ''){
			$id_disposisi = $this->Kesepakatan_model->add_disposisi($disposisi);
		}

		echo "<script>window.location.href = '".base_url()."kesepakatan';</script>";


	}

	// fungsi yang digunakan utnuk update catatan external pada pejabat
	public function update_catatan()
	{
		date_default_timezone_set('Asia/Jakarta');
		// ini kondisi untuk ketika inputan yang dimasukkan adalah kosong atau null dan menghapus spasi
		if (trim($this->input->post('dis_catatan')) == '' || trim($this->input->post('dis_catatan')) == null) {
			$data['dis_catatan'] = "Catatan yang dikirim sebelumnya kosong :)";
		}else {
			$data['dis_catatan'] = trim($this->input->post('dis_catatan'));
		}
		$prm_id = $this->input->post('prm_id');
		$dis_id = $this->input->post('dis_id');
		$data['dis_status'] = '2'; // set status disposisi sudah didisposisi set 2
		$data['dis_updated'] = date('Y-m-d H:i:s');
		$data_permohonan['prm_status'] = '2'; // set status permohonan sudah didisposisi set 2

		$id_tugastambahan = $this->session->userdata('id_tugastambahan');

		// kondisi kedua adalah untuk ketika disposisi rektor
		if($id_tugastambahan == 1){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			// model yang dipanggil adalah untuk mengupdate permohonan berupa catatan dan status
			$this->Permohonan_model->update_kesepahaman($data_permohonan,$prm_id);
			// kondisi dimana acc disposisi untuk warek 3 ke biro
			$id_pegawai = $this->Permohonan_model->cek_wa3_biro();
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //

		// kondisi selanjutnya adalah untuk wakil rektor 3
		}else if($id_tugastambahan == 4){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			// insert biro dan kabag
			$id_pegawai = $this->Permohonan_model->cek_disposisi(353, 369);
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //

		// kondisi selanjutnya adalah digunakan untuk biro AAK
		}else if($id_tugastambahan == 353){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			// insert kabag dan kasubag
			$id_pegawai = $this->Permohonan_model->cek_disposisi(369, 370);
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //

		// kondisi dimana untuk kepala bagian kerjasama
		}else if($id_tugastambahan == 369){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			// insert kasubag ke pegawai
			$id_pegawai = $this->Permohonan_model->cek_disposisi(370, 375);
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //

		// kondisi dimana untuk kepala sub bagian
		}else if($id_tugastambahan == 370){

			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			// insert pegawai to pegawai
			$id_pegawai = $this->Permohonan_model->cek_disposisi(375, 375);
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //

		}

		// proses menyimpan ke array untuk menyimpan keselanjutnya
		$disposisi['dis_jenis'] = 1; // 1 karena permohonan 1
		$disposisi['dis_entitas'] = $prm_id; //
		$disposisi['dis_pegawaiasal'] = $id_pegawai[0]->id; //
		$disposisi['dis_pegawaitujuan'] = $id_pegawai[1]->id; //
		$disposisi['dis_status'] = 1;

		// cek disposisi apakah sudah ada atau tidak jika belum ada isi kalau sudah ada tidak perlu di isi
		$cek_disposisi = $this->Permohonan_model->cek_disposisi2($disposisi);
		if($cek_disposisi['dis_id'] == ''){
			$id_disposisi = $this->Permohonan_model->add_disposisi($disposisi);
		}


		echo "<script>window.location.href = '".base_url()."permohonan';</script>";
	}

	// fungsi yang digunakan untuk menampilkan detail untuk melihat detail inputan
	public function detail($dis_id = 0)
	{
		// model yang digunakan untuk mengambil disposisi detail
		$data['detail'] = $this->Kesepakatan_model->getdisposisiDetail($dis_id);
		//mengambil nama unit atau fakultas yang akan digunakan untuk ditampilkan
		$data['unit'] = $this->Kesepakatan_model->getFakultas($data['detail']['spk_unit']);
		// print_r ($data['unit']);
		$this->load->view('base/header',$data);
		$this->load->view('kesepakatan/ex_detail',$data);
		$this->load->view('base/footer',$data);
	}

}
