<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('Home_model');
		$this->load->database();
		$this->load->helper(array('form','url','file','download'));

	}
	public function index(){
		$user = $this->session->userdata('user_id');
		if ($user == '') {
			header("location:".base_url()."login");
		}else {
			$controller = $this->cekRedirect();
			// print_r ($controller);
			// if ($controller == "login") {
			// 	echo "<script>alert ('Anda Tidak Mempunyai Akses diAplikasi ini Silahkan Menghubungi PUSTIPD');window.location.href = '".base_url()."login/logout';</script>";
			// }else {
			// 	if($this->session->userdata('menu') == ''){
			// 		$menu = $this->cekMenu();
			// 		$this->session->set_userdata('menu',$menu);
			// 	}
			// 	header("location:".base_url().$controller);	
			// }
			if($this->session->userdata('menu') == ''){
				$menu = $this->cekMenu();
				$this->session->set_userdata('menu',$menu);
			}
			header("location:".base_url().$controller);	

		}
		// $this->load->view('login');
	}

	// fungsi untuk ngecek redirect ke menu mana yang akan dipacu 
	public function cekRedirect()
	{
		// get session yang digunakan untuk get id user contohnya untuk UIN adlaah id dalam tbpegawai
		$idUser = $this->session->userdata('user_id');
		// get session yang digunakan untuk membedakan user UIN atau user External UIN
		$user_jenis = $this->session->userdata('user_jenis');
		if ($user_jenis == 1) 
		{
			// user jenis 1 adalah punya mitra atau user external 
			$controller = "permohonan";
		}else { // untuk userjenis yang lain adalah pegawai UIN Sunan Ampel Surabaya
			// get session yang digunakan untuk get jabatan yang dia ampu sekarang
			$id_tugastambahan = $this->session->userdata('id_tugastambahan');
			$this->load->model('Kesepakatan_model');
			$cek_pangkat = $this->Kesepakatan_model->cek_pangkat($id_tugastambahan);
			$sub_kalimat = substr($cek_pangkat['nama_jastruk'],0,5);
			if ($sub_kalimat == "Wakil" || $sub_kalimat == "Dekan" || $id_tugastambahan == 1 || 
				$id_tugastambahan == 4 || $id_tugastambahan == 353 || $id_tugastambahan == 369 ||
				$id_tugastambahan == 370 || $id_tugastambahan == 375 || $id_tugastambahan == 374  ) 
			{
				$controller = "permohonan";
				
			}else 
			{
				$cekPICPJ = $this->Home_model->cekPICPJ($idUser);
				if(count($cekPICPJ) >= 1)
				{
					$controller = "kesepakatan";
				}else 
				{
					$getUnit = $this->Home_model->getUnit($id_tugastambahan);
					if($getUnit['kode_unit'] == 76 || $getUnit['kode_unit'] == 92 )
					{
						$controller = "m_mitra";
					}else 
					{
						$controller = "permohonan";
					}
				}

			}

		}

		return $controller;
	}
	
	// function yang digunakan untuk mendapatkan hak akses untuk menu2 yang ada disidarmaku
	public function cekMenu()
	{
		$id_tugastambahan = $this->session->userdata('id_tugastambahan');
		$idUser = $this->session->userdata('user_id');
		$user_jenis = $this->session->userdata('user_jenis');
		if ($user_jenis == 1) 
		{
			$data['permohonan'] = 1;
			$data['kesepahaman'] = 1;
			$data['kesepakatan'] = 1;
			$data['laporan'] = 0;
			$data['master'] = 0;
			$data['jenis'] = 0;
		}else 
		{
			// untuk menu permohonan, kesepahaman 
			$user_jenis = $this->session->userdata('user_jenis');
			$id_tugastambahan = $this->session->userdata('id_tugastambahan');
			$this->load->model('Kesepakatan_model');
			$cek_pangkat = $this->Kesepakatan_model->cek_pangkat($id_tugastambahan);
			$sub_kalimat = substr($cek_pangkat['nama_jastruk'],0,5);
			if($sub_kalimat == "Wakil" || $sub_kalimat == "Dekan" || $id_tugastambahan == 1 || 
				$id_tugastambahan == 4 || $id_tugastambahan == 353 || $id_tugastambahan == 369 ||
				$id_tugastambahan == 370 || $id_tugastambahan == 375 || $id_tugastambahan == 374 ||
				$user_jenis == 1 || $this->session->userdata('nama_jastruk') == "Kepala Bagian Tata Usaha")
			{
				$data['permohonan'] = 1;
				$data['kesepahaman'] = 1;
				$data['kesepakatan'] = 1;
			}else 
			{
				$cekPICPJ = $this->Home_model->cekPICPJ($idUser);
				if(count($cekPICPJ) >= 1)
				{
					$data['kesepakatan'] = 1;
				}else 
				{
					$data['kesepakatan'] = 0;			
					$data['permohonan'] = 0;
					$data['kesepahaman'] = 0;
				}

			}


			// get menu laporan
			$cekPIC = $this->Home_model->cekPIC($idUser);
			if(count($cekPICPJ) >= 1)
			{
				$data['laporan'] = 1;
			}else 
			{
				$data['laporan'] = 0;			
			}

			$getUnit = $this->Home_model->getUnit($id_tugastambahan);
			if($getUnit['kode_unit'] == 76 || $getUnit['kode_unit'] == 92 )
			{
				$data['jenis'] = 1;
				$data['master'] = 1;
				$data['permohonan'] = 1;
				$data['kesepahaman'] = 1;
				$data['laporan'] = 1;
				$data['kesepakatan'] = 1;
			}else 
			{
				$data['master'] = 0;
				$data['jenis'] = 0;
			}
		}
		return $data;
	}
	public function hakAksesMenu($menu){
		
	}
	public function notfound(){
		$this->load->view('404');
	}
	
}