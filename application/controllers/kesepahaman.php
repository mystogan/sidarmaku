<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kesepahaman extends CI_Controller {
	function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('Kesepahaman_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		$menu = $this->session->userdata('menu');
		// 
		if ($menu['kesepahaman'] == 0 || $menu['kesepahaman'] == '' ) {
			// echo "<script>alert ('Menu Laporan tidak untuk diakses untuk user ini');
			// window.location.href = '".base_url()."home/notfound';</script>";
			header("location:".base_url()."home/notfound");

		}
	}

	public function index()
	{
		// get session dan disimpan dalam variabel
		$id_tugastambahan = $this->session->userdata('id_tugastambahan');
		$user_id = $this->session->userdata('user_id');
		$user_jenis = $this->session->userdata('user_jenis');
		// menangkap post year dan internal post
		$data['yearPost'] = $this->input->post('yearPost');
		$data['internalPost'] = $this->input->post('internalPost');
		// kondisi dimana ketika tidak ada yang dipost maka akan didefault karena akan dikirim ke parameter  
		if($data['yearPost'] == null)
		{
			$data['yearPost'] = date('Y');
		}
		if($data['internalPost'] == null)
		{
			$data['internalPost'] = 1;
		}
		// user_jenis 1 untuk external dan user_jenis 2 untuk internal
		if($user_jenis == 1 ){
			// default untuk parameter external
			$data['internalPost'] = 2;
			// get models kesepahaman external
			$data['kesepahaman'] = $this->Kesepahaman_model->getkesepahaman_ex($user_id,$data['yearPost'],$data['internalPost']);		
			$form = 'ex_index';
		}else if($user_jenis == 2){ // kondisi untuk internal karena user_jenis 2
			// kondisi dimana untuk Kabag Fakultas contohnya disaintek adlah bu hasmi
			if($this->session->userdata('nama_jastruk') == "Kepala Bagian Tata Usaha"){
				// get kesepahaman internal pada kabag ada 3 parameter kode unit, tahun, dan internal
				$data['kesepahaman'] = $this->Kesepahaman_model->getkesepahaman_in($this->session->userdata('id_kode_unit'),$data['yearPost'],$data['internalPost']);				
				// get wakil dekan untuk if else ditampilan  karena setiap fakultas mempunyai wakil dekan yang tidak sama 
				$data['wadek3'] = $this->Kesepahaman_model->get_jastrukwadek3($this->session->userdata('id_kode_unit'));	
				// get dekan untuk if else ketika ditampilkan di view 
				$data['dekan'] = $this->Kesepahaman_model->get_jastrukdekan($this->session->userdata('id_kode_unit'));				
								
				$form = 'in_index';
			// khusus untuk jastruk Pengadministrasi pada kerjasama , sbg input kerja sama dengan bagian yang lain
			}else if($id_tugastambahan == 374){ // khusus untuk jastruk Pengadministrasi pada kerjasama
				// model khusu yang digunakan untuk menampilkan semua permintaan dari masing-masing
				// 			bagian atau unit dalam kampur uin sunan ampel surabaya
				$data['kesepahaman'] = $this->Kesepahaman_model->getkesepahaman_rek($data['yearPost']);
				// print_r ($data['kesepahaman']);
				$form = 'in_index_rektorat';
			}else {
				// kondisi terakhir adalah untuk pejabat wadek, dekan, rektor 
				// get dibawah untuk mengambil disposisi masing-masing pejabat
				$data['disposisi'] = $this->Kesepahaman_model->getdisposisi($id_tugastambahan,$data['yearPost'],$data['internalPost']);
				$j = 0;
				// perulangan ini digunakan untuk mendapatkan disposisi terakhir ketika ada yang digagalkan pengajuannya  
				foreach($data['disposisi'] as $value){
					// get disposisi terakhir dan join ke tabel m_jastruk di simpeg karena untuk mengambil 
					// 			nama jastruk dari tabel jastruk digunakan untuk if else di view 
					$data['paksa'][$j++] = $this->Kesepahaman_model->getDisposisiLast($value['sph_id']);
				}
				$form = 'ex_uin';
			}
			
		}else {
			// redirect ketika sessionnya hilang atau habis
			header("location:".base_url()."login");
		}		
		$this->load->view('base/header',$data);
		$this->load->view('kesepahaman/'.$form,$data);
		$this->load->view('base/footer',$data);
		
	}
	
	// fungsi digunakan untuk add dan update kesepahaman external
	public function add($id = 0)
	{
		// deklarasi model jenis model digunakan untuk get master jenis
		$this->load->model('Jenis_model');
		if($id != 0){
			// digunakan ketika edit akan diselect berdasarkan id permohonan
			$data['detilpermohonan'] = $this->Kesepahaman_model->getDetPermohonan($id);
		}
		// get untuk mendapatkan seluruh unit dalam kampus
		$data['unit'] = $this->Kesepahaman_model->getunit();
		
		// get seluruh pegawai di uin sunan ampel surabaya
		$data['pegawai'] = $this->Kesepahaman_model->getAllPegawai();
		// get model untuk mengambil kesepahaman
		$data['kesepahaman'] = $this->Kesepahaman_model->getDetilKesepahaman($id);
		// get model permohonan untuk dropdown
		$data['permohonan'] = $this->Kesepahaman_model->getpermohonan($id);
		$this->load->view('base/header',$data);
		$this->load->view('kesepahaman/ex_add',$data);
		$this->load->view('base/footer',$data);
	}

	// fungsi digunakan untuk menghapus kesepahaman
	public function setpermohonan($prm_id,$dis_id)
	{
		// cek jika parameter tidak ada akan diredirect kembali ke kesepahaman
		if($prm_id != null)
		{
			// untuk pertama update disposisi terakhir karena pejabat terakhir yang membatalkan
			// tau terakhir dari di_id karena yang dikirim adalah dis_id terakhir
			$datas['dis_status'] = 3;
			// model update disposisi
			$this->Kesepahaman_model->update_disposisi($datas,$dis_id);
			// kemudian update pada kesepahaman dengan status 3 karena untuk memastikan bahwa kesepahaman tersebut dibatalkan
			$data['sph_status'] = 3;
			// model update kesepahaman
			$this->Kesepahaman_model->update_kesepahaman($data,$prm_id);
			echo "<script>alert ('Pembatalan Kesepahaman Selesai');
			window.location.href = '".base_url()."kesepahaman';</script>";
		}else 
		{
			echo "<script>alert ('Failed Silahkan Ulangi Kembali');
			window.location.href = '".base_url()."kesepahaman';</script>";
		}
	}
	
	// fungsi yang untuk add dan update kesepahaman internal
	public function in_add($id = 0)
	{
		// deklarasi model jenis model digunakan untuk get master jenis
		$this->load->model('Jenis_model');
		if($id != 0){
			// digunakan ketika edit akan diselect berdasarkan id kesepahaman
			// $data['detilkesepahaman'] = $this->Kesepahaman_model->getDetPermohonan($id);
		}
		// kondisi untuk pegawai input 
		if($this->session->userdata('id_tugastambahan') == 374){
			$form = 'in_add_rektorat';
		}else {
			$form = 'in_add';
		} 
		// get seluruh pegawai di uin sunan ampel surabaya
		$data['pegawai'] = $this->Kesepahaman_model->getAllPegawai();
		// get model untuk mengambil mitra 
		$data['mitra'] = $this->Kesepahaman_model->getmitra();
		// get untuk mendapatkan unit orangnya 
		$data['units'] = $this->Kesepahaman_model->getunit();
		$data['unit'] = $this->Kesepahaman_model->getunit_single($this->session->userdata('user_username'));
		// get model untuk mengambil kesepahaman
		$data['kesepahaman'] = $this->Kesepahaman_model->getDetilKesepahaman($id);
		// get model permohonan untuk dropdown
		$data['permohonan'] = $this->Kesepahaman_model->getpermohonanFix();
		// print_r ($data['kesepahaman']);
		$this->load->view('base/header',$data);
		$this->load->view('kesepahaman/'.$form,$data);
		$this->load->view('base/footer',$data);
	}
	
	// fungsi yang digunakan untuk menambahkan dan mengupdate Kesepahaman external
	public function addKesepahaman_mitra($id = 0)
	{
		$data['sph_id'] = $this->input->post('sph_id'); // cek untuk id update 
		$data['mtr_id'] = $this->session->userdata('user_id'); // cek session untuk mitra
		$data['prm_id'] = $this->input->post('prm_id'); //id untuk permohonan
		$data['sph_nomor'] = $this->input->post('sph_nomor'); // id untuk nomor
		$data['sph_judul'] = $this->input->post('sph_judul');  
		$data['sph_tanggal'] = $this->input->post('sph_tanggal'); 
		$data['sph_namapihak1'] = $this->input->post('sph_namapihak1'); 
		$data['sph_jabatanpihak1'] = $this->input->post('sph_jabatanpihak1'); 
		$data['sph_skpihak1'] = $this->input->post('sph_skpihak1'); 
		$data['sph_pegawaipihak1'] = $this->input->post('sph_pegawaipihak1'); 
		$data['sph_namapihak2'] = $this->input->post('sph_namapihak2'); 
		$data['sph_jabatanpihak2'] = $this->input->post('sph_jabatanpihak2'); 
		$data['sph_skpihak2'] = $this->input->post('sph_skpihak2'); 
		$data['sph_tujuan'] = $this->input->post('sph_tujuan'); 
		$data['sph_mulai'] = $this->input->post('sph_mulai'); 
		$data['sph_akhir'] = $this->input->post('sph_akhir'); 
		$data['sph_internal'] = $this->input->post('sph_internal'); // set internal 1 
		$data['sph_status'] = "1"; // set 
				
		// berfungsi untuk get format file
		$file_upload = $_FILES["sph_file"]["name"];
		if($file_upload != "" || $file_upload != null){
			// memanggil fungsi untuk looping 
			$terima = $this->loop("sph_file");
		}
		// menyimpan nama file untuk disimpan
		$data['sph_file'] = $terima['nama'];
		
		if($terima['cek'] == 0 ){
			// cek ketika error dan menampilkan pesan error
			echo "<script> alert ('asd ".$terima['pesan']."')</script>";
		}else {
			// if untuk mengecek update atau insert 
			if($data['sph_id'] == ""){
				// untuk mnyimpan kesepahaman 
				$id_permohonan = $this->Kesepahaman_model->add_kesepahaman($data);
				// untuk mendapatkan id pegawai dan id_tugastambahan pada rektor dan warek 3
				$id_pegawai = $this->Kesepahaman_model->cek_rek_wa3();
				$disposisi['dis_jenis'] = 2; // 2 karena kesepahaman 2
				$disposisi['dis_entitas'] = $id_permohonan; // 
				$disposisi['dis_pegawaiasal'] = $id_pegawai[0]->id; // 
				$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; // 
				$disposisi['dis_pegawaitujuan'] = $id_pegawai[1]->id; // 
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; // 
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; // 
				$disposisi['dis_status'] = 1; 
				// insert disposisi rektor dan warek 3
				$id_disposisi = $this->Kesepahaman_model->add_disposisi($disposisi);
				 
			}else {
				$this->Kesepahaman_model->update_kesepahaman($data,$data['sph_id']);
			}
			
		}
		echo "<script>window.location.href = '".base_url()."kesepahaman';</script>";
		
	}
	
	// fungsi yang digunakan untuk menambahkan dan mengupdate Kesepahaman internal dan external
	// khusus untuk kesepahaman input external dan internal sama
	public function addKesepahaman_unit($id = 0)
	{
		$data['sph_id'] = $this->input->post('sph_id'); // cek untuk id update 
		$data['mtr_id'] = $this->input->post('mtr_id');
		$data['prm_id'] = $this->input->post('prm_id'); //id untuk permohonan
		$data['sph_nomor'] = $this->input->post('sph_nomor'); // id untuk nomor
		$data['sph_judul'] = $this->input->post('sph_judul');  
		$data['sph_tanggal'] = $this->input->post('sph_tanggal'); 
		$data['sph_namapihak1'] = $this->input->post('sph_namapihak1'); 
		$data['sph_jabatanpihak1'] = $this->input->post('sph_jabatanpihak1'); 
		$data['sph_skpihak1'] = $this->input->post('sph_skpihak1'); 
		$data['sph_pegawaipihak1'] = $this->input->post('sph_pegawaipihak1'); 
		$data['sph_namapihak2'] = $this->input->post('sph_namapihak2'); 
		$data['sph_jabatanpihak2'] = $this->input->post('sph_jabatanpihak2'); 
		$data['sph_skpihak2'] = $this->input->post('sph_skpihak2'); 
		$data['sph_tujuan'] = $this->input->post('sph_tujuan'); 
		$data['sph_mulai'] = $this->input->post('sph_mulai'); 
		$data['sph_akhir'] = $this->input->post('sph_akhir'); 
		$data['sph_internal'] = $this->input->post('sph_internal'); // set internal 1 
		$data['sph_status'] = "1"; // set 
		
		$unit = $this->input->post('sph_unit');
		
		// berfungsi untuk get format file
		$file_upload = $_FILES["sph_file"]["name"];
		if($file_upload != "" || $file_upload != null){
			// memanggil fungsi untuk looping 
			$terima = $this->loop("sph_file");
		}
		// untuk menyimpan pada array dan yang akan disimpan
		$data['sph_file'] = $terima['nama'];
		if($terima['cek'] == 0 ){
			// cek ketika error dan menampilkan pesan error
			echo "<script> alert ('".$terima['pesan']."')</script>";
		}else {
			// if untuk mengecek update atau insert 
			if($data['sph_id'] == ""){
				// untuk mnyimpan kesepahaman 
				$id_kesepahaman = $this->Kesepahaman_model->add_kesepahaman($data);
				// untuk mendapatkan id pegawai dan id_tugastambahan wadek 3 dan dekan 
				$id_pegawai = $this->Kesepahaman_model->cek_wadek3_dekan($unit);
				
				$disposisi['dis_jenis'] = 2; // 1 karena kesepahaman 1
				$disposisi['dis_entitas'] = $id_kesepahaman; // 
				$disposisi['dis_pegawaiasal'] = $id_pegawai[0]->id; // 
				$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; // 
				$disposisi['dis_pegawaitujuan'] = $id_pegawai[1]->id; // 
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; // 
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; // 
				$disposisi['dis_status'] = 1; 
				print_r ($id_pegawai);
				// insert disposisi wadek 3 dan dekan 
				$id_disposisi = $this->Kesepahaman_model->add_disposisi($disposisi);
				 
			}else {
				// update permohonan untuk mengedit permohonan
				$this->Kesepahaman_model->update_kesepahaman($data,$data['sph_id']);
			}
			
		}
		echo "<script>window.location.href = '".base_url()."kesepahaman';</script>";
		
	}

	// fungsi yang digunakan untuk menambahkan kesepahaman rektorat
	public function addKesepahaman_rektorat($id = 0)
	{
		$sph_id = $this->input->post('sph_id'); // cek untuk id update 
		$data['mtr_id'] = $this->input->post('mtr_id');
		$data['prm_id'] = $this->input->post('prm_id'); //id untuk permohonan
		$data['sph_nomor'] = $this->input->post('sph_nomor'); // id untuk nomor
		$data['sph_judul'] = $this->input->post('sph_judul');  
		$data['sph_tanggal'] = $this->input->post('sph_tanggal'); 
		$data['sph_namapihak1'] = $this->input->post('sph_namapihak1'); 
		$data['sph_jabatanpihak1'] = $this->input->post('sph_jabatanpihak1'); 
		$data['sph_skpihak1'] = $this->input->post('sph_skpihak1'); 
		$data['sph_pegawaipihak1'] = $this->input->post('sph_pegawaipihak1'); 
		$data['sph_namapihak2'] = $this->input->post('sph_namapihak2'); 
		$data['sph_jabatanpihak2'] = $this->input->post('sph_jabatanpihak2'); 
		$data['sph_skpihak2'] = $this->input->post('sph_skpihak2'); 
		$data['sph_tujuan'] = $this->input->post('sph_tujuan'); 
		$data['sph_mulai'] = $this->input->post('sph_mulai'); 
		$data['sph_akhir'] = $this->input->post('sph_akhir'); 
		$data['sph_internal'] = $this->input->post('sph_internal'); // set internal 1 
		$data['sph_status'] = "1"; // set 
		
		// berfungsi untuk get format file
		$file_upload = $_FILES["sph_file"]["name"];
		if($file_upload != "" || $file_upload != null){
			// memanggil fungsi untuk looping 
			$terima = $this->loop("sph_file");
		}
		// untuk menyimpan pada array dan yang akan disimpan
		$data['sph_file'] = $terima['nama'];
		
		if($terima['cek'] == 0  ){
			// cek ketika error dan menampilkan pesan error
			echo "<script> alert ('".$terima['error']."')</script>";
		}else {
			// if untuk mengecek update atau insert 
			if($sph_id == ""){
				$id_permohonan = $this->Kesepahaman_model->add_kesepahaman($data);
				// untuk mendapatkan id pegawai dan id_tugastambahan warek 3 dan rektor  
				$id_pegawai = $this->Kesepahaman_model->cek_disposisi_in(4,1);
				$disposisi['dis_jenis'] = 2; // 1 karena permohonan 1
				$disposisi['dis_entitas'] = $id_permohonan; // 
				$disposisi['dis_pegawaiasal'] = $id_pegawai[0]->id; // 
				$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; // 
				$disposisi['dis_pegawaitujuan'] = $id_pegawai[1]->id; // 
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan;  
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; 
				$disposisi['dis_status'] = 1; 
				
				// insert disposisi wadek 3 dan dekan 
				$id_disposisi = $this->Kesepahaman_model->add_disposisi($disposisi);
				 
			}else {
				$this->Kesepahaman_model->update_kesepahaman($data,$sph_id);
			}
			
		}
		echo "<script>window.location.href = '".base_url()."kesepahaman';</script>";
		
	}

	// fungsi yang digunakan untuk proses upload menggunakan plugins codeigniter
	function loop($nama)
	{
		
		$config['upload_path'] = './assets/upload/';
		$config['allowed_types'] = 'gif|jpg|png|exe|xls|doc|docx|xlsx|jpeg';
		$config['max_size']      = '10000000'; 
		$config['remove_spaces']=TRUE;  //it will remove all spaces
		$config['encrypt_name']=TRUE;   //it wil encrypte the original file name
		$this->load->library('upload', $config);
  
		if (!$this->upload->do_upload($nama))
		{
			// set variabel error pesan 
			$pesan = $this->upload->display_errors();
			$cek = 0;
		}
		else
		{
			// proses upload file dan akan disimpan pada file nama untuk nama file
		   $data = $this->upload->data();
		   $pesan = "Upload Sukses";
		   $nama = $data['file_name'];
		   $cek = 1;
		}
		$datas['cek'] = $cek ;
		$datas['pesan'] = $pesan ;
		$datas['nama'] = $nama ;
		// mengembalikan nilai cek,pesan dan nama untuk dicek selanjutnya
		return $datas;
	}
	
	// fungsi yang digunakan untuk menampilkan catatan pada internal dan external akan tetapi dipisahkan 
	// 			dengan kondisi 
	public function catatan($dis_id = 0)
	{
		// get session id_tugastambahan
		$id_tugastambahan = $this->session->userdata('id_tugastambahan');
		// get disposisi detail pada id disposisi
		$data['detail'] = $this->Kesepahaman_model->getdisposisiDetail($dis_id);
		// get semua disposisi pada permohonan untuk mendapatkan catatan 
		$data['catatan'] = $this->Kesepahaman_model->get_dis_catatan($data['detail']['sph_id']);	
		$data['dis_id'] = $dis_id;
		// kondisi dimana untuk membedakan tampilan antara external dan internal
		if($data['detail']['sph_internal'] == 3){		
			$view = 'in_catatan_rektorat';
		}else if($data['detail']['sph_internal'] == 1){
			$view = 'in_catatan';
		}else {
			$view = 'ex_catatan';
		}
		$this->load->view('base/header',$data);
		$this->load->view('kesepahaman/'.$view,$data);
		$this->load->view('base/footer',$data);
	}

	// fungsi yang digunakan utnuk update catatan internal pada pejabat 
	public function update_catatan_internal()
	{
		date_default_timezone_set('Asia/Jakarta');
		// ini kondisi untuk ketika inputan yang dimasukkan adalah kosong atau null dan menghapus spasi
		if (trim($this->input->post('dis_catatan')) == '' || trim($this->input->post('dis_catatan')) == null) {
			$data['dis_catatan'] = "Catatan yang dikirim sebelumnya kosong :)";
		}else {
			$data['dis_catatan'] = trim($this->input->post('dis_catatan'));
		}
		$prm_id = $this->input->post('sph_id');
		$dis_id = $this->input->post('dis_id');
		$data['dis_status'] = '2'; // set status karena sudah didisposisi 
		$data['dis_updated'] = date('Y-m-d H:i:s'); 
		$data_permohonan['sph_status'] = '2'; // set status kesepahaman sudah didisposisi set 2
		$id_tugastambahan = $this->session->userdata('id_tugastambahan');
		
		// untuk mengecek apakah dia wadek atau dekan 
		$cek_pangkat = $this->Kesepahaman_model->cek_pangkat($id_tugastambahan);
		// query dibawah untuk mengecek apakah warek 3 disposisi ke 1 atau yang ke 2
		$count_kesepahaman = $this->Kesepahaman_model->count_kesepahaman($prm_id);
		
		// fungsi substring digunakan untuk mendapatkan kata wakil dan dekan
		$sub_kalimat = substr($cek_pangkat['nama_jastruk'],0,5);
		// kondisi ini ada 3 yang harus dilewati digunakan untuk mengecek warek 3 apakah disposisi 
		// 		pertama atau kedua karena ketika yang input adalah kasubag kerjasama 
		// pertama adalah id_tugas tambahan 4 yaitu wakil rektor 3
		// kedua unit yang dituju
		// ketiga adalah jumlah permohonan 
		if($id_tugastambahan == 4 && $this->input->post('sph_internal') == 3 
			&& count($count_kesepahaman) == 1){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Kesepahaman_model->update_disposisi($data,$dis_id);
			// model yang dipanggil adalah untuk mengupdate kesepahaman berupa catatan dan status
			$this->Kesepahaman_model->update_kesepahaman($data_permohonan,$prm_id);
			/////////// insert warek 3 dan rektor 
			$id_pegawai = $this->Kesepahaman_model->cek_disposisi_in(1, 4);
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; // 

		// kondisi kedua adalah untuk ketika disposisi rektor
		}else if($id_tugastambahan == 1){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Kesepahaman_model->update_disposisi($data,$dis_id);
			// model yang dipanggil adalah untuk mengupdate kesepahaman berupa catatan dan status
			$this->Kesepahaman_model->update_kesepahaman($data_permohonan,$prm_id);			
			///////////kondisi dimana acc disposisi untuk warek 3 ke biro
			$id_pegawai = $this->Kesepahaman_model->cek_wa3_biro();
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; // 

		// kondisi selanjutnya adalah untuk wakil rektor 3
		}else if($id_tugastambahan == 4){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Kesepahaman_model->update_disposisi($data,$dis_id);
			/////////// insert biro dan kabag 
			$id_pegawai = $this->Kesepahaman_model->cek_disposisi(353, 369);
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; // 

		// kondisi selanjutnya adalah digunakan untuk biro AAK
		}else if($id_tugastambahan == 353){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Kesepahaman_model->update_disposisi($data,$dis_id);
			///////////insert kabag dan kasubag
			$id_pegawai = $this->Kesepahaman_model->cek_disposisi(369, 370);
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; // 

		// kondisi dimana untuk kepala bagian kerjasama
		}else if($id_tugastambahan == 369){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Kesepahaman_model->update_disposisi($data,$dis_id);
			// insert kasubag ke pegawai
			$id_pegawai = $this->Kesepahaman_model->cek_disposisi(370, 375);
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; // 

		// kondisi dimana untuk kepala sub bagian
		}else if($id_tugastambahan == 370){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Kesepahaman_model->update_disposisi($data,$dis_id);
			// insert pegawai to pegawai kerjasama
			$id_pegawai = $this->Kesepahaman_model->cek_disposisi(375, 375);
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; // 

		// kondisi dimana digunakan untuk wakil dekan masing-masing fakultas
		}else if($sub_kalimat == "Wakil"){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Kesepahaman_model->update_disposisi($data,$dis_id);
			// model yang dipanggil adalah untuk mengupdate permohonan berupa catatan dan status
			$this->Kesepahaman_model->update_kesepahaman($data_permohonan,$prm_id);	
			// kondisi dimana insert dekan dan rektor 
			$id_pegawai = $this->Kesepahaman_model->cek_disposisi_in($cek_pangkat['id_pimpinan'], 1);
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; // 

		// kondisi dimana digunakan untuk dekan masing-masing fakultas
		}else if($sub_kalimat == "Dekan"){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Kesepahaman_model->update_disposisi($data,$dis_id);
			// model yang dipanggil adalah untuk mengupdate permohonan berupa catatan dan status
			$this->Kesepahaman_model->update_kesepahaman($data_permohonan,$prm_id);			
			///////////kondisi dimana acc disposisi untuk warek 3 ke biro
			$id_pegawai = $this->Kesepahaman_model->cek_disposisi_in(1, 4);
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; // 

		}
		
		// proses menyimpan ke array untuk menyimpan keselanjutnya 
		$disposisi['dis_jenis'] = 2; // 1 karena kesepahaman adalah 1
		$disposisi['dis_entitas'] = $prm_id; 
		$disposisi['dis_pegawaiasal'] = $id_pegawai[0]->id; // 
		$disposisi['dis_pegawaitujuan'] = $id_pegawai[1]->id; // 
		$disposisi['dis_status'] = 1; // 1 karena insert dan belum didisposisi
		
		///////cek disposisi apakah sudah ada atau tidak jika belum ada isi kalau sudah ada tidak perlu di isi 
		$cek_disposisi = $this->Kesepahaman_model->cek_disposisi2($disposisi);
		if($cek_disposisi['dis_id'] == ''){
			$id_disposisi = $this->Kesepahaman_model->add_disposisi($disposisi);			
		}
		
		echo "<script>window.location.href = '".base_url()."kesepahaman';</script>";
		
		
	}

	// fungsi yang digunakan utnuk update catatan external pada pejabat 	
	public function update_catatan()
	{
		date_default_timezone_set('Asia/Jakarta');
		// ini kondisi untuk ketika inputan yang dimasukkan adalah kosong atau null dan menghapus spasi
		if (trim($this->input->post('dis_catatan')) == '' || trim($this->input->post('dis_catatan')) == null) {
			$data['dis_catatan'] = "Catatan yang dikirim sebelumnya kosong :)";
		}else {
			$data['dis_catatan'] = trim($this->input->post('dis_catatan'));
		}
		$prm_id = $this->input->post('prm_id');
		$dis_id = $this->input->post('dis_id');
		$data['dis_status'] = '2'; // set status disposisi sudah didisposisi set 2
		$data['dis_updated'] = date('Y-m-d H:i:s');
		$data_permohonan['prm_status'] = '2'; // set status permohonan sudah didisposisi set 2
		
		$id_tugastambahan = $this->session->userdata('id_tugastambahan');
		
		// kondisi kedua adalah untuk ketika disposisi rektor
		if($id_tugastambahan == 1){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			// model yang dipanggil adalah untuk mengupdate permohonan berupa catatan dan status
			$this->Permohonan_model->update_kesepahaman($data_permohonan,$prm_id);			
			// kondisi dimana acc disposisi untuk warek 3 ke biro
			$id_pegawai = $this->Permohonan_model->cek_wa3_biro();
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; // 

		// kondisi selanjutnya adalah untuk wakil rektor 3
		}else if($id_tugastambahan == 4){			
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			// insert biro dan kabag 
			$id_pegawai = $this->Permohonan_model->cek_disposisi(353, 369);
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; // 

		// kondisi selanjutnya adalah digunakan untuk biro AAK
		}else if($id_tugastambahan == 353){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			// insert kabag dan kasubag
			$id_pegawai = $this->Permohonan_model->cek_disposisi(369, 370);
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; // 

		// kondisi dimana untuk kepala bagian kerjasama
		}else if($id_tugastambahan == 369){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			// insert kasubag ke pegawai 
			$id_pegawai = $this->Permohonan_model->cek_disposisi(370, 375);
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; // 

		// kondisi dimana untuk kepala sub bagian
		}else if($id_tugastambahan == 370){
			
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			// insert pegawai to pegawai
			$id_pegawai = $this->Permohonan_model->cek_disposisi(375, 375);
			// dibawah ini adalah mengeset untuk inputan disposisi selanjutnya
			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; // 
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; // 

		}
		
		// proses menyimpan ke array untuk menyimpan keselanjutnya 		
		$disposisi['dis_jenis'] = 1; // 1 karena permohonan 1
		$disposisi['dis_entitas'] = $prm_id; // 
		$disposisi['dis_pegawaiasal'] = $id_pegawai[0]->id; // 
		$disposisi['dis_pegawaitujuan'] = $id_pegawai[1]->id; // 
		$disposisi['dis_status'] = 1; 
		
		// cek disposisi apakah sudah ada atau tidak jika belum ada isi kalau sudah ada tidak perlu di isi 
		$cek_disposisi = $this->Permohonan_model->cek_disposisi2($disposisi);
		if($cek_disposisi['dis_id'] == ''){
			$id_disposisi = $this->Permohonan_model->add_disposisi($disposisi);			
		}
		
		
		echo "<script>window.location.href = '".base_url()."permohonan';</script>";
	}

	// fungsi yang digunakan untuk menampilkan detail untuk melihat detail inputan
	public function detail($dis_id = 0)
	{
		// model yang digunakan untuk mengambil disposisi detail
		$data['detail'] = $this->Kesepahaman_model->getdisposisiDetail($dis_id);
		//mengambil nama unit atau fakultas yang akan digunakan untuk ditampilkan 
		$data['unit'] = $this->Kesepahaman_model->getFakultas($data['detail']['prm_unit']);
		// print_r ($data['unit']);
		$this->load->view('base/header',$data);
		$this->load->view('kesepahaman/ex_detail',$data);
		$this->load->view('base/footer',$data);
	}
	
}
