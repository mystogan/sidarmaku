<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Coba extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('session');
		
		$this->load->model('Jenis_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		$user = $this->session->userdata('user_jenis');
		if ($user == '') {
			header("location:".base_url()."login");
		}
	}

	public function index(){
		
		// // print_r($data['dataMoA']);
		$this->load->view('base/header');
		// // $this->load->view('kesepakatan/daftarmoa',$data);
		$this->load->view('base/footer');
	}

	
}
