<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_mitra extends CI_Controller {
	function __construct(){
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('Mitra_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		// kondisi untuk mengecek session untuk ridirect ke login
		$menu = $this->session->userdata('menu');
		// print_r ($menu);
		if ($menu['master'] == 0) {
			// echo "<script>alert ('Menu Mitra tidak untuk diakses untuk user ini');
			// window.location.href = '".base_url()."home';</script>";
			header("location:".base_url()."home/notfound");

		}

	}
	public function index(){
		$data['mitra'] = $this->Mitra_model->getMitra();
		$this->load->view('base/header',$data);
		$this->load->view('mitra/index',$data);
		$this->load->view('base/footer',$data);
	}
	public function add($id = 0){
		if($id != 0){
			$data['detilmitra'] = $this->Mitra_model->getDetMitra($id);
		}
		$data['mitra'] = $this->Mitra_model->getMitra();
		$data['prov'] = $this->Mitra_model->getProvinsi();
		// print_r ($data['detilmitra']);
		$this->load->view('base/header',$data);
		$this->load->view('mitra/add',$data);
		$this->load->view('base/footer',$data);
	}
	public function pilih_kabupaten($id_profinsi = 0){
		$kab = $this->Mitra_model->getKabupaten($id_profinsi);
		// print_r ($kab);
		foreach($kab as $hasil){
			echo "<option value = '".$hasil['id_kabupaten']."'>".$hasil['Nama_kab']."</option>";
		}
		
	}
	public function pilih_kecamatan($id_kota = 0){
		$kab = $this->Mitra_model->getKecamatan($id_kota);
		foreach($kab as $hasil){
			echo "<option value = '".$hasil['id_kecamatan']."'>".$hasil['Nama_kec']."</option>";
		}
		
	}
	public function reset_kecamatan(){
		echo "<option>Silahkan Pilih Kabupaten</option>";
		
	}
	public function save(){
		$data['mtr_id'] = trim($this->input->post('mtr_id'));
		$data['mtr_parent'] = trim($this->input->post('mtr_parent'));
		$data['mtr_namainstansi'] = trim($this->input->post('mtr_namainstansi'));
		$data['mtr_alamat'] = trim($this->input->post('mtr_alamat'));
		$data['mtr_propinsi'] = trim($this->input->post('mtr_propinsi'));
		$data['mtr_kota'] = trim($this->input->post('mtr_kota'));
		$data['mtr_kecamatan'] = trim($this->input->post('mtr_kecamatan'));
		$data['mtr_telepon'] = trim($this->input->post('mtr_telepon'));
		$data['mtr_handphone'] = trim($this->input->post('mtr_handphone'));
		$data['mtr_email'] = trim($this->input->post('mtr_email'));
		$data['mtr_username'] = trim($this->input->post('mtr_username'));
		$data['mtr_password'] = md5($this->input->post('mtr_namainstansi'));
		
		// print_r ($data);
		if($data['mtr_id'] == ''){
			$this->Mitra_model->save($data);			
		}else {
			$this->Mitra_model->update($data,$data['mtr_id']);	
		}
		header("location:".base_url()."m_mitra");
	}
	
	
	
}
