<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_jenis extends CI_Controller {
	function __construct(){
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('Jenis_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		// kondisi untuk mengecek session untuk ridirect ke login
		$menu = $this->session->userdata('menu');
		// print_r ($menu);
		if ($menu['master'] == 0) {
			// echo "<script>alert ('Menu Jenis tidak untuk diakses untuk user ini');
			// window.location.href = '".base_url()."home';</script>";
			header("location:".base_url()."home/notfound");

		}
	}
	public function index(){
		$data['jenis'] = $this->Jenis_model->getJenis();
		$this->load->view('base/header',$data);
		$this->load->view('jenis/index',$data);
		$this->load->view('base/footer',$data);
	}
	public function add($id = 0){
		if($id != 0){
			$data['detiljenis'] = $this->Jenis_model->getDetJenis($id);
		}
		print_r ($data['detilmitra']);
		$this->load->view('base/header',$data);
		$this->load->view('jenis/add',$data);
		$this->load->view('base/footer',$data);
	}
	public function save(){
		$data['jns_id'] = trim($this->input->post('jns_id'));
		$data['jns_kerjasama'] = trim($this->input->post('jns_kerjasama'));
		$data['jns_delete'] = 1;
		
		if($data['jns_id'] == ''){
			$this->Jenis_model->save($data);			
		}else {
			$this->Jenis_model->update($data,$data['jns_id']);	
		}
		header("location:".base_url()."m_jenis");
	}
	public function hapus($id_jenis){
		$data['jns_delete'] = 2;
		$this->Jenis_model->update($data,$id_jenis);	
		header("location:".base_url()."m_jenis");
	}
	
	
	
}
