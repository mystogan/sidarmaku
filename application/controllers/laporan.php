<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {
	function __construct(){
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('Laporan_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		// kondisi untuk mengecek session untuk ridirect ke login
		$menu = $this->session->userdata('menu');
		// print_r ($menu);
		if ($menu['laporan'] == 0 || $menu['laporan'] == '' ) {
			// echo "<script>alert ('Menu Laporan tidak untuk diakses untuk user ini');
			// window.location.href = '".base_url()."home/notfound';</script>";
			header("location:".base_url()."home/notfound");

		}

	}
	public function index()
	{
		// berfungsi untuk get session masing-masing nama
		$id_tugastambahan = $this->session->userdata('id_tugastambahan');
		$this->session->userdata('id_tugastambahan');
		$user_id = $this->session->userdata('user_id');
		$user_jenis = $this->session->userdata('user_jenis');
		$getPJ_id = $this->Laporan_model->getPJ_id($user_id);
		// kondisi yang digunakan untuk memilah berdasarkan jabatan yang akan digunakan
        //      dalam hal ini adalah 4 untuk warek 3, 353 untuk pak biro, 369 untuk kabag kerjasama
        //      370 untuk kasubag kerjasama dan yang 375 untuk petugas kerjasama yang id jastruk
		if($id_tugastambahan == '4' || $id_tugastambahan == '353'
			|| $id_tugastambahan == '369' || $id_tugastambahan == '370'
			|| $id_tugastambahan == '375' || $getPJ_id['sum'] > '0'){
			// if pertama digunakan untuk warek 3 = 4, 353 biro aak, 369 = kabag kerjasama, 370 = kasubag kerjasama, 375 = jastruk kerjasama
			$form = 'disposisi';
			// get untuk laporan disposisi
			$data['kesepakatan'] = $this->Laporan_model->getLaporanDisposisi($id_tugastambahan);
		}else {
			// else yang kedua adalah get kesepakatan
			$data['kesepakatan'] = $this->Laporan_model->getkesepakatan($user_id);
			$form = 'index';
		}
		// untuk view
		$this->load->view('base/header',$data);
		$this->load->view('laporan/'.$form,$data);
		$this->load->view('base/footer',$data);

	}

    // fungsi ini digunakan untuk melihat detail dari laporan itu sendiri
	public function detail($id_kesepakatan = 0)
	{
	    // get untuk mendapatkan data dari model laporan model data laporan
		$data['laporan'] = $this->Laporan_model->getlaporan($id_kesepakatan);
		// untuk menampilkan tampilan detail untuk laporan
		$this->load->view('base/header',$data);
		$this->load->view('laporan/detail',$data);
		$this->load->view('base/footer',$data);
	}

    // 	fungsi ini digunakan untuk melihat detail dari disposisi itu sendiri
	public function details($id_kesepakatan = 0)
	{
	    // get session untuk id yang login
		$user_id = $this->session->userdata('user_id');
        //  get id tugas tambahan yang digunakan sudah disimpan session pada saat login
		$id_tugastambahan = $this->session->userdata('id_tugastambahan');
        //  get data untuk disposisi idtugas tambahan dan idkesepakatan
		$data['laporan'] = $this->Laporan_model->getdisposisi($id_tugastambahan,$id_kesepakatan);
		//  kondisi yang digunakan utnuk memilah mana yang pic mana yang disposisi atau pejabat yang login
		if($data['laporan'][0]['spk_pegawaipic'] == $user_id){
			$view = 'detail';
		}else {
			$view = 'detail2';
		}
        //  untuk menampilkan disposisi
		$this->load->view('base/header',$data);
		$this->load->view('laporan/'.$view,$data);
		$this->load->view('base/footer',$data);
	}

    //  fungsi ini digunakan untuk menambahbahkan laporan atau mengedit laporan yang perlu dieidt
	public function add($id = 0)
	{

		$user_id = $this->session->userdata('user_id');
        //  kondisi ini digunakan untuk melihat apakah fungsi ini berfungsi untuk edit atau tambah
        //          id tidak ada maka akan ditampilkan form yang kosong dan jika ada idnya maka akan ditampilkan
        //          form yang sudah ada data yang akan diedit
		if($id != 0){
			$data['laporan'] = $this->Laporan_model->getlaporanDetail($id);
		}
        // 	get kesepakatan untuk memilih kesepakatan mana yang akan dipilih
		$data['kesepakatan'] = $this->Laporan_model->getAllkesepakatan($user_id);
		$this->load->view('base/header',$data);
		$this->load->view('laporan/add',$data);
		$this->load->view('base/footer',$data);
	}

    //   fungsi ini digunakan untuk menambahbahkan laporan atau mengedit laporan yang perlu dieidt
	public function add2($id = 0){
		$user_id = $this->session->userdata('user_id');
		//  kondisi ini digunakan untuk melihat apakah fungsi ini berfungsi untuk edit atau tambah
        //          id tidak ada maka akan ditampilkan form yang kosong dan jika ada idnya maka akan ditampilkan
        //          form yang sudah ada data yang akan diedit
		if($id != 0){
			$data['laporan'] = $this->Laporan_model->getlaporanDetail($id);
		}
		$data['kesepakatan'] = $this->Laporan_model->getAllkesepakatan();
		$this->load->view('base/header',$data);
		$this->load->view('laporan/add2',$data);
		$this->load->view('base/footer',$data);
	}

    // 	fungsi ini digunakan untuk menambahkan laporan kedalam database
	public function addlaporan($id = 0)
	{
        // dubawah ini terdapat fungsi-fungsi yang digunakan untuk menerima post data untuk menyimpan laporan
		$data['spk_id'] = $this->input->post('spk_id');
		$data['lak_jenis'] = $this->input->post('lak_jenis');
		$data['lak_periode'] = $this->input->post('lak_periode');
		$data['lak_anggaran'] = $this->input->post('lak_anggaran');
		$data['lak_tanggal'] = $this->input->post('lak_tanggal');
		$data['lak_pengesahan'] = $this->input->post('lak_pengesahan');
		$data['lak_penandatangan'] = $this->input->post('lak_penandatangan');
		$data['lak_latarbelakang'] = $this->input->post('lak_latarbelakang');
		$data['lak_tujuan'] = $this->input->post('lak_tujuan');
		$data['lak_program'] = $this->input->post('lak_program');
		$data['lak_pelaksanaan'] = $this->input->post('lak_pelaksanaan');
		$data['lak_hasil'] = $this->input->post('lak_hasil');
		$data['lak_rtl'] = $this->input->post('lak_rtl');
		$data['lak_kesimpulan'] = $this->input->post('lak_kesimpulan');
		$data['lak_saran'] = $this->input->post('lak_saran');

        // perintah yang digunakan utnuk menerima nama file laporan
		$file_upload = $_FILES["lak_filelaporan"]["name"];
        // 	kondisi yang digunakan untuk memilih apakah jadi upload atau tidak
		if($file_upload != "" || $file_upload != null){
			$terima = $this->loop($file_upload,"lak_filelaporan");
		}
		$data['lak_filelaporan'] = $terima['nama_file'];

		$file_upload = $_FILES["lak_filelampiran"]["name"];
		if($file_upload != "" || $file_upload != null){
			$terima = $this->loop($file_upload,"lak_filelampiran");
		}
		$data['lak_filelampiran'] = $terima['nama_file'];
		$this->load->model('Permohonan_model');

		// print_r ($data);


		if($this->input->post('lak_id') == ""){
			$getPJ = $this->Laporan_model->getPJ($data['spk_id']);
			$id_laporan = $this->Laporan_model->add_laporan($data);
			$id_pegawai = $this->Laporan_model->cek_disposisi_khusus($getPJ['spk_pegawaipihak1'], 4);
			// print_r ($id_pegawai);
			$disposisi['dis_jenis'] = 4; // 4 karena laporan 4
			$disposisi['dis_entitas'] = $id_laporan; //
			if($id_pegawai[0]->id_tugastambahan == 0){
				$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk;
			}else {
				$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan;
			}
			// kondisi yang sama akan tetapi untuk disposisi tujuan
			if($id_pegawai[1]->id_tugastambahan == 0){
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk;
			}else {
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan;
			}

			$disposisi['dis_pegawaiasal'] = $id_pegawai[0]->id; //
			$disposisi['dis_pegawaitujuan'] = $id_pegawai[1]->id; //
			$disposisi['dis_status'] = 1;
			$id_disposisi = $this->Laporan_model->add_disposisi($disposisi);

		}else {
			$lak_id = $this->input->post('lak_id'); // cek untuk id update
			$this->Laporan_model->update_laporan($data,$lak_id);
		}
		echo "<script>window.location.href = '".base_url()."laporan/detail/".$data['spk_id']."';</script>";

	}
	function loop($simpan,$nama){
		$sub = substr($simpan,-5);
		$string = uniqid().$sub;
		$target_dir = "assets/upload/";
		$target_file = $target_dir . $string;
		$data['cek'] = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

		if(isset($_POST["submit"])) {
			$check = getimagesize($_FILES[$nama]["tmp_name"]);
			if($check !== false) {
				$data['cek'] = 1;
			} else {
				$data['cek'] = 0;
			}
		}
		if($imageFileType != "jpg" && $imageFileType != "png" &&
	 			$imageFileType != "JPG" && $imageFileType != "PNG" &&
				$imageFileType != "jpeg" && $imageFileType != "JPEG" &&
				$imageFileType != "gif" && $imageFileType != "GIF" &&
				$imageFileType != "pdf" && $imageFileType != "PDF" &&
				$imageFileType != "docx" && $imageFileType != "DOCX" &&
				$imageFileType != "xlsx" && $imageFileType != "XLXS"
				) {
			$data['error'] = 'Format Harus JPG, JPEG, PNG & GIF !';
			$data['cek'] = 0;
		}
		if ($data['cek'] != 0) {
			if (move_uploaded_file($_FILES[$nama]["tmp_name"], $target_file)) {
				$data['nama_file'] = $string;
				$data['cek'] = 1;
			} else {
				$data['error'] = 'Ada Kesalahan Waktu Upload silahkan data diisi Kembali ';
				$data['cek'] = 0;
			}
		}
		return $data;
	}
	public function catatan($id_disposisi = 0){
		$this->load->model('Laporan_model');
		$id_tugastambahan = $this->session->userdata('id_tugastambahan');
		$hasil = $this->Laporan_model->getid_laporan($id_disposisi);
		$data['catatan'] = $this->Laporan_model->get_dis_catatan($hasil[0]['lak_id']);
		$data['dis_id'] = $id_disposisi;
		$data['detail'] = $hasil[0];
		// print_r ($data['catatan']);
		$this->load->view('base/header',$data);
		$this->load->view('laporan/catatan',$data);
		$this->load->view('base/footer',$data);
	}
	public function update_catatan_internal(){
		date_default_timezone_set('Asia/Jakarta');
		$data['dis_catatan'] = $this->input->post('dis_catatan');
		$prm_id = $this->input->post('prm_id');
		$dis_id = $this->input->post('dis_id');
		$data['dis_status'] = '2';
		$data['dis_updated'] = date('Y-m-d H:i:s');
		$data_permohonan['prm_status'] = '2';

		$id_tugastambahan = $this->session->userdata('id_tugastambahan');

		// untuk mengecek apakah dia wadek atau dekan
		$cek_pangkat = $this->Permohonan_model->cek_pangkat($id_tugastambahan);

		$sub_kalimat = substr($cek_pangkat['nama_jastruk'],0,5);

		if($sub_kalimat == "Wakil"){
			/////////// kondisi dimana acc disposisi untuk dekan ke rektor
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			$this->Permohonan_model->update_permohonan($data_permohonan,$prm_id);
			$id_pegawai = $this->Permohonan_model->cek_disposisi_in($cek_pangkat['id_pimpinan'], 1);

			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; //

		}else if($sub_kalimat == "Dekan"){
			///////////kondisi dimana acc disposisi untuk warek 3 ke biro
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			$this->Permohonan_model->update_permohonan($data_permohonan,$prm_id);
			$id_pegawai = $this->Permohonan_model->cek_disposisi_in(1, 4);

			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan; //

		}else if($id_tugastambahan == 1){
			///////////kondisi dimana acc disposisi untuk warek 3 ke biro
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			$this->Permohonan_model->update_permohonan($data_permohonan,$prm_id);
			$id_pegawai = $this->Permohonan_model->cek_wa3_biro();

			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //

		}else if($id_tugastambahan == 4){

			$this->Permohonan_model->update_disposisi($data,$dis_id);
			/////////// insert biro dan kabag
			$id_pegawai = $this->Permohonan_model->cek_disposisi(353, 369);

			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //

		}else if($id_tugastambahan == 353){

			$this->Permohonan_model->update_disposisi($data,$dis_id);
			///////////insert kabag dan kasubag
			$id_pegawai = $this->Permohonan_model->cek_disposisi(369, 370);

			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //

		}else if($id_tugastambahan == 369){

			$this->Permohonan_model->update_disposisi($data,$dis_id);
			$id_pegawai = $this->Permohonan_model->cek_disposisi(370, 375);

			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //

		}else if($id_tugastambahan == 370){

			$this->Permohonan_model->update_disposisi($data,$dis_id);
			$id_pegawai = $this->Permohonan_model->cek_disposisi(375, 375);


			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //

		}


		$disposisi['dis_jenis'] = 1; // 1 karena permohonan 1
		$disposisi['dis_entitas'] = $prm_id; //
		$disposisi['dis_pegawaiasal'] = $id_pegawai[0]->id; //
		$disposisi['dis_pegawaitujuan'] = $id_pegawai[1]->id; //
		$disposisi['dis_status'] = 1;

		///////////cek disposisi apakah sudah ada atau tidak jika belum ada isi kalau sudah ada tidak perlu di isi
		$cek_disposisi = $this->Permohonan_model->cek_disposisi2($disposisi);
		if($cek_disposisi['dis_id'] == ''){
			$id_disposisi = $this->Permohonan_model->add_disposisi($disposisi);
		}


		echo "<script>window.location.href = '".base_url()."permohonan';</script>";
	}
	public function update_catatan(){
		date_default_timezone_set('Asia/Jakarta');
		$data['dis_catatan'] = $this->input->post('dis_catatan');
		$prm_id = $this->input->post('prm_id');
		$spk_id = $this->input->post('spk_id');
		$dis_id = $this->input->post('dis_id');
		$data['dis_status'] = '2';
		$data['dis_updated'] = date('Y-m-d H:i:s');
		$data_permohonan['prm_status'] = '2';
		$this->load->model('Permohonan_model');
		$this->load->model('Kesepakatan_model');
		$id_tugastambahan = $this->session->userdata('id_tugastambahan');
		$user_id = $this->session->userdata('user_id');

		$detil = $this->Kesepakatan_model->getDetilkesepakatan($spk_id);
		// get pjkesepakatan pada tabel kesepakatan dimodel atas
		$pjKesepakatan = $detil['spk_pegawaipihak1'];
		if($user_id == $pjKesepakatan){
			// model yang dipanggil adalah untuk mengupdate disposisi berupa catatan dan status
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			// insert kabag dan kasubag
			$id_pegawai = $this->Permohonan_model->cek_disposisi(4, 353);
			// kondisi dimana mengecek apakah dia dosen atau tidak jika dia dosen maka akan diambil
			// 		id_tugastambahan dan jika dia pegawai biasa akan diambil id_jastruk
			if($id_pegawai[0]->id_tugastambahan == 0){
				$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk;
			}else {
				$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_tugastambahan;
			}
			// kondisi yang sama akan tetapi untuk disposisi tujuan
			if($id_pegawai[1]->id_tugastambahan == 0){
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk;
			}else {
				$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_tugastambahan;
			}

		}else if($id_tugastambahan == 4){
			//
			$this->Permohonan_model->update_disposisi($data,$dis_id);
			// insert biro dan kabag
			$id_pegawai = $this->Permohonan_model->cek_disposisi(353, 369);

			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //

		}else if($id_tugastambahan == 353){

			$this->Permohonan_model->update_disposisi($data,$dis_id);
			// insert kabag dan kasubag
			$id_pegawai = $this->Permohonan_model->cek_disposisi(369, 370);

			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //

		}else if($id_tugastambahan == 369){

			$this->Permohonan_model->update_disposisi($data,$dis_id);
			$id_pegawai = $this->Permohonan_model->cek_disposisi(370, 375);

			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //

		}else if($id_tugastambahan == 370){

			$this->Permohonan_model->update_disposisi($data,$dis_id);
			$id_pegawai = $this->Permohonan_model->cek_disposisi(375, 375);


			$disposisi['dis_jastrukasal'] = $id_pegawai[0]->id_jastruk; //
			$disposisi['dis_jastruktujuan'] = $id_pegawai[1]->id_jastruk; //

		}


		$disposisi['dis_jenis'] = 4; // 4 karena laporan 4
		$disposisi['dis_entitas'] = $prm_id; //
		$disposisi['dis_pegawaiasal'] = $id_pegawai[0]->id; //
		$disposisi['dis_pegawaitujuan'] = $id_pegawai[1]->id; //
		$disposisi['dis_status'] = 1;

		// cek disposisi apakah sudah ada atau tidak jika belum ada isi kalau sudah ada tidak perlu di isi
		$cek_disposisi = $this->Permohonan_model->cek_disposisi2($disposisi);
		if($cek_disposisi['dis_id'] == ''){
			$id_disposisi = $this->Permohonan_model->add_disposisi($disposisi);
		}


		// echo "<script>window.location.href = '".base_url()."laporan/details/".$prm_id."';</script>";
		echo "<script>window.location.href = '".base_url()."laporan/';</script>";
	}

}
