<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Home_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
	}
	public function index(){
		$this->load->view('base/login');
	}
	
	public function cek(){
		$username = trim($this->input->post('username'));
		$password = md5($this->input->post('password'));
		// cek data mitra dulu kalau ada masuk
		$data = $this->Home_model->login_mitra($username,$password);
		// print_r ($data);
		if ($data != '') {
			$this->session->set_userdata('user_jenis','1');
			$this->session->set_userdata('user_id',$data['mtr_id']);
			$this->session->set_userdata('user_username',$data['mtr_username']);
			$this->session->set_userdata('user_nama',$data['mtr_namainstansi']);
			$this->session->set_userdata('id_tugastambahan','Mitra');
			$this->session->set_userdata('kode_unit','Mitra');
			$this->session->set_userdata('nama_jastruk','Mitra');
			$this->session->set_userdata('id_kode_unit','0');
			

			echo "<script>window.location.href = '".base_url()."';</script>";
		}else {
			$data_uin = $this->Home_model->login_uin($username,$password);
			// print_r ($data_uin);
			if($data_uin != ''){
				$this->session->set_userdata('user_jenis','2');
				$this->session->set_userdata('user_id',$data_uin['id']);
				$this->session->set_userdata('user_username',$data_uin['nip']); 
				$this->session->set_userdata('user_nama',$data_uin['nama']);
				
				if($data_uin['id_tugastambahan'] != 0){
					$this->session->set_userdata('kode_unit',$data_uin['id_unittugastambahan']);	
					$this->session->set_userdata('id_tugastambahan',$data_uin['id_tugastambahan']);					
					$this->session->set_userdata('nama_jastruk',$data_uin['nama_jastruk']);					
					$this->session->set_userdata('id_kode_unit',$data_uin['id_kode_unit']);					
				}else {
					$this->session->set_userdata('kode_unit',$data_uin['id_unitkerja']);					
					$this->session->set_userdata('id_tugastambahan',$data_uin['id_jastruk']);					
					$this->session->set_userdata('nama_jastruk',$data_uin['nama_jastruk']);	
					$this->session->set_userdata('id_kode_unit',$data_uin['id_kode_unit']);						
				}
				
				echo "<script>window.location.href = '".base_url()."';</script>";
			}else {
				echo "<script>alert ('Username atau Password Salah ! Silahkan ulangi lagi');window.location.href = '".base_url()."';</script>";				
			}
		}
	}
	
	
	public function logout(){
		$this->session->unset_userdata(array('user_jenis' => ''));
		$this->session->unset_userdata(array('user_id' => ''));
		$this->session->unset_userdata(array('user_username' => ''));
		$this->session->unset_userdata(array('user_nama' => ''));
		$this->session->unset_userdata(array('id_tugastambahan' => ''));
		$this->session->unset_userdata(array('kode_unit' => ''));
		$this->session->unset_userdata(array('nama_jastruk' => ''));
		$this->session->unset_userdata(array('menu' => ''));
		echo "<script>window.location.href = '".base_url()."login';</script>";
		
	}
	
}
