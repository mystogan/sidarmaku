/*
SQLyog Community v12.2.6 (64 bit)
MySQL - 10.1.25-MariaDB : Database - sidarmaku
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `disposisi` */

CREATE TABLE `disposisi` (
  `dis_id` int(11) NOT NULL AUTO_INCREMENT,
  `dis_jenis` int(11) DEFAULT NULL,
  `dis_entitas` int(11) DEFAULT NULL,
  `dis_pegawaiasal` int(11) DEFAULT NULL,
  `dis_jastrukasal` int(11) DEFAULT NULL,
  `dis_pegawaitujuan` int(11) DEFAULT NULL,
  `dis_jastruktujuan` int(11) DEFAULT NULL,
  `dis_catatan` text,
  `dis_status` smallint(6) DEFAULT NULL,
  `dis_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dis_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`dis_id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=latin1 COMMENT='Disposisi berdasarkan masing-masing entitas,\nJenis :\n                              ';

/*Data for the table `disposisi` */

insert  into `disposisi`(`dis_id`,`dis_jenis`,`dis_entitas`,`dis_pegawaiasal`,`dis_jastrukasal`,`dis_pegawaitujuan`,`dis_jastruktujuan`,`dis_catatan`,`dis_status`,`dis_created`,`dis_updated`) values 
(1,1,1,395,1,256,4,'ini catatan rektor tentang Sertifikat Informasi Teknologi ',2,'2019-01-03 22:19:21','2019-01-03 22:19:21'),
(2,1,1,256,4,1071,353,'ini catatan oleh warek 3 tentang permohonan Sertifikat Informasi Teknologi',2,'2019-01-03 22:20:22','2019-01-03 22:20:22'),
(3,1,1,1071,353,1145,353,'ini catatan biro AAK tentang Sertifikat Informasi Teknologi ',2,'2019-01-03 22:21:54','2019-01-03 22:21:54'),
(4,1,1,216,369,79,370,'ini catatan dari kepala baguan kerjasama Sertifikat Informasi Teknologi ',2,'2019-01-03 22:22:43','2019-01-03 22:22:43'),
(5,1,1,79,370,78,375,'ini catatan kasubag bagian kerjasama Sertifikat Informasi Teknologi ',2,'2019-01-03 22:23:32','2019-01-03 22:23:32'),
(6,1,1,78,375,78,375,NULL,1,'2019-01-03 22:23:32','0000-00-00 00:00:00'),
(7,1,2,554,198,372,195,'ini catatn dari wadek 3 pak yusam dalam permohonan Magang untuk Mahasiswa',2,'2019-01-04 06:47:28','2019-01-04 06:47:28'),
(8,1,2,372,195,395,1,'ini catatan dari dekan saintek untuk permohonan Magang untuk Mahasiswa',2,'2019-01-04 06:48:57','2019-01-04 06:48:57'),
(9,1,2,395,1,256,4,'ini catatan dari rektor untuk permohonan Magang untuk Mahasiswa',2,'2019-01-04 06:49:55','2019-01-04 06:49:55'),
(10,1,2,256,4,1071,353,'ini catatan dari wakil rektor 3 untuk permohonan Magang untuk Mahasiswa',2,'2019-01-04 06:51:21','2019-01-04 06:51:21'),
(11,1,2,1071,353,1145,353,'ini catatan dari pak biro AAK untuk permohonan Magang untuk Mahasiswa',2,'2019-01-04 06:58:39','2019-01-04 06:58:39'),
(12,1,2,216,369,79,370,'ini adalah disposisi dari kabag kerjasama untuk dilanjutkan ke kasubag kerjasama atas permohonan  Magang untuk Mahasiswa\r\n',2,'2019-01-04 06:59:46','2019-01-04 06:59:46'),
(13,1,2,79,370,78,375,'ini catatan dari kasubag kerjasama dengan permohonan  Magang untuk Mahasiswa',2,'2019-01-04 07:02:05','2019-01-04 07:02:05'),
(14,1,2,78,375,78,375,NULL,1,'2019-01-04 07:02:05','0000-00-00 00:00:00'),
(17,1,4,554,198,372,195,NULL,3,'2019-01-04 21:30:30','0000-00-00 00:00:00'),
(18,1,5,554,198,372,195,'ini catatan wadek 3 dan tidak ditoal dalam kegiatan Magang Ngisi POM 2.0',2,'2019-01-04 21:37:33','2019-01-04 21:37:33'),
(19,1,5,372,195,395,1,NULL,3,'2019-01-04 21:41:09','0000-00-00 00:00:00'),
(20,1,6,554,198,372,195,'ini catatan dari wadek 3 fakultas disposisi lek yo dengan kegiatan Masang Wifi ',2,'2019-01-04 22:09:41','2019-01-04 22:09:41'),
(21,1,6,372,195,395,1,'ini disposisi dari dekan lek wkwkwkw mugo ae jos yo kegiatan masang wifi ',2,'2019-01-04 22:10:17','2019-01-04 22:10:17'),
(22,1,6,395,1,256,4,NULL,3,'2019-01-04 22:10:35','0000-00-00 00:00:00'),
(23,1,7,554,198,372,195,'ini adalah catatan dari wadek 3 kegiatan judul permohonan kerja sama',2,'2019-01-04 22:55:57','2019-01-04 22:55:57'),
(24,1,7,372,195,395,1,'ini catatan dari dekan ',2,'2019-01-04 22:57:09','2019-01-04 22:57:09'),
(25,1,7,395,1,256,4,'ini catatan dari rektor ',2,'2019-01-04 22:57:33','2019-01-04 22:57:33'),
(26,1,7,256,4,1071,353,NULL,3,'2019-01-04 22:57:51','0000-00-00 00:00:00'),
(27,1,8,395,1,256,4,NULL,1,'2019-01-04 23:06:17','0000-00-00 00:00:00'),
(28,1,9,395,1,256,4,NULL,3,'2019-01-04 23:16:13','0000-00-00 00:00:00'),
(29,1,10,395,1,256,4,'ini catatan dari rektor pak heheh ',2,'2019-01-04 23:17:09','2019-01-04 23:17:09'),
(30,1,10,256,4,1071,353,NULL,3,'2019-01-04 23:18:27','0000-00-00 00:00:00'),
(31,1,11,395,1,256,4,'ini catatan dari rektor untuk permohonan fakultas dakwah ',2,'2019-01-04 23:20:24','2019-01-04 23:20:24'),
(32,1,11,256,4,1071,353,'ini catatan warek 3 untuk permohonan fakultas dakwah dan ilmu komunikasi ',2,'2019-01-04 23:21:03','2019-01-04 23:21:03'),
(33,1,11,1071,353,1145,353,NULL,3,'2019-01-04 23:21:27','0000-00-00 00:00:00'),
(34,1,1,NULL,NULL,NULL,NULL,NULL,1,'2019-01-05 01:58:47','0000-00-00 00:00:00'),
(35,2,2,554,198,372,195,'ini adalah catatan dari wadek 3 untuk kesepahaman judul kesepahaman 1.3',2,'2019-01-05 12:44:15','2019-01-05 12:44:15'),
(36,2,3,554,198,372,195,NULL,3,'2019-01-05 14:29:51','0000-00-00 00:00:00'),
(37,2,4,554,198,372,195,'ini adalah catatan dari wadek 3 fakultas saintek untuk kesepahaman dengan judul judul kesepahaman 1.1',2,'2019-01-05 15:41:50','2019-01-05 15:41:50'),
(38,2,2,372,195,395,1,'ini catatan dari dekan untuk kesepahaman kesepahaman 1.3 ',2,'2019-01-05 12:51:36','2019-01-05 12:51:36'),
(39,2,2,395,1,256,4,'ini catatan kesepahaman dari rektor yang berjudul judul kesepahaman 1.3',2,'2019-01-05 13:59:54','2019-01-05 13:59:54'),
(40,2,2,256,4,1071,353,'ini adalah catatan dari warek 3 kesepahaman yang berjudul judul kesepahaman 1.3 ',2,'2019-01-05 14:01:10','2019-01-05 14:01:10'),
(41,2,2,1071,353,1145,353,'ini adalah catatan dari biro AAK untuk kesepahaman dengan judul judul kesepahaman 1.3',2,'2019-01-05 14:03:56','2019-01-05 14:03:55'),
(42,2,2,216,369,79,370,'ini catatan kesepahaman dari kepala bagian kerjasama untuk judul kesepahaman judul kesepahaman 1.3 ',2,'2019-01-05 14:04:45','2019-01-05 14:04:45'),
(43,2,2,79,370,78,375,'ini adalah catatan disposisi dari kasubag kerjasama untuk judul kesepahaman  judul kesepahaman 1.3 ',2,'2019-01-05 14:05:32','2019-01-05 14:05:32'),
(44,2,2,78,375,78,375,NULL,1,'2019-01-05 14:05:32','0000-00-00 00:00:00'),
(45,2,4,372,195,395,1,NULL,3,'2019-01-05 15:42:38','0000-00-00 00:00:00'),
(46,2,5,395,1,256,4,'ini adalah catatan rektor untuk kesepahaman mitra dengan judul  Magang Untuk Mahasiswa edit\r\n',2,'2019-01-05 16:15:01','2019-01-05 16:15:01'),
(47,2,5,256,4,1071,353,'catatan dari wareg 3 untuk disposisi mitra ke kesepahaman dengan judul  Magang Untuk Mahasiswa edit ',2,'2019-01-05 16:18:07','2019-01-05 16:18:07'),
(48,2,5,1071,353,1145,353,'ini adalah catatan dari pak biro AAK untuk kesepahaman mitra dengan judul Magang Untuk Mahasiswa edit',2,'2019-01-05 16:19:05','2019-01-05 16:19:05'),
(49,2,5,216,369,79,370,'ini catatan dari kepala bagian kerjasama untuk kesepahaman dengan judul  Magang Untuk Mahasiswa edit\r\n',2,'2019-01-05 16:20:08','2019-01-05 16:20:08'),
(50,2,5,79,370,78,375,'ini adalah catatan disposisi dari kepala sub bagian kerjasama untuk kesepahaman dengan judul Magang Untuk Mahasiswa edit ',2,'2019-01-05 16:21:06','2019-01-05 16:21:06'),
(51,2,5,78,375,78,375,NULL,1,'2019-01-05 16:21:06','0000-00-00 00:00:00'),
(52,2,6,395,1,256,4,NULL,3,'2019-01-05 16:26:24','0000-00-00 00:00:00'),
(53,2,7,395,1,256,4,'ini adalah catatan disposisi dari rektor hehe dengan judul multimatic 2.0',2,'2019-01-05 16:26:57','2019-01-05 16:26:56'),
(54,2,7,256,4,1071,353,NULL,3,'2019-01-05 16:27:18','0000-00-00 00:00:00'),
(55,1,12,256,4,395,1,'ini adalah catatan internal 3 dari wakil rektor 3 yang pertama dan permohonan dengan judul Instal ulang Komputer di Lab Intregasi edit',2,'2019-01-05 17:39:21','2019-01-05 17:39:21'),
(56,1,12,395,1,256,4,'ini rektor cak ',2,'2019-01-05 17:41:30','2019-01-05 17:41:30'),
(57,1,12,256,4,1071,353,NULL,3,'2019-01-05 17:42:27','0000-00-00 00:00:00'),
(59,2,10,256,4,395,1,'ini adalah catatan warek 3 untuk disposisi pustipd pak ',2,'2019-01-05 19:23:06','2019-01-05 19:23:06'),
(60,2,10,395,1,256,4,'ini catatan dari rektor untuk warek 3 yang kedua pak tentang sertifikasi mahasiswa angkatan 2015',2,'2019-01-05 19:23:51','2019-01-05 19:23:51'),
(61,2,10,256,4,1071,353,'ini catatan dari warek 3 yang kedua pak yo ',2,'2019-01-05 19:24:51','2019-01-05 19:24:51'),
(62,2,10,1071,353,1145,353,'ini catatan dari biro AAK lek ',2,'2019-01-05 19:30:07','2019-01-05 19:30:07'),
(63,2,10,216,369,79,370,'ini adalah catatan dari kepala bagian kerjasama bu emy ',2,'2019-01-05 19:30:53','2019-01-05 19:30:53'),
(64,2,10,79,370,78,375,'ini bagian terakhir untuk kerjasama yang subbagian pak hehe',2,'2019-01-05 19:31:31','2019-01-05 19:31:31'),
(65,2,10,78,375,78,375,NULL,1,'2019-01-05 19:31:31','0000-00-00 00:00:00'),
(66,1,13,395,1,256,4,'ini disposisi dari rektor untuk permohonan external permohonan kerjasama exchange ke australia',2,'2019-01-06 12:08:19','2019-01-06 12:08:19'),
(67,1,13,256,4,1071,353,'ini disposisi dari wakil rektor untuk permohonan disposisi permohonan kerjasama exchange ke australia ',2,'2019-01-06 12:10:43','2019-01-06 12:10:43'),
(68,1,13,1071,353,1145,353,'ini disposisi dari Biro AAK untuk permohoanan disposisi permohonan kerjasama ezchange ke australia ',2,'2019-01-06 12:12:04','2019-01-06 12:12:03'),
(69,1,13,216,369,79,370,'ini disposisi dari Kasubag Kerjasama permohonan disposisi permohonan kerjasama exchange ke australia',2,'2019-01-06 12:13:00','2019-01-06 12:13:00'),
(70,1,13,79,370,78,375,'ini disosisi dari kasubag tenanan kerjasama permohonan disposisi permohonan kerjasama exchange ke australia',2,'2019-01-06 12:14:53','2019-01-06 12:14:53'),
(71,1,13,78,375,78,375,NULL,1,'2019-01-06 12:14:53','0000-00-00 00:00:00'),
(72,1,14,395,1,256,4,NULL,3,'2019-01-06 12:21:36','0000-00-00 00:00:00'),
(73,1,15,395,1,256,4,'ini disposisi ini sertifikat jatah.e warek 3',2,'2019-01-06 12:22:21','2019-01-06 12:22:21'),
(74,1,16,395,1,256,4,'disposisi untuk permohonan  permohonan untuk dibatalkan biro AAK',2,'2019-01-06 12:23:28','2019-01-06 12:23:28'),
(75,1,17,395,1,256,4,'disposisi permohonan  permohonan untuk dibatalkan kabag kerjasama',2,'2019-01-06 12:23:44','2019-01-06 12:23:44'),
(76,1,18,395,1,256,4,'disposisi untuk permohonan permohonan untuk dibatalkan kasubag kerjasama ',2,'2019-01-06 12:24:06','2019-01-06 12:24:06'),
(77,1,15,256,4,1071,353,NULL,3,'2019-01-06 12:24:42','0000-00-00 00:00:00'),
(78,1,16,256,4,1071,353,'disposisi permohonan permohonan untuk dibatalkan biro AAK ',2,'2019-01-06 12:25:24','2019-01-06 12:25:24'),
(79,1,17,256,4,1071,353,'disposisi untuk permohonan untuk dibatalkan kabag kerjasama',2,'2019-01-06 12:25:48','2019-01-06 12:25:48'),
(80,1,18,256,4,1071,353,'disposisi untuk permohonan untuk dibatalkan kasubag kerjasama ',2,'2019-01-06 12:26:06','2019-01-06 12:26:06'),
(81,1,16,1071,353,1145,353,NULL,3,'2019-01-06 12:26:48','0000-00-00 00:00:00'),
(82,1,17,1071,353,1145,353,'disposisi untuk permohonan untuk dibatalkan kabag kerjasama',2,'2019-01-06 12:27:20','2019-01-06 12:27:20'),
(83,1,18,1071,353,1145,353,'disposisi untuk permohonan untuk dibatalkan kabag kerjasama',2,'2019-01-06 12:27:32','2019-01-06 12:27:32'),
(84,1,17,216,369,79,370,NULL,3,'2019-01-06 12:28:27','0000-00-00 00:00:00'),
(85,1,18,216,369,79,370,'dispodidi untuk permohonan untuk dibatalkan kasubag kerjasama ',2,'2019-01-06 12:28:50','2019-01-06 12:28:50'),
(86,1,18,79,370,78,375,NULL,3,'2019-01-06 12:29:20','0000-00-00 00:00:00'),
(87,1,20,NULL,NULL,NULL,NULL,NULL,1,'2019-01-06 12:41:01','0000-00-00 00:00:00'),
(88,1,21,NULL,NULL,NULL,NULL,NULL,1,'2019-01-06 13:02:58','0000-00-00 00:00:00'),
(90,1,23,641,173,625,170,'ini catatan dari dekan 3 untuk dekan hehehe ',2,'2019-01-06 13:12:07','2019-01-06 13:12:07'),
(91,1,23,625,170,665,170,NULL,1,'2019-01-06 13:12:07','0000-00-00 00:00:00'),
(92,1,24,641,173,625,170,NULL,3,'2019-01-06 13:13:14','0000-00-00 00:00:00'),
(93,2,11,641,173,625,170,NULL,1,'2019-01-06 13:23:24','0000-00-00 00:00:00'),
(94,2,12,256,4,395,1,'ini catatan untuk wareg 3 cak eheheh sangar wes pokok.e hehehe ',2,'2019-01-06 13:31:20','2019-01-06 13:31:20'),
(95,2,12,395,1,256,4,'ini catatan dari rektor cak hehehe sangar wes pokok.e hehehe ',2,'2019-01-06 13:32:58','2019-01-06 13:32:58'),
(96,2,12,256,4,1071,353,'ini wareg yang kedua hehehe ',2,'2019-01-06 13:39:46','2019-01-06 13:39:46'),
(97,2,12,1071,353,1145,353,NULL,1,'2019-01-06 13:39:46','0000-00-00 00:00:00'),
(98,2,13,256,4,395,1,NULL,1,'2019-01-06 13:42:19','0000-00-00 00:00:00');

/*Table structure for table `jenis` */

CREATE TABLE `jenis` (
  `jns_id` int(11) NOT NULL AUTO_INCREMENT,
  `jns_kerjasama` varchar(50) DEFAULT NULL,
  `jns_delete` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`jns_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `jenis` */

insert  into `jenis`(`jns_id`,`jns_kerjasama`,`jns_delete`) values 
(1,'Jasa',1),
(2,'Luar Negeri',1),
(3,'Swasta',1);

/*Table structure for table `kesepahaman` */

CREATE TABLE `kesepahaman` (
  `sph_id` int(11) NOT NULL AUTO_INCREMENT,
  `mtr_id` int(11) DEFAULT NULL,
  `prm_id` int(11) DEFAULT NULL,
  `sph_nomor` varchar(20) DEFAULT NULL,
  `sph_judul` varchar(255) DEFAULT NULL,
  `sph_tanggal` date DEFAULT NULL,
  `sph_namapihak1` varchar(50) DEFAULT NULL,
  `sph_jabatanpihak1` varchar(50) DEFAULT NULL,
  `sph_skpihak1` varchar(50) DEFAULT NULL,
  `sph_pegawaipihak1` int(11) DEFAULT NULL,
  `sph_namapihak2` varchar(50) DEFAULT NULL,
  `sph_jabatanpihak2` varchar(50) DEFAULT NULL,
  `sph_skpihak2` varchar(50) DEFAULT NULL,
  `sph_tujuan` text,
  `sph_mulai` date DEFAULT NULL,
  `sph_akhir` date DEFAULT NULL,
  `sph_status` smallint(6) DEFAULT NULL,
  `sph_file` varchar(50) DEFAULT NULL,
  `sph_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sph_internal` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`sph_id`),
  KEY `FK_mitra_kesepahaman` (`mtr_id`),
  KEY `FK_permohonan_mou` (`prm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `kesepahaman` */

insert  into `kesepahaman`(`sph_id`,`mtr_id`,`prm_id`,`sph_nomor`,`sph_judul`,`sph_tanggal`,`sph_namapihak1`,`sph_jabatanpihak1`,`sph_skpihak1`,`sph_pegawaipihak1`,`sph_namapihak2`,`sph_jabatanpihak2`,`sph_skpihak2`,`sph_tujuan`,`sph_mulai`,`sph_akhir`,`sph_status`,`sph_file`,`sph_created`,`sph_internal`) values 
(2,1,2,'999999','judul kesepahaman 1.3','2019-01-17','nama pihak 1 ','jabatan pihak 1 ','sk pihak 1',1,'nama pihak 2','jabatan pihak 2','sk pihak 2','tujuan kesepahaman 1.3','2019-01-22','2019-01-16',2,'272ffb946052d9ab7846aeb74ffd8ee3.jpg','2019-01-05 12:39:38',1),
(3,3,2,'072747367','judul kesepahaman 1.2','2019-01-24','nama pihak 1.2','jabatan 1.2','sk pihak 1.2',706,'nama pihak 2','jabatan pihak 2','sk pihak 2','tujuan kesepahaman 1.2','2019-01-10','2019-01-24',3,'f1f72a7fa401c1e286f33f4c4f0d1bc9.xlsx','2019-01-05 14:30:04',1),
(4,1,1,'08124335','judul kesepahaman 1.1','2019-01-17','nama pihak 1 ','jabatan pihak 1 ','sk pihak 1',1,'nama pihak 2','jabatan pihak 2','sk pihak 2','tujuan kesepahaman 1.1','2019-01-22','2019-01-16',3,'7edec88669e0b234256cc82d029a693c.jpg','2019-01-05 15:42:38',1),
(5,1,2,'10000016257259999','Magang Untuk Mahasiswa edit','2019-01-07','nama pihak 1.0 edit','jabatan pihak 1.0 edit','sk pihak 1.0 edit',743,'nama pihak 2.0 edit','jabatan pihak 2.0 edit','sk pihak 2.0 edit','tujuan kesepahaman 0.0 edit','2019-01-07','2019-01-23',2,'bae3bca80c55dcaae76bc4ce290fb732.docx','2019-01-05 16:15:01',2),
(6,1,1,'11111111111111112222','inixindo untuk berkontribusi ','2019-01-07','nama pihak 1.1','jabatan pihak 1.1','sk pihak 1.1 ',550,'nama pihak 2.1','jabatan pihak 2.1','sk pihak 2.1','tujuan 0.1','2019-01-07','2019-01-16',3,'aaf8de5eac752c8411e3f496996d295f.jpeg','2019-01-05 16:26:24',2),
(7,1,2,'222222222222','multimatic 2.0','2019-01-08','nama pihak 1.2','jabatan pihak 1.2','sk pihak 1.2',574,'nama pihak 2,2','jabatan pihak 2.2','sk pihak 2,2','tujuan 0.2','2019-01-15','2019-01-31',3,'1ebdcaffa5aba52ab6d3392570b74833.jpg','2019-01-05 16:27:18',2),
(10,3,2,'9999991','sertifikasi IT untuk angkatan 2015','2019-01-07','nama pihak 1.9','jabatan pihak 1.9','sk pihak 1.9',1128,'nama pihak 2.9','jabatan pihak 2.9','sk pihak 2.9','tujuan kesepahaman 0.9','2019-01-07','2019-01-11',2,'9f8d89f10a8a391eb61847dab4288d84.jpg','2019-01-05 19:23:06',3),
(11,3,23,'888888','judul kesepahaman ','2019-01-08','nama pihak 1','jabatan pihak 1','sk pihak 1',1086,'nama pihak 2','jabatan pihak 2','sk pihak 2','tujuan 0','2019-01-14','2019-01-31',1,'2231b17f5815fa57ca98b38a080c4d3f.jpg','2019-01-06 13:23:24',1),
(12,4,13,'2121212121212121','sertifikasi untuk bahasa dan sastra ','2019-01-24','nama pihak 1.0','jabatan pihak 1.0','sk pihak 1.0',5,'nama pihak 2.0','jabatan pihak 2.0','sk pihak 2.0 ','tujuan 0.0','2019-01-07','2019-01-14',2,'73b07dffd51ac7d2163ee9702c590725.xlsx','2019-01-06 13:31:20',3),
(13,3,1,'09876543','hjg','2019-01-25','jhg','jhg','jhgjh',1,'hjg','jhg','jhgjh','gjhg','2019-01-07','2019-01-24',1,'c4cd33bdff8f96d7c45405d554d838cf.png','2019-01-06 13:42:19',3);

/*Table structure for table `kesepakatan` */

CREATE TABLE `kesepakatan` (
  `spk_id` int(11) NOT NULL AUTO_INCREMENT,
  `sph_id` int(11) DEFAULT NULL,
  `mtr_id` int(11) DEFAULT NULL,
  `jns_id` int(11) DEFAULT NULL,
  `spk_nomor` varchar(20) DEFAULT NULL,
  `spk_judul` varchar(255) DEFAULT NULL,
  `spk_tanggal` date DEFAULT NULL,
  `spk_namapihak1` varchar(50) DEFAULT NULL,
  `spk_jabatanpihak1` varchar(50) DEFAULT NULL,
  `spk_skpihak1` varchar(50) DEFAULT NULL,
  `spk_pegawaipihak1` int(11) DEFAULT NULL,
  `spk_namapihak2` varchar(50) DEFAULT NULL,
  `spk_jabatanpihak2` varchar(50) DEFAULT NULL,
  `spk_skpihak2` varchar(50) DEFAULT NULL,
  `spk_tujuan` text,
  `spk_mulai` date DEFAULT NULL,
  `spk_akhir` date DEFAULT NULL,
  `spk_unit` int(11) DEFAULT NULL,
  `spk_pegawaipic` int(11) DEFAULT NULL,
  `spk_anggaran` float DEFAULT NULL,
  `spk_status` smallint(6) DEFAULT NULL,
  `spk_file` varchar(50) DEFAULT NULL,
  `spk_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`spk_id`),
  KEY `FK_jenis_kesepakatan` (`jns_id`),
  KEY `FK_mitra_kesepakatan` (`mtr_id`),
  KEY `FK_mou_kesepakatan` (`sph_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kesepakatan` */

/*Table structure for table `laporan` */

CREATE TABLE `laporan` (
  `lak_id` int(11) NOT NULL AUTO_INCREMENT,
  `spk_id` int(11) DEFAULT NULL,
  `lak_periode` varchar(10) DEFAULT NULL,
  `lak_anggaran` float DEFAULT NULL,
  `lak_tanggal` date DEFAULT NULL,
  `lak_pengesahan` date DEFAULT NULL,
  `lak_penandatangan` varchar(50) DEFAULT NULL,
  `lak_latarbelakang` text,
  `lak_tujuan` text,
  `lak_program` text,
  `lak_pelaksanaan` text,
  `lak_hasil` text,
  `lak_rtl` text,
  `lak_kesimpulan` text,
  `lak_saran` text,
  `lak_filelaporan` varchar(50) DEFAULT NULL,
  `lak_filelampiran` varchar(50) DEFAULT NULL,
  `lak_jenis` smallint(6) DEFAULT NULL,
  `lak_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`lak_id`),
  KEY `FK_lak_kesepakatan` (`spk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `laporan` */

/*Table structure for table `m_kabupaten` */

CREATE TABLE `m_kabupaten` (
  `id_kabupaten` int(11) NOT NULL AUTO_INCREMENT,
  `No_Kab` int(3) NOT NULL,
  `Nama_Kab` varchar(255) NOT NULL,
  `id_propinsi` int(11) NOT NULL,
  PRIMARY KEY (`id_kabupaten`),
  KEY `id_propinsi` (`id_propinsi`)
) ENGINE=MyISAM AUTO_INCREMENT=9272 DEFAULT CHARSET=latin1;

/*Data for the table `m_kabupaten` */

insert  into `m_kabupaten`(`id_kabupaten`,`No_Kab`,`Nama_Kab`,`id_propinsi`) values 
(1101,1,'ACEH SELATAN',11),
(1102,2,'ACEH TENGGARA',11),
(1103,3,'ACEH TIMUR',11),
(1104,4,'ACEH TENGAH',11),
(1105,5,'ACEH BARAT',11),
(1106,6,'ACEH BESAR',11),
(1107,7,'PIDIE',11),
(1108,8,'ACEH UTARA',11),
(1109,9,'SIMEULUE',11),
(1110,10,'ACEH SINGKIL',11),
(1111,11,'BIREUEN',11),
(1112,12,'ACEH BARAT DAYA',11),
(1113,13,'GAYO LUES',11),
(1114,14,'ACEH JAYA',11),
(1115,15,'NAGAN RAYA',11),
(1116,16,'ACEH TAMIANG',11),
(1117,17,'BENER MERAH',11),
(1118,18,'PIDIE JAYA',11),
(1171,71,'KOTA BANDA ACEH',11),
(1172,72,'KOTA SABANG',11),
(1173,73,'KOTA LHOKSEUMAWE',11),
(1174,74,'KOTA LANGSA',11),
(1175,75,'KOTA SUBULUSSALAM',11),
(1201,1,'TAPANULI TENGAH',12),
(1202,2,'TAPANULI UTARA',12),
(1203,3,'TAPANULI SELATAN',12),
(1204,4,'NIAS',12),
(1205,5,'LANGKAT',12),
(1206,6,'KARO',12),
(1207,7,'DELI SERDANG',12),
(1208,8,'SIMALUNGUN',12),
(1209,9,'ASAHAN',12),
(1210,10,'LABUAN BATU',12),
(1211,11,'DAIRI',12),
(1212,12,'TOBA SAMOSIR',12),
(1213,13,'MANDAILING NATAL',12),
(1214,14,'NIAS SELATAN',12),
(1215,15,'PAKPAK BHARAT',12),
(1216,16,'HUMBANG HASUNDUTAN',12),
(1217,17,'SAMOSIR',12),
(1218,18,'SERDANG BEDAGAI',12),
(1219,19,'BATU BARA',12),
(1271,71,'KOTA MEDAN',12),
(1272,72,'KOTA PEMATANG SIANTAR',12),
(1273,73,'KOTA SIBOLGA',12),
(1274,74,'KOTA TANJUNG BALAI',12),
(1275,75,'KOTA BINJAI',12),
(1276,76,'KOTA TEBING TINGGI',12),
(1277,77,'KOTA PADANG SIDEMPUAN',12),
(1301,1,'PESISIR SELATAN',13),
(1302,2,'SOLOK',13),
(1303,3,'SAWAHLUNTO/SIJUNJUNG',13),
(1304,4,'TANAH DATAR',13),
(1305,5,'PADANG PARIAMAN',13),
(1306,6,'AGAM',13),
(1307,7,'LIMA PULUH KOTO',13),
(1308,8,'PASAMAN',13),
(1309,9,'KEPULAUAN MENTAWAI',13),
(1310,10,'SOLOK SELATAN',13),
(1311,11,'DHARMASRAYA',13),
(1312,12,'PASAMAN BARAT',13),
(1371,71,'KOTA PADANG',13),
(1372,72,'KOTA SOLOK',13),
(1373,73,'KOTA SAWAH LUNTO',13),
(1374,74,'KOTA PADANG PANJANG',13),
(1375,75,'KOTA BUKITTINGGI',13),
(1376,76,'KOTA PAYAKUMBUH',13),
(1377,77,'KOTA PARIAMAN',13),
(1401,1,'KAMPAR',14),
(1402,2,'INDRAGIRI HULU',14),
(1403,3,'BENGKALIS',14),
(1404,4,'INDRAGIRI HILIR',14),
(1405,5,'PELALAWAN',14),
(1406,6,'ROKAN HULU',14),
(1407,7,'ROKAN HILIR',14),
(1408,8,'SIAK',14),
(1409,9,'KUANTAN SINGINGI',14),
(1471,71,'KOTA PEKANBARU',14),
(1472,72,'KOTA DUMAI',14),
(1501,1,'KERINCI',15),
(1502,2,'MERANGIN',15),
(1503,3,'SAROLANGUN',15),
(1504,4,'BATANG HARI',15),
(1505,5,'MUARO JAMBI',15),
(1506,6,'TANJUNG JABUNG BARAT',15),
(1507,7,'TANJUNG JABUNG TIMUR',15),
(1508,8,'BUNGO',15),
(1509,9,'TEBO',15),
(1571,71,'KOTA JAMBI',15),
(1601,1,'OGAN KOMERING ULU',16),
(1602,2,'OGAN KOMERING ILIR',16),
(1603,3,'MUARA ENIM',16),
(1604,4,'LAHAT',16),
(1605,5,'MUSI RAWAS',16),
(1606,6,'MUSI BANYUASIN',16),
(1607,7,'BANYUASIN',16),
(1608,8,'OGAN KOMERING ULU TIMUR',16),
(1609,9,'OGAN KOMERING ULU SELATAN',16),
(1610,10,'OGAN ILIR',16),
(1611,11,'EMPAT LAWANG',16),
(1671,71,'KOTA PALEMBANG',16),
(1672,72,'KOTA PAGAR ALAM',16),
(1673,73,'KOTA LUBUK LINGGAU',16),
(1674,74,'KOTA PRABUMULIH',16),
(1701,1,'BENGKULU SELATAN',17),
(1702,2,'REJANG LEBONG',17),
(1703,3,'BENGKULU UTARA',17),
(1704,4,'KAUR',17),
(1705,5,'SELUMA',17),
(1706,6,'MUKO-MUKO',17),
(1707,7,'LEBONG',17),
(1708,8,'KEPAHIANG',17),
(1771,71,'BENGKULU',17),
(1801,1,'LAMPUNG SELATAN',18),
(1802,2,'LAMPUNG TENGAH',18),
(1803,3,'LAMPUNG UTARA',18),
(1804,4,'LAMPUNG BARAT',18),
(1805,5,'TULANG BAWANG',18),
(1806,6,'TANGGAMUS',18),
(1807,7,'LAMPUNG TIMUR',18),
(1808,8,'WAY KANAN',18),
(1871,71,'BANDAR LAMPUNG',18),
(1872,72,'METRO',18),
(1901,1,'BANGKA',19),
(1902,2,'BELITUNG',19),
(1903,3,'BANGKA SELATAN',19),
(1904,4,'BANGKA TENGAH',19),
(1905,5,'BANGKA BARAT',19),
(1906,6,'BELITUNG TIMUR',19),
(1971,71,'KOTA PANGKAL PINANG',19),
(2101,1,'BINTAN',21),
(2102,2,'KARIMUN',21),
(2103,3,'NATUNA',21),
(2104,4,'LINGGA',21),
(2171,71,'KOTA BATAM',21),
(2172,72,'KOTA TANJUNG PINANG',21),
(3101,1,'ADM. KEPULAUAN SERIBU',31),
(3171,71,'KODYA JAKARTA PUSAT',31),
(3172,72,'KODYA JAKARTA UTARA',31),
(3173,73,'KODYA JAKARTA BARAT',31),
(3174,74,'KODYA JAKARTA SELATAN',31),
(3175,75,'KODYA JAKARTA TIMUR',31),
(3201,1,'BOGOR',32),
(3202,2,'SUKABUMI',32),
(3203,3,'CIANJUR',32),
(3204,4,'BANDUNG',32),
(3205,5,'GARUT',32),
(3206,6,'TASIKMALAYA',32),
(3207,7,'CIAMIS',32),
(3208,8,'KUNINGAN',32),
(3209,9,'CIREBON',32),
(3210,10,'MAJALENGKA',32),
(3211,11,'SUMEDANG',32),
(3212,12,'INDRAMAYU',32),
(3213,13,'SUBANG',32),
(3214,14,'PURWAKARTA',32),
(3215,15,'KARAWANG',32),
(3216,16,'BEKASI',32),
(3217,17,'BANDUNG BARAT',32),
(3271,71,'KOTA BOGOR',32),
(3272,72,'KOTA SUKABUMI',32),
(3273,73,'KOTA BANDUNG',32),
(3274,74,'KOTA CIREBON',32),
(3275,75,'KOTA BEKASI',32),
(3276,76,'KOTA DEPOK',32),
(3277,77,'KOTA CIMAHI',32),
(3278,78,'KOTA TASIKMALAYA',32),
(3279,79,'KOTA BANJAR',32),
(3301,1,'CILACAP',33),
(3302,2,'BANYUMAS',33),
(3303,3,'PURBALINGGA',33),
(3304,4,'BANJARNEGARA',33),
(3305,5,'KEBUMEN',33),
(3306,6,'PURWOREJO',33),
(3307,7,'WONOSOBO',33),
(3308,8,'MAGELANG',33),
(3309,9,'BOYOLALI',33),
(3310,10,'KLATEN',33),
(3311,11,'SUKOHARJO',33),
(3312,12,'WONOGIRI',33),
(3313,13,'KARANGANYAR',33),
(3314,14,'SRAGEN',33),
(3315,15,'GROBOGAN',33),
(3316,16,'BLORA',33),
(3317,17,'REMBANG',33),
(3318,18,'PATI',33),
(3319,19,'KUDUS',33),
(3320,20,'JEPARA',33),
(3321,21,'DEMAK',33),
(3322,22,'SEMARANG',33),
(3323,23,'TEMANGGUNG',33),
(3324,24,'KENDAL',33),
(3325,25,'BATANG',33),
(3326,26,'PEKALONGAN',33),
(3327,27,'PEMALANG',33),
(3328,28,'TEGAL',33),
(3329,29,'BREBES',33),
(3371,71,'KOTA MAGELANG',33),
(3372,72,'KOTA SURAKARTA',33),
(3373,73,'KOTA SALATIGA',33),
(3374,74,'KOTA SEMARANG',33),
(3375,75,'KOTA PEKALONGAN',33),
(3376,76,'KOTA TEGAL',33),
(3401,1,'KULON PROGO',34),
(3402,2,'BANTUL',34),
(3403,3,'GUNUNG KIDUL',34),
(3404,4,'SLEMAN',34),
(3471,71,'KOTA YOGYAKARTA',34),
(3501,1,'PACITAN',35),
(3502,2,'PONOROGO',35),
(3503,3,'TRENGGALEK',35),
(3504,4,'TULUNGAGUNG',35),
(3505,5,'BLITAR',35),
(3506,6,'KEDIRI',35),
(3507,7,'MALANG',35),
(3508,8,'LUMAJANG',35),
(3509,9,'JEMBER',35),
(3510,10,'BANYUWANGI',35),
(3511,11,'BONDOWOSO',35),
(3512,12,'SITUBONDO',35),
(3513,13,'PROBOLINGGO',35),
(3514,14,'PASURUAN',35),
(3515,15,'SIDOARJO',35),
(3516,16,'MOJOKERTO',35),
(3517,17,'JOMBANG',35),
(3518,18,'NGANJUK',35),
(3519,19,'MADIUN',35),
(3520,20,'MAGETAN',35),
(3521,21,'NGAWI',35),
(3522,22,'BOJONEGORO',35),
(3523,23,'TUBAN',35),
(3524,24,'LAMONGAN',35),
(3525,25,'GRESIK',35),
(3526,26,'BANGKALAN',35),
(3527,27,'SAMPANG',35),
(3528,28,'PAMEKASAN',35),
(3529,29,'SUMENEP',35),
(3571,71,'KOTA KEDIRI',35),
(3572,72,'KOTA BLITAR',35),
(3573,73,'KOTA MALANG',35),
(3574,74,'KOTA PROBOLINGGO',35),
(3575,75,'KOTA PASURUAN',35),
(3576,76,'KOTA MOJOKERTO',35),
(3577,77,'KOTA MADIUN',35),
(3578,78,'KOTA SURABAYA',35),
(3579,79,'KOTA BATU',35),
(3601,1,'PANDEGLANG',36),
(3602,2,'LEBAK',36),
(3603,3,'TANGERANG',36),
(3604,4,'SERANG',36),
(3671,71,'KOTA TANGERANG',36),
(3672,72,'KOTA CILEGON',36),
(5101,1,'JEMBRANA',51),
(5102,2,'TABANAN',51),
(5103,3,'BADUNG',51),
(5104,4,'GIANYAR',51),
(5105,5,'KLUNGKUNG',51),
(5106,6,'BANGLI',51),
(5107,7,'KARANGASEM',51),
(5108,8,'BULELENG',51),
(5171,71,'KOTA DENPASAR',51),
(5201,1,'LOMBOK BARAT',52),
(5202,2,'LOMBOK TENGAH',52),
(5203,3,'LOMBOK TIMUR',52),
(5204,4,'SUMBAWA',52),
(5205,5,'DOMPU',52),
(5206,6,'BIMA',52),
(5207,7,'SUMBAWA BARAT',52),
(5271,71,'KOTA MATARAM',52),
(5272,72,'KOTA BIMA',52),
(5301,1,'KUPANG',53),
(5302,2,'TIMOR TENGAH SELATAN',53),
(5303,3,'TIMOR TENGAH UTARA',53),
(5304,4,'BELU',53),
(5305,5,'ALOR',53),
(5306,6,'FLORES TIMUR',53),
(5307,7,'SIKKA',53),
(5308,8,'ENDE',53),
(5309,9,'NGADA',53),
(5310,10,'MANGGARAI',53),
(5311,11,'SUMBA TIMUR',53),
(5312,12,'SUMBA BARAT',53),
(5313,13,'LEMBATA',53),
(5314,14,'ROTE NDAO',53),
(5315,15,'MANGGARAI BARAT',53),
(5316,16,'NAGEKEO',53),
(5317,17,'SUMBA TENGAH',53),
(5318,18,'SUMBA BARAT DAYA',53),
(5371,71,'KOTA KUPANG',53),
(6101,1,'SAMBAS',61),
(6102,2,'PONTIANAK',61),
(6103,3,'SANGGAU',61),
(6104,4,'KETAPANG',61),
(6105,5,'SINTANG',61),
(6106,6,'KAPUAS HULU',61),
(6107,7,'BENGKAYANG',61),
(6108,8,'LANDAK',61),
(6109,9,'SEKADAU',61),
(6110,10,'MELAWI',61),
(6111,11,'KAYONG UTARA',61),
(6171,71,'KOTA PONTIANAK',61),
(6172,72,'KOTA SINGKAWANG',61),
(6201,1,'KOTA',62),
(6202,2,'KOTA',62),
(6203,3,'KAPU',62),
(6204,4,'BARI',62),
(6205,5,'BARI',62),
(6206,6,'KATI',62),
(6207,7,'SERU',62),
(6208,8,'SUKA',62),
(6209,9,'LAMA',62),
(6210,10,'GUNU',62),
(6211,11,'PULA',62),
(6212,12,'MURU',62),
(6213,13,'BARI',62),
(6271,71,'KOTA',62),
(6301,1,'TANAH LAUT',63),
(6302,2,'KOTA BARU',63),
(6303,3,'BANJAR',63),
(6304,4,'BARITO KUALA',63),
(6305,5,'TAPIN',63),
(6306,6,'HULU SUNGAI SELATAN',63),
(6307,7,'HULU SUNGAI TENGAH',63),
(6308,8,'HULU SUNGAI UTARA',63),
(6309,9,'TABALONG',63),
(6310,10,'TANAH BUMBU',63),
(6311,11,'BALANGAN',63),
(6371,71,'KOTA BANJARMASIN',63),
(6372,72,'KOTA BANJAR BARU',63),
(6401,1,'PASIR',64),
(6402,2,'KUTAI KERTANEGARA',64),
(6403,3,'BERAU',64),
(6404,4,'BULUNGAN',64),
(6405,5,'NUNUKAN',64),
(6406,6,'MALINAU',64),
(6407,7,'KUTAI BARAT',64),
(6408,8,'KUTAI TIMUR',64),
(6409,9,'PENAJAM PASER UTARA',64),
(6471,71,'KOTA BALIK PAPAN',64),
(6472,72,'KOTA SAMARINDA',64),
(6473,73,'KOTA TARAKAN',64),
(6474,74,'KOTA BONTANG',64),
(7101,1,'BOLAANG MONGONDO',71),
(7102,2,'MINAHASA',71),
(7103,3,'KEPULAUAN SANGIHE',71),
(7104,4,'KEPULAUAN TALAUD',71),
(7105,5,'MINAHASA SELATAN',71),
(7106,6,'MINAHASA UTARA',71),
(7107,7,'MINAHASA TENGGARA',71),
(7108,8,'BOLAANG MONGONDOW UTARA',71),
(7109,9,'KEPULAUAN SIAU TAGULANDANG BIARO ATAU SITARO',71),
(7171,71,'KOTA MANADO',71),
(7172,72,'KOTA BITUNG',71),
(7173,73,'KOTA TOMOHON',71),
(7174,74,'KOTA KOTAMOBAGU',71),
(7201,1,'BANGGAI',72),
(7202,2,'POSO',72),
(7203,3,'DONGGALA',72),
(7204,4,'TOLI-TOLI',72),
(7205,5,'BUOL',72),
(7206,6,'MOROWALI',72),
(7207,7,'BANGGAI KEPULAUAN',72),
(7208,8,'PARIMO',72),
(7209,9,'TOJO UNA-UNA',72),
(7271,71,'KOTA PALU',72),
(7301,1,'SELAYAR',73),
(7302,2,'BULUKUMBA',73),
(7303,3,'BANTAENG',73),
(7304,4,'JENEPONTO',73),
(7305,5,'TAKALAR',73),
(7306,6,'GOWA',73),
(7307,7,'SINJAI',73),
(7308,8,'BONE',73),
(7309,9,'MAROS',73),
(7310,10,'PANGKAJENE DAN KEPULAUAN',73),
(7311,11,'BARRU',73),
(7312,12,'SOPPENG',73),
(7313,13,'WAJO',73),
(7314,14,'SIDENRENG RAPPANG',73),
(7315,15,'PINRANG',73),
(7316,16,'ENREKANG',73),
(7317,17,'LUWU',73),
(7318,18,'TANA TORAJA',73),
(7322,22,'LUWU UTARA',73),
(7324,24,'LUWU TIMUR',73),
(7371,71,'KOTA MAKASSAR',73),
(7372,72,'KOTA PARE-PARE',73),
(7373,73,'KOTA PALOPO',73),
(7401,1,'KOLAKA',74),
(7402,2,'KENDARI',74),
(7403,3,'MUNA',74),
(7404,4,'BUTON',74),
(7405,5,'KONAWE SELATAN',74),
(7406,6,'BOMBANA',74),
(7407,7,'WAKATOBI',74),
(7408,8,'KOLAKA UTARA',74),
(7409,9,'KONAWE UTARA',74),
(7410,10,'BUTON UTARA',74),
(7471,71,'KOTA KENDARI',74),
(7472,72,'KOTA BAU-BAU',74),
(7501,1,'GORONTALO',75),
(7502,2,'BOALEMO',75),
(7503,3,'BONE BOLANGO',75),
(7504,4,'POHUWATO',75),
(7505,5,'GORONTALO UTARA',75),
(7571,71,'KOTA GORONTALO',75),
(7601,1,'MAMUJU UTARA',76),
(7602,2,'MAMUJU',76),
(7603,3,'MAMASA',76),
(7604,4,'POLEWALI MAMASA',76),
(7605,5,'MAJENE',76),
(8101,1,'MALUKU TENGAH',81),
(8102,2,'MALUKU TENGGARA',81),
(8103,3,'MALUKU TENGGARA BARAT',81),
(8104,4,'BURU',81),
(8105,5,'KEPULAUAN ARU',81),
(8106,6,'SERAM BAGIAN BARAT',81),
(8107,7,'SERAM BAGIAN TIMUR',81),
(8171,71,'KOTA AMBON',81),
(8201,1,'HALMAHERA BARAT',82),
(8202,2,'HALMAHERA TENGAH',82),
(8203,3,'HALMAHERA UTARA',82),
(8204,4,'HALMAHERA SELATAN',82),
(8205,5,'KEPULAUAN SULA',82),
(8206,6,'HALMAHERA TIMUR',82),
(8271,71,'KOTA TERNATE',82),
(8272,72,'KOTA TIDORE KEPULAUAN',82),
(9101,1,'MERAUKE',91),
(9102,2,'JAYAWIJAYA',91),
(9103,3,'JAYAPURA',91),
(9104,4,'NABIRE',91),
(9105,5,'YAPEN WAROPEN',91),
(9106,6,'BIAK NUMFOR',91),
(9107,7,'PUNCAK JAYA',91),
(9108,8,'PANIAI',91),
(9109,9,'MIMIKA',91),
(9110,10,'SARMI',91),
(9111,11,'KEEROM',91),
(9112,12,'PEGUNUNGAN BINTANG',91),
(9113,13,'YAHUKIMO',91),
(9114,14,'TOLIKARA',91),
(9115,15,'WAROPEN',91),
(9116,16,'BOVEN DIGOEL',91),
(9117,17,'MAPPI',91),
(9118,18,'ASMAT',91),
(9119,19,'SUPIORI',91),
(9120,20,'MAMBERAMO RAYA',91),
(9171,71,'KOTA JAYAPURA',91),
(9201,1,'SORONG',92),
(9202,2,'MANOKWARI',92),
(9203,3,'FAKFAK',92),
(9204,4,'SORONG SELATAN',92),
(9205,5,'RAJA AMPAT',92),
(9206,6,'TELUK BINTUNI',92),
(9207,7,'TELUK WONDAMA',92),
(9208,8,'KAIMANA',92),
(9271,71,'KOTA SORONG',92);

/*Table structure for table `m_kecamatan` */

CREATE TABLE `m_kecamatan` (
  `id_kecamatan` int(11) NOT NULL,
  `No_Kec` int(3) NOT NULL,
  `Nama_Kec` varchar(255) NOT NULL,
  `id_kabupaten` int(11) NOT NULL,
  PRIMARY KEY (`id_kecamatan`),
  KEY `id_kabupaten` (`id_kabupaten`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `m_kecamatan` */

insert  into `m_kecamatan`(`id_kecamatan`,`No_Kec`,`Nama_Kec`,`id_kabupaten`) values 
(110101,1,'BAKONGAN',1101),
(110102,2,'KLUET UTARA',1101),
(110103,3,'KLUET SELATAN',1101),
(110104,4,'LABUHAN HAJI',1101),
(110105,5,'MEUKEK',1101),
(110106,6,'SAMA DUA',1101),
(110107,7,'SAWANG',1101),
(110108,8,'TAPAK TUAN',1101),
(110109,9,'TRUMON',1101),
(110110,10,'PASIE RAJA',1101),
(110201,1,'LAWE ALAS',1102),
(110202,2,'LAWE SIGALA-GALA',1102),
(110203,3,'BAMBEL',1102),
(110204,4,'BABUSSALAM',1102),
(110205,5,'BADAR',1102),
(110206,6,'BABUL MAKMUR',1102),
(110207,7,'DARUL HASANAH',1102),
(110212,12,'KUMALA',1102),
(110301,1,'DARUL AMAN',1103),
(110302,2,'JULOK',1103),
(110303,3,'IDI RAYEUK',1103),
(110304,4,'BIREM BAYEUN',1103),
(110305,5,'SERBAJADI',1103),
(110306,6,'NURUSSALAM',1103),
(110307,7,'PEUREULAK',1103),
(110308,8,'RANTAU SELAMAT',1103),
(110309,9,'SIMPANG ULIM',1103),
(110310,10,'RANTO PEURELAK',1103),
(110311,11,'PANTE BIDARI',1103),
(110312,12,'MADAT',1103),
(110313,13,'INDRA MAKMUR',1103),
(110314,14,'IDI TUNONG',1103),
(110315,15,'BANDA ALAM',1103),
(110316,16,'PEUDAWA',1103),
(110317,17,'PEUREULAK TIMUR',1103),
(110318,18,'PEURELAK BARAT',1103),
(110319,19,'SUNGAI RAYA',1103),
(110401,1,'LINGE ISAQ',1104),
(110402,2,'SILIH NARA',1104),
(110403,3,'BEBESEN',1104),
(110407,7,'PEGASING',1104),
(110408,8,'BINTANG',1104),
(110410,10,'KETOL',1104),
(110411,11,'KEBAYAKAN',1104),
(110412,12,'KUTE PANANG',1104),
(110413,13,'CELALA',1104),
(110417,17,'LUT TAWAR',1104),
(110418,18,'KOTA TAKENGON',1104),
(110501,1,'JOHAN PAHLAWAN',1105),
(110502,2,'KAWAY XVI',1105),
(110503,3,'SUNGAI MAS',1105),
(110504,4,'WOYLA',1105),
(110505,5,'SAMA TIGA',1105),
(110506,6,'BUBON',1105),
(110507,7,'ARONGAN LAMBALEK',1105),
(110508,8,'PANTE CEUREUMEN',1105),
(110509,9,'MEUREUBO',1105),
(110601,1,'LHOONG',1106),
(110602,2,'LHOKNGA',1106),
(110603,3,'INDRAPURI',1106),
(110604,4,'SEULIMEUM',1106),
(110605,5,'MONTASIK',1106),
(110606,6,'SUKA MAKMUR',1106),
(110607,7,'DARUL IMARAH',1106),
(110608,8,'PEUKAN BADA',1106),
(110609,9,'MESJID RAYA',1106),
(110610,10,'INGIN JAYA',1106),
(110611,11,'KUTA BARO',1106),
(110612,12,'DARUSSALAM',1106),
(110613,13,'PULO ACEH',1106),
(110614,14,'LEMBAH SEULAWAH',1106),
(110615,15,'KOTA JANTHO',1106),
(110616,16,'KUTA COT GLIE',1106),
(110617,17,'KUTA MALAKA',1106),
(110618,18,'SIMPANG TIGA',1106),
(110619,19,'DARUL KAMAL',1106),
(110620,20,'BAITUSSALAM',1106),
(110621,21,'KRUENG BARONA JAYA',1106),
(110622,22,'LEUPUNG',1106),
(110703,3,'BATEE',1107),
(110704,4,'DELIMA',1107),
(110705,5,'GEUMPANG',1107),
(110706,6,'GLUMPANG TIGA',1107),
(110707,7,'INDRA JAYA',1107),
(110708,8,'KEMBANG TANJUNG',1107),
(110709,9,'KOTA SIGLI',1107),
(110711,11,'MILA',1107),
(110712,12,'MUARA TIGA',1107),
(110713,13,'MUTIARA',1107),
(110714,14,'PADANG TIJI',1107),
(110715,15,'PEUKAN BARO',1107),
(110716,16,'PIDIE',1107),
(110717,17,'SAKTI',1107),
(110718,18,'SIMPANG TIGA',1107),
(110719,19,'TANGSE',1107),
(110721,21,'TIRO/TRUSEB',1107),
(110722,22,'TITEUA/KEUMALA',1107),
(110724,24,'MUTIARA TIMUR',1107),
(110725,25,'GRONG GRONG',1107),
(110727,27,'MANE',1107),
(110729,29,'GLUMPANG BARO',1107),
(110801,1,'BAKTIYA',1108),
(110802,2,'DEWANTARA',1108),
(110803,3,'KUTA MAKMUR',1108),
(110804,4,'LHOK SUKON',1108),
(110805,5,'MATANGKULI',1108),
(110806,6,'MUARA BATU',1108),
(110807,7,'MEURAH MULIA',1108),
(110808,8,'SAMUDERA',1108),
(110809,9,'SEUNUDON',1108),
(110810,10,'SYAMTALIRA ARON',1108),
(110811,11,'SYAMTALIRA BAYU',1108),
(110812,12,'TANAH LUAS',1108),
(110813,13,'TANAH PASIR',1108),
(110814,14,'TANAH JAMBO AYE',1108),
(110815,15,'SAWANG',1108),
(110816,16,'NISAM',1108),
(110817,17,'COT GIREK',1108),
(110818,18,'LANGKAHAN',1108),
(110819,19,'BAKTIYA BARAT',1108),
(110820,20,'PAYA BAKONG',1108),
(110821,21,'NIBONG',1108),
(110822,22,'SIMPANG KERAMAT',1108),
(110901,1,'SIMEULU TENGAH',1109),
(110902,2,'SALANG',1109),
(110904,4,'SIMEULU TIMUR',1109),
(110906,6,'SIMEULU BARAT',1109),
(110907,7,'TEUPAH SELATAN',1109),
(111001,1,'PULAU BANYAK',1110),
(111002,2,'SIMPANG KANAN',1110),
(111004,4,'SINGKIL',1110),
(111006,6,'GUNUNG MERIAH',1110),
(111009,9,'KOTA BAHARU',1110),
(111010,10,'SINGKIL UTARA',1110),
(111011,11,'DANAU PARIS',1110),
(111012,12,'SURO MAKMUR',1110),
(111013,13,'SINGKOHOR',1110),
(111014,14,'KOTA BAHARU',1110),
(111101,1,'SAMALANGA',1111),
(111102,2,'JEUNIEB',1111),
(111103,3,'PEUDADA',1111),
(111104,4,'JEUMPA',1111),
(111105,5,'PEUSANGAN',1111),
(111106,6,'MAKMUR',1111),
(111107,7,'GANDA PURA',1111),
(111108,8,'PANDRAH',1111),
(111109,9,'JULI',1111),
(111110,10,'JANGKA',1111),
(111201,1,'BLANG PIDIE',1112),
(111202,2,'TANGAN-TANGAN',1112),
(111203,3,'MANGGENG',1112),
(111204,4,'SUSOH',1112),
(111205,5,'KUALA BATEE',1112),
(111206,6,'BABAH ROT',1112),
(111301,1,'KUTA PANJANG',1113),
(111302,2,'BLANG KEJEREN',1113),
(111303,3,'RIKIT GAIB',1113),
(111304,4,'TERANGON',1113),
(111305,5,'PINDING',1113),
(111401,1,'TEUNOM',1114),
(111402,2,'KRUENG SABEE',1114),
(111403,3,'SETIA BAKTI',1114),
(111404,4,'SAMPOINIET',1114),
(111405,5,'JAYA',1114),
(111406,6,'PANGA',1114),
(111501,1,'KUALA',1115),
(111502,2,'SEUNAGAN',1115),
(111503,3,'SEUNAGAN TIMUR',1115),
(111504,4,'BEUTONG',1115),
(111505,5,'DARUL MAKMUR',1115),
(111601,1,'MANYAK PAYED',1116),
(111602,2,'BENDAHARA',1116),
(111603,3,'KARANG BARU',1116),
(111604,4,'SERUWAY',1116),
(111605,5,'KOTA KUALA SIMPANG',1116),
(111606,6,'KEJURUAN MUDA',1116),
(111607,7,'TAMIANG HULU',1116),
(111608,8,'RANTAU',1116),
(111701,1,'TIMANG GAJAH',1117),
(111702,2,'PINTU RIME GAYO',1117),
(111703,3,'BUKIT',1117),
(111704,4,'WIH PESEM',1117),
(111705,5,'BANDAR',1117),
(111706,6,'SYIAH UTAMA',1117),
(111707,7,'PERMATA',1117),
(111801,1,'MEUREUDU',1118),
(111802,2,'ULIM',1118),
(111803,3,'JANGKA BUAYA',1118),
(111804,4,'BANDAR DUA',1118),
(111805,5,'MEURAH DUA',1118),
(111806,6,'BANDAR BARU',1118),
(111807,7,'PANTERAJA',1118),
(111808,8,'TRIENGGADENG',1118),
(117101,1,'BAITURRAHMAN',1171),
(117102,2,'KUTA ALAM',1171),
(117103,3,'MEURAKSA',1171),
(117104,4,'SYIAH KUALA',1171),
(117105,5,'LUENG BATA',1171),
(117106,6,'KUTA RAJA',1171),
(117107,7,'BANDA RAYA',1171),
(117108,8,'JAYA BARU',1171),
(117109,9,'ULEE KARENG',1171),
(117201,1,'SUKAKARYA',1172),
(117202,2,'SUKAJAYA',1172),
(117301,1,'MUARA DUA',1173),
(117302,2,'BANDA SAKTI',1173),
(117303,3,'BLANG MANGAT',1173),
(117304,4,'MUARA SATU',1173),
(117401,1,'LANGSA TIMUR',1174),
(117402,2,'LANGSA BARAT',1174),
(117403,3,'LANGSA KOTA',1174),
(117501,1,'SIMPANG KIRI',1175),
(117502,2,'PENANGGALAN',1175),
(117503,3,'RUNDENG',1175),
(117504,4,'SULTAN DAULAT',1175),
(117505,5,'LONGKIB',1175),
(120101,1,'BARUS',1201),
(120102,2,'SORKAM',1201),
(120103,3,'SIBOLGA',1201),
(120104,4,'LUMUT',1201),
(120105,5,'MANDUAMAS',1201),
(120106,6,'KOLANG',1201),
(120107,7,'TAPIAN NAULI',1201),
(120108,8,'SIBABANGUN',1201),
(120109,9,'SOSOR GADONG',1201),
(120110,10,'SORKAM BARAT',1201),
(120111,11,'SIRANDORUNG',1201),
(120112,12,'ANDAM DEWI',1201),
(120113,13,'SITAHUIS',1201),
(120114,14,'TUKKA',1201),
(120115,15,'BADIRI',1201),
(120201,1,'TARUTUNG',1202),
(120202,2,'SIATAS BARITA',1202),
(120203,3,'ADIANKOTING',1202),
(120204,4,'SIPOHOLON',1202),
(120205,5,'PAHAE JULU',1202),
(120206,6,'PAHAE JAE',1202),
(120207,7,'SIMANGUMBAN',1202),
(120208,8,'PURBATUA',1202),
(120209,9,'SIBORONG-BORONG',1202),
(120210,10,'PAGARAN',1202),
(120211,11,'PARMONANGAN',1202),
(120212,12,'SIPAHUTAR',1202),
(120213,13,'PANGARIBUAN',1202),
(120214,14,'GAROGA',1202),
(120215,15,'MUARA',1202),
(120301,1,'PADANGSIDIMPUAN BARAT',1203),
(120302,2,'BATANG TORU',1203),
(120303,3,'PADANGSIDIMPUAN TIMUR',1203),
(120304,4,'SIPIROK',1203),
(120305,5,'SAIPAR DOLOK HOLE',1203),
(120306,6,'SIAIS',1203),
(120307,7,'BATANG ANGKOLA',1203),
(120308,8,'PADANG BOLAK',1203),
(120309,9,'DOLOK',1203),
(120310,10,'BARUMUN TENGAH',1203),
(120311,11,'SOSOPAN',1203),
(120312,12,'BARUMUN',1203),
(120313,13,'SOSA',1203),
(120314,14,'ARSE',1203),
(120315,15,'HALONGONAN',1203),
(120316,16,'PADANG BOLAK JULU',1203),
(120317,17,'BATANG ONANG',1203),
(120318,18,'DOLOK SIGOMPULON',1203),
(120319,19,'HUTA RAJA TINGGI',1203),
(120320,20,'MARANCAR',1203),
(120321,21,'SAYUR MATINGGI',1203),
(120322,22,'AEK BILAH',1203),
(120323,23,'PORTIBI',1203),
(120324,24,'HURISTAK',1203),
(120325,25,'SIMANGAMBAT',1203),
(120326,26,'ULU BARUMUN',1203),
(120327,27,'LUBUK BARUMUN',1203),
(120328,28,'BATANG LUBU SUTAM',1203),
(120401,1,'GUNUNGSITOLI',1204),
(120402,2,'TUHEMBERUA',1204),
(120403,3,'LAHEWA',1204),
(120404,4,'ALASA',1204),
(120405,5,'HILIDUHO',1204),
(120406,6,'GIDO',1204),
(120407,7,'LOLOFITU MOI',1204),
(120408,8,'MANDREHE',1204),
(120409,9,'SIROMBU',1204),
(120410,10,'IDANO  GAWO',1204),
(120411,11,'BAWOLATO',1204),
(120412,12,'NAMOHALU ESIWA',1204),
(120413,13,'LOTU',1204),
(120414,14,'AFULU',1204),
(120501,1,'BOHOROK',1205),
(120502,2,'SALAPIAN',1205),
(120503,3,'KUALA',1205),
(120504,4,'SEI BINGAI',1205),
(120505,5,'BINJAI',1205),
(120506,6,'SELESAI',1205),
(120507,7,'STABAT',1205),
(120508,8,'WAMPU',1205),
(120509,9,'SECANGGANG',1205),
(120510,10,'HINAI',1205),
(120511,11,'TG. PURA',1205),
(120512,12,'PD. TUALANG',1205),
(120513,13,'GEBANG',1205),
(120514,14,'BABALAN',1205),
(120515,15,'PANGKALAN SUSU',1205),
(120516,16,'BESITANG',1205),
(120517,17,'SEI LEPAN',1205),
(120518,18,'BRANDAN BARAT',1205),
(120519,19,'BT. SERANGAN',1205),
(120520,20,'SWT. SEBERANG',1205),
(120601,1,'KABANJAHE',1206),
(120602,2,'BERASTAGI',1206),
(120603,3,'BARUSJAHE',1206),
(120604,4,'TIGA PANAH',1206),
(120605,5,'MEREK',1206),
(120606,6,'MUNTHE',1206),
(120607,7,'JUHAR',1206),
(120608,8,'TIGA BINANGA',1206),
(120609,9,'LAU BELANG',1206),
(120610,10,'MARDINGDING',1206),
(120611,11,'PAYUNG',1206),
(120612,12,'SIMPANG EMPAT',1206),
(120613,13,'KUTA BULUH',1206),
(120701,1,'GUNUNG MERIAH',1207),
(120702,2,'TANJUNG MORAWA',1207),
(120703,3,'SIBOLANGIT',1207),
(120704,4,'KUTALIMBARU',1207),
(120705,5,'PANCUR BATU',1207),
(120706,6,'NAMO RAMBE',1207),
(120707,7,'BIRU-BIRU',1207),
(120708,8,'STM HILIR',1207),
(120709,9,'BANGUN PURBA',1207),
(120719,19,'GALANG',1207),
(120720,20,'SINEMBAH TANJUNG MUDA HULU',1207),
(120721,21,'PATUMBAK',1207),
(120722,22,'DELI TUA',1207),
(120723,23,'SUNGGAL',1207),
(120724,24,'HAMPARAN PERAK',1207),
(120725,25,'LABUHAN DELI',1207),
(120726,26,'PERCUT SEI TUAN',1207),
(120727,27,'BATANG KUIS',1207),
(120728,28,'LUBUK PAKAM',1207),
(120731,31,'PAGAR MERBAU',1207),
(120732,32,'PANTAI LABU',1207),
(120733,33,'BERINGIN',1207),
(120801,1,'SIANTAR',1208),
(120802,2,'GUNUNG MALELA',1208),
(120803,3,'GUNUNG MALIGAS',1208),
(120804,4,'PANEI',1208),
(120805,5,'PANOMBEIAN PANEI',1208),
(120806,6,'JORLANG HATARAN',1208),
(120807,7,'RAYA KAHEAN',1208),
(120808,8,'BOSAR MALIGAS',1208),
(120809,9,'SIDAMANIK',1208),
(120810,10,'PEMATANG SIDAMANIK',1208),
(120811,11,'TANAH JAWA',1208),
(120812,12,'HATONDUHAN',1208),
(120813,13,'DOLOK PANRIBUAN',1208),
(120814,14,'PURBA',1208),
(120815,15,'HARANGGAOL HORISON',1208),
(120816,16,'GIRSANG SIPANGAN BOLON',1208),
(120817,17,'DOLOK BATU NANGGAR',1208),
(120818,18,'HUTA BAYU RAJA',1208),
(120819,19,'JAWA MARAJA BAH JAMBI',1208),
(120820,20,'DOLOK PARDAMEAN',1208),
(120821,21,'PEMATANG BANDAR',1208),
(120822,22,'BANDAR HULUAN',1208),
(120823,23,'BANDAR',1208),
(120824,24,'BANDAR MASILAM',1208),
(120825,25,'SILIMAKUTA',1208),
(120826,26,'DOLOK SILAU',1208),
(120827,27,'SILOU KAHEAN',1208),
(120828,28,'TAPIAN DOLOK',1208),
(120829,29,'RAYA',1208),
(120830,30,'UJUNG PADANG',1208),
(120908,8,'MERANTI',1209),
(120909,9,'AIR JOMAN',1209),
(120910,10,'TANJUNG BALAI',1209),
(120911,11,'SEI KEPAYANG',1209),
(120912,12,'SIMPANG EMPAT',1209),
(120913,13,'AIR BATU',1209),
(120914,14,'PULAU RAKYAT',1209),
(120915,15,'BANDAR PULAU',1209),
(120916,16,'BUNTU PANE',1209),
(120917,17,'BANDAR PASIR MANDOGE',1209),
(120918,18,'AEK KUASAN',1209),
(120919,19,'KISARAN BARAT',1209),
(120920,20,'KISARAN TIMUR',1209),
(121001,1,'RANTAU UTARA',1210),
(121002,2,'RANTAU SELATAN',1210),
(121003,3,'KUALUH HULU',1210),
(121004,4,'AEK NATAS',1210),
(121005,5,'NA IX-X',1210),
(121006,6,'MARBAU',1210),
(121007,7,'BILAH BARAT',1210),
(121008,8,'BILAH HILIR',1210),
(121009,9,'BILAH HULU',1210),
(121010,10,'KAMPUNG RAKYAT',1210),
(121011,11,'SILANGKITANG',1210),
(121012,12,'KOTA PINANG',1210),
(121013,13,'TORGAMBA',1210),
(121014,14,'PANGKATAN',1210),
(121015,15,'SUNGAI KANAN',1210),
(121016,16,'KUALUH HILIR',1210),
(121017,17,'KUALUH LEIDONG',1210),
(121018,18,'PANAI TENGAH',1210),
(121019,19,'PANAI HILIR',1210),
(121020,20,'PANAI HULU',1210),
(121021,21,'KUALUH SELATAN',1210),
(121022,22,'AEK KUO',1210),
(121101,1,'SIDIKALANG',1211),
(121102,2,'SUMBUL',1211),
(121103,3,'TIGALINGGA',1211),
(121104,4,'SIEMPAT NEMPU',1211),
(121105,5,'SILIMA PUNGGA-PUNGGA',1211),
(121106,6,'TANAH PINEM',1211),
(121107,7,'SIEMPAT NEMPU HULU',1211),
(121108,8,'SIEMPAT NEMPU HILIR',1211),
(121109,9,'PEGAGAN HILIR',1211),
(121110,10,'PARBULUAN',1211),
(121112,12,'LAE PARIRA',1211),
(121201,1,'BALIGE',1212),
(121202,2,'LAGUBOTI',1212),
(121203,3,'SILAEN',1212),
(121204,4,'HABINSARAN',1212),
(121205,5,'PINTU POHAN MERANTI',1212),
(121206,6,'BOR-BOR',1212),
(121207,7,'PORSEA',1212),
(121208,8,'AJIBATA',1212),
(121209,9,'LUMBAN JULU',1212),
(121210,10,'ULUAN',1212),
(121301,1,'PANYABUNGAN',1213),
(121302,2,'PANYABUNGAN UTARA',1213),
(121303,3,'PANYABUNGAN TIMUR',1213),
(121304,4,'PANYABUNGAN SELATAN',1213),
(121305,5,'PANYABUNGAN BARAT',1213),
(121306,6,'SIABU',1213),
(121307,7,'BUKIT MALINTANG',1213),
(121308,8,'KOTANOPAN',1213),
(121309,9,'LEMBAH SORIK MERAPI',1213),
(121310,10,'TAMBANGAN',1213),
(121311,11,'ULU PUNGKUT',1213),
(121312,12,'MUARA SIPONGI',1213),
(121313,13,'BATANG NATAL',1213),
(121314,14,'LINGGA BAYU',1213),
(121315,15,'BATAHAN',1213),
(121316,16,'NATAL',1213),
(121317,17,'MUARA BATANG GADIS',1213),
(121401,1,'LOLOMATUA',1214),
(121402,2,'GOMO',1214),
(121403,3,'LAHUSA',1214),
(121404,4,'HIBALA',1214),
(121405,5,'PULAU-PULAU BATU',1214),
(121406,6,'TELUK DALAM',1214),
(121407,7,'AMANDRAYA',1214),
(121408,8,'LOLOWAU',1214),
(121501,1,'SITELLU TALI URANG JEHE',1215),
(121502,2,'KERAJAAN',1215),
(121503,3,'SALAK',1215),
(121601,1,'PARLILITAN',1216),
(121602,2,'POLLUNG',1216),
(121603,3,'BAKTI RAJA',1216),
(121604,4,'PARANGINAN',1216),
(121605,5,'LINTONG NIHUTA',1216),
(121606,6,'DOLOK SANGGUL',1216),
(121607,7,'SIJAMA POLANG',1216),
(121608,8,'ONAN GANJANG',1216),
(121609,9,'PAKKAT',1216),
(121610,10,'TARA BINTANG',1216),
(121701,1,'SIANJUR MULA-MULA',1217),
(121702,2,'HARIAN',1217),
(121703,3,'SITIO-TIO',1217),
(121704,4,'ONAN RUNGGU',1217),
(121705,5,'NAINGGOLAN',1217),
(121706,6,'PALIPI',1217),
(121707,7,'RONGGUR NIHUTA',1217),
(121708,8,'PANGURURAN',1217),
(121709,9,'SIMANINDO',1217),
(121801,1,'KOTARIH',1218),
(121802,2,'SIPISPIS',1218),
(121803,3,'DOLOK MERAWAN',1218),
(121804,4,'TEBING TINGGI',1218),
(121805,5,'BANDAR KHALIPAH',1218),
(121806,6,'TANJUNG BERINGIN',1218),
(121807,7,'SEI RAMPAH',1218),
(121808,8,'DOLOK MASIHUL',1218),
(121809,9,'PERBAUNGAN',1218),
(121810,10,'TELUK MENGKUDU',1218),
(121811,11,'PANTAI CERMIN',1218),
(121901,1,'MEDANG DERAS',1219),
(121902,2,'SEI SUKA',1219),
(121903,3,'AIR PUTIH',1219),
(121904,4,'LIMA PULUH',1219),
(121905,5,'TALAWI',1219),
(121906,6,'TANJUNG TIRAM',1219),
(121907,7,'SEI BALAI',1219),
(127101,1,'MEDAN KOTA',1271),
(127102,2,'MEDAN SUNGGAL',1271),
(127103,3,'MEDAN HELVETIA',1271),
(127104,4,'MEDAN DENAI',1271),
(127105,5,'MEDAN BARAT',1271),
(127106,6,'MEDAN DELI',1271),
(127107,7,'MEDAN TUNTUNGAN',1271),
(127108,8,'MEDAN BELAWAN',1271),
(127109,9,'MEDAN AMPLAS',1271),
(127110,10,'MEDAN AREA',1271),
(127111,11,'MEDAN JOHOR',1271),
(127112,12,'MEDAN MARELAN',1271),
(127113,13,'MEDAN LABUHAN',1271),
(127114,14,'MEDAN TEMBUNG',1271),
(127115,15,'MEDAN MAIMUN',1271),
(127116,16,'MEDAN POLONIA',1271),
(127117,17,'MEDAN BARU',1271),
(127118,18,'MEDAN PERJUANGAN',1271),
(127119,19,'MEDAN PETISAH',1271),
(127120,20,'MEDAN TIMUR',1271),
(127121,21,'MEDAN SELAYANG',1271),
(127201,1,'SIANTAR TIMUR',1272),
(127202,2,'SIANTAR BARAT',1272),
(127203,3,'SIANTAR UTARA',1272),
(127204,4,'SIANTAR SELATAN',1272),
(127205,5,'SIANTAR MARIHAT',1272),
(127206,6,'SIANTAR MARTOBA',1272),
(127301,1,'SIBOLGA UTARA',1273),
(127302,2,'SIBOLGA KOTA',1273),
(127303,3,'SIBOLGA SELATAN',1273),
(127304,4,'SIBOLGA SAMBAS',1273),
(127401,1,'TANJUNGBALAI SELATAN',1274),
(127402,2,'TANJUNGBALAI UTARA',1274),
(127403,3,'SEI TUALANG RASO',1274),
(127404,4,'TELUK NIBUNG',1274),
(127405,5,'DATUK BANDAR',1274),
(127501,1,'BINJAI UTARA',1275),
(127502,2,'BINJAI KOTA',1275),
(127503,3,'BINJAI BARAT',1275),
(127504,4,'BINJAI TIMUR',1275),
(127505,5,'BINJAI SELATAN',1275),
(127601,1,'PADANG HULU',1276),
(127602,2,'RAMBUTAN',1276),
(127603,3,'PADANG HILIR',1276),
(127701,1,'PADANGSIDIMPUAN UTARA',1277),
(127702,2,'PADANGSIDIMPUAN SELATAN',1277),
(127703,3,'PADANGSIDIMPUAN BATUNADUA',1277),
(127704,4,'PADANGSIDIMPUAN HUTAIMBARU',1277),
(127705,5,'PADANGSIDIMPUAN TENGGARA',1277),
(130101,1,'PANCUNG SOAL',1301),
(130102,2,'RANAH PESISIR',1301),
(130103,3,'LENGAYANG',1301),
(130104,4,'BATANG KAPAS',1301),
(130105,5,'IV JURAI',1301),
(130106,6,'BAYANG',1301),
(130107,7,'KOTO XI TARUSAN',1301),
(130108,8,'SUTERA',1301),
(130109,9,'LINGGO SARI BAGANTI',1301),
(130110,10,'LUNANG SILAUT',1301),
(130111,11,'BASA IV BALAI TAPAN',1301),
(130203,3,'PANTAI CERMIN',1302),
(130204,4,'LEMBAH GUMANTI',1302),
(130205,5,'PAYUNG SEKAKI',1302),
(130206,6,'LEMBANG JAYA',1302),
(130207,7,'GUNUNG TALANG',1302),
(130208,8,'BUKIT SUNDI',1302),
(130209,9,'IX KOTO SEI LASI',1302),
(130210,10,'KUBUNG',1302),
(130211,11,'X KOTO SINGKARAK',1302),
(130212,12,'X KOTO DIATAS',1302),
(130213,13,'JUNJUNG SIRIH',1302),
(130217,17,'HILIRAN GUMANTI',1302),
(130218,18,'TIGO LURAH',1302),
(130219,19,'DANAU KEMBAR',1302),
(130303,3,'TANJUNG GADANG',1303),
(130304,4,'SIJUNJUNG',1303),
(130305,5,'IV NAGARI',1303),
(130306,6,'KAMANG BARU',1303),
(130307,7,'LUBUK TAROK',1303),
(130308,8,'KOTO TUJUH',1303),
(130309,9,'SUMPUR KUDUS',1303),
(130310,10,'KUPITAN',1303),
(130401,1,'SEPULUH KOTO',1304),
(130402,2,'BATIPUH',1304),
(130403,3,'RAMBATAN',1304),
(130404,4,'LIMA KAUM',1304),
(130405,5,'TANJUNG EMAS',1304),
(130406,6,'LINTAU BUO',1304),
(130407,7,'SUNGAYANG',1304),
(130408,8,'SUNGAI TARAB',1304),
(130409,9,'PARIANGAN',1304),
(130410,10,'SALIMPAUNG',1304),
(130411,11,'PADANG GANTING',1304),
(130501,1,'LUBUK ALUNG',1305),
(130502,2,'BATANG ANAI',1305),
(130503,3,'NAN SABARIS',1305),
(130504,4,'2 X 11 KAYU TANAM',1305),
(130505,5,'VII KOTO SUNGAI SARIK',1305),
(130506,6,'V KOTO KP DALAM',1305),
(130507,7,'SUNGAI GERINGGING',1305),
(130508,8,'SUNGAI LIMAU',1305),
(130509,9,'IV KOTO AUR MALINTANG',1305),
(130510,10,'ULAKAN TAPAKIS',1305),
(130511,11,'SINTUK TOBOH GADANG',1305),
(130512,12,'PADANG SAGO',1305),
(130513,13,'BATANG GASAN',1305),
(130514,14,'V KOTO TIMUR',1305),
(130515,15,'II.X.XI.VI.LINGKUNG',1305),
(130516,16,'PATAMUAN',1305),
(130517,17,'VI.LINGKUNG',1305),
(130601,1,'TANJUNG MUTIARA',1306),
(130602,2,'LUBUK BASUNG',1306),
(130603,3,'TANJUNG RAYA',1306),
(130604,4,'MATUR',1306),
(130605,5,'IV KOTO',1306),
(130606,6,'BANUHAMPU',1306),
(130607,7,'EMPAT ANGKAT CANDUNG',1306),
(130608,8,'BASO',1306),
(130609,9,'TILATANG KAMANG',1306),
(130610,10,'PALUPUH',1306),
(130611,11,'PALEMBAYAN',1306),
(130612,12,'SUNGAI PUA',1306),
(130613,13,'IV NAGARI',1306),
(130614,14,'CANDUNG',1306),
(130615,15,'KAMANG MAGEK',1306),
(130616,16,'KECAMATAN SATU',1306),
(130701,1,'SULIKI GUNUNG MAS',1307),
(130702,2,'GUGUK',1307),
(130703,3,'PAYAKUMBUH',1307),
(130704,4,'LUHAK',1307),
(130705,5,'HARAU',1307),
(130706,6,'PANGKALAN KOTO BARU',1307),
(130707,7,'KAPUR SEMBILAN',1307),
(130708,8,'GUNUNG MAS',1307),
(130709,9,'LAREH SAGO HALABAN',1307),
(130710,10,'SITUJUAN LAMO NAGARI',1307),
(130711,11,'MUNGKA',1307),
(130712,12,'BUKIT BARISAN',1307),
(130713,13,'AKABILURU',1307),
(130804,4,'BONJOL',1308),
(130805,5,'LUBUK SIKAPING',1308),
(130807,7,'PANTI',1308),
(130808,8,'MAPAT TUNGGUL',1308),
(130812,12,'II KOTO',1308),
(130813,13,'III NAGARI',1308),
(130814,14,'RAO',1308),
(130901,1,'PAGAI UTARA/SELATAN',1309),
(130902,2,'SIPORA',1309),
(130903,3,'SIBERUT SELATAN',1309),
(130904,4,'SIBERUT UTARA',1309),
(131001,1,'KOTO BARU',1310),
(131002,2,'PULAU PUNJUNG',1310),
(131003,3,'SUNGAI RUMBAI',1310),
(131004,4,'SITIUNG',1310),
(131101,1,'SANGIR',1311),
(131102,2,'SEI PAGU',1311),
(131103,3,'K PARIK G DISTEH',1311),
(131104,4,'SANGIR JUJUHAN',1311),
(131105,5,'SANGIR BATANG HARI',1311),
(131201,1,'SEI BEREMAS',1312),
(131202,2,'RANAH BATAHAN',1312),
(131203,3,'LEMBAH MELINTANG',1312),
(131204,4,'GUNUNG TULEH',1312),
(131205,5,'PASAMAN',1312),
(131206,6,'KINALI',1312),
(131207,7,'TALAMAU',1312),
(137101,1,'PADANG SELATAN',1371),
(137102,2,'PADANG TIMUR',1371),
(137103,3,'PADANG BARAT',1371),
(137104,4,'PADANG UTARA',1371),
(137105,5,'BUNGUS TELUK KABUNG',1371),
(137106,6,'LUBUK BEGALUNG',1371),
(137107,7,'LUBUK KILANGAN',1371),
(137108,8,'PAUH',1371),
(137109,9,'KURANJI',1371),
(137110,10,'NANGGALO',1371),
(137111,11,'KOTO TANGAH',1371),
(137201,1,'LUBUK SIKARAH',1372),
(137202,2,'TANJUNG HARAPAN',1372),
(137301,1,'LEMBAH SEGAR',1373),
(137302,2,'BARANGIN',1373),
(137303,3,'SILUNGKANG',1373),
(137304,4,'TALAWI',1373),
(137401,1,'PADANG PANJANG TIMUR',1374),
(137402,2,'PADANG PANJANG BARAT',1374),
(137501,1,'GUGUK PANJANG',1375),
(137502,2,'MANDIANGIN KOTO SELAYAN',1375),
(137503,3,'AUR BIRUGO TIGO BALEH',1375),
(137601,1,'PAYAKUMBUH BARAT',1376),
(137602,2,'PAYAKUMBUH UTARA',1376),
(137603,3,'PAYAKUMBUH TIMUR',1376),
(137701,1,'PARIAMAN TENGAH',1377),
(137702,2,'PARIAMAN UTARA',1377),
(137703,3,'PARIAMAN SELATAN',1377),
(140101,1,'BANGKINANG',1401),
(140102,2,'KAMPAR',1401),
(140103,3,'TAMBANG',1401),
(140104,4,'XIII KOTO KAMPAR',1401),
(140105,5,'BANGKINANG BARAT',1401),
(140106,6,'SIAK HULU',1401),
(140107,7,'KAMPAR KIRI',1401),
(140108,8,'KAMPAR KIRI HILIR',1401),
(140109,9,'KAMPAR KIRI HULU',1401),
(140110,10,'TAPUNG',1401),
(140111,11,'TAPUNG HILIR',1401),
(140112,12,'TAPUNG HULU',1401),
(140113,13,'SALO',1401),
(140114,14,'RUMBIO JAYA',1401),
(140115,15,'BANGKINANG SEBERANG',1401),
(140116,16,'PERHENTIAN RAJA',1401),
(140117,17,'KAMPAR TIMUR',1401),
(140118,18,'KAMPAR UTARA',1401),
(140119,19,'KAMPAR KIRI TENGAH',1401),
(140120,20,'GUNUNG SAHILAN',1401),
(140201,1,'RENGAT',1402),
(140202,2,'RENGAT BARAT',1402),
(140203,3,'KELAYANG',1402),
(140204,4,'PASIR PENYU',1402),
(140205,5,'PERANAP',1402),
(140206,6,'SEBERIDA',1402),
(140207,7,'BATANG CENAKU',1402),
(140208,8,'BATANG GANGSAL',1402),
(140209,9,'LIRIK',1402),
(140210,10,'KUALA CENAKU',1402),
(140211,11,'SUNGAI LALA',1402),
(140212,12,'LUBUK BATU JAYA',1402),
(140213,13,'RAKIT KULIM',1402),
(140214,14,'BATANG PERANAP',1402),
(140301,1,'BENGKALIS',1403),
(140302,2,'BANTAN',1403),
(140303,3,'BUKIT BATU',1403),
(140304,4,'MERBAU',1403),
(140305,5,'TEBING TINGGI',1403),
(140306,6,'TEBING TINGGI BARAT',1403),
(140307,7,'RANGSANG',1403),
(140308,8,'RANGSANG BARAT',1403),
(140309,9,'MANDAU',1403),
(140310,10,'RUPAT',1403),
(140311,11,'RUPAT UTARA',1403),
(140312,12,'SIAK KECIL',1403),
(140313,13,'PINGGIR',1403),
(140401,1,'RETEH',1404),
(140402,2,'ENOK',1404),
(140403,3,'KUALA INDRAGIRI',1404),
(140404,4,'TEMBILAHAN',1404),
(140405,5,'TEMPULING',1404),
(140406,6,'GAUNG ANAK SERKA',1404),
(140407,7,'MANDAH',1404),
(140408,8,'KATEMAN',1404),
(140409,9,'KERITANG',1404),
(140410,10,'TANAH MERAH',1404),
(140411,11,'BATANG TUAKA',1404),
(140412,12,'GAUNG',1404),
(140413,13,'TEMBILAHAN HULU',1404),
(140414,14,'KEMUNING',1404),
(140415,15,'PELANGIRAN',1404),
(140416,16,'TELUK BELENGKONG',1404),
(140417,17,'PULAU BURUNG',1404),
(140418,18,'CONCONG',1404),
(140419,19,'KEMPAS',1404),
(140420,20,'SUNGAI BATANG',1404),
(140501,1,'UKUI',1405),
(140502,2,'PANGKALAN KERINCI',1405),
(140503,3,'PANGKALAN KURAS',1405),
(140504,4,'PANGKALAN LESUNG',1405),
(140505,5,'LANGGAM',1405),
(140506,6,'PELALAWAN',1405),
(140507,7,'KERUMUTAN',1405),
(140508,8,'BUNUT',1405),
(140509,9,'TELUK MERANTI',1405),
(140510,10,'KUALA KAMPAR',1405),
(140511,11,'BANDAR SEI KIJANG',1405),
(140512,12,'BANDAR PETALANGAN ',1405),
(140601,1,'UJUNG BATU',1406),
(140602,2,'ROKAN IV KOTO',1406),
(140603,3,'R A M B A H',1406),
(140604,4,'T A M B U S A I',1406),
(140605,5,'KEPENUHAN',1406),
(140606,6,'KUNTO DARUSSALAM',1406),
(140607,7,'RAMBAH SAMO',1406),
(140608,8,'RAMBAH HILIR',1406),
(140609,9,'TAMBUSAI UTARA',1406),
(140610,10,'BANGUN PURBA',1406),
(140611,11,'T A N D U N',1406),
(140612,12,'KABUN',1406),
(140613,13,'BONAI DARUSSALAM',1406),
(140614,14,'PANGARAN TAPAH DARUSSALAM',1406),
(140615,15,'KEPENUHAN HULU',1406),
(140616,16,'PENDALIAN V KOTO',1406),
(140701,1,'KUBU',1407),
(140702,2,'BANGKO',1407),
(140703,3,'TANAH PUTIH',1407),
(140704,4,'RIMBA MELINTANG',1407),
(140705,5,'BAGAN SINEMBAH',1407),
(140706,6,'PASIR LIMAU KAPAS',1407),
(140707,7,'SENABOI',1407),
(140708,8,'PUJUD',1407),
(140709,9,'TANAH PUTIH TANJUNG MELAWAN',1407),
(140710,10,'BANGKO PUSAKO',1407),
(140711,11,'SIMPANG KANAN',1407),
(140712,12,'BATU HAMPAR',1407),
(140713,13,'RANTAU KOPAR',1407),
(140801,1,'SIAK',1408),
(140802,2,'SUNGAI APIT',1408),
(140803,3,'MINAS',1408),
(140804,4,'TUALANG',1408),
(140805,5,'SUNGAI MANDAU',1408),
(140806,6,'DAYUN',1408),
(140807,7,'KERINCI KANAN',1408),
(140808,8,'BUNGA RAYA',1408),
(140809,9,'KOTO GASIB',1408),
(140810,10,'KANDIS',1408),
(140811,11,'LUBUK DALAM',1408),
(140812,12,'SABAK AUH',1408),
(140813,13,'MEMPURA',1408),
(140901,1,'KUANTAN MUDIK',1409),
(140902,2,'KUANTAN TENGAH',1409),
(140903,3,'SINGINGI',1409),
(140904,4,'KUANTAN HILIR',1409),
(140905,5,'CERENTI',1409),
(140906,6,'BENAI',1409),
(140907,7,'GUNUNG TOAR',1409),
(140908,8,'SINGINGI HILIR',1409),
(140909,9,'PANGEAN',1409),
(140910,10,'LOGAS TANAH DARAT',1409),
(140911,11,'INUMAN',1409),
(140912,12,'HULU KUANTAN',1409),
(147101,1,'SUKAJADI',1471),
(147102,2,'PEKAN BARU KOTA',1471),
(147103,3,'SAIL',1471),
(147104,4,'LIMA PULUH',1471),
(147105,5,'SENAPELAN',1471),
(147106,6,'RUMBAI',1471),
(147107,7,'BUKIT RAYA',1471),
(147108,8,'TAMPAN',1471),
(147109,9,'MARPOYAN DAMAI',1471),
(147110,10,'TENAYAN RAYA',1471),
(147111,11,'PAYUNG SEKAKI',1471),
(147112,12,'RUMBAI PESISIR',1471),
(147201,1,'DUMAI BARAT',1472),
(147202,2,'DUMAI TIMUR',1472),
(147203,3,'BUKIT KAPUR',1472),
(147204,4,'SUNGAI SEMBILAN',1472),
(147205,5,'MEDANG KAMPAI',1472),
(150101,1,'GUNUNG RAYA',1501),
(150102,2,'DANAU KERINCI',1501),
(150103,3,'SUNGAI PENUH',1501),
(150104,4,'SITINJAU LAUT',1501),
(150105,5,'AIR HANGAT',1501),
(150106,6,'GUNUNG KERINCI',1501),
(150107,7,'BATANG MERANGIN',1501),
(150108,8,'KELILING DANAU',1501),
(150109,9,'KAYU ARO',1501),
(150110,10,'HAMPARAN RAWANG',1501),
(150111,11,'AIR HANGAT TIMUR',1501),
(150201,1,'JANGKAT',1502),
(150202,2,'BANGKO',1502),
(150203,3,'MUARA SIAU',1502),
(150204,4,'SUNGAI MANAU',1502),
(150205,5,'TABIR',1502),
(150206,6,'PAMENANG',1502),
(150207,7,'TABIR ULU',1502),
(150301,1,'BATANG ASAI',1503),
(150302,2,'LIMUN',1503),
(150303,3,'SAROLANGUN',1503),
(150304,4,'PAUH',1503),
(150305,5,'PELAWAN SINGKUT',1503),
(150306,6,'MANDIANGIN',1503),
(150401,1,'MERSAM',1504),
(150402,2,'MUARA TEMBESI',1504),
(150403,3,'MUARA BULIAN',1504),
(150404,4,'BATIN XXIV',1504),
(150405,5,'PEMAYUNG',1504),
(150406,6,'MARO SEBO ULU',1504),
(150407,7,'BAJUBANG',1504),
(150408,8,'MARO SEBO ILIR',1504),
(150501,1,'JAMBI LUAR KOTA',1505),
(150502,2,'SEKERNAN',1505),
(150503,3,'KUMPEH',1505),
(150504,4,'MARO SEBO',1505),
(150505,5,'MESTONG',1505),
(150506,6,'KUMPEH ULU',1505),
(150507,7,'SUNGAI BAHAR',1505),
(150601,1,'TUNGKAL ULU',1506),
(150602,2,'TUNGKAL ILIR',1506),
(150603,3,'PENGABUAN',1506),
(150604,4,'BETARA',1506),
(150605,5,'MERLUNG',1506),
(150701,1,'MUARA SABAK',1507),
(150702,2,'NIPAH PANJANG',1507),
(150703,3,'MENDAHARA',1507),
(150704,4,'RANTAU RASAU',1507),
(150705,5,'SADU',1507),
(150706,6,'DENDANG',1507),
(150801,1,'TANAH TUMBUH',1508),
(150802,2,'RANTAU PANDAN',1508),
(150803,3,'MUARA BUNGO',1508),
(150804,4,'JUJUHAN',1508),
(150805,5,'TANAH SEPENGGAL',1508),
(150806,6,'PELEPAT',1508),
(150807,7,'LIMBUR LUBUK MENGKUANG',1508),
(150808,8,'MUKO-MUKO BATHIN VII',1508),
(150809,9,'PELEPAT ILIR',1508),
(150810,10,'BATHIN II BABEKO',1508),
(150901,1,'TEBO TENGAH',1509),
(150902,2,'TEBO ILIR',1509),
(150903,3,'TEBO ULU',1509),
(150904,4,'RIMBO BUJANG',1509),
(150905,5,'SUMAY',1509),
(150906,6,'VII KOTO',1509),
(150907,7,'RIMBO ULU',1509),
(150908,8,'RIMBO ILIR',1509),
(150909,9,'TENGAH ILIR',1509),
(157101,1,'TELANAIPURA',1571),
(157102,2,'JAMBI SELATAN',1571),
(157103,3,'JAMBI TIMUR',1571),
(157104,4,'PASAR JAMBI',1571),
(157105,5,'PELAYANGAN',1571),
(157106,6,'DANAU TELUK',1571),
(157107,7,'KOTA BARU',1571),
(157108,8,'JELUTUNG',1571),
(160107,7,'SOSOH BUAY RAYAP',1601),
(160108,8,'PENGANDONAN',1601),
(160109,9,'PENINJAUAN',1601),
(160113,13,'BATURAJA BARAT',1601),
(160114,14,'BATURAJA TIMUR',1601),
(160120,20,'ULU OGAN',1601),
(160121,21,'SEMIDANG AJI',1601),
(160122,22,'LUBUK BATANG',1601),
(160128,28,'LENGKITI',1601),
(160129,29,'SINAR PENINJAUAN',1601),
(160130,30,'LUBUK RAJA',1601),
(160201,1,'MUARA KUANG',1602),
(160202,2,'TANJUNG LUBUK',1602),
(160203,3,'PEDAMARAN',1602),
(160204,4,'MESUJI',1602),
(160205,5,'KOTA KAYU AGUNG',1602),
(160206,6,'TANJUNG BATU',1602),
(160207,7,'TANJUNG RAJA',1602),
(160208,8,'SIRAH PULAU PADANG',1602),
(160209,9,'INDRALAYA',1602),
(160210,10,'PEMULUTAN',1602),
(160211,11,'TULUNG SELAPAN',1602),
(160212,12,'PAMPANGAN',1602),
(160213,13,'LEMPUING',1602),
(160214,14,'AIR SUGIHAN',1602),
(160215,15,'PEMATANG PANGGANG',1602),
(160216,16,'RANTAU ALAI',1602),
(160217,17,'JEJAWI',1602),
(160218,18,'CENGAL',1602),
(160301,1,'TANJUNG AGUNG',1603),
(160302,2,'MUARA ENIM',1603),
(160303,3,'RAMBANG DANGKU',1603),
(160304,4,'GUNUNG MEGANG',1603),
(160305,5,'TALANG UBI',1603),
(160306,6,'GELUMBANG',1603),
(160307,7,'LAWANG KIDUL',1603),
(160308,8,'SEMENDE DARAT LAUT',1603),
(160309,9,'SEMENDE DARAT TENGAH',1603),
(160310,10,'SEMENDE DARAT ULU',1603),
(160311,11,'UJAN MAS',1603),
(160312,12,'TANAH ABANG',1603),
(160313,13,'PENUKAL ABAB',1603),
(160314,14,'LUBAI',1603),
(160315,15,'RAMBANG',1603),
(160316,16,'SUNGAI ROTAN',1603),
(160317,17,'LEMBAK',1603),
(160318,18,'PENUKAL UTARA',1603),
(160319,19,'BENAKAT',1603),
(160401,1,'TANJUNG SAKTI PUMU',1604),
(160406,6,'JARAI',1604),
(160407,7,'KOTA AGUNG',1604),
(160408,8,'PULAU PINANG',1604),
(160409,9,'MERAPI BARAT',1604),
(160410,10,'LAHAT',1604),
(160412,12,'PAJAR BULAN',1604),
(160415,15,'MULAK ULU',1604),
(160416,16,'KIKIM SELATAN',1604),
(160417,17,'KIKIM TIMUR',1604),
(160418,18,'KIKIM TENGAH',1604),
(160419,19,'KIKIM BARAT',1604),
(160420,20,'PSEKSU',1604),
(160421,21,'GUMAYTALANG',1604),
(160422,22,'PAGAR GUNUNG',1604),
(160423,23,'MERAPI TIMUR',1604),
(160424,24,'TANJUNG SAKTI PUMI',1604),
(160501,1,'TUGUMULYO',1605),
(160502,2,'MUARA LAKITAN',1605),
(160503,3,'MUARA KELINGI',1605),
(160504,4,'RAWAS ILIR',1605),
(160505,5,'RAWAS ULU',1605),
(160506,6,'ULU RAWAS',1605),
(160507,7,'RUPIT',1605),
(160508,8,'JAYA LOKA',1605),
(160509,9,'MUARA BELITI',1605),
(160510,10,'STL.ULU TERAWAS',1605),
(160511,11,'SELANGIT',1605),
(160512,12,'MEGANG SAKTI',1605),
(160513,13,'PURWODADI',1605),
(160514,14,'BTS ULU',1605),
(160515,15,'KARANG JAYA',1605),
(160516,16,'NIBUNG',1605),
(160517,17,'KARANG DAPO',1605),
(160518,18,'TIANG PUMPUNG KEPUNGUT',1605),
(160519,19,'SUMBER HARTA',1605),
(160520,20,'TUAH NEGERI',1605),
(160521,21,'SUKA KARYA',1605),
(160601,1,'SEKAYU',1606),
(160602,2,'LAIS',1606),
(160603,3,'SUNGAI KERUH',1606),
(160604,4,'BATANG HARI LEKO',1606),
(160605,5,'SANGA DESA',1606),
(160606,6,'BABAT TOMAN',1606),
(160607,7,'SUNGAI LILIN',1606),
(160608,8,'KELUANG',1606),
(160609,9,'BAYUNG LENCIR',1606),
(160701,1,'BANYUASIN I',1607),
(160702,2,'BANYUASIN II',1607),
(160703,3,'BANYUASIN III',1607),
(160704,4,'PULAU RIMAU',1607),
(160705,5,'BETUNG',1607),
(160706,6,'RAMBUTAN',1607),
(160707,7,'MUARA PADANG',1607),
(160708,8,'MUARA TELANG',1607),
(160709,9,'MAKARTI JAYA',1607),
(160710,10,'TALANG KELAPA',1607),
(160711,11,'RANTAU BAYUR',1607),
(160901,1,'MUARADUA',1609),
(160902,2,'PULAU BERINGIN',1609),
(160903,3,'BANDING AGUNG',1609),
(160904,4,'MUARA DUA KISAM',1609),
(160905,5,'SIMPANG',1609),
(160906,6,'MARTAPURA',1609),
(161101,1,'MUARA PINANG',1611),
(161102,2,'PENDOPO',1611),
(161103,3,'ULU MUSI',1611),
(161104,4,'TEBING TINGGI',1611),
(161105,5,'LINTANG KANAN',1611),
(161106,6,'TALANG PADANG',1611),
(161107,7,'PASEMAH AIR KERUH',1611),
(167101,1,'ILIR BARAT II',1671),
(167102,2,'SEBERANG ULU I',1671),
(167103,3,'SEBERANG ULU II',1671),
(167104,4,'ILIR BARAT I',1671),
(167105,5,'ILIR TIMUR I',1671),
(167106,6,'ILIR TIMUR II',1671),
(167107,7,'SUKARAMI',1671),
(167108,8,'SAKO',1671),
(167109,9,'KEMUNING',1671),
(167110,10,'KALIDONI',1671),
(167111,11,'BUKIT KECIL',1671),
(167112,12,'GANDUS',1671),
(167113,13,'KERTAPATI',1671),
(167114,14,'PLAJU',1671),
(167201,1,'PAGAR ALAM UTARA',1672),
(167202,2,'PAGAR ALAM SELATAN',1672),
(167203,3,'DEMPO UTARA',1672),
(167204,4,'DEMPO SELATAN',1672),
(167205,5,'DEMPO TENGAH',1672),
(167301,1,'LUBUKLINGGAU TIMUR I',1673),
(167302,2,'LUBUKLINGGAU BARAT I',1673),
(167303,3,'LUBUKLINGGAU SELATAN I',1673),
(167304,4,'LUBUKLINGGAU UTARA I',1673),
(167401,1,'PRABUMULIH BARAT',1674),
(167402,2,'PRABUMULIH TIMUR',1674),
(167403,3,'CAMBAI',1674),
(167404,4,'RAMBANG KAPAK TENGAH',1674),
(167405,5,'PRABUMULIH UTARA',1674),
(167406,6,'PRABUMULIH SELATAN',1674),
(170101,1,'KEDURANG',1701),
(170102,2,'SEGINIM',1701),
(170103,3,'PINO',1701),
(170104,4,'MANNA',1701),
(170105,5,'KOTA MANNA',1701),
(170106,6,'PINORAYA',1701),
(170206,6,'KOTA PADANG',1702),
(170207,7,'PADANG ULAK TANDING',1702),
(170208,8,'SINDANG KELINGI',1702),
(170209,9,'CURUP',1702),
(170210,10,'BERMANI ULU',1702),
(170211,11,'SELUPU REJANG',1702),
(170301,1,'ENGGANO',1703),
(170302,2,'TALANG EMPAT',1703),
(170303,3,'KARANG TINGGI',1703),
(170304,4,'TABA PENANJUNG',1703),
(170305,5,'PAGAR JATI',1703),
(170306,6,'KERKAP',1703),
(170307,7,'ARGA MAKMUR',1703),
(170308,8,'GIRI MULIA',1703),
(170309,9,'PADANG JAYA',1703),
(170310,10,'LAIS',1703),
(170311,11,'BATIK NAU',1703),
(170312,12,'KETAHUN',1703),
(170313,13,'NAPAL PUTIH',1703),
(170314,14,'PUTRI HIJAU',1703),
(170315,15,'AIR BESI',1703),
(170316,16,'AIR NAPAL',1703),
(170317,17,'PEMATANG TIGA',1703),
(170318,18,'PONDOK KELAPA',1703),
(170401,1,'KINAL',1704),
(170402,2,'TANJUNG KEMUNING',1704),
(170403,3,'KAUR UTARA',1704),
(170404,4,'KAUR TENGAH',1704),
(170405,5,'KAUR SELATAN',1704),
(170406,6,'MAJE',1704),
(170407,7,'NASAL',1704),
(170501,1,'SUKARAJA',1705),
(170502,2,'SELUMA',1705),
(170503,3,'TALO',1705),
(170504,4,'SEMIDANG ALAS',1705),
(170505,5,'SEMIDANG ALAS MARAS',1705),
(170601,1,'LUBUK PINANG',1706),
(170602,2,'MUKOMUKO UTARA',1706),
(170603,3,'MUKO-MUKO SELATAN',1706),
(170604,4,'TERAS TERUNJAM',1706),
(170605,5,'PONDOK SUGUH',1706),
(170701,1,'RIMBO PENGADANG',1707),
(170702,2,'LEBONG SELATAN',1707),
(170703,3,'LEBONG TENGAH',1707),
(170704,4,'LEBONG ATAS',1707),
(170705,5,'LEBONG UTARA',1707),
(170806,6,'BERMANI ILIR',1708),
(170807,7,'TEBAT KARAI',1708),
(170808,8,'KEPAHIANG',1708),
(170809,9,'UJAN MAS',1708),
(177101,1,'SELEBAR',1771),
(177102,2,'GADING CEMPAKA',1771),
(177103,3,'TELUK SEGARA',1771),
(177104,4,'MUARA BANGKA HULU',1771),
(180101,1,'PADANG CERMIN',1801),
(180102,2,'KEDONDONG',1801),
(180103,3,'GEDUNG TATAAN',1801),
(180104,4,'NATAR',1801),
(180105,5,'TANJUNG BINTANG',1801),
(180106,6,'KALIANDA',1801),
(180107,7,'SIDOMULYO',1801),
(180108,8,'KATIBUNG',1801),
(180109,9,'PENENGAHAN',1801),
(180110,10,'PALAS',1801),
(180111,11,'NEGERI KATON',1801),
(180112,12,'TEGINENENG',1801),
(180113,13,'JATI AGUNG',1801),
(180114,14,'KETAPANG',1801),
(180115,15,'SRAGI',1801),
(180116,16,'RAJABASA',1801),
(180117,17,'CANDIPURO',1801),
(180118,18,'MERBAU MATARAM',1801),
(180119,19,'WAY LIMA',1801),
(180120,20,'PUNDUH PIDADA',1801),
(180201,1,'KALIREJO',1802),
(180202,2,'BANGUNREJO',1802),
(180203,3,'PADANG RATU',1802),
(180204,4,'GUNUNG SUGIH',1802),
(180205,5,'TRIMURJO',1802),
(180206,6,'PUNGGUR',1802),
(180207,7,'TERBANGGI BESAR',1802),
(180208,8,'SEPUTIH RAMAN',1802),
(180209,9,'RUMBIA',1802),
(180210,10,'SEPUTIH BANYAK',1802),
(180211,11,'SEPUTIH MATARAM',1802),
(180212,12,'SEPUTIH SURABAYA',1802),
(180213,13,'TERUSAN NUNYAI',1802),
(180214,14,'BUMI RATU NUBAN',1802),
(180215,15,'BEKRI',1802),
(180216,16,'SEPUTIH AGUNG',1802),
(180217,17,'WAY PENGUBUAN',1802),
(180218,18,'BANDAR MATARAM',1802),
(180219,19,'PUBIAN',1802),
(180220,20,'SELAGAI LINGGA',1802),
(180221,21,'ANAK TUHA',1802),
(180222,22,'SENDANG AGUNG',1802),
(180223,23,'KOTA GAJAH',1802),
(180224,24,'BUMI NABUNG',1802),
(180225,25,'WAY SEPUTIH',1802),
(180226,26,'BANDAR SURABAYA',1802),
(180301,1,'BUKIT KEMUNING',1803),
(180302,2,'KOTABUMI',1803),
(180303,3,'SUNGKAI SELATAN',1803),
(180304,4,'TANJUNG RAJA',1803),
(180305,5,'ABUNG TIMUR',1803),
(180306,6,'ABUNG BARAT',1803),
(180307,7,'ABUNG SELATAN',1803),
(180308,8,'SUNGKAI UTARA',1803),
(180309,9,'KOTABUMI UTARA',1803),
(180310,10,'KOTABUMI SELATAN',1803),
(180311,11,'ABUNG TENGAH',1803),
(180312,12,'ABUNG TINGGI',1803),
(180313,13,'ABUNG SEMULI',1803),
(180314,14,'ABUNG SURAKARTA',1803),
(180315,15,'MUARA SUNGKAI',1803),
(180316,16,'BUNGA MAYANG',1803),
(180401,1,'PESISIR SELATAN',1804),
(180402,2,'PESISIR TENGAH',1804),
(180403,3,'PESISIR UTARA',1804),
(180404,4,'BALIK BUKIT',1804),
(180405,5,'SUMBER JAYA',1804),
(180406,6,'BELALAU',1804),
(180407,7,'WAY TENONG',1804),
(180408,8,'SEKINCAU',1804),
(180409,9,'SUOH',1804),
(180410,10,'BATU BRAK',1804),
(180411,11,'SUKAU',1804),
(180412,12,'KARYA PENGGAWA',1804),
(180413,13,'LEMONG',1804),
(180414,14,'BENGKUNAT',1804),
(180501,1,'MESUJI',1805),
(180502,2,'MENGGALA',1805),
(180503,3,'TULANG BAWANG TENGAH',1805),
(180504,4,'TULANG BAWANG UDIK',1805),
(180505,5,'SIMPANG PEMATANG',1805),
(180506,6,'GEDUNG AJI',1805),
(180507,7,'GUNUNG TERANG',1805),
(180508,8,'BANJAR AGUNG',1805),
(180509,9,'WAY SERDANG',1805),
(180510,10,'TANJUNG RAYA',1805),
(180511,11,'GEDUNG MENENG',1805),
(180512,12,'RAWA JITU SELATAN',1805),
(180513,13,'PENAWAR TAMA',1805),
(180514,14,'RAWA JITU UTARA',1805),
(180515,15,'LAMBU KIBANG',1805),
(180516,16,'TUMI JAJAR',1805),
(180601,1,'KOTA AGUNG',1806),
(180602,2,'TALANG PADANG',1806),
(180603,3,'WONOSOBO',1806),
(180604,4,'PULAU PANGGUNG',1806),
(180605,5,'PAGELARAN',1806),
(180606,6,'PRINGSEWU',1806),
(180607,7,'SUKOHARJO',1806),
(180608,8,'PARDASUKA',1806),
(180609,9,'CUKUH BALAK',1806),
(180610,10,'GADING REJO',1806),
(180611,11,'PUGUNG',1806),
(180612,12,'SEMAKA',1806),
(180613,13,'SUMBEREJO',1806),
(180614,14,'ADI LUWIH',1806),
(180615,15,'ULUBELU',1806),
(180616,16,'PEMATANG SAWA',1806),
(180617,17,'KELUMBAYAN',1806),
(180701,1,'SUKADANA',1807),
(180702,2,'LABUHAN MARINGGAI',1807),
(180703,3,'JABUNG',1807),
(180704,4,'PEKALONGAN',1807),
(180705,5,'SEKAMPUNG',1807),
(180706,6,'BATANGHARI',1807),
(180707,7,'WAY JEPARA',1807),
(180708,8,'PURBOLINGGO',1807),
(180709,9,'RAMAN UTARA',1807),
(180710,10,'METRO KIBANG',1807),
(180711,11,'MARGATIGA',1807),
(180712,12,'SEKAMPUNG UDIK',1807),
(180713,13,'BATANGHARI NUBAN',1807),
(180714,14,'BUMI AGUNG',1807),
(180715,15,'BANDAR SRIBAWONO',1807),
(180716,16,'MATARAM BARU',1807),
(180717,17,'MELINTING',1807),
(180718,18,'GUNUNG PELINDUNG',1807),
(180719,19,'PASIR SAKTI',1807),
(180720,20,'WAWAY KARYA',1807),
(180721,21,'LABUHAN RATU',1807),
(180722,22,'BRAJA SLEBAH',1807),
(180723,23,'WAY BUNGUR',1807),
(180724,24,'PURBOLINGGO UTARA',1807),
(180801,1,'BLAMBANGAN UMPU',1808),
(180802,2,'KASUI',1808),
(180803,3,'BANJIT',1808),
(180804,4,'BARADATU',1808),
(180805,5,'BAHUGA',1808),
(180806,6,'PAKUAN RATU',1808),
(180807,7,'NEGERI AGUNG',1808),
(180808,8,'WAY TUBA',1808),
(180809,9,'REBANG TANGKAS',1808),
(180810,10,'GUNUNG LABUHAN',1808),
(180811,11,'NEGARA BATIN',1808),
(180812,12,'NEGERI BESAR',1808),
(187101,1,'KEDATON',1871),
(187102,2,'SUKARAME',1871),
(187103,3,'TANJUNG KARANG BARAT',1871),
(187104,4,'PANJANG',1871),
(187105,5,'TANJUNG KARANG TIMUR',1871),
(187106,6,'TANJUNG KARANG PUSAT',1871),
(187107,7,'TELUK BETUNG SELATAN',1871),
(187108,8,'TELUK BETUNG BARAT',1871),
(187109,9,'TELUK BETUNG UTARA',1871),
(187110,10,'RAJABASA',1871),
(187111,11,'TANJUNG SENANG',1871),
(187112,12,'SUKABUMI',1871),
(187113,13,'KEMILING',1871),
(187201,1,'METRO PUSAT',1872),
(187202,2,'METRO UTARA',1872),
(187203,3,'METRO BARAT',1872),
(187204,4,'METRO TIMUR',1872),
(187205,5,'METRO SELATAN',1872),
(190101,1,'SUNGAILIAT',1901),
(190102,2,'BELINYU',1901),
(190103,3,'MERAWANG',1901),
(190104,4,'MENDO BARAT',1901),
(190105,5,'PEMALI',1901),
(190106,6,'BAKAM',1901),
(190107,7,'RIAU SILIP',1901),
(190108,8,'PUDING BESAR',1901),
(190201,1,'TANJUNG PANDAN',1902),
(190202,2,'MEMBALONG',1902),
(190203,3,'SELAT NASIK',1902),
(190204,4,'SIJUK',1902),
(190205,5,'BADAU',1902),
(190301,1,'TOBOALI',1903),
(190302,2,'LEPAR PONGOK',1903),
(190303,3,'AIR GEGAS',1903),
(190304,4,'SIMPANG RIMBA',1903),
(190305,5,'PAYUNG',1903),
(190401,1,'KOBA',1904),
(190402,2,'PANGKALAN BARU',1904),
(190403,3,'SUNGAI SELAN',1904),
(190404,4,'SIMPANG KATIS',1904),
(190501,1,'MENTOK',1905),
(190502,2,'SIMPANG TERITIP',1905),
(190503,3,'JEBUS',1905),
(190504,4,'KELAPA',1905),
(190505,5,'TEMPILANG',1905),
(190601,1,'MANGGAR',1906),
(190602,2,'GANTUNG',1906),
(190603,3,'DENDANG',1906),
(190604,4,'KELAPA KAMPIT',1906),
(197101,1,'BUKIT INTAN',1971),
(197102,2,'TAMANSARI',1971),
(197103,3,'PANGKAL BALAM',1971),
(197104,4,'RANGKUI',1971),
(197105,5,'GERUNGGANG',1971),
(210104,4,'GUNUNG KIJANG',2101),
(210106,6,'BINTAN TIMUR',2101),
(210107,7,'BINTAN UTARA',2101),
(210108,8,'TELUK BINTAN',2101),
(210109,9,'TAMBELAN',2101),
(210110,10,'TELUK SEBONG',2101),
(210112,12,'TOAPAYA',2101),
(210113,13,'MANTANG',2101),
(210114,14,'BINTAN PESISIR',2101),
(210115,15,'SERI KUALA LOBAM',2101),
(210201,1,'MORO',2102),
(210202,2,'KUNDUR',2102),
(210203,3,'KARIMUN',2102),
(210204,4,'MERAL',2102),
(210205,5,'TEBING',2102),
(210206,6,'BURU',2102),
(210207,7,'KUNDUR UTARA',2102),
(210208,8,'KUNDUR BARAT',2102),
(210209,9,'DURAI',2102),
(210301,1,'JEMAJA',2103),
(210302,2,'PALMATAK',2103),
(210303,3,'SIANTAN',2103),
(210304,4,'MIDAI',2103),
(210305,5,'BUNGURAN BARAT',2103),
(210306,6,'S E R A S A N',2103),
(210307,7,'BUNGURAN TIMUR',2103),
(210308,8,'BUNGURAN UTARA',2103),
(210309,9,'SUBI',2103),
(210310,10,'PULAU LAUT',2103),
(210311,11,'PULAU TIGA',2103),
(210312,12,'JEMAJA TIMUR',2103),
(210313,13,'SIANTAN SELATAN',2103),
(210314,14,'SIANTAN TIMUR',2103),
(210315,15,'BUNGURAN TIMUR LAUT',2103),
(210316,16,'BUNGURAN TENGAH',2103),
(210401,1,'SINGKEP',2104),
(210402,2,'LINGGA',2104),
(210403,3,'SENAYANG',2104),
(210404,4,'SINGKEP BARAT',2104),
(210405,5,'LINGGA UTARA',2104),
(217101,1,'BELAKANG PADANG',2171),
(217102,2,'BATU AMPAR',2171),
(217103,3,'SEKUPANG',2171),
(217104,4,'NONGSA',2171),
(217105,5,'BULANG',2171),
(217106,6,'LUBUK BAJA',2171),
(217107,7,'SEI BEDUK',2171),
(217108,8,'GALANG',2171),
(217109,9,'BENGKONG',2171),
(217110,10,'BATAM KOTA',2171),
(217111,11,'SAGULUNG',2171),
(217112,12,'BATU AJI',2171),
(217201,1,'TANJUNG PINANG BARAT',2172),
(217202,2,'TANJUNG PINANG TIMUR',2172),
(217203,3,'TANJUNG PINANG KOTA',2172),
(217204,4,'BUKIT LESTARI',2172),
(310101,1,'KEPULAUAN SERIBU UTARA',3101),
(310102,2,'KEPULAUAN SERIBU SELATAN',3101),
(317101,1,'GAMBIR',3171),
(317102,2,'SAWAH BESAR',3171),
(317103,3,'KEMAYORAN',3171),
(317104,4,'S E N E N',3171),
(317105,5,'CEMPAKA PUTIH',3171),
(317106,6,'MENTENG',3171),
(317107,7,'TANAH ABANG',3171),
(317108,8,'JOHAR BARU',3171),
(317201,1,'PENJARINGAN',3172),
(317202,2,'TANJUNG PERIOK',3172),
(317203,3,'KOJA',3172),
(317204,4,'CILINCING',3172),
(317205,5,'PADEMANGAN',3172),
(317206,6,'KELAPA GADING',3172),
(317301,1,'CENGKARENG',3173),
(317302,2,'GROGOL PETAMBURAN',3173),
(317303,3,'TAMAN SARI',3173),
(317304,4,'TAMBORA',3173),
(317305,5,'KEBON JERUK',3173),
(317306,6,'KALI DERES',3173),
(317307,7,'PAL MERAH',3173),
(317308,8,'KEMBANGAN',3173),
(317401,1,'TEBET',3174),
(317402,2,'SETIA BUDI',3174),
(317403,3,'MAMPANG PRAPATAN',3174),
(317404,4,'PASAR MINGGU',3174),
(317405,5,'KEBAYORAN LAMA',3174),
(317406,6,'CILANDAK',3174),
(317407,7,'KEBAYORAN BARU',3174),
(317408,8,'PANCORAN',3174),
(317409,9,'JAGAKARSA',3174),
(317410,10,'PESANGGRAHAN',3174),
(317501,1,'MATRAMAN',3175),
(317502,2,'PULO GADUNG',3175),
(317503,3,'JATINEGARA',3175),
(317504,4,'KRAMATJATI',3175),
(317505,5,'PASAR REBO',3175),
(317506,6,'CAKUNG',3175),
(317507,7,'DUREN SAWIT',3175),
(317508,8,'MAKASAR',3175),
(317509,9,'CIRACAS',3175),
(317510,10,'CIPAYUNG',3175),
(320101,1,'CIBINONG',3201),
(320102,2,'GUNUNG PUTRI',3201),
(320103,3,'CITEUREUP',3201),
(320104,4,'SUKARAJA',3201),
(320105,5,'BABAKAN MADANG',3201),
(320106,6,'JONGGOL',3201),
(320107,7,'CILEUNGSI',3201),
(320108,8,'CARIU',3201),
(320109,9,'SUKAMAKMUR',3201),
(320110,10,'PARUNG',3201),
(320111,11,'GUNUNG SINDUR',3201),
(320112,12,'KEMANG',3201),
(320113,13,'BOJONGGEDE',3201),
(320114,14,'LEUWILIANG',3201),
(320115,15,'CIAMPEA',3201),
(320116,16,'CIBUNGBULANG',3201),
(320117,17,'PAMIJAHAN',3201),
(320118,18,'RUMPIN',3201),
(320119,19,'JASINGA',3201),
(320120,20,'PARUNG PANJANG',3201),
(320121,21,'NANGGUNG',3201),
(320122,22,'CIGUDEG',3201),
(320123,23,'TENJO',3201),
(320124,24,'CIAWI',3201),
(320125,25,'CISARUA',3201),
(320126,26,'MEGAMENDUNG',3201),
(320127,27,'CARINGIN',3201),
(320128,28,'CIJERUK',3201),
(320129,29,'CIOMAS',3201),
(320130,30,'DRAMAGA',3201),
(320131,31,'TAMANSARI',3201),
(320132,32,'KLAPANUNGGAL',3201),
(320133,33,'CISEENG',3201),
(320134,34,'RANCABUNGUR',3201),
(320135,35,'SUKAJAYA',3201),
(320201,1,'PELABUHAN RATU',3202),
(320202,2,'SIMPENAN',3202),
(320203,3,'CIKAKAK',3202),
(320204,4,'BANTARGADUNG',3202),
(320205,5,'CISOLOK',3202),
(320206,6,'CIKIDANG',3202),
(320207,7,'LENGKONG',3202),
(320208,8,'JAMPANG TENGAH',3202),
(320209,9,'WARUNG KIARA',3202),
(320210,10,'CIKEMBAR',3202),
(320211,11,'CIBADAK',3202),
(320212,12,'NAGRAK',3202),
(320213,13,'PARUNG KUDA',3202),
(320214,14,'BOJONG GENTENG',3202),
(320215,15,'PARAKAN SALAK',3202),
(320216,16,'CICURUG',3202),
(320217,17,'CIDAHU',3202),
(320218,18,'KALAPA NUNGGAL',3202),
(320219,19,'KABANDUNGAN',3202),
(320220,20,'WALURAN',3202),
(320221,21,'JAMPANG KULON',3202),
(320222,22,'CIEMAS',3202),
(320223,23,'KALI BUNDER',3202),
(320224,24,'SURADE',3202),
(320225,25,'CIBITUNG',3202),
(320226,26,'CIRACAP',3202),
(320227,27,'GUNUNGGURUH',3202),
(320228,28,'CICANTAYAN',3202),
(320229,29,'CISAAT',3202),
(320230,30,'KADUDAMPIT',3202),
(320231,31,'CARINGIN',3202),
(320232,32,'SUKABUMI',3202),
(320233,33,'SUKARAJA',3202),
(320234,34,'KEBONPEDES',3202),
(320235,35,'CIREUNGHAS',3202),
(320236,36,'SUKALARANG',3202),
(320237,37,'PABUARAN',3202),
(320238,38,'PURABAYA',3202),
(320239,39,'NYALINDUNG',3202),
(320240,40,'GEGER BITUNG',3202),
(320241,41,'SAGARANTEN',3202),
(320242,42,'CURUGKEMBAR',3202),
(320243,43,'CIDOLOG',3202),
(320244,44,'CIDADAP',3202),
(320245,45,'TEGAL BULEUD',3202),
(320301,1,'CIANJUR',3203),
(320302,2,'WARUNGKONDANG',3203),
(320303,3,'CIBEBER',3203),
(320304,4,'CILAKU',3203),
(320305,5,'CIRANJANG',3203),
(320306,6,'BOJONGPICUNG',3203),
(320307,7,'KARANGTENGAH',3203),
(320308,8,'MANDE',3203),
(320309,9,'SUKALUYU',3203),
(320310,10,'PACET',3203),
(320311,11,'CUGENANG',3203),
(320312,12,'CIKALONGKULON',3203),
(320313,13,'SUKARESMI',3203),
(320314,14,'SUKANAGARA',3203),
(320315,15,'CAMPAKA',3203),
(320316,16,'TAKOKAK',3203),
(320317,17,'KADUPANDAK',3203),
(320318,18,'PAGELARAN',3203),
(320319,19,'TANGGEUNG',3203),
(320320,20,'CIBINONG',3203),
(320321,21,'SINDANGBARANG',3203),
(320322,22,'AGRABINTA',3203),
(320323,23,'CIDAUN',3203),
(320324,24,'NARINGGUL',3203),
(320325,25,'CAMPAKA MULYA',3203),
(320326,26,'CIKADU',3203),
(320405,5,'CILEUNYI',3204),
(320406,6,'CIMEUNYAN',3204),
(320407,7,'CILENGKRANG',3204),
(320408,8,'BOJONGSOANG',3204),
(320409,9,'MARGAHAYU',3204),
(320410,10,'MARGAASIH',3204),
(320411,11,'KATAPANG',3204),
(320412,12,'DAYEUHKOLOT',3204),
(320413,13,'BANJARAN',3204),
(320414,14,'PAMEUNGPEUK',3204),
(320415,15,'PANGALENGAN',3204),
(320416,16,'ARJASARI',3204),
(320417,17,'CIMAUNG',3204),
(320425,25,'CICALENGKA',3204),
(320426,26,'NAGREG',3204),
(320427,27,'CIKANCUNG',3204),
(320428,28,'RANCAEKEK',3204),
(320429,29,'CIPARAY',3204),
(320430,30,'PACET',3204),
(320431,31,'KERTASARI',3204),
(320432,32,'BALEENDAH',3204),
(320433,33,'MAJALAYA',3204),
(320434,34,'SOLOKAN JERUK',3204),
(320435,35,'PASEH',3204),
(320436,36,'IBUN',3204),
(320437,37,'SOREANG',3204),
(320438,38,'PASIRJAMBU',3204),
(320439,39,'CIWIDEY',3204),
(320440,40,'RANCABALI',3204),
(320501,1,'GARUT KOTA',3205),
(320502,2,'KARANGPAWITAN',3205),
(320503,3,'WANARAJA',3205),
(320506,6,'BANYURESMI',3205),
(320507,7,'SAMARANG',3205),
(320508,8,'PASIRWANGI',3205),
(320509,9,'LELES',3205),
(320510,10,'KADUNGORA',3205),
(320511,11,'LEUWIGOONG',3205),
(320512,12,'CIBATU',3205),
(320513,13,'KERSAMANAH',3205),
(320514,14,'MALANGBONG',3205),
(320515,15,'SUKAWENING',3205),
(320516,16,'KARANGTENGAH',3205),
(320517,17,'BAYONGBONG',3205),
(320519,19,'CILAWU',3205),
(320520,20,'CISURUPAN',3205),
(320521,21,'SUKARESMI',3205),
(320522,22,'CIKAJANG',3205),
(320523,23,'BANJARWANGI',3205),
(320524,24,'SINGAJAYA',3205),
(320525,25,'CIHURIP',3205),
(320526,26,'PEUNDEUY',3205),
(320527,27,'PAMEUNGPEUK',3205),
(320528,28,'CISOMPET',3205),
(320529,29,'CIBALONG',3205),
(320530,30,'CIKELET',3205),
(320531,31,'BUNGBULANG',3205),
(320533,33,'PAKENJENG',3205),
(320534,34,'PAMULIHAN',3205),
(320535,35,'CISEWU',3205),
(320536,36,'CARINGIN',3205),
(320537,37,'TALEGONG',3205),
(320538,38,'BLUBUR LIMBANGAN',3205),
(320539,39,'SELAAWI',3205),
(320540,40,'CIBIUK',3205),
(320541,41,'TAROGONG',3205),
(320601,1,'CIPATUJAH',3206),
(320602,2,'KARANGNUNGGAL',3206),
(320603,3,'CIKALONG',3206),
(320604,4,'PANCATENGAH',3206),
(320605,5,'CIKATOMAS',3206),
(320606,6,'CIBALONG',3206),
(320607,7,'PARUNGPONTENG',3206),
(320608,8,'BANTARKALONG',3206),
(320609,9,'BOJONGASIH',3206),
(320610,10,'CULAMEGA',3206),
(320611,11,'BOJONGGAMBIR',3206),
(320612,12,'SODONGHILIR',3206),
(320613,13,'TARAJU',3206),
(320614,14,'SALAWU',3206),
(320615,15,'PUSPAHIANG',3206),
(320616,16,'TANJUNGJAYA',3206),
(320617,17,'SUKARAJA',3206),
(320618,18,'SALOPA',3206),
(320619,19,'JATIWARAS',3206),
(320620,20,'CINEAM',3206),
(320621,21,'KARANGJAYA',3206),
(320622,22,'MANONJAYA',3206),
(320623,23,'GUNUNGTANJUNG',3206),
(320624,24,'SINGAPARNA',3206),
(320625,25,'MANGUNREJA',3206),
(320626,26,'SUKARAME',3206),
(320627,27,'CIGALONTANG',3206),
(320628,28,'LEUWISARI',3206),
(320629,29,'PADAKEMBANG',3206),
(320630,30,'SARIWANGI',3206),
(320631,31,'SUKARATU',3206),
(320632,32,'CISAYONG',3206),
(320633,33,'SUKAHENING',3206),
(320634,34,'RAJAPOLAH',3206),
(320635,35,'JAMANIS',3206),
(320636,36,'CIAWI',3206),
(320637,37,'KADIPATEN',3206),
(320638,38,'PAGERAGEUNG',3206),
(320639,39,'SUKARESIK',3206),
(320701,1,'CIAMIS',3207),
(320702,2,'CIKONENG',3207),
(320703,3,'CIJEUNGJING',3207),
(320704,4,'SADANANYA',3207),
(320705,5,'CIDOLOG',3207),
(320706,6,'CIHAURBEUTI',3207),
(320707,7,'PANUMBANGAN',3207),
(320708,8,'PANJALU',3207),
(320709,9,'KAWALI',3207),
(320710,10,'PANAWANGAN',3207),
(320711,11,'CIPAKU',3207),
(320712,12,'JATINAGARA',3207),
(320713,13,'RAJADESA',3207),
(320714,14,'SUKADANA',3207),
(320715,15,'RANCAH',3207),
(320716,16,'TAMBAKSARI',3207),
(320717,17,'LAKBOK',3207),
(320718,18,'BANJARSARI',3207),
(320719,19,'PAMARICAN',3207),
(320720,20,'PADAHERANG',3207),
(320721,21,'KALIPUCANG',3207),
(320722,22,'PANGANDARAN',3207),
(320723,23,'SIDAMULIH',3207),
(320724,24,'PARIGI',3207),
(320725,25,'CIJULANG',3207),
(320726,26,'CIMERAK',3207),
(320727,27,'CIGUGUR',3207),
(320728,28,'LANGKAPLANCAR',3207),
(320729,29,'CIMARAGAS',3207),
(320730,30,'CISAGA',3207),
(320901,1,'WALED',3209),
(320902,2,'CILEDUG',3209),
(320903,3,'LOSARI',3209),
(320904,4,'PABEDILAN',3209),
(320905,5,'BABAKAN',3209),
(320906,6,'KARANGSEMBUNG',3209),
(320907,7,'LEMAHABANG',3209),
(320908,8,'SUSUKAN LEBAK',3209),
(320909,9,'SEDONG',3209),
(320910,10,'ASTANAJAPURA',3209),
(320911,11,'PANGENAN',3209),
(320912,12,'MUNDU',3209),
(320913,13,'BEBER',3209),
(320914,14,'TALUN',3209),
(320915,15,'SUMBER',3209),
(320916,16,'DUKUPUNTANG',3209),
(320917,17,'PALIMANAN',3209),
(320918,18,'PLUMBON',3209),
(320919,19,'WERU',3209),
(320920,20,'KEDAWUNG',3209),
(320921,21,'GUNUNGJATI',3209),
(320922,22,'KAPETAKAN',3209),
(320923,23,'KLANGENAN',3209),
(320924,24,'ARJAWINANGUN',3209),
(320925,25,'PANGURAGAN',3209),
(320926,26,'CIWARINGIN',3209),
(320927,27,'SUSUKAN',3209),
(320928,28,'GEGESIK',3209),
(320929,29,'KALIWEDI',3209),
(320930,30,'GEBANG',3209),
(320931,31,'DEPOK',3209),
(320932,32,'PASALEMAN',3209),
(320933,33,'PABUARAN',3209),
(320934,34,'KARANGWARENG',3209),
(320935,35,'TENGAH TANI',3209),
(320936,36,'PLERED',3209),
(320937,37,'GEMPOL',3209),
(320938,38,'GREGED',3209),
(320939,39,'SURANENGGALA',3209),
(320940,40,'JAMBLANG',3209),
(321001,1,'LEMAHSUGIH',3210),
(321002,2,'BANTARUJEG',3210),
(321003,3,'CIKIJING',3210),
(321004,4,'TALAGA',3210),
(321005,5,'ARGAPURA',3210),
(321006,6,'MAJA',3210),
(321007,7,'MAJALENGKA',3210),
(321008,8,'SUKAHAJI',3210),
(321009,9,'RAJAGALUH',3210),
(321010,10,'LEUWIMUNDING',3210),
(321011,11,'JATIWANGI',3210),
(321012,12,'DAWUAN',3210),
(321013,13,'KADIPATEN',3210),
(321014,14,'KERTAJATI',3210),
(321015,15,'JATITUJUH',3210),
(321016,16,'LIGUNG',3210),
(321017,17,'SUMBERJAYA',3210),
(321018,18,'PANYINGKIRAN',3210),
(321019,19,'PALASAH',3210),
(321020,20,'CIGASONG',3210),
(321021,21,'SINDANGWANGI',3210),
(321022,22,'BANJARAN',3210),
(321023,23,'CINGAMBUL',3210),
(321101,1,'WADO',3211),
(321102,2,'JATINUNGGAL',3211),
(321103,3,'DARMARAJA',3211),
(321104,4,'CIBUGEL',3211),
(321105,5,'CISITU',3211),
(321106,6,'SITURAJA',3211),
(321107,7,'CONGGEANG',3211),
(321108,8,'PASEH',3211),
(321109,9,'SURIAN',3211),
(321110,10,'BUAHDUA',3211),
(321111,11,'TANJUNGSARI',3211),
(321112,12,'SUKASARI',3211),
(321113,13,'PAMULIHAN',3211),
(321114,14,'CIMANGGUNG',3211),
(321115,15,'JATINANGOR',3211),
(321116,16,'RANCAKALONG',3211),
(321117,17,'SUMEDANG SELATAN',3211),
(321118,18,'SUMEDANG UTARA',3211),
(321119,19,'GANEAS',3211),
(321120,20,'TANJUNGKERTA',3211),
(321121,21,'TANJUNGMEDAR',3211),
(321122,22,'CIMALAKA',3211),
(321123,23,'CISARUA',3211),
(321124,24,'TOMO',3211),
(321125,25,'UJUNG JAYA',3211),
(321126,26,'JATIGEDE',3211),
(321201,1,'HAURGEULIS',3212),
(321202,2,'KROYA',3212),
(321203,3,'GABUSWETAN',3212),
(321204,4,'CIKEDUNG',3212),
(321205,5,'LELEA',3212),
(321206,6,'BANGODUA',3212),
(321207,7,'WIDASARI',3212),
(321208,8,'KERTASEMAYA',3212),
(321209,9,'KRANGKENG',3212),
(321210,10,'KARANGAMPEL',3212),
(321211,11,'JUNTINYUAT',3212),
(321212,12,'SLIYEG',3212),
(321213,13,'JATIBARANG',3212),
(321214,14,'BALONGAN',3212),
(321215,15,'INDRAMAYU',3212),
(321216,16,'SINDANG',3212),
(321217,17,'CANTIGI',3212),
(321218,18,'LOHBENER',3212),
(321219,19,'ARAHAN',3212),
(321220,20,'LOSARANG',3212),
(321221,21,'KANDANGHAUR',3212),
(321222,22,'BONGAS',3212),
(321223,23,'ANJATAN',3212),
(321224,24,'SUKRA',3212),
(321301,1,'SAGALAHERANG',3213),
(321302,2,'CISALAK',3213),
(321303,3,'SUBANG',3213),
(321304,4,'KALIJATI',3213),
(321305,5,'PABUARAN',3213),
(321306,6,'PURWADADI',3213),
(321307,7,'PAGADEN',3213),
(321308,8,'BINONG',3213),
(321309,9,'CIASEM',3213),
(321310,10,'PUSAKANAGARA',3213),
(321311,11,'PAMANUKAN',3213),
(321312,12,'JALANCAGAK',3213),
(321313,13,'BLANAKAN',3213),
(321314,14,'TANJUNGSIANG',3213),
(321315,15,'COMPRENG',3213),
(321316,16,'PATOKBEUSI',3213),
(321317,17,'CIBOGO',3213),
(321318,18,'CIPUNAGARA',3213),
(321319,19,'CIJAMBE',3213),
(321320,20,'CIPEUNDEUY',3213),
(321321,21,'LEGONKULON',3213),
(321322,22,'CIKAUM',3213),
(321401,1,'PURWAKARTA',3214),
(321402,2,'CAMPAKA',3214),
(321403,3,'JATILUHUR',3214),
(321404,4,'PLERED',3214),
(321405,5,'SUKATANI',3214),
(321406,6,'DARANGDAN',3214),
(321407,7,'MANIIS',3214),
(321408,8,'TEGAL WARU',3214),
(321409,9,'WANAYASA',3214),
(321410,10,'PASAWAHAN',3214),
(321411,11,'BOJONG',3214),
(321412,12,'BABAKANCIKAO',3214),
(321413,13,'BUNGURSARI',3214),
(321414,14,'CIBATU',3214),
(321415,15,'SUKASARI',3214),
(321416,16,'PONDOKSALAM',3214),
(321417,17,'KIARAPEDES',3214),
(321501,1,'KARAWANG',3215),
(321502,2,'PANGKALAN',3215),
(321503,3,'TELUKJAMBE',3215),
(321504,4,'CIAMPEL',3215),
(321505,5,'KLARI',3215),
(321506,6,'RENGASDENGKLOK',3215),
(321507,7,'KUTAWALUYA',3215),
(321508,8,'BATUJAYA',3215),
(321509,9,'TIRTAJAYA',3215),
(321510,10,'PEDES',3215),
(321511,11,'CIBUAYA',3215),
(321512,12,'PAKISJAYA',3215),
(321513,13,'CIKAMPEK',3215),
(321514,14,'JATISARI',3215),
(321515,15,'CIMALAYA WETAN',3215),
(321516,16,'TIRTAMULYA',3215),
(321517,17,'TALAGASARI',3215),
(321518,18,'RAWAMERTA',3215),
(321519,19,'LEMAHABANG',3215),
(321520,20,'TEMPURAN',3215),
(321521,21,'MAJALAYA',3215),
(321522,22,'JAYAKERTA',3215),
(321523,23,'CIMALAYA KULON',3215),
(321524,24,'BANYUSARI',3215),
(321525,25,'KOTA BARU',3215),
(321601,1,'TARUMA JAYA',3216),
(321602,2,'BABELAN',3216),
(321603,3,'SUKAWANGI',3216),
(321604,4,'TAMBELANG',3216),
(321605,5,'TAMBUN UTARA',3216),
(321606,6,'TAMBUN SELATAN',3216),
(321607,7,'CIBITUNG',3216),
(321608,8,'CIKARANG BARAT',3216),
(321609,9,'CIKARANG UTARA',3216),
(321610,10,'KARANG BAHAGIA',3216),
(321611,11,'CIKARANG TIMUR',3216),
(321612,12,'KEDUNGWARINGIN',3216),
(321613,13,'PEBAYURAN',3216),
(321614,14,'SUKAKARYA',3216),
(321615,15,'SUKATANI',3216),
(321616,16,'CABANGBUNGIN',3216),
(321617,17,'MUARAGEMBONG',3216),
(321618,18,'SETU',3216),
(321619,19,'CIKARANG SELATAN',3216),
(321620,20,'CIKARANG PUSAT',3216),
(321621,21,'SERANG BARU',3216),
(321622,22,'CIBARUSAH',3216),
(321623,23,'BOJONGMANGU',3216),
(321701,1,'LEBANG',3217),
(321702,2,'PARONGPONG',3217),
(321703,3,'CISARUA',3217),
(321704,4,'CIKALONGWETAN',3217),
(321705,5,'CIPEUNDEUY',3217),
(321706,6,'NGAMPRAH',3217),
(321707,7,'CIPATAT',3217),
(321708,8,'PADALARANG',3217),
(321709,9,'BATUJAJAR',3217),
(321710,10,'CIHAMPELAS',3217),
(321711,11,'CILILIN',3217),
(321712,12,'CIPONGKOR',3217),
(321713,13,'RONGGA',3217),
(321714,14,'SINDANGKERTA',3217),
(321715,15,'GUNUNGHALU',3217),
(327101,1,'KOTA BOGOR SELATAN',3271),
(327102,2,'KOTA BOGOR TIMUR',3271),
(327103,3,'KOTA BOGOR TENGAH',3271),
(327104,4,'KOTA BOGOR BARAT',3271),
(327105,5,'KOTA BOGOR UTARA',3271),
(327106,6,'TANAH SEREAL',3271),
(327201,1,'GUNUNG PUYUH',3272),
(327202,2,'CIKOLE',3272),
(327203,3,'CITAMIANG',3272),
(327204,4,'WARUDOYONG',3272),
(327205,5,'BAROS',3272),
(327206,6,'LEMBURSITU',3272),
(327207,7,'CIBEUREUM',3272),
(327301,1,'SUKASARI',3273),
(327302,2,'COBLONG',3273),
(327303,3,'BABAKAN CIPARAY',3273),
(327304,4,'BOJONGLOA KALER',3273),
(327305,5,'ANDIR',3273),
(327306,6,'CICENDO',3273),
(327307,7,'SUKAJADI',3273),
(327308,8,'CIDADAP',3273),
(327309,9,'BANDUNG WETAN',3273),
(327310,10,'ASTANA ANYAR',3273),
(327311,11,'REGOL',3273),
(327312,12,'BATUNUNGGAL',3273),
(327313,13,'LENGKONG',3273),
(327314,14,'CIBEUNYING KIDUL',3273),
(327315,15,'BANDUNG KULON',3273),
(327316,16,'KIARACONDONG',3273),
(327317,17,'BOJONGLOA KIDUL',3273),
(327318,18,'CIBEUNYING KALER',3273),
(327319,19,'SUMUR BANDUNG',3273),
(327320,20,'CICADAS',3273),
(327321,21,'BANDUNG KIDUL',3273),
(327322,22,'MARGACINTA',3273),
(327323,23,'RANCASARI',3273),
(327324,24,'ARCAMANIK',3273),
(327325,25,'CIBIRU',3273),
(327326,26,'UJUNGBERUNG',3273),
(327401,1,'KEJAKSAN',3274),
(327402,2,'LEMAHWUNGKUK',3274),
(327403,3,'HARJAMUKTI',3274),
(327404,4,'PEKALIPAN',3274),
(327405,5,'KESAMBI',3274),
(327501,1,'BEKASI TIMUR',3275),
(327502,2,'BEKASI BARAT',3275),
(327503,3,'BEKASI UTARA',3275),
(327504,4,'BEKASI SELATAN',3275),
(327505,5,'RAWALUMBU',3275),
(327506,6,'MEDAN SATRIA',3275),
(327507,7,'BANTARGEBANG',3275),
(327508,8,'PONDOKGEDE',3275),
(327509,9,'JATIASIH',3275),
(327510,10,'JATISAMPURNA',3275),
(327601,1,'PANCORAN MAS',3276),
(327602,2,'CIMANGGIS',3276),
(327603,3,'SAWANGAN',3276),
(327604,4,'LIMO',3276),
(327605,5,'SUKMA JAYA',3276),
(327606,6,'BEJI',3276),
(327701,1,'CIMAHI SELATAN',3277),
(327702,2,'CIMAHI TENGAH',3277),
(327703,3,'CIMAHI UTARA',3277),
(327801,1,'CIHIDEUNG',3278),
(327802,2,'CIPEDES',3278),
(327803,3,'TAWANG',3278),
(327804,4,'INDIHIANG',3278),
(327805,5,'KAWALU',3278),
(327806,6,'CIBEUREUM',3278),
(327807,7,'TAMANSARI',3278),
(327808,8,'MANGKUBUMI',3278),
(327901,1,'BANJAR',3279),
(327902,2,'PATARUMAN',3279),
(327903,3,'PURWAHARJA',3279),
(327904,4,'LANGENSARI',3279),
(330101,1,'KEDUNGREJA',3301),
(330102,2,'KESUGIHAN',3301),
(330103,3,'ADIPALA',3301),
(330104,4,'BINANGUN',3301),
(330105,5,'NUSAWUNGU',3301),
(330106,6,'KROYA',3301),
(330107,7,'MAOS',3301),
(330108,8,'JERUKLEGI',3301),
(330109,9,'KAWUNGANTEN',3301),
(330110,10,'GANDRUNGMANGU',3301),
(330111,11,'SIDAREJA',3301),
(330112,12,'KARANGPUCUNG',3301),
(330113,13,'CIMANGGU',3301),
(330114,14,'MAJENANG',3301),
(330115,15,'WANAREJA',3301),
(330116,16,'DAYEUHLUHUR',3301),
(330117,17,'SAMPANG',3301),
(330118,18,'CIPARI',3301),
(330119,19,'PATIMUAN',3301),
(330120,20,'BANTARSARI',3301),
(330121,21,'CILACAP SELATAN',3301),
(330122,22,'CILACAP TENGAH',3301),
(330123,23,'CILACAP UTARA',3301),
(330124,24,'KAMPUNG LAUT',3301),
(330201,1,'LUMBIR',3302),
(330202,2,'WANGON',3302),
(330203,3,'JATI LAWANG',3302),
(330204,4,'RAWALO',3302),
(330205,5,'KEBASEN',3302),
(330206,6,'KEMRANJEN',3302),
(330207,7,'SUMPIUH',3302),
(330208,8,'TAMBAK',3302),
(330209,9,'SOMAGEDE',3302),
(330210,10,'KALIBAGOR',3302),
(330211,11,'BANYUMAS',3302),
(330212,12,'PATIKRAJA',3302),
(330213,13,'PURWOJATI',3302),
(330214,14,'AJIBARANG',3302),
(330215,15,'GUMELAR',3302),
(330216,16,'PEKUNCEN',3302),
(330217,17,'CILONGOK',3302),
(330218,18,'KARANGLEWAS',3302),
(330219,19,'SOKARAJA',3302),
(330220,20,'KEMBARAN',3302),
(330221,21,'SUMBANG',3302),
(330222,22,'BATURADEN',3302),
(330223,23,'KEDUNG BANTENG',3302),
(330224,24,'PURWOKERTO SELATAN',3302),
(330225,25,'PURWOKERTO BARAT',3302),
(330226,26,'PURWOKERTO TIMUR',3302),
(330227,27,'PURWOKERTO UTARA',3302),
(330301,1,'KEMANGKON',3303),
(330302,2,'BUKATEJA',3303),
(330303,3,'KEJOBONG',3303),
(330304,4,'KALIGONDANG',3303),
(330305,5,'PURBALINGGA',3303),
(330306,6,'KALIMANAH',3303),
(330307,7,'KUTASARI',3303),
(330308,8,'MREBET',3303),
(330309,9,'BOBOTSARI',3303),
(330310,10,'KARANGREJA',3303),
(330311,11,'KARANGANYAR',3303),
(330312,12,'KARANGMONCOL',3303),
(330313,13,'REMBANG',3303),
(330314,14,'BOJONGSARI',3303),
(330315,15,'PADAMARA',3303),
(330316,16,'PENGADEGAN',3303),
(330317,17,'KARANGJAMBU',3303),
(330318,18,'KERTANEGARA',3303),
(330401,1,'SUSUKAN',3304),
(330402,2,'PURWOREJO KLAMPOK',3304),
(330403,3,'MANDIRAJA',3304),
(330404,4,'PURWANEGARA',3304),
(330405,5,'BAWANG',3304),
(330406,6,'BANJARNEGARA',3304),
(330407,7,'SIGALUH',3304),
(330408,8,'MADUKARA',3304),
(330409,9,'BANJARMANGU',3304),
(330410,10,'WANADADI',3304),
(330411,11,'RAKIT',3304),
(330412,12,'PUNGGELAN',3304),
(330413,13,'KARANGKOBAR',3304),
(330414,14,'PAGENTAN',3304),
(330415,15,'PEJAWARAN',3304),
(330416,16,'BATUR',3304),
(330417,17,'WANAYASA',3304),
(330418,18,'KALIBENING',3304),
(330419,19,'PANDAN ARUM',3304),
(330420,20,'PAGEDONGAN',3304),
(330501,1,'AYAH',3305),
(330502,2,'BUAYAN',3305),
(330503,3,'PURING',3305),
(330504,4,'PETANAHAN',3305),
(330505,5,'KLIRONG',3305),
(330506,6,'BULUPESANTREN',3305),
(330507,7,'AMBAL',3305),
(330508,8,'MIRIT',3305),
(330509,9,'PREMBUN',3305),
(330510,10,'KUTOWINANGUN',3305),
(330511,11,'ALIAN',3305),
(330512,12,'KEBUMEN',3305),
(330513,13,'PEJAGOAN',3305),
(330514,14,'SRUWENG',3305),
(330515,15,'ADIMULYO',3305),
(330516,16,'KUWARASAN',3305),
(330517,17,'ROWOKELE',3305),
(330518,18,'SEMPOR',3305),
(330519,19,'GOMBONG',3305),
(330520,20,'KARANGANYAR',3305),
(330521,21,'KARANGGAYAM',3305),
(330522,22,'SADANG',3305),
(330523,23,'BONOROWO',3305),
(330524,24,'PADURESO',3305),
(330525,25,'PONCOWARNO',3305),
(330526,26,'KARANGSAMBUNG',3305),
(330601,1,'GRABAG',3306),
(330602,2,'NGOMBOL',3306),
(330603,3,'PURWODADI',3306),
(330604,4,'BAGELEN',3306),
(330605,5,'KALIGESING',3306),
(330606,6,'PURWOREJO',3306),
(330607,7,'BANYU URIP',3306),
(330608,8,'BAYAN',3306),
(330609,9,'KUTOARJO',3306),
(330610,10,'BUTUH',3306),
(330611,11,'PITURUH',3306),
(330612,12,'KEMIRI',3306),
(330613,13,'BRUNO',3306),
(330614,14,'GEBANG',3306),
(330615,15,'LOANO',3306),
(330616,16,'BENER',3306),
(330701,1,'WADASLINTANG',3307),
(330702,2,'KEPIL',3307),
(330703,3,'SAPURAN',3307),
(330704,4,'KALIWIRO',3307),
(330705,5,'LEKSONO',3307),
(330706,6,'SELOMERTO',3307),
(330707,7,'KALIKAJAR',3307),
(330708,8,'KERTEK',3307),
(330709,9,'WONOSOBO',3307),
(330710,10,'WATUMALANG',3307),
(330711,11,'MOJOTENGAH',3307),
(330712,12,'GARUNG',3307),
(330713,13,'KEJAJAR',3307),
(330714,14,'SUKOHARJO',3307),
(330715,15,'KALI BAWANG',3307),
(330801,1,'SALAMAN',3308),
(330802,2,'BOROBUDUR',3308),
(330803,3,'NGLUWAR',3308),
(330804,4,'SALAM',3308),
(330805,5,'SRUMBUNG',3308),
(330806,6,'DUKUN',3308),
(330807,7,'SAWANGAN',3308),
(330808,8,'MUNTILAN',3308),
(330809,9,'MUNGKID',3308),
(330810,10,'MARTOYUDAN',3308),
(330811,11,'TEMPURAN',3308),
(330812,12,'KAJORAN',3308),
(330813,13,'KALIANGKRIK',3308),
(330814,14,'BANDONGAN',3308),
(330815,15,'CANDIMULYO',3308),
(330816,16,'PAKIS',3308),
(330817,17,'NGABLAK',3308),
(330818,18,'GRABAG',3308),
(330819,19,'TEGALREJO',3308),
(330820,20,'SECANG',3308),
(330821,21,'WINDUSARI',3308),
(330901,1,'SELO',3309),
(330902,2,'AMPEL',3309),
(330903,3,'CEPOGO',3309),
(330904,4,'MUSUK',3309),
(330905,5,'BOYOLALI',3309),
(330906,6,'MOJOSONGO',3309),
(330907,7,'TERAS',3309),
(330908,8,'SAWIT',3309),
(330909,9,'BANYUDONO',3309),
(330910,10,'SAMBI',3309),
(330911,11,'NGEMPLAK',3309),
(330912,12,'NOGOSARI',3309),
(330913,13,'SIMO',3309),
(330914,14,'KARANGGEDE',3309),
(330915,15,'KLEGO',3309),
(330916,16,'ANDONG',3309),
(330917,17,'KEMUSU',3309),
(330918,18,'WONOSEGORO',3309),
(330919,19,'JUWANGI',3309),
(331001,1,'PRAMBANAN',3310),
(331002,2,'GANTIWARNO',3310),
(331003,3,'WEDI',3310),
(331004,4,'BAYAT',3310),
(331005,5,'CAWAS',3310),
(331006,6,'TRUCUK',3310),
(331007,7,'KEBONARUM',3310),
(331008,8,'JOGONALAN',3310),
(331009,9,'MANISRENGGO',3310),
(331010,10,'KARANGNONGKO',3310),
(331011,11,'CEPER',3310),
(331012,12,'PEDAN',3310),
(331013,13,'KARANGDOWO',3310),
(331014,14,'JUWIRING',3310),
(331015,15,'WONOSARI',3310),
(331016,16,'DELANGGU',3310),
(331017,17,'POLANHARJO',3310),
(331018,18,'KARANGANOM',3310),
(331019,19,'TULUNG',3310),
(331020,20,'JATINOM',3310),
(331021,21,'KEMALANG',3310),
(331022,22,'NGAWEN',3310),
(331023,23,'KALIKOTES',3310),
(331024,24,'KLATEN UTARA',3310),
(331025,25,'KLATEN TENGAH',3310),
(331026,26,'KLATEN SELATAN',3310),
(331101,1,'WERU',3311),
(331102,2,'BULU',3311),
(331103,3,'TAWANGSARI',3311),
(331104,4,'SUKOHARJO',3311),
(331105,5,'NGUTER',3311),
(331106,6,'BENDOSARI',3311),
(331107,7,'POLOKARTO',3311),
(331108,8,'MOJOLABAN',3311),
(331109,9,'GROGOL',3311),
(331110,10,'BAKI',3311),
(331111,11,'GATAK',3311),
(331112,12,'KARTASURA',3311),
(331201,1,'PRACIMANTORO',3312),
(331202,2,'GIRITONTRO',3312),
(331203,3,'GIRIWOYO',3312),
(331204,4,'BATUWARNO',3312),
(331205,5,'TIRTOMOYO',3312),
(331206,6,'NGUNTORONADI',3312),
(331207,7,'BATURETNO',3312),
(331208,8,'EROMOKO',3312),
(331209,9,'WURYANTORO',3312),
(331210,10,'MANYARAN',3312),
(331211,11,'SELOGIRI',3312),
(331212,12,'WONOGIRI',3312),
(331213,13,'NGADIROJO',3312),
(331214,14,'SIDOHARJO',3312),
(331215,15,'JATIROTO',3312),
(331216,16,'KISMANTORO',3312),
(331217,17,'PURWANTORO',3312),
(331218,18,'BULUKERTO',3312),
(331219,19,'SLOGOHIMO',3312),
(331220,20,'JATISRONO',3312),
(331221,21,'JATIPURNO',3312),
(331222,22,'GIRIMARTO',3312),
(331223,23,'KARANGTENGAH',3312),
(331224,24,'PARANGGUPITO',3312),
(331225,25,'PUHPELEM',3312),
(331301,1,'JATIPURO',3313),
(331302,2,'JATIYOSO',3313),
(331303,3,'JUMAPOLO',3313),
(331304,4,'JUMANTONO',3313),
(331305,5,'MATESIH',3313),
(331306,6,'TAWANGMANGU',3313),
(331307,7,'NGARGOYOSO',3313),
(331308,8,'KARANGPANDAN',3313),
(331309,9,'KARANGANYAR',3313),
(331310,10,'TASIKMADU',3313),
(331311,11,'JATEN',3313),
(331312,12,'COLOMADU',3313),
(331313,13,'GONDANGREJO',3313),
(331314,14,'KEBAKKRAMAT',3313),
(331315,15,'MOJOGEDANG',3313),
(331316,16,'KERJO',3313),
(331317,17,'JENAWI',3313),
(331401,1,'KALIJAMBE',3314),
(331402,2,'PLUPUH',3314),
(331403,3,'MASARAN',3314),
(331404,4,'KEDAWUNG',3314),
(331405,5,'SAMBIREJO',3314),
(331406,6,'GONDANG',3314),
(331407,7,'SAMBUNG MACAN',3314),
(331408,8,'NGRAMPAL',3314),
(331409,9,'KARANGMALANG',3314),
(331410,10,'SRAGEN',3314),
(331411,11,'SIDOHARJO',3314),
(331412,12,'TANON',3314),
(331413,13,'GEMOLONG',3314),
(331414,14,'MIRI',3314),
(331415,15,'SUMBERLAWANG',3314),
(331416,16,'MONDOKAN',3314),
(331417,17,'SUKODONO',3314),
(331418,18,'GESI',3314),
(331419,19,'TANGEN',3314),
(331420,20,'JENAR',3314),
(331501,1,'KEDUNGJATI',3315),
(331502,2,'KARANGRAYUNG',3315),
(331503,3,'PENAWANGAN',3315),
(331504,4,'TOROH',3315),
(331505,5,'GEYER',3315),
(331506,6,'PULOKULON',3315),
(331507,7,'KRADENAN',3315),
(331508,8,'GABUS',3315),
(331509,9,'NGARINGAN',3315),
(331510,10,'WIROSARI',3315),
(331511,11,'TAWANGHARJO',3315),
(331512,12,'GROBOGAN',3315),
(331513,13,'PURWODADI',3315),
(331514,14,'BRATI',3315),
(331515,15,'KLAMBU',3315),
(331516,16,'GODONG',3315),
(331517,17,'GUBUG',3315),
(331518,18,'TEGOWANU',3315),
(331519,19,'TANGGUNGHARJO',3315),
(331601,1,'JATI',3316),
(331602,2,'RANDUBLATUNG',3316),
(331603,3,'KRADENAN',3316),
(331604,4,'KEDUNGTUBAN',3316),
(331605,5,'CEPU',3316),
(331606,6,'SAMBONG',3316),
(331607,7,'JIKEN',3316),
(331608,8,'JEPON',3316),
(331609,9,'KOTA BLORA',3316),
(331610,10,'TUNJUNGAN',3316),
(331611,11,'BANJAREJO',3316),
(331612,12,'NGAWEN',3316),
(331613,13,'KUNDURAN',3316),
(331614,14,'TODANAN',3316),
(331615,15,'BOGOREJO',3316),
(331616,16,'JAPAH',3316),
(331701,1,'SUMBER',3317),
(331702,2,'BULU',3317),
(331703,3,'GUNEM',3317),
(331704,4,'SALE',3317),
(331705,5,'SARANG',3317),
(331706,6,'SEDAN',3317),
(331707,7,'PAMOTAN',3317),
(331708,8,'SULANG',3317),
(331709,9,'KALIORI',3317),
(331710,10,'REMBANG',3317),
(331711,11,'PANCUR',3317),
(331712,12,'KRAGAN',3317),
(331713,13,'SLUKE',3317),
(331714,14,'LASEM',3317),
(331801,1,'SUKOLILO',3318),
(331802,2,'KAYEN',3318),
(331803,3,'TAMBAKROMO',3318),
(331804,4,'WINONG',3318),
(331805,5,'PUCAKWANGI',3318),
(331806,6,'JAKEN',3318),
(331807,7,'BATANGAN',3318),
(331808,8,'JUWANA',3318),
(331809,9,'JAKENAN',3318),
(331810,10,'PATI',3318),
(331811,11,'GABUS',3318),
(331812,12,'MARGOREJO',3318),
(331813,13,'GEMBONG',3318),
(331814,14,'TLOGOWUNGU',3318),
(331815,15,'WEDARIJAKSA',3318),
(331816,16,'MARGOYOSO',3318),
(331817,17,'GUNGWUNGKAL',3318),
(331818,18,'CLUWAK',3318),
(331819,19,'TAYU',3318),
(331820,20,'DUKUHSETI',3318),
(331821,21,'TRANGKIL',3318),
(331901,1,'KALIWUNGU',3319),
(331902,2,'KOTA KUDUS',3319),
(331903,3,'JATI',3319),
(331904,4,'UNDAAN',3319),
(331905,5,'MEJOBO',3319),
(331906,6,'JEKULO',3319),
(331907,7,'BAE',3319),
(331908,8,'GEBOG',3319),
(331909,9,'DAWE',3319),
(332001,1,'KEDUNG',3320),
(332002,2,'PECANGAAN',3320),
(332003,3,'WELAHAN',3320),
(332004,4,'MAYONG',3320),
(332005,5,'BATEALIT',3320),
(332006,6,'JEPARA',3320),
(332007,7,'MLONGGO',3320),
(332008,8,'BANGSRI',3320),
(332009,9,'KELING',3320),
(332010,10,'KARIMUNJAWA',3320),
(332011,11,'TAHUNAN',3320),
(332012,12,'NALUMSARI',3320),
(332013,13,'KALINYAMATAN',3320),
(332014,14,'KEMBANG',3320),
(332101,1,'MRANGGEN',3321),
(332102,2,'KARANGAWEN',3321),
(332103,3,'GUNTUR',3321),
(332104,4,'SAYUNG',3321),
(332105,5,'KARANG TENGAH',3321),
(332106,6,'WONOSALAM',3321),
(332107,7,'DEMPET',3321),
(332108,8,'GAJAH',3321),
(332109,9,'KARANGANYAR',3321),
(332110,10,'MIJEN',3321),
(332111,11,'DEMAK',3321),
(332112,12,'BONANG',3321),
(332113,13,'WEDUNG',3321),
(332114,14,'KEBONAGUNG',3321),
(332201,1,'GETASAN',3322),
(332202,2,'TENGARAN',3322),
(332203,3,'SUSUKAN',3322),
(332204,4,'SURUH',3322),
(332205,5,'PABELAN',3322),
(332206,6,'TUNTANG',3322),
(332207,7,'BANYUBIRU',3322),
(332208,8,'JAMBU',3322),
(332209,9,'SOMOWONO',3322),
(332210,10,'AMBARAWA',3322),
(332211,11,'BAWEN',3322),
(332212,12,'BRINGIN',3322),
(332213,13,'BERGAS',3322),
(332214,14,'UNGARAN',3322),
(332215,15,'PRINGAPUS',3322),
(332216,16,'BANCAK',3322),
(332217,17,'KALIWUNGU',3322),
(332301,1,'BULU',3323),
(332302,2,'TEMBARAK',3323),
(332303,3,'TEMANGGUNG',3323),
(332304,4,'PRINGSURAT',3323),
(332305,5,'KALORAN',3323),
(332306,6,'KANDANGAN',3323),
(332307,7,'KEDU',3323),
(332308,8,'PARAKAN',3323),
(332309,9,'NGADIREJO',3323),
(332310,10,'JUMO',3323),
(332311,11,'TRETEP',3323),
(332312,12,'CANDIROTO',3323),
(332313,13,'KRANGGAN',3323),
(332314,14,'TLOGOMULYO',3323),
(332315,15,'SELOPAMPANG',3323),
(332316,16,'BANSARI',3323),
(332317,17,'KLEDUNG',3323),
(332318,18,'BEJEN',3323),
(332319,19,'WONOBOYO',3323),
(332320,20,'GEMAWANG',3323),
(332401,1,'PLANTUNGAN',3324),
(332402,2,'PAGERRUYUNG',3324),
(332403,3,'SUKOREJO',3324),
(332404,4,'PAKIS',3324),
(332405,5,'SINGOROJO',3324),
(332406,6,'LIMBANGAN',3324),
(332407,7,'BOJA',3324),
(332408,8,'KALIWUNGU',3324),
(332409,9,'BRANGSONG',3324),
(332410,10,'PEGANDON',3324),
(332411,11,'GEMUH',3324),
(332412,12,'WELERI',3324),
(332413,13,'CIPIRING',3324),
(332414,14,'PATEBON',3324),
(332415,15,'KOTA KENDAL',3324),
(332416,16,'ROWOSARI',3324),
(332417,17,'KANGKUNG',3324),
(332418,18,'RINGINARUM',3324),
(332419,19,'NGAMPEL',3324),
(332501,1,'WONOTUNGGAL',3325),
(332502,2,'BANDAR',3325),
(332503,3,'BLADO',3325),
(332504,4,'REBAN',3325),
(332505,5,'BAWANG',3325),
(332506,6,'TERSONO',3325),
(332507,7,'GRINGSING',3325),
(332508,8,'LIMPUNG',3325),
(332509,9,'SUBAH',3325),
(332510,10,'TULIS',3325),
(332511,11,'BATANG',3325),
(332512,12,'WARUNG ASEM',3325),
(332601,1,'KANDANGSERANG',3326),
(332602,2,'PANINGGARAN',3326),
(332603,3,'LEBAKBARANG',3326),
(332604,4,'PETUNGKRIONO',3326),
(332605,5,'TALUN',3326),
(332606,6,'DORO',3326),
(332607,7,'KARANGANYAR',3326),
(332608,8,'KAJEN',3326),
(332609,9,'KESESI',3326),
(332610,10,'SRAGI',3326),
(332611,11,'BOJONG',3326),
(332612,12,'WONOPRINGGO',3326),
(332613,13,'KEDUNGWUNI',3326),
(332614,14,'BUARAN',3326),
(332615,15,'T I R T O',3326),
(332616,16,'WIRADESA',3326),
(332617,17,'SIWALAN',3326),
(332618,18,'KARANGDADAP',3326),
(332619,19,'WONOKERTO',3326),
(332701,1,'MOGA',3327),
(332702,2,'PULOSARI',3327),
(332703,3,'BELIK',3327),
(332704,4,'WATUKUMPUL',3327),
(332705,5,'BODEH',3327),
(332706,6,'BANTARBOLANG',3327),
(332707,7,'RANDUDONGKAL',3327),
(332708,8,'PEMALANG',3327),
(332709,9,'TAMAN',3327),
(332710,10,'PETARUKAN',3327),
(332711,11,'AMPELGADING',3327),
(332712,12,'COMAL',3327),
(332713,13,'ULUJAMI',3327),
(332714,14,'WARUNGPRING',3327),
(332801,1,'MARGASARI',3328),
(332802,2,'BUMIJAWA',3328),
(332803,3,'BOJONG',3328),
(332804,4,'BALAPULANG',3328),
(332805,5,'PAGERBARANG',3328),
(332806,6,'LEBAKSIU',3328),
(332807,7,'JATINEGARA',3328),
(332808,8,'KEDUNG BANTENG',3328),
(332809,9,'PANGKAH',3328),
(332810,10,'SLAWI',3328),
(332811,11,'ADIWERNA',3328),
(332812,12,'TALANG',3328),
(332813,13,'DUKUHTURI',3328),
(332814,14,'TARUB',3328),
(332815,15,'KRAMAT',3328),
(332816,16,'SURADADI',3328),
(332817,17,'WARUREJA',3328),
(332818,18,'DUKUHWARU',3328),
(332901,1,'SALEM',3329),
(332902,2,'BANTARKAWUNG',3329),
(332903,3,'BUMIAYU',3329),
(332904,4,'PAGUYANGAN',3329),
(332905,5,'SIRAMPOG',3329),
(332906,6,'TONJONG',3329),
(332907,7,'JATIBARANG',3329),
(332908,8,'WANASARI',3329),
(332909,9,'BREBES',3329),
(332910,10,'SONGGOM',3329),
(332911,11,'KERSANA',3329),
(332912,12,'LOSARI',3329),
(332913,13,'TANJUNG',3329),
(332914,14,'BULAKAMBA',3329),
(332915,15,'LARANGAN',3329),
(332916,16,'KETANGGUNGAN',3329),
(332917,17,'BANJARHARJO',3329),
(337101,1,'MAGELANG UTARA',3371),
(337102,2,'MAGELANG SELATAN',3371),
(337201,1,'LAWEYAN',3372),
(337202,2,'SERENGAN',3372),
(337203,3,'PASAR KLIWON',3372),
(337204,4,'JEBRES',3372),
(337205,5,'BANJARSARI',3372),
(337301,1,'SIDOREJO',3373),
(337302,2,'TINGKIR',3373),
(337303,3,'ARGOMULYO',3373),
(337304,4,'SIDOMUKTI',3373),
(337401,1,'SEMARANG TENGAH',3374),
(337402,2,'SEMARANG UTARA',3374),
(337403,3,'SEMARANG TIMUR',3374),
(337404,4,'GAYAMSARI',3374),
(337405,5,'GENUK',3374),
(337406,6,'PEDURUNGAN',3374),
(337407,7,'SEMARANG SELATAN',3374),
(337408,8,'CANDISARI',3374),
(337409,9,'GAJAH MUNGKUR',3374),
(337410,10,'TEMBALANG',3374),
(337411,11,'BANYUMANIK',3374),
(337412,12,'GUNUNG PATI',3374),
(337413,13,'SEMARANG BARAT',3374),
(337414,14,'MIJEN',3374),
(337415,15,'NGALIYAN',3374),
(337416,16,'TUGU',3374),
(337501,1,'PEKALONGAN BARAT',3375),
(337502,2,'PEKALONGAN TIMUR',3375),
(337503,3,'PEKALONGAN UTARA',3375),
(337504,4,'PEKALONGAN SELATAN',3375),
(337601,1,'TEGAL BARAT',3376),
(337602,2,'TEGAL TIMUR',3376),
(337603,3,'TEGAL SELATAN',3376),
(337604,4,'MARGADANA',3376),
(340101,1,'TEMON',3401),
(340102,2,'WATES',3401),
(340103,3,'PANJATAN',3401),
(340104,4,'GALUR',3401),
(340105,5,'LENDAH',3401),
(340106,6,'SENTOLO',3401),
(340107,7,'PENGASIH',3401),
(340108,8,'KOKAP',3401),
(340109,9,'GIRIMULYO',3401),
(340110,10,'NANGGULAN',3401),
(340111,11,'SAMIGALUH',3401),
(340112,12,'KALIBAWANG',3401),
(340201,1,'SRANDAKAN',3402),
(340202,2,'SANDEN',3402),
(340203,3,'KRETEK',3402),
(340204,4,'PUNDONG',3402),
(340205,5,'BAMBANG LIPURO',3402),
(340206,6,'PANDAK',3402),
(340207,7,'PAJANGAN',3402),
(340208,8,'BANTUL',3402),
(340209,9,'JETIS',3402),
(340210,10,'IMOGIRI',3402),
(340211,11,'DLINGO',3402),
(340212,12,'BANGUNTAPAN',3402),
(340213,13,'PLERET',3402),
(340214,14,'PIYUNGAN',3402),
(340215,15,'SEWON',3402),
(340216,16,'KASIHAN',3402),
(340217,17,'SEDAYU',3402),
(340301,1,'WONOSARI',3403),
(340302,2,'NGLIPAR',3403),
(340303,3,'PLAYEN',3403),
(340304,4,'PATUK',3403),
(340305,5,'PALIYAN',3403),
(340306,6,'PANGGANG',3403),
(340307,7,'TEPUS',3403),
(340308,8,'SEMANU',3403),
(340309,9,'KARANGMOJO',3403),
(340310,10,'PONJONG',3403),
(340311,11,'RONGKOP',3403),
(340312,12,'SEMIN',3403),
(340313,13,'NGAWEN',3403),
(340314,14,'GEDANGSARI',3403),
(340315,15,'SAPTOSARI',3403),
(340316,16,'GIRISUBO',3403),
(340317,17,'TANJUNGSARI',3403),
(340318,18,'PURWOSARI',3403),
(340401,1,'GAMPING',3404),
(340402,2,'GODEAN',3404),
(340403,3,'MOYUDAN',3404),
(340404,4,'MINGGIR',3404),
(340405,5,'SEYEGAN',3404),
(340406,6,'MLATI',3404),
(340407,7,'DEPOK',3404),
(340408,8,'BERBAH',3404),
(340409,9,'PRAMBANAN',3404),
(340410,10,'KALASAN',3404),
(340411,11,'NGEMPLAK',3404),
(340412,12,'NGAGLIK',3404),
(340413,13,'SLEMAN',3404),
(340414,14,'TEMPEL',3404),
(340415,15,'TURI',3404),
(340416,16,'PAKEM',3404),
(340417,17,'CANGKRINGAN',3404),
(347101,1,'TEGALREJO',3471),
(347102,2,'JETIS',3471),
(347103,3,'GONDOKUSUMAN',3471),
(347104,4,'DANUREJAN',3471),
(347105,5,'GEDONGTANGEN',3471),
(347106,6,'NGAMPILAN',3471),
(347107,7,'WIROBRAJAN',3471),
(347108,8,'MANTRIJERON',3471),
(347109,9,'KRATON',3471),
(347110,10,'GONDOMANAN',3471),
(347111,11,'PAKUALAMAN',3471),
(347112,12,'MERGANGSAN',3471),
(347113,13,'UMBULHARJO',3471),
(347114,14,'KOTAGEDE',3471),
(350101,1,'DONOROJO',3501),
(350102,2,'PRINGKUKU',3501),
(350103,3,'PUNUNG',3501),
(350104,4,'PACITAN',3501),
(350105,5,'KEBON AGUNG',3501),
(350106,6,'ARJOSARI',3501),
(350107,7,'NAWANGAN',3501),
(350108,8,'BANDAR',3501),
(350109,9,'TEGALOMBO',3501),
(350110,10,'TULAKAN',3501),
(350111,11,'NGADIROJO',3501),
(350112,12,'SUDIMORO',3501),
(350201,1,'SLAHUNG',3502),
(350202,2,'NGRAYUN',3502),
(350203,3,'BUNGKAL',3502),
(350204,4,'SAMBIT',3502),
(350205,5,'SAWOO',3502),
(350206,6,'SOOKO',3502),
(350207,7,'PULUNG',3502),
(350208,8,'MLARAK',3502),
(350209,9,'JETIS',3502),
(350210,10,'SIMAN',3502),
(350211,11,'BALONG',3502),
(350212,12,'KAUMAN',3502),
(350213,13,'BADEGAN',3502),
(350214,14,'SAMPUNG',3502),
(350215,15,'SUKOREJO',3502),
(350216,16,'BABADAN',3502),
(350217,17,'PONOROGO',3502),
(350218,18,'JENANGAN',3502),
(350219,19,'NGEBEL',3502),
(350220,20,'JAMBON',3502),
(350221,21,'PUDAK',3502),
(350301,1,'PANGGUL',3503),
(350302,2,'MUNJUNGAN',3503),
(350303,3,'PULE',3503),
(350304,4,'DONGKO',3503),
(350305,5,'TUGU',3503),
(350306,6,'KARANGAN',3503),
(350307,7,'KAMPAK',3503),
(350308,8,'WATULIMO',3503),
(350309,9,'BENDUNGAN',3503),
(350310,10,'GANDUSARI',3503),
(350311,11,'TRENGGALEK',3503),
(350312,12,'POGALAN',3503),
(350313,13,'DURENAN',3503),
(350314,14,'SURUH',3503),
(350401,1,'TULUNGAGUNG',3504),
(350402,2,'BOYOLANGU',3504),
(350403,3,'KEDUNGWARU',3504),
(350404,4,'NGANTRU',3504),
(350405,5,'KAUMAN',3504),
(350406,6,'PAGER WOJO',3504),
(350407,7,'SENDANG',3504),
(350408,8,'KARANGREJO',3504),
(350409,9,'GONDANG',3504),
(350410,10,'SUMBER GEMPOL',3504),
(350411,11,'NGUNUT',3504),
(350412,12,'PUCANG LABAN',3504),
(350413,13,'REJOTANGAN',3504),
(350414,14,'KALIDAWIR',3504),
(350415,15,'BESUKI',3504),
(350416,16,'CAMPUR DARAT',3504),
(350417,17,'BANDUNG',3504),
(350418,18,'PAKEL',3504),
(350419,19,'TANGGUNG GUNUNG',3504),
(350501,1,'WONODADI',3505),
(350502,2,'UDANAWU',3505),
(350503,3,'SRENGAT',3505),
(350504,4,'KADEMANGAN',3505),
(350505,5,'BAKUNG',3505),
(350506,6,'PONGGOK',3505),
(350507,7,'SANANKULON',3505),
(350508,8,'WONOTIRTO',3505),
(350509,9,'NGLEGOK',3505),
(350510,10,'KANIGORO',3505),
(350511,11,'GARUM',3505),
(350512,12,'SUTOJAYAN',3505),
(350513,13,'PANGGUNGREJO',3505),
(350514,14,'TALUN',3505),
(350515,15,'GANDUSARI',3505),
(350516,16,'BINANGUN',3505),
(350517,17,'WLINGI',3505),
(350518,18,'DOKO',3505),
(350519,19,'KESAMBEN',3505),
(350520,20,'WATES',3505),
(350521,21,'SELOREJO',3505),
(350522,22,'SELOPURO',3505),
(350601,1,'SEMEN',3506),
(350602,2,'MOJO',3506),
(350603,3,'KRAS',3506),
(350604,4,'NGADILUWIH',3506),
(350605,5,'KANDAT',3506),
(350606,6,'WATES',3506),
(350607,7,'NGANCAR',3506),
(350608,8,'PUNCU',3506),
(350609,9,'PLOSOKLATEN',3506),
(350610,10,'GURAH',3506),
(350611,11,'PAGU',3506),
(350612,12,'GAMPENGREJO',3506),
(350613,13,'GROGOL',3506),
(350614,14,'PAPAR',3506),
(350615,15,'PURWOASRI',3506),
(350616,16,'PLEMAHAN',3506),
(350617,17,'PARE',3506),
(350618,18,'KEPUNG',3506),
(350619,19,'KANDANGAN',3506),
(350620,20,'TAROKAN',3506),
(350621,21,'KUNJANG',3506),
(350622,22,'BANYAKAN',3506),
(350623,23,'RINGINREJO',3506),
(350701,1,'DONOMULYO',3507),
(350702,2,'PAGAK',3507),
(350703,3,'BANTUR',3507),
(350704,4,'SUMBERMANJING/ WETAN',3507),
(350705,5,'DAMPIT',3507),
(350706,6,'AMPELGADING',3507),
(350707,7,'PONCOKUSUMO',3507),
(350708,8,'WAJAK',3507),
(350709,9,'TUREN',3507),
(350710,10,'GONDANGLEGI',3507),
(350711,11,'KALIPARE',3507),
(350712,12,'SUMBER PUCUNG',3507),
(350713,13,'KEPANJEN',3507),
(350714,14,'BULULAWANG',3507),
(350715,15,'TAJINAN',3507),
(350716,16,'TUMPANG',3507),
(350717,17,'JABUNG',3507),
(350718,18,'PAKIS',3507),
(350719,19,'PAKISAJI',3507),
(350720,20,'NGAJUM',3507),
(350721,21,'WAGIR',3507),
(350722,22,'DAU',3507),
(350723,23,'KARANGPLOSO',3507),
(350724,24,'SINGOSARI',3507),
(350725,25,'LAWANG',3507),
(350726,26,'PUJON',3507),
(350727,27,'NGANTANG',3507),
(350728,28,'KASEMBON',3507),
(350729,29,'GEDANGAN',3507),
(350730,30,'TIRTO YUDO',3507),
(350731,31,'KROMENGAN',3507),
(350732,32,'WONOSARI',3507),
(350733,33,'PAGELARAN',3507),
(350801,1,'TEMPURSARI',3508),
(350802,2,'PRONOJIWO',3508),
(350803,3,'CANDIPURO',3508),
(350804,4,'PASIRIAN',3508),
(350805,5,'TEMPEH',3508),
(350806,6,'KUNIR',3508),
(350807,7,'YOSOWILANGUN',3508),
(350808,8,'ROWOKANGKUNG',3508),
(350809,9,'TEKUNG',3508),
(350810,10,'LUMAJANG',3508),
(350811,11,'PASRUJAMBE',3508),
(350812,12,'SENDURO',3508),
(350813,13,'GUCIALIT',3508),
(350814,14,'PADANG',3508),
(350815,15,'SUKODONO',3508),
(350816,16,'KEDUNGJAJANG',3508),
(350817,17,'JATIROTO',3508),
(350818,18,'RANDUAGUNG',3508),
(350819,19,'KLAKAH',3508),
(350820,20,'RANUYOSO',3508),
(350821,21,'SUMBERSUKO',3508),
(350901,1,'JOMBANG',3509),
(350902,2,'KENCONG',3509),
(350903,3,'SUMBER BARU',3509),
(350904,4,'GUMUK MAS',3509),
(350905,5,'UMBULSARI',3509),
(350906,6,'TANGGUL',3509),
(350907,7,'SEMBORO',3509),
(350908,8,'PUGER',3509),
(350909,9,'BANGSALSARI',3509),
(350910,10,'BALUNG',3509),
(350911,11,'WULUHAN',3509),
(350912,12,'AMBULU',3509),
(350913,13,'RAMBIPUJI',3509),
(350914,14,'PANTI',3509),
(350915,15,'SUKORAMBI',3509),
(350916,16,'JENGGAWAH',3509),
(350917,17,'AJUNG',3509),
(350918,18,'TEMPUREJO',3509),
(350919,19,'KALIWATES',3509),
(350920,20,'PATRANG',3509),
(350921,21,'SUMBERSARI',3509),
(350922,22,'ARJASA',3509),
(350923,23,'MUMBULSARI',3509),
(350924,24,'PAKUSARI',3509),
(350925,25,'JELBUK',3509),
(350926,26,'MAYANG',3509),
(350927,27,'KALISAT',3509),
(350928,28,'LEDOKOMBO',3509),
(350929,29,'SUKOWONO',3509),
(350930,30,'SILO',3509),
(350931,31,'SUMBERJAMBE',3509),
(351001,1,'PESANGGARAN',3510),
(351002,2,'BANGOREJO',3510),
(351003,3,'PURWOHARJO',3510),
(351004,4,'TEGALDLIMO',3510),
(351005,5,'MUNCAR',3510),
(351006,6,'CLURING',3510),
(351007,7,'GAMBIRAN',3510),
(351008,8,'SRONO',3510),
(351009,9,'GENTENG',3510),
(351010,10,'GLENMORE',3510),
(351011,11,'KALIBARU',3510),
(351012,12,'SINGOJURUH',3510),
(351013,13,'ROGOJAMPI',3510),
(351014,14,'KABAT',3510),
(351015,15,'GLAGAH',3510),
(351016,16,'BANYUWANGI',3510),
(351017,17,'GIRI',3510),
(351018,18,'WONGSOREJO',3510),
(351019,19,'SONGGON',3510),
(351020,20,'SEMPU',3510),
(351021,21,'KALIPURO',3510),
(351101,1,'MAESAN',3511),
(351102,2,'TAMANAN',3511),
(351103,3,'TLOGOSARI',3511),
(351104,4,'SUKOSARI',3511),
(351105,5,'PUJER',3511),
(351106,6,'GRUJUGAN',3511),
(351107,7,'CURAHDAMI',3511),
(351108,8,'TENGGARANG',3511),
(351109,9,'WONOSARI',3511),
(351110,10,'TAPEN',3511),
(351111,11,'BONDOWOSO',3511),
(351112,12,'WRINGIN',3511),
(351113,13,'TEGALAMPEL',3511),
(351114,14,'KLABANG',3511),
(351115,15,'CERMEE',3511),
(351116,16,'PRAJEKAN',3511),
(351117,17,'PAKEM',3511),
(351118,18,'SUMBER WRINGIN',3511),
(351119,19,'SEMPOL',3511),
(351120,20,'BINAKAL',3511),
(351201,1,'JATIBANTENG',3512),
(351202,2,'BESUKI',3512),
(351203,3,'SUBOH',3512),
(351204,4,'MLANDINGAN',3512),
(351205,5,'KENDIT',3512),
(351206,6,'PANARUKAN',3512),
(351207,7,'SITUBONDO',3512),
(351208,8,'PANJI',3512),
(351209,9,'MANGARAN',3512),
(351210,10,'KAPONGAN',3512),
(351211,11,'ARJASA',3512),
(351212,12,'JANGKAR',3512),
(351213,13,'ASEMBAGUS',3512),
(351214,14,'BANYUPUTIH',3512),
(351215,15,'SUMBERMALANG',3512),
(351216,16,'BANYUGLUGUR',3512),
(351217,17,'BUNGATAN',3512),
(351301,1,'SUKAPURA',3513),
(351302,2,'SUMBER',3513),
(351303,3,'KURIPAN',3513),
(351304,4,'BANTARAN',3513),
(351305,5,'LECES',3513),
(351306,6,'BANYU ANYAR',3513),
(351307,7,'TIRIS',3513),
(351308,8,'KRUCIL',3513),
(351309,9,'GADING',3513),
(351310,10,'PAKUNIRAN',3513),
(351311,11,'KOTA ANYAR',3513),
(351312,12,'PAITON',3513),
(351313,13,'BESUK',3513),
(351314,14,'KRAKSAAN',3513),
(351315,15,'KREJENGAN',3513),
(351316,16,'PAJARAKAN',3513),
(351317,17,'MARON',3513),
(351318,18,'GENDING',3513),
(351319,19,'DRINGU',3513),
(351320,20,'TEGAL SIWALAN',3513),
(351321,21,'SUMBER ASIH',3513),
(351322,22,'WONOMERTO',3513),
(351323,23,'TONGAS',3513),
(351324,24,'LUMBANG',3513),
(351401,1,'PURWODADI',3514),
(351402,2,'TUTUR',3514),
(351403,3,'PUSPO',3514),
(351404,4,'LUMBANG',3514),
(351405,5,'PASREPAN',3514),
(351406,6,'KEJAYAN',3514),
(351407,7,'WONOREJO',3514),
(351408,8,'PURWOSARI',3514),
(351409,9,'SUKOREJO',3514),
(351410,10,'PRIGEN',3514),
(351411,11,'PANDAAN',3514),
(351412,12,'GEMPOL',3514),
(351413,13,'BEJI',3514),
(351414,14,'BANGIL',3514),
(351415,15,'REMBANG',3514),
(351416,16,'KRATON',3514),
(351417,17,'POHJENTREK',3514),
(351418,18,'GONDANG WETAN',3514),
(351419,19,'WINONGAN',3514),
(351420,20,'GRATI',3514),
(351421,21,'NGULING',3514),
(351422,22,'LEKOK',3514),
(351423,23,'REJOSO',3514),
(351424,24,'TOSARI',3514),
(351501,1,'TARIK',3515),
(351502,2,'PRAMBON',3515),
(351503,3,'KREMBUNG',3515),
(351504,4,'PORONG',3515),
(351505,5,'JABON',3515),
(351506,6,'TANGGULANGIN',3515),
(351507,7,'CANDI',3515),
(351508,8,'SIDOARJO',3515),
(351509,9,'TULANGAN',3515),
(351510,10,'WONOAYU',3515),
(351511,11,'KRIAN',3515),
(351512,12,'BALONG BENDO',3515),
(351513,13,'TAMAN',3515),
(351514,14,'SUKODONO',3515),
(351515,15,'BUDURAN',3515),
(351516,16,'GEDANGAN',3515),
(351517,17,'SEDATI',3515),
(351518,18,'WARU',3515),
(351601,1,'JATIREJO',3516),
(351602,2,'GONDANG',3516),
(351603,3,'PACET',3516),
(351604,4,'TRAWAS',3516),
(351605,5,'NGORO',3516),
(351606,6,'PUNGGING',3516),
(351607,7,'KUTOREJO',3516),
(351608,8,'MOJOSARI',3516),
(351609,9,'DLANGGU',3516),
(351610,10,'BANGSAL',3516),
(351611,11,'PURI',3516),
(351612,12,'TROWULAN',3516),
(351613,13,'SOOKO',3516),
(351614,14,'GEDEK',3516),
(351615,15,'KEMLAGI',3516),
(351616,16,'JETIS',3516),
(351617,17,'DAWAR BLANDONG',3516),
(351618,18,'MOJOANYAR',3516),
(351701,1,'PERAK',3517),
(351702,2,'GUDO',3517),
(351703,3,'NGORO',3517),
(351704,4,'BARENG',3517),
(351705,5,'WONOSALAM',3517),
(351706,6,'MOJOAGUNG',3517),
(351707,7,'MOJOWARNO',3517),
(351708,8,'DIWEK',3517),
(351709,9,'JOMBANG',3517),
(351710,10,'PETERONGAN',3517),
(351711,11,'SUMOBITO',3517),
(351712,12,'KESAMBEN',3517),
(351713,13,'TEMBELANG',3517),
(351714,14,'PLOSO',3517),
(351715,15,'PLANDAAN',3517),
(351716,16,'KABUH',3517),
(351717,17,'KUDU',3517),
(351718,18,'BANDAR KEDUNG MULYO',3517),
(351719,19,'JOGOROTO',3517),
(351720,20,'MEGALUH',3517),
(351721,21,'NGUSIKAN',3517),
(351801,1,'SAWAHAN',3518),
(351802,2,'NGETOS',3518),
(351803,3,'BERBEK',3518),
(351804,4,'LOCERET',3518),
(351805,5,'PACE',3518),
(351806,6,'PRAMBON',3518),
(351807,7,'NGRONGGOT',3518),
(351808,8,'KERTOSONO',3518),
(351809,9,'PATIANROWO',3518),
(351810,10,'BARON',3518),
(351811,11,'TAJUNGANOM',3518),
(351812,12,'SUKOMORO',3518),
(351813,13,'NGANJUK',3518),
(351814,14,'BAGOR',3518),
(351815,15,'WILANGAN',3518),
(351816,16,'REJOSO',3518),
(351817,17,'GONDANG',3518),
(351818,18,'NGLUYU',3518),
(351819,19,'LENGKONG',3518),
(351820,20,'JATIKALEN',3518),
(351901,1,'KEBONSARI',3519),
(351902,2,'DOLOPO',3519),
(351903,3,'GEGER',3519),
(351904,4,'DAGANGAN',3519),
(351905,5,'KARE',3519),
(351906,6,'GEMARANG',3519),
(351907,7,'WUNGU',3519),
(351908,8,'MADIUN',3519),
(351909,9,'JIWAN',3519),
(351910,10,'BALEREJO',3519),
(351911,11,'MEJAYAN',3519),
(351912,12,'SARADAN',3519),
(351913,13,'PILANGKENCENG',3519),
(351914,14,'SAWAHAN',3519),
(351915,15,'WONOASRI',3519),
(352001,1,'PONCOL',3520),
(352002,2,'PARANG',3520),
(352003,3,'LEMBEYAN',3520),
(352004,4,'TAKERAN',3520),
(352005,5,'KAWEDANAN',3520),
(352006,6,'MAGETAN',3520),
(352007,7,'PLAOSAN',3520),
(352008,8,'PANEKAN',3520),
(352009,9,'SUKOMORO',3520),
(352010,10,'BENDO',3520),
(352011,11,'MAOSPATI',3520),
(352012,12,'KARANGMOJO',3520),
(352013,13,'KARANGREJO',3520),
(352014,14,'KARAS',3520),
(352015,15,'KARTO HARJO',3520),
(352016,16,'NGARIBOYO',3520),
(352101,1,'SINE',3521),
(352102,2,'NGRAMBE',3521),
(352103,3,'JOGOROGO',3521),
(352104,4,'KENDAL',3521),
(352105,5,'GENENG',3521),
(352106,6,'KWADUNGAN',3521),
(352107,7,'KARANGJATI',3521),
(352108,8,'PADAS',3521),
(352109,9,'NGAWI',3521),
(352110,10,'PARON',3521),
(352111,11,'KEDUNGGALAR',3521),
(352112,12,'WIDODAREN',3521),
(352113,13,'MANTINGAN',3521),
(352114,14,'PANGKUR',3521),
(352115,15,'BRINGIN',3521),
(352116,16,'PITU',3521),
(352117,17,'KARANGANYAR',3521),
(352201,1,'NGRAHO',3522),
(352202,2,'TAMBAKREJO',3522),
(352203,3,'NGAMBON',3522),
(352204,4,'NGASEM',3522),
(352205,5,'BUBULAN',3522),
(352206,6,'DANDER',3522),
(352207,7,'SUGIHWARAS',3522),
(352208,8,'KEDUNGADEM',3522),
(352209,9,'KEPOH BARU',3522),
(352210,10,'BAURENO',3522),
(352211,11,'KANOR',3522),
(352212,12,'SUMBEREJO',3522),
(352213,13,'BALEN',3522),
(352214,14,'KAPAS',3522),
(352215,15,'BOJONEGORO',3522),
(352216,16,'KALITIDU',3522),
(352217,17,'MALO',3522),
(352218,18,'PURWOSARI',3522),
(352219,19,'PADANGAN',3522),
(352220,20,'KASIMAN',3522),
(352221,21,'TEMAYANG',3522),
(352222,22,'MARGOMULYO',3522),
(352223,23,'TRUCUK',3522),
(352224,24,'SUKOSEWU',3522),
(352225,25,'KEDEWAN',3522),
(352226,26,'GONDANG',3522),
(352227,27,'SEKAR',3522),
(352301,1,'KENDURUAN',3523),
(352302,2,'JATIROGO',3523),
(352303,3,'BANGILAN',3523),
(352304,4,'BANCAR',3523),
(352305,5,'SENORI',3523),
(352306,6,'TAMBAKBOYO',3523),
(352307,7,'SINGGAHAN',3523),
(352308,8,'KEREK',3523),
(352309,9,'PARENGAN',3523),
(352310,10,'MONTONG',3523),
(352311,11,'SOKO',3523),
(352312,12,'JENU',3523),
(352313,13,'MERAKURAK',3523),
(352314,14,'RENGEL',3523),
(352315,15,'SEMANDING',3523),
(352316,16,'TUBAN',3523),
(352317,17,'PLUMPANG',3523),
(352318,18,'PALANG',3523),
(352319,19,'WIDANG',3523),
(352401,1,'SUKORAME',3524),
(352402,2,'BLULUK',3524),
(352403,3,'MODO',3524),
(352404,4,'NGIMBANG',3524),
(352405,5,'BABAT',3524),
(352406,6,'KEDUNGPRING',3524),
(352407,7,'BRONDONG',3524),
(352408,8,'LAREN',3524),
(352409,9,'SEKARAN',3524),
(352410,10,'MADURAN',3524),
(352411,11,'SAMBENG',3524),
(352412,12,'SUGIO',3524),
(352413,13,'PUCUK',3524),
(352414,14,'PACIRAN',3524),
(352415,15,'SOLOKURO',3524),
(352416,16,'MANTUP',3524),
(352417,17,'SUKODADI',3524),
(352418,18,'KARANG GENENG',3524),
(352419,19,'KEMBANGBAHU',3524),
(352420,20,'KALITENGAH',3524),
(352421,21,'TURI',3524),
(352422,22,'LAMONGAN',3524),
(352423,23,'TIKUNG',3524),
(352424,24,'KARANGBINANGUN',3524),
(352425,25,'DEKET',3524),
(352426,26,'GLAGAH',3524),
(352427,27,'SARIREJO',3524),
(352501,1,'DUKUN',3525),
(352502,2,'BALONGPANGGANG',3525),
(352503,3,'PANCENG',3525),
(352504,4,'BENJENG',3525),
(352505,5,'DUDUKSAMPEYAN',3525),
(352506,6,'WRINGINANOM',3525),
(352507,7,'UJUNGPANGKAH',3525),
(352508,8,'KEDAMEAN',3525),
(352509,9,'SIDAYU',3525),
(352510,10,'MANYAR',3525),
(352511,11,'CERME',3525),
(352512,12,'BUNGAH',3525),
(352513,13,'MENGANTI',3525),
(352514,14,'KEBOMAS',3525),
(352515,15,'DRIYOREJO',3525),
(352516,16,'GRESIK',3525),
(352517,17,'SANGKAPURA',3525),
(352518,18,'TAMBAK',3525),
(352601,1,'BANGKALAN',3526),
(352602,2,'SOCAH',3526),
(352603,3,'BURNEH',3526),
(352604,4,'KAMAL',3526),
(352605,5,'AROSBAYA',3526),
(352606,6,'GEGER',3526),
(352607,7,'KLAMPIS',3526),
(352608,8,'SEPULU',3526),
(352609,9,'TANJUNGBUMI',3526),
(352610,10,'KOKOP',3526),
(352611,11,'KWANYAR',3526),
(352612,12,'LABANG',3526),
(352613,13,'TANAH MERAH',3526),
(352614,14,'TRAGAH',3526),
(352615,15,'BLEGA',3526),
(352616,16,'MODUNG',3526),
(352617,17,'KONANG',3526),
(352618,18,'GALIS',3526),
(352701,1,'SRESEH',3527),
(352702,2,'TORJUN',3527),
(352703,3,'SAMPANG',3527),
(352704,4,'CAMPLONG',3527),
(352705,5,'OMBEN',3527),
(352706,6,'KEDUNGDUNG',3527),
(352707,7,'JRENGIK',3527),
(352708,8,'TAMBELANGAN',3527),
(352709,9,'BANYUATES',3527),
(352710,10,'ROBATAL',3527),
(352711,11,'SOKOBANAH',3527),
(352712,12,'KETAPANG',3527),
(352801,1,'TLANAKAN',3528),
(352802,2,'PADEMAWU',3528),
(352803,3,'GALIS',3528),
(352804,4,'PAMEKASAN',3528),
(352805,5,'PROPPO',3528),
(352806,6,'PALENGAN',3528),
(352807,7,'PEGANTENAN',3528),
(352808,8,'LARANGAN',3528),
(352809,9,'PAKONG',3528),
(352810,10,'WARU',3528),
(352811,11,'BATU MARMAR',3528),
(352812,12,'KADUR',3528),
(352813,13,'PASEAN',3528),
(352901,1,'KOTA SUMENEP',3529),
(352902,2,'KALIANGET',3529),
(352903,3,'MANDING',3529),
(352904,4,'TALANGO',3529),
(352905,5,'BLUTO',3529),
(352906,6,'SARONGGI',3529),
(352907,7,'LENTENG',3529),
(352908,8,'GILIGENTENG',3529),
(352909,9,'GULUK GULUK',3529),
(352910,10,'GANDING',3529),
(352911,11,'PRAGAAN',3529),
(352912,12,'AMBUNTEN',3529),
(352913,13,'PASONGSONGAN',3529),
(352914,14,'DASUK',3529),
(352915,15,'RUBARU',3529),
(352916,16,'BATANG BATANG',3529),
(352917,17,'BATUPUTIH',3529),
(352918,18,'DUNGKEK',3529),
(352919,19,'GAPURA',3529),
(352920,20,'GAYAM',3529),
(352921,21,'NONGGUNONG',3529),
(352922,22,'RAAS',3529),
(352923,23,'MASALEMBU',3529),
(352924,24,'ARJASA',3529),
(352925,25,'SAPEKEN',3529),
(357101,1,'MOJOROTO',3571),
(357102,2,'KOTA KEDIRI',3571),
(357103,3,'PESANTREN',3571),
(357201,1,'KEPANJEN KIDUL',3572),
(357202,2,'SUKOREJO',3572),
(357203,3,'SANANWETAN',3572),
(357301,1,'BLIMBING',3573),
(357302,2,'KLOJEN',3573),
(357303,3,'KEDUNGKANDANG',3573),
(357304,4,'SUKUN',3573),
(357305,5,'LOWOKWARU',3573),
(357401,1,'KADEMANGAN',3574),
(357402,2,'WONOASIH',3574),
(357403,3,'MAYANGAN',3574),
(357501,1,'GADINGREJO',3575),
(357502,2,'PURWOREJO',3575),
(357503,3,'BUGULKIDUL',3575),
(357601,1,'PRAJURIT KULON',3576),
(357602,2,'MAGERSARI',3576),
(357701,1,'KARTOHARJO',3577),
(357702,2,'MANGU HARJO',3577),
(357703,3,'TAMAN',3577),
(357801,1,'KARANG PILANG',3578),
(357802,2,'WONOCOLO',3578),
(357803,3,'RUNGKUT',3578),
(357804,4,'WONOKROMO',3578),
(357805,5,'TEGALSARI',3578),
(357806,6,'SAWAHAN',3578),
(357807,7,'GENTENG',3578),
(357808,8,'GUBENG',3578),
(357809,9,'SUKOLILO',3578),
(357810,10,'TAMBAK SARI',3578),
(357811,11,'SIMOKERTO',3578),
(357812,12,'PABEAN CANTIAN',3578),
(357813,13,'BUBUTAN',3578),
(357814,14,'TANDES',3578),
(357815,15,'KREMBANGAN',3578),
(357816,16,'SEMAMPIR',3578),
(357817,17,'KENJERAN',3578),
(357818,18,'LAKAR SANTRI',3578),
(357819,19,'BENOWO',3578),
(357820,20,'WIYUNG',3578),
(357821,21,'DUKUH PAKIS',3578),
(357822,22,'GAYUNGAN',3578),
(357823,23,'JAMBANGAN',3578),
(357824,24,'TENGGILIS MEJOYO',3578),
(357825,25,'GUNUNG ANYAR',3578),
(357826,26,'MULYOREJO',3578),
(357827,27,'SUKOMANUNGGAL',3578),
(357828,28,'ASEMROWO',3578),
(357829,29,'BULAK',3578),
(357830,30,'PAKAL',3578),
(357831,31,'SAMBI KEREP',3578),
(357901,1,'BATU',3579),
(357902,2,'BUMIAJI',3579),
(357903,3,'JUNREJO',3579),
(360101,1,'SUMUR',3601),
(360102,2,'CIMANGGU',3601),
(360103,3,'CIBALIUNG',3601),
(360104,4,'CIKEUSIK',3601),
(360105,5,'CIGEULIS',3601),
(360106,6,'PANIMBANG',3601),
(360107,7,'ANGSANA',3601),
(360108,8,'MUNJUL',3601),
(360109,9,'PAGELARAN',3601),
(360110,10,'BOJONG',3601),
(360111,11,'PICUNG',3601),
(360112,12,'LABUAN',3601),
(360113,13,'MENES',3601),
(360114,14,'SAKETI',3601),
(360115,15,'CIPEUCANG',3601),
(360116,16,'JIPUT',3601),
(360117,17,'MANDALAWANGI',3601),
(360118,18,'CIMANUK',3601),
(360119,19,'KADUHEJO',3601),
(360120,20,'BANJAR',3601),
(360121,21,'PANDEGLANG',3601),
(360122,22,'CADASARI',3601),
(360123,23,'CISATA',3601),
(360124,24,'PATIA',3601),
(360125,25,'KARANG TANJUNG',3601),
(360126,26,'CIKEDAL',3601),
(360127,27,'CIBITUNG',3601),
(360128,28,'CARITA',3601),
(360129,29,'SUKARESMI',3601),
(360130,30,'MEKARJAYA',3601),
(360131,31,'SINDANGRESMI',3601),
(360132,32,'PULOSARI',3601),
(360133,33,'KORONCONG',3601),
(360201,1,'MALINGPING',3602),
(360202,2,'PANGGARANGAN',3602),
(360203,3,'BAYAH',3602),
(360204,4,'CIPANAS',3602),
(360205,5,'MUNCANG',3602),
(360206,6,'LEUWIDAMAR',3602),
(360207,7,'BOJONGMANIK',3602),
(360208,8,'GUNUNG KENCANA',3602),
(360209,9,'BANJARSARI',3602),
(360210,10,'CILELES',3602),
(360211,11,'CIMARGA',3602),
(360212,12,'SAJIRA',3602),
(360213,13,'MAJA',3602),
(360214,14,'RANGKASBITUNG',3602),
(360215,15,'WARUNGGUNUNG',3602),
(360216,16,'CIJAKU',3602),
(360217,17,'CIKULUR',3602),
(360218,18,'CIBADAK',3602),
(360219,19,'CIBEBER',3602),
(360301,1,'BALARAJA',3603),
(360302,2,'JAYANTI',3603),
(360303,3,'TIGARAKSA',3603),
(360304,4,'JAMBE',3603),
(360305,5,'CISOKA',3603),
(360306,6,'KRESEK',3603),
(360307,7,'KRONJO',3603),
(360308,8,'M A U K',3603),
(360309,9,'KEMIRI',3603),
(360310,10,'SUKADIRI',3603),
(360311,11,'RAJEG',3603),
(360312,12,'PASAR KEMIS',3603),
(360313,13,'TELUKNAGA',3603),
(360314,14,'KOSAMBI',3603),
(360315,15,'PAKUHAJI',3603),
(360316,16,'SEPATAN',3603),
(360317,17,'CURUG',3603),
(360318,18,'CIKUPA',3603),
(360319,19,'PANONGAN',3603),
(360320,20,'LEGOK',3603),
(360321,21,'SERPONG',3603),
(360322,22,'PAGEDANGAN',3603),
(360323,23,'CISAUK',3603),
(360324,24,'PONDOK AREN',3603),
(360325,25,'PAMULANG',3603),
(360326,26,'CIPUTAT',3603),
(360401,1,'SERANG',3604),
(360402,2,'CIPOCOK JAYA',3604),
(360403,3,'KASEMEN',3604),
(360404,4,'TAKTAKAN',3604),
(360405,5,'KRAMATWATU',3604),
(360406,6,'WARINGINKURUNG',3604),
(360407,7,'BOJONEGORO',3604),
(360408,8,'PULO AMPEL',3604),
(360409,9,'CIRUAS',3604),
(360410,10,'WALANTAKA',3604),
(360411,11,'KRAGILAN',3604),
(360412,12,'PONTANG',3604),
(360413,13,'TIRTAYASA',3604),
(360414,14,'TANARA',3604),
(360415,15,'CIKANDE',3604),
(360416,16,'KIBIN',3604),
(360417,17,'CARENANG',3604),
(360418,18,'BINUANG',3604),
(360419,19,'PETIR',3604),
(360420,20,'TUNJUNG TEJA',3604),
(360421,21,'CURUG',3604),
(360422,22,'BAROS',3604),
(360423,23,'CIKEUSAL',3604),
(360424,24,'PAMARAYAN',3604),
(360425,25,'KOPO',3604),
(360426,26,'JAWILAN',3604),
(360427,27,'CIOMAS',3604),
(360428,28,'PABUARAN',3604),
(360429,29,'PADARINCANG',3604),
(360430,30,'ANYAR',3604),
(360431,31,'CINANGKA',3604),
(360432,32,'MANCAK',3604),
(367101,1,'TANGERANG',3671),
(367102,2,'JATIUWUNG',3671),
(367103,3,'BATU CEPER',3671),
(367104,4,'BENDA',3671),
(367105,5,'CIPONDOH',3671),
(367106,6,'CILEDUG',3671),
(367107,7,'KARAWACI',3671),
(367108,8,'PERIUK',3671),
(367109,9,'CIBODAS',3671),
(367110,10,'NEGLASARI',3671),
(367111,11,'PINANG',3671),
(367112,12,'KARANG TENGAH',3671),
(367113,13,'LARANGAN',3671),
(367201,1,'CIBEBER',3672),
(367202,2,'CILEGON',3672),
(367203,3,'PULOMERAK',3672),
(367204,4,'CIWANDAN',3672),
(367205,5,'JOMBANG',3672),
(367206,6,'GEROGOL',3672),
(367207,7,'PURWAKARTA',3672),
(367208,8,'CITANGKIL',3672),
(510101,1,'NEGARA',5101),
(510102,2,'MENDOYO',5101),
(510103,3,'PEKUTATAN',5101),
(510104,4,'MELAYA',5101),
(510201,1,'SELEMADEG',5102),
(510202,2,'SALAMADEG TIMUR',5102),
(510203,3,'SALEMADEG BARAT',5102),
(510204,4,'KERAMBITAN',5102),
(510205,5,'TABANAN',5102),
(510206,6,'KEDIRI',5102),
(510207,7,'MARGA',5102),
(510208,8,'PENEBEL',5102),
(510209,9,'BATURITI',5102),
(510210,10,'PUPUAN',5102),
(510301,1,'KUTA',5103),
(510302,2,'MENGWI',5103),
(510303,3,'ABIANSEMAL',5103),
(510304,4,'PETANG',5103),
(510305,5,'KUTA SELATAN',5103),
(510306,6,'KUTA UTARA',5103),
(510401,1,'SUKAWATI',5104),
(510402,2,'BLAHBATUH',5104),
(510403,3,'GIANYAR',5104),
(510404,4,'TAMPAK SIRING',5104),
(510405,5,'UBUD',5104),
(510406,6,'TEGALLALANG',5104),
(510407,7,'PAYANGAN',5104),
(510501,1,'NUSA PENIDA',5105),
(510502,2,'BANJARANGKAN',5105),
(510503,3,'KLUNGKUNG',5105),
(510504,4,'DAWAN',5105),
(510601,1,'SUSUT',5106),
(510602,2,'BANGLI',5106),
(510603,3,'TEMBUKU',5106),
(510604,4,'KINTAMANI',5106),
(510701,1,'RENDANG',5107),
(510702,2,'SIDEMEN',5107),
(510703,3,'MANGGIS',5107),
(510704,4,'KARANG ASEM',5107),
(510705,5,'ABANG',5107),
(510706,6,'BEBANDEM',5107),
(510707,7,'SELAT',5107),
(510708,8,'KUBU',5107),
(510801,1,'GROKGAK',5108),
(510802,2,'SERIRIT',5108),
(510803,3,'BUSUNGBIU',5108),
(510804,4,'BANJAR',5108),
(510805,5,'SUKASADA',5108),
(510806,6,'BULELENG',5108),
(510807,7,'SAWAN',5108),
(510808,8,'KUBU TAMBAHAN',5108),
(510809,9,'TEJAKULA',5108),
(517101,1,'DENPASAR SELATAN',5171),
(517102,2,'DENPASAR TIMUR',5171),
(517103,3,'DENPASAR BARAT',5171),
(517104,4,'DENPASAR UTARA',5171),
(520101,1,'GERUNG',5201),
(520102,2,'KEDIRI',5201),
(520103,3,'NARMADA',5201),
(520104,4,'TANJUNG',5201),
(520105,5,'GANGGA',5201),
(520106,6,'BAYAN',5201),
(520107,7,'SEKOTONG TENGAH',5201),
(520108,8,'LABU API',5201),
(520109,9,'GUNUNG SARI',5201),
(520110,10,'KAYANGAN',5201),
(520111,11,'PEMENANG',5201),
(520112,12,'LINGSAR',5201),
(520113,13,'LEMBAR',5201),
(520114,14,'BATU LAYAR',5201),
(520115,15,'KURIPAN',5201),
(520201,1,'PRAYA',5202),
(520202,2,'JONGGAT',5202),
(520203,3,'BATUKLIANG',5202),
(520204,4,'PUJUT',5202),
(520205,5,'PRAYA BARAT',5202),
(520206,6,'PRAYA TIMUR',5202),
(520207,7,'JANAPRIA',5202),
(520208,8,'PRINGGARATA',5202),
(520209,9,'KOPANG',5202),
(520210,10,'PRAYA TENGAH',5202),
(520211,11,'PRAYA BARAT DAYA',5202),
(520212,12,'BATUKLIANG UTARA',5202),
(520301,1,'KERUAK',5203),
(520302,2,'SAKRA',5203),
(520303,3,'TERARA',5203),
(520304,4,'SIKUR',5203),
(520305,5,'MASBAGIK',5203),
(520306,6,'SUKAMULIA',5203),
(520307,7,'SELONG',5203),
(520308,8,'PRINGGABAYA',5203),
(520309,9,'AIKMEL',5203),
(520310,10,'SAMBELIA',5203),
(520311,11,'MONTONG GADING',5203),
(520312,12,'PRINGGASELA',5203),
(520313,13,'SURALAGA',5203),
(520314,14,'WANASABA',5203),
(520315,15,'SEMBALUN',5203),
(520316,16,'SUWELA',5203),
(520317,17,'LABUHAN HAJI',5203),
(520318,18,'SAKRA TIMUR',5203),
(520319,19,'SAKRA BARAT',5203),
(520320,20,'JEROWARU',5203),
(520401,1,'JEREWEH',5204),
(520402,2,'LUNYUK',5204),
(520403,3,'TALIWANG',5204),
(520404,4,'SETELUK',5204),
(520405,5,'ALAS',5204),
(520406,6,'UTAN RHEE',5204),
(520407,7,'BATULANTEH',5204),
(520408,8,'SUMBAWA',5204),
(520409,9,'MOYOHILIR',5204),
(520410,10,'MOYOHULU',5204),
(520411,11,'ROPANG',5204),
(520412,12,'LAPE LOPOK',5204),
(520413,13,'PLAMPANG',5204),
(520414,14,'EMPANG',5204),
(520415,15,'SEKONGKANG',5204),
(520416,16,'BRANG REA',5204),
(520417,17,'ALAS BARAT',5204),
(520418,18,'LABUHAN BADAS',5204),
(520419,19,'LABANGKA',5204),
(520501,1,'DOMPU',5205),
(520502,2,'KEMPO',5205),
(520503,3,'HU\'U',5205),
(520504,4,'KILO',5205),
(520505,5,'WOJA',5205),
(520506,6,'PEKAT',5205),
(520507,7,'MANGGALEWA',5205),
(520508,8,'PAJO',5205),
(520601,1,'MONTA',5206),
(520602,2,'BOLO',5206),
(520603,3,'WOHA',5206),
(520604,4,'BELO',5206),
(520605,5,'WAWO',5206),
(520606,6,'SAPE',5206),
(520607,7,'WERA',5206),
(520608,8,'DONGGO',5206),
(520609,9,'SANGGAR',5206),
(520610,10,'AMBALAWI',5206),
(520611,11,'LANGGUDU',5206),
(520612,12,'LAMBU',5206),
(520613,13,'MADAPANGGA',5206),
(520614,14,'TAMBORA',5206),
(520703,3,'TALIWANG',5207),
(520704,4,'SETELUK',5207),
(527101,1,'AMPENAN',5271),
(527102,2,'MATARAM',5271),
(527103,3,'CAKRANEGARA',5271),
(527201,1,'RASANAE BARAT',5272),
(527202,2,'RASANAE TIMUR',5272),
(527203,3,'ASAKOTA',5272),
(530101,1,'RAIJUA',5301),
(530102,2,'SABU BARAT',5301),
(530103,3,'SABU TIMUR',5301),
(530104,4,'SEMAU',5301),
(530105,5,'KUPANG BARAT',5301),
(530106,6,'KUPANG TIMUR',5301),
(530107,7,'SULAMU',5301),
(530108,8,'KUPANG TENGAH',5301),
(530109,9,'AMARASI',5301),
(530110,10,'FATULEU',5301),
(530111,11,'TAKARI',5301),
(530112,12,'AMFOANG SELATAN',5301),
(530113,13,'AMFOANG UTARA',5301),
(530114,14,'HAWU MEHARA',5301),
(530115,15,'SABU LIAE',5301),
(530116,16,'NEKAMESE',5301),
(530117,17,'AMARASI BARAT',5301),
(530118,18,'AMARASI SELATAN',5301),
(530119,19,'AMARASI TIMUR',5301),
(530120,20,'AMABI OEFETO TIMUR',5301),
(530121,21,'AMFOANG BARAT DAYA',5301),
(530122,22,'AMFOANG BARAT LAUT',5301),
(530201,1,'KOTA SOE',5302),
(530202,2,'MOLLO SELATAN',5302),
(530203,3,'MOLLO UTARA',5302),
(530204,4,'AMANUBAN TIMUR',5302),
(530205,5,'AMANUBAN TENGAH',5302),
(530206,6,'AMANUBAN SELATAN',5302),
(530207,7,'AMANUBAN BARAT',5302),
(530208,8,'AMANATUN SELATAN',5302),
(530209,9,'AMANATUN UTARA',5302),
(530210,10,'KIE',5302),
(530211,11,'KUAN FATU',5302),
(530212,12,'FATUMNASI',5302),
(530213,13,'POLEN',5302),
(530214,14,'BATU PUTIH',5302),
(530215,15,'BOKING',5302),
(530216,16,'TOIANAS',5302),
(530217,17,'NUNKOLO',5302),
(530218,18,'OENIO',5302),
(530219,19,'KOLBANO',5302),
(530220,20,'KOT\'OLIN',5302),
(530221,21,'KUALIN',5302),
(530301,1,'MIOMAFFO TIMUR',5303),
(530302,2,'MIOMAFFO BARAT',5303),
(530303,3,'BIBOKI SELATAN',5303),
(530304,4,'NOEMUTI',5303),
(530305,5,'KOTA KEFAMENANU',5303),
(530306,6,'BIBOKI UTARA',5303),
(530307,7,'BIBOKI ANLEU',5303),
(530308,8,'INSANA',5303),
(530309,9,'INSANA UTARA',5303),
(530401,1,'LAMAKNEN',5304),
(530402,2,'TASIFETO TIMUR',5304),
(530403,3,'RAIHAT',5304),
(530404,4,'TASIFETO BARAT',5304),
(530405,5,'KAKULUK MESAK',5304),
(530406,6,'MALAKA TIMUR',5304),
(530407,7,'KOBALIMA',5304),
(530408,8,'MALAKA TENGAH',5304),
(530409,9,'SASITA MEAN',5304),
(530410,10,'MALAKA BARAT',5304),
(530411,11,'RINHAT',5304),
(530412,12,'KOTA ATAMBUA',5304),
(530501,1,'TELUK MUTIARA',5305),
(530502,2,'ALOR BARAT LAUT',5305),
(530503,3,'ALOR BARAT DAYA',5305),
(530504,4,'ALOR SELATAN',5305),
(530505,5,'ALOR TIMUR',5305),
(530506,6,'PANTAR',5305),
(530507,7,'ALOR TENGAH UTARA',5305),
(530508,8,'ALOR TIMUR LAUT',5305),
(530509,9,'PANTAR BARAT',5305),
(530601,1,'WULANGGITANG',5306),
(530602,2,'TITEHENA',5306),
(530603,3,'LARANTUKA',5306),
(530604,4,'ILE MANDIRI',5306),
(530605,5,'TANJUNG BUNGA',5306),
(530606,6,'SOLOR BARAT',5306),
(530607,7,'SOLOR TIMUR',5306),
(530608,8,'ADONARA BARAT',5306),
(530609,9,'WOTAN ULU MANDO',5306),
(530610,10,'ADONARA TIMUR',5306),
(530611,11,'KELUBAGOLIT',5306),
(530612,12,'WITIHAMA',5306),
(530613,13,'ILE BOLENG',5306),
(530701,1,'PAGA',5307),
(530702,2,'MEGO',5307),
(530703,3,'LELA',5307),
(530704,4,'NITA',5307),
(530705,5,'ALOK',5307),
(530706,6,'PALUE',5307),
(530707,7,'MAUMERE',5307),
(530708,8,'TALIBURA',5307),
(530709,9,'WAIGETE',5307),
(530710,10,'KEWAPANTE',5307),
(530711,11,'BOLA',5307),
(530801,1,'NANGA PANDA',5308),
(530802,2,'PULAU ENDE',5308),
(530803,3,'ENDE',5308),
(530804,4,'ENDE SELATAN',5308),
(530805,5,'NDONA',5308),
(530806,6,'DETUSOKO',5308),
(530807,7,'WEWARIA',5308),
(530808,8,'WOLO WARU',5308),
(530809,9,'WOLOJITA',5308),
(530810,10,'MAUROLE',5308),
(530811,11,'MAUKARO',5308),
(530812,12,'WATUNESO',5308),
(530813,13,'KOTA BARU',5308),
(530814,14,'NDONA TIMUR',5308),
(530815,15,'KELIMUTU',5308),
(530816,16,'DETUKELI',5308),
(530901,1,'AIMERE',5309),
(530902,2,'GOLEWA',5309),
(530906,6,'BAJAWA',5309),
(530907,7,'SOA',5309),
(530909,9,'RIUNG',5309),
(530912,12,'JEREBUU',5309),
(530914,14,'RIUNG BARAT',5309),
(530915,15,'BAJAWA UTARA',5309),
(530916,16,'RIUNG SELATAN',5309),
(531001,1,'WAE RII',5310),
(531002,2,'POCO RANAKA',5310),
(531003,3,'RUTENG',5310),
(531004,4,'LAMBA LEDA',5310),
(531005,5,'SATARMESE',5310),
(531006,6,'CIBAL',5310),
(531007,7,'ELAR',5310),
(531008,8,'BORONG',5310),
(531009,9,'KOTA KOMBA',5310),
(531010,10,'SAMBI RAMPAS',5310),
(531011,11,'REOK',5310),
(531012,12,'LANGKE REMBONG',5310),
(531101,1,'KOTA WAINGAPU',5311),
(531102,2,'HAHARU',5311),
(531103,3,'LEWA',5311),
(531104,4,'NGGAHA ORIANGU',5311),
(531105,5,'TABUNDUNG',5311),
(531106,6,'PIRAPAHAR',5311),
(531107,7,'PANDAWAI',5311),
(531108,8,'UMALULU',5311),
(531109,9,'RINDI',5311),
(531110,10,'PAHUNGA LODU',5311),
(531111,11,'WULA WAIJELU',5311),
(531112,12,'PABERIWAI',5311),
(531113,13,'KARERA',5311),
(531114,14,'KAHAUNGU ETI',5311),
(531115,15,'MATAWAI LA PPAWAU',5311),
(531204,4,'TANA RIGHU',5312),
(531210,10,'LOLI',5312),
(531211,11,'WANOKAKA',5312),
(531212,12,'LAMBOYA',5312),
(531215,15,'KOTA WAIKABUBAK',5312),
(531301,1,'NAGAWUTUNG',5313),
(531302,2,'ATADEI',5313),
(531303,3,'ILE APE',5313),
(531304,4,'LEBATUKAN',5313),
(531305,5,'NUBATUKAN',5313),
(531306,6,'OMESURI',5313),
(531307,7,'BUYASARI',5313),
(531308,8,'WULANDONI',5313),
(531401,1,'ROTE BARAT DAYA',5314),
(531402,2,'ROTE BARAT LAUT',5314),
(531403,3,'LOBALAIN',5314),
(531404,4,'ROTE TENGAH',5314),
(531405,5,'PANTAI BARU',5314),
(531406,6,'ROTE TIMUR',5314),
(531501,1,'MACANG PACAR',5315),
(531502,2,'KUWUS',5315),
(531503,3,'LEMBOR',5315),
(531504,4,'SANO NGGOANG',5315),
(531505,5,'KOMODO',5315),
(531601,1,'AESESA',5316),
(531602,2,'NANGARORO',5316),
(531603,3,'BOAWAE',5316),
(531604,4,'MAUPONGGO',5316),
(531605,5,'WOLOWAE',5316),
(531606,6,'KEO TENGAH',5316),
(531607,7,'AESESA SELATAN',5316),
(531701,1,'KATIKU TANA',5317),
(531702,2,'UMBU RATU NGGAY BARAT',5317),
(531703,3,'MAMBORO',5317),
(531704,4,'UMBU RATU NGGAY',5317),
(531801,1,'LAURA',5318),
(531802,2,'WEWEWA UTARA',5318),
(531803,3,'WEWEWA TIMUR',5318),
(531804,4,'WEWEWA BARAT',5318),
(531805,5,'WEWEWA SELATAN',5318),
(531806,6,'KODI BANGEDO',5318),
(531807,7,'KODI',5318),
(531808,8,'KODI UTARA',5318),
(537101,1,'ALAK',5371),
(537102,2,'MAULAFA',5371),
(537103,3,'KELAPA LIMA',5371),
(537104,4,'OEBOBO',5371),
(610101,1,'SAMBAS',6101),
(610102,2,'TELUK KERAMAT',6101),
(610103,3,'JAWAI',6101),
(610104,4,'TEBAS',6101),
(610105,5,'PEMANGKAT',6101),
(610106,6,'SEJANGKUNG',6101),
(610107,7,'SELAKAU',6101),
(610108,8,'PALOH',6101),
(610109,9,'SAJINGAN BESAR',6101),
(610110,10,'SUBAH',6101),
(610111,11,'GALING',6101),
(610112,12,'TEKARANG',6101),
(610201,1,'MEMPAWAH HILIR',6102),
(610202,2,'SUNGAI RAYA',6102),
(610203,3,'TELOK PAKEDAI',6102),
(610204,4,'SUNGAI KAKAP',6102),
(610205,5,'TERENTANG',6102),
(610206,6,'TOHO',6102),
(610207,7,'SUNGAI PINYUH',6102),
(610208,8,'SIANTAN',6102),
(610209,9,'KUBU',6102),
(610210,10,'BATU AMPAR',6102),
(610211,11,'SUNGAI AMBAWANG',6102),
(610212,12,'SUNGAI KUNYIT',6102),
(610213,13,'KUALA MANDOR-B',6102),
(610214,14,'RASAU JAYA',6102),
(610301,1,'SANGGAU KAPUAS',6103),
(610302,2,'MUKOK',6103),
(610303,3,'NOYAN',6103),
(610304,4,'JANGKANG',6103),
(610305,5,'BONTI',6103),
(610306,6,'BEDUWAI',6103),
(610307,7,'SEKAYAM',6103),
(610308,8,'KEMBAYAN',6103),
(610309,9,'PARINDU',6103),
(610310,10,'TAYAN HULU',6103),
(610311,11,'TAYAN HILIR',6103),
(610312,12,'BALAI',6103),
(610313,13,'TOBA',6103),
(610320,20,'MELIAU',6103),
(610321,21,'ENTIKONG',6103),
(610401,1,'MATAN HILIR UTARA',6104),
(610402,2,'MARAU',6104),
(610403,3,'MANIS MATA',6104),
(610404,4,'KENDAWANGAN',6104),
(610405,5,'SANDAI',6104),
(610407,7,'SUNGAI LAUR',6104),
(610408,8,'SIMPANG HULU',6104),
(610411,11,'NANGA TAYAP',6104),
(610412,12,'MATAN HILIR SELATAN',6104),
(610413,13,'TUMBANG TITI',6104),
(610414,14,'JELAI HULU',6104),
(610419,19,'HULU SUNGAI',6104),
(610420,20,'SIMPANG DUA',6104),
(610421,21,'AIR UPAS',6104),
(610422,22,'SINGKUP',6104),
(610424,24,'PEMAHAN',6104),
(610425,25,'SUNGAI MELAYU RAYAK',6104),
(610501,1,'SINTANG',6105),
(610502,2,'TEMPUNAK',6105),
(610503,3,'SEPAUK',6105),
(610504,4,'KETUNGAU HILIR',6105),
(610505,5,'KETUNGAU TENGAH',6105),
(610506,6,'KETUNGAU HULU',6105),
(610507,7,'DEDAI',6105),
(610508,8,'KAYAN HILIR',6105),
(610509,9,'KAYAN HULU',6105),
(610514,14,'NANGA SERAWAI',6105),
(610515,15,'AMBALAU',6105),
(610519,19,'KELAM PERMAI',6105),
(610520,20,'SUNGAI TEBELIAN',6105),
(610521,21,'BINJAI HULU',6105),
(610601,1,'PUTUSSIBAU',6106),
(610602,2,'MANDAY',6106),
(610603,3,'EMBALOH HILIR',6106),
(610604,4,'EMBALOH HULU',6106),
(610605,5,'BUNUT HILIR',6106),
(610606,6,'BUNUT HULU',6106),
(610607,7,'EMBAU',6106),
(610608,8,'HULU GURUNG',6106),
(610609,9,'SELIMBAU',6106),
(610610,10,'SEMITAU',6106),
(610611,11,'SEBERUANG',6106),
(610612,12,'BATANG LUPAR',6106),
(610613,13,'EMPANANG',6106),
(610614,14,'BADAU',6106),
(610615,15,'SILAT HILIR',6106),
(610616,16,'SILAT HULU',6106),
(610617,17,'KEDAMIN',6106),
(610618,18,'KALIS',6106),
(610619,19,'BOYAN TANJUNG',6106),
(610620,20,'MENTEBAH',6106),
(610621,21,'BATU DATU',6106),
(610622,22,'SUHAID',6106),
(610623,23,'PURING KENCANA',6106),
(610701,1,'SUNGAI RAYA',6107),
(610702,2,'SAMALANTAN',6107),
(610703,3,'LEDO',6107),
(610704,4,'BENGKAYANG',6107),
(610705,5,'SELUAS',6107),
(610706,6,'SANGGAU LEDO',6107),
(610707,7,'JAGOI BABANG',6107),
(610708,8,'MONTERADO',6107),
(610709,9,'TERIAK',6107),
(610710,10,'SUTI SEMARANG',6107),
(610801,1,'NGABANG',6108),
(610802,2,'MEMPAWAH HULU',6108),
(610803,3,'MENJALIN',6108),
(610804,4,'MANDOR',6108),
(610805,5,'AIR BESAR',6108),
(610806,6,'MENYUKE',6108),
(610807,7,'SENGAH TEMILA',6108),
(610808,8,'MERANTI',6108),
(610809,9,'KUALA BEHE',6108),
(610810,10,'SEBANGKI',6108),
(610901,1,'SEKADAU HILIR',6109),
(610902,2,'SEKADAU HULU',6109),
(610903,3,'NANGA TAMAN',6109),
(610904,4,'NANGA MAHAP',6109),
(610905,5,'BELITANG HILIR',6109),
(610906,6,'BELITANG HULU',6109),
(610907,7,'BELITANG',6109),
(611001,1,'BELIMBING',6110),
(611002,2,'NANGA PINOH',6110),
(611003,3,'ELLA HILIR',6110),
(611004,4,'MENUKUNG',6110),
(611005,5,'SAYAN',6110),
(611006,6,'TANAH PINOH',6110),
(611007,7,'SOKAN',6110),
(611101,1,'SUKADANA',6111),
(611102,2,'SIMPANG HILIR',6111),
(611103,3,'TELUK BATANG',6111),
(611104,4,'PULAU MAYA KARIMATA',6111),
(611105,5,'SEPONTI',6111),
(617101,1,'PONTIANAK SELATAN',6171),
(617102,2,'PONTIANAK TIMUR',6171),
(617103,3,'PONTIANAK BARAT',6171),
(617104,4,'PONTIANAK UTARA',6171),
(617105,5,'PONTIANAK KOTA',6171),
(617201,1,'SINGKAWANG TENGAH',6172),
(617202,2,'SINGKAWANG BARAT',6172),
(617203,3,'SINGKAWANG TIMUR',6172),
(617204,4,'SINGKAWANG UTARA',6172),
(617205,5,'SINGKAWANG SELATAN',6172),
(620101,1,'KUMAI',6201),
(620102,2,'ARUT SELATAN',6201),
(620103,3,'KOTAWARINGIN LAMA',6201),
(620104,4,'ARUT UTARA',6201),
(620201,1,'KOTA BESI',6202),
(620202,2,'CEMPAGA',6202),
(620203,3,'MENTAYA HULU',6202),
(620204,4,'PARENGGEAN',6202),
(620205,5,'BAAMAG',6202),
(620206,6,'MENTAYA BARU KETAPANG',6202),
(620207,7,'MENTAYA HILIR UTARA',6202),
(620208,8,'MENTAYA HILIR SELATAN',6202),
(620209,9,'PULAU HANAUT',6202),
(620210,10,'ANTANG KALANG',6202),
(620301,1,'SELAT',6203),
(620302,2,'KAPUAS HILIR',6203),
(620303,3,'KAPUAS TIMUR',6203),
(620304,4,'KAPUAS KUALA',6203),
(620305,5,'KAPUAS BARAT',6203),
(620306,6,'PULAU PETAK',6203),
(620307,7,'KAPUAS MURUNG',6203),
(620308,8,'BASARANG',6203),
(620309,9,'MANTANGAI',6203),
(620310,10,'TIMPAH',6203),
(620311,11,'KAPUAS TENGAH',6203),
(620312,12,'KAPUAS HULU',6203),
(620401,1,'JENAMAS',6204),
(620402,2,'DUSUN HILIR',6204),
(620403,3,'KARAU KUALA',6204),
(620404,4,'DUSUN UTARA',6204),
(620405,5,'GUNUNG BINTANG AWAI',6204),
(620406,6,'DUSUN SELATAN',6204),
(620501,1,'MONTALAT',6205),
(620502,2,'GUNUNG TIMANG',6205),
(620503,3,'GUNUNG PUREI',6205),
(620504,4,'TEWEH TIMUR',6205),
(620505,5,'TEWEH TENGAH',6205),
(620506,6,'LAHEI',6205),
(620601,1,'KAMIPANG',6206),
(620602,2,'KATINGAN HILIR',6206),
(620603,3,'TEWEH SANGALANG GR',6206),
(620604,4,'PULAU MALAN',6206),
(620605,5,'KATINGAN TENGAH',6206),
(620606,6,'SANAMAN MANTIKEI',6206),
(620607,7,'MARIKIT',6206),
(620608,8,'KATINGAN HULU',6206),
(620609,9,'MENDAWAI',6206),
(620610,10,'KATINGAN KUALA',6206),
(620611,11,'TASIK PAYAWAN',6206),
(620701,1,'SERUYAN HILIR',6207),
(620702,2,'SERUYAN TENGAH',6207),
(620703,3,'DANAU SEMBULUH',6207),
(620704,4,'HANAU',6207),
(620705,5,'SERUYAN HULU',6207),
(620801,1,'SUKAMARA',6208),
(620802,2,'JELAI',6208),
(620803,3,'BALAI RIAM',6208),
(620901,1,'LAMANDAU',6209),
(620902,2,'DELANG',6209),
(620903,3,'BULIK',6209),
(621001,1,'SEPANG SIMIN',6210),
(621002,2,'KURUN',6210),
(621003,3,'TEWAH',6210),
(621004,4,'KAHAYAN HULU UTARA',6210),
(621005,5,'RUNGAN',6210),
(621006,6,'MANUHING',6210),
(621007,7,'MIHING RAYA',6210),
(621008,8,'DAMANG BATU',6210),
(621009,9,'MIRI MANASA',6210),
(621010,10,'RUNGAN HULU',6210),
(621011,11,'MAHUNING RAYA',6210),
(621101,1,'PANDIH BATU',6211),
(621102,2,'KAHAYAN KUALA',6211),
(621103,3,'KAHAYAN TENGAH',6211),
(621104,4,'BANAMA TINGANG',6211),
(621105,5,'KAHAYAN HILIR',6211),
(621106,6,'MALIKU',6211),
(621201,1,'MURUNG',6212),
(621202,2,'TANAH SIANG',6212),
(621203,3,'LAUNG TUHUP',6212),
(621204,4,'PERMATA INTAN',6212),
(621205,5,'SUMBER BARITO',6212),
(621301,1,'DUSUN TIMUR',6213),
(621302,2,'BENUA LIMA',6213),
(621303,3,'PATANGKEP TUTUI',6213),
(621304,4,'AWANG',6213),
(621305,5,'DUSUN TENGAH',6213),
(621306,6,'PEMATANG KARAU',6213),
(627101,1,'PAHANDUT',6271),
(627102,2,'BUKIT BATU',6271),
(627103,3,'JEKAN RAYA',6271),
(627104,4,'SABANGAU',6271),
(627105,5,'RAKUMPIT',6271),
(630101,1,'TAKISUNG',6301),
(630102,2,'JORONG',6301),
(630103,3,'PELAIHARI',6301),
(630104,4,'KURAU',6301),
(630105,5,'BATI - BATI',6301),
(630106,6,'PANYIPATAN',6301),
(630107,7,'KINTAP',6301),
(630108,8,'TAMBANG ULANG',6301),
(630109,9,'BATU AMPAR',6301),
(630201,1,'P. SEMBILAN',6302),
(630202,2,'PULAU LAUT BARAT',6302),
(630203,3,'P. LAUT SELATAN',6302),
(630204,4,'P. LAUT TIMUR',6302),
(630205,5,'P. SEBUKU',6302),
(630206,6,'P. LAUT UTARA',6302),
(630207,7,'KELUMPANG SELATAN',6302),
(630208,8,'KELUMPANG HULU',6302),
(630209,9,'KELUMPANG TENGAH',6302),
(630210,10,'KELUMPANG UTARA',6302),
(630211,11,'PAMUKAN SELATAN',6302),
(630212,12,'SAMPANAHAN',6302),
(630213,13,'PAMUKAN UTARA',6302),
(630214,14,'HAMPANG',6302),
(630215,15,'SEI DURIAN',6302),
(630216,16,'PULAU LAUT TENGAH',6302),
(630217,17,'KELUMPANG HILIR',6302),
(630218,18,'KELUMPANG BARAT',6302),
(630301,1,'ALUH - ALUH',6303),
(630302,2,'KERTAK HANYAR',6303),
(630303,3,'GAMBUT',6303),
(630304,4,'SUNGAI TABUK',6303),
(630305,5,'MARTAPURA',6303),
(630306,6,'KARANG INTAN',6303),
(630307,7,'ASTAMBUL',6303),
(630308,8,'SIMPANG EMPAT',6303),
(630309,9,'PENGARON',6303),
(630310,10,'SEI PINANG',6303),
(630311,11,'ARANIO',6303),
(630312,12,'MATARAMAN',6303),
(630313,13,'BERUNTUNG BARU',6303),
(630314,14,'MARTAPURA BARAT',6303),
(630315,15,'MARTAPURA TIMUR',6303),
(630316,16,'SAMBUNG MAKMUR',6303),
(630401,1,'TABUNGANEN',6304),
(630402,2,'TAMBAN',6304),
(630403,3,'ANJIR PASAR',6304),
(630404,4,'ANJIR MUARA',6304),
(630405,5,'ALALAK',6304),
(630406,6,'MANDASTANA',6304),
(630407,7,'RANTAU BADUAH',6304),
(630408,8,'BELAWANG',6304),
(630409,9,'CERBON',6304),
(630410,10,'BAKUMPAI',6304),
(630411,11,'KURIPAN',6304),
(630412,12,'TABUKAN',6304),
(630413,13,'MEKAR SARI',6304),
(630414,14,'BARAMBAI',6304),
(630415,15,'MARABAHAN',6304),
(630416,16,'WANARAYA',6304),
(630417,17,'JEJANGKIT',6304),
(630501,1,'BINUANG',6305),
(630502,2,'TAPIN SELATAN',6305),
(630503,3,'TAPIN TENGAH',6305),
(630504,4,'TAPIN UTARA',6305),
(630505,5,'CANDI LARAS SELATAN',6305),
(630506,6,'CANDI LARAS UTARA',6305),
(630507,7,'BAKARANGAN',6305),
(630508,8,'PIANI',6305),
(630509,9,'BUNGUR',6305),
(630510,10,'LOKPAIKAT',6305),
(630601,1,'SUNGAI RAYA',6306),
(630602,2,'PADANG BATUNG',6306),
(630603,3,'TELAGA LANGSAT',6306),
(630604,4,'ANGKINANG',6306),
(630605,5,'KANDANGAN',6306),
(630606,6,'SIMPUR',6306),
(630607,7,'DAHA SELATAN',6306),
(630608,8,'DAHA UTARA',6306),
(630609,9,'KALUMPANG',6306),
(630610,10,'LOKSADO',6306),
(630611,11,'DAHA BARAT',6306),
(630701,1,'HARUYAN',6307),
(630702,2,'BATU BENAWA',6307),
(630703,3,'LABUAN AMAS SELATAN',6307),
(630704,4,'LABUAN AMAS UTARA',6307),
(630705,5,'PANDAWAN',6307),
(630706,6,'BARABAI',6307),
(630707,7,'BATANG ALAI SELATAN',6307),
(630708,8,'BATANG ALAI UTARA',6307),
(630709,9,'HANTAKAN',6307),
(630710,10,'BATANG ALAI TIMUR',6307),
(630711,11,'LIMPASU',6307),
(630801,1,'DANAU PANGGANG',6308),
(630802,2,'BABIRIK',6308),
(630803,3,'SUNGAI PANDAN',6308),
(630804,4,'AMUNTAI SELATAN',6308),
(630805,5,'AMUNTAI TENGAH',6308),
(630806,6,'AMUNTAI UTARA',6308),
(630807,7,'BANJANG',6308),
(630808,8,'HAUR GADING',6308),
(630809,9,'PAMINGGIR',6308),
(630810,10,'SUNGAI TABUKAN ',6308),
(630901,1,'BANUA LAWAS',6309),
(630902,2,'KELUA',6309),
(630903,3,'TANTA',6309),
(630904,4,'TANJUNG',6309),
(630905,5,'HARUAI',6309),
(630906,6,'MURUNG PUDAK',6309),
(630907,7,'MUARA UYA',6309),
(630908,8,'MUARA HARUS',6309),
(630909,9,'PUGAAN',6309),
(630910,10,'UPAU',6309),
(630911,11,'JARO',6309),
(630912,12,'BINTANG ARA',6309),
(631001,1,'BATU LICIN',6310),
(631002,2,'KUSAN HILIR',6310),
(631003,3,'SUNGAI LOBAN',6310),
(631004,4,'SATUI',6310),
(631005,5,'KUSAN HULU',6310),
(631006,6,'SIMPANG EMPAT',6310),
(631007,7,'KARANG BINTANG',6310),
(631008,8,'MANTEWE',6310),
(631009,9,'ANGSANA',6310),
(631010,10,'KURANJI',6310),
(631101,1,'JUAI',6311),
(631102,2,'HALONG',6311),
(631103,3,'AWAYAN',6311),
(631104,4,'BATU MANDI',6311),
(631105,5,'LAMPIHONG',6311),
(631106,6,'PARINGIN',6311),
(637101,1,'BANJARMASIN SELATAN',6371),
(637102,2,'BANJARMASIN TIMUR',6371),
(637103,3,'BANJARMASIN BARAT',6371),
(637104,4,'BANJARMASIN UTARA',6371),
(637105,5,'BANJARMASIN TENGAH',6371),
(637202,2,'LANDASAN ULIN',6372),
(637203,3,'CEMPAKA',6372),
(637204,4,'BANJARBARU UTARA',6372),
(637205,5,'BANJARBARU SELATAN',6372),
(637206,6,'LIANG ANGGANG',6372),
(640101,1,'BATU SOPANG',6401),
(640102,2,'TANJUNG ARU',6401),
(640103,3,'PASIR BELENGKONG',6401),
(640104,4,'TANAH GROGOT',6401),
(640105,5,'KUARO',6401),
(640106,6,'LONG IKIS',6401),
(640107,7,'MUARA KOMAM',6401),
(640108,8,'LONG KALI',6401),
(640201,1,'MUARA MUNTAI',6402),
(640202,2,'LOA KULU',6402),
(640203,3,'LOA JANAN',6402),
(640204,4,'ANGGANA',6402),
(640205,5,'MUARA BADAK',6402),
(640206,6,'TENGGARONG',6402),
(640207,7,'SEBULU',6402),
(640208,8,'KOTA BANGUN',6402),
(640209,9,'KENOHAN',6402),
(640210,10,'KEMBANG JANGGUT',6402),
(640211,11,'MUARA KAMAN',6402),
(640212,12,'TABANG',6402),
(640213,13,'SEMBOJA',6402),
(640214,14,'MUARA JAWA',6402),
(640215,15,'SANGA-SANGA',6402),
(640216,16,'TENGGARONG SEBERANG',6402),
(640217,17,'MARANG KAYU',6402),
(640218,18,'MUARA WIS',6402),
(640301,1,'KELAY',6403),
(640302,2,'TALISAYAN',6403),
(640303,3,'SAMBALIUNG',6403),
(640304,4,'SEGAH',6403),
(640305,5,'TANJUNG REDEB',6403),
(640306,6,'GUNUNG TABUR',6403),
(640307,7,'PULAU DERAWAN',6403),
(640308,8,'BIDUK BIDUK',6403),
(640309,9,'TELUK BAYUR',6403),
(640310,10,'TUBAAN',6403),
(640311,11,'MARATUA',6403),
(640401,1,'TANJUNG PALAS',6404),
(640402,2,'TANJUNG PALAS BARAT',6404),
(640403,3,'TANJUNG PALAS UTARA',6404),
(640404,4,'TANJUNG PALAS TIMUR',6404),
(640405,5,'TANJUNG SELOR',6404),
(640406,6,'TANJUNG PALAS TENGAH',6404),
(640407,7,'PESO',6404),
(640408,8,'PESO ILIR',6404),
(640409,9,'SEKATAK',6404),
(640410,10,'SESAYAP',6404),
(640411,11,'SESAYAP HILIR',6404),
(640412,12,'BUNYU',6404),
(640413,13,'TANAH LIA',6404),
(640501,1,'SEBATIK',6405),
(640502,2,'NUNUKAN',6405),
(640503,3,'SEMBAKUNG',6405),
(640504,4,'LUMBIS',6405),
(640505,5,'KRAYAN',6405),
(640601,1,'MENTARANG',6406),
(640602,2,'MALINAU',6406),
(640603,3,'PUJUNGAN',6406),
(640604,4,'KAYAN HILIR',6406),
(640605,5,'KAYAN HULU',6406),
(640606,6,'MALINAU SELATAN',6406),
(640607,7,'MALINAU UTARA',6406),
(640608,8,'MALINAU BARAT',6406),
(640609,9,'SUNGAI BOH',6406),
(640701,1,'LONG APARI',6407),
(640702,2,'LONG PAHANGAI',6407),
(640703,3,'LONG BAGUN',6407),
(640704,4,'LONG HUBUNG',6407),
(640705,5,'LONG IRAM',6407),
(640706,6,'MELAK',6407),
(640707,7,'BARONG TONGKOK',6407),
(640708,8,'DAMAI',6407),
(640709,9,'MUARA LAWA',6407),
(640710,10,'MUARA PAHU',6407),
(640711,11,'JEMPANG',6407),
(640712,12,'BONGAN',6407),
(640713,13,'PENYINGGAHAN',6407),
(640714,14,'BENTIAN BESAR',6407),
(640715,15,'LINGGANG BIGUNG',6407),
(640801,1,'MUARA ANCALONG',6408),
(640802,2,'MUARA WAHAU',6408),
(640803,3,'MUARA BENGKAL',6408),
(640804,4,'SANGATTA',6408),
(640805,5,'SANGKULIRANG',6408),
(640806,6,'BUSANG',6408),
(640807,7,'TELEN',6408),
(640808,8,'KOMBENG',6408),
(640809,9,'BENGALON',6408),
(640810,10,'KALIORANG',6408),
(640811,11,'SANDARAN',6408),
(640901,1,'PENAJAM',6409),
(640902,2,'WARU',6409),
(640903,3,'BABULU',6409),
(640904,4,'SEPAKU',6409),
(647101,1,'BALIKPAPAN TIMUR',6471),
(647102,2,'BALIKPAPAN BARAT',6471),
(647103,3,'BALIKPAPAN UTARA',6471),
(647104,4,'BALIKPAPAN TENGAH',6471),
(647105,5,'BALIKPAPAN SELATAN',6471),
(647201,1,'PALARAN',6472),
(647202,2,'SAMARINDA SEBERANG',6472),
(647203,3,'SAMARINDA ULU',6472),
(647204,4,'SAMARINDA ILIR',6472),
(647205,5,'SAMARINDA UTARA',6472),
(647206,6,'SUNGAI KUNJANG',6472),
(647301,1,'TARAKAN BARAT',6473),
(647302,2,'TARAKAN TENGAH',6473),
(647303,3,'TARAKAN TIMUR',6473),
(647304,4,'TARAKAN UTARA',6473),
(647401,1,'BONTANG UTARA',6474),
(647402,2,'BONTANG SELATAN',6474),
(647403,3,'BONTANG BARAT',6474),
(710101,1,'PINOGALUMAN',7101),
(710102,2,'KAIDIPANG',7101),
(710103,3,'BOLANG ITANG',7101),
(710104,4,'BINTAUNA',7101),
(710105,5,'SANGTOMBOLANG',7101),
(710106,6,'POSIGADAN',7101),
(710107,7,'BOLANG UKI',7101),
(710108,8,'PINOLOSIAN',7101),
(710109,9,'DUMOGA BARAT',7101),
(710110,10,'DUMOGA TIMUR',7101),
(710111,11,'DUMOGA UTARA',7101),
(710112,12,'LOLAK',7101),
(710113,13,'BOLAANG',7101),
(710114,14,'LOLAYAN',7101),
(710115,15,'KOTABUNAN',7101),
(710116,16,'NUANGAN',7101),
(710117,17,'KOTAMOBAGU',7101),
(710118,18,'MODAYAG',7101),
(710119,19,'PASSI',7101),
(710120,20,'POIGAR',7101),
(710201,1,'TONDANO BARAT',7102),
(710202,2,'TONDANO TIMUR',7102),
(710203,3,'ERIS',7102),
(710204,4,'KOMBI',7102),
(710205,5,'LEMBEAN TIMUR',7102),
(710206,6,'KAKAS',7102),
(710207,7,'TOMPASO',7102),
(710208,8,'REMBOKEN',7102),
(710209,9,'LANGOWAN TIMUR',7102),
(710210,10,'LANGOWAN BARAT',7102),
(710211,11,'SONDER',7102),
(710212,12,'KAWANGKOAN',7102),
(710213,13,'PINELENG',7102),
(710214,14,'TOMBULU',7102),
(710215,15,'TOMBARIRI',7102),
(710216,16,'KEMA',7102),
(710217,17,'KAUDITAN',7102),
(710218,18,'AIRMADIDI',7102),
(710219,19,'WORI',7102),
(710220,20,'DIMEMBE',7102),
(710221,21,'LIKUPANG BARAT',7102),
(710222,22,'LIKUPANG TIMUR',7102),
(710301,1,'TAGULANDANG',7103),
(710302,2,'TAGULANDANG UTARA',7103),
(710303,3,'BIARO',7103),
(710304,4,'SIAU TIMUR',7103),
(710305,5,'SIAU TIMUR SELATAN',7103),
(710306,6,'SIAU BARAT',7103),
(710307,7,'SIAU BARAT SELATAN',7103),
(710308,8,'TABUKAN UTARA',7103),
(710309,9,'NUSA TABUKAN',7103),
(710310,10,'MANGANITU SELATAN',7103),
(710311,11,'TOTARENG',7103),
(710312,12,'TAMAKO',7103),
(710313,13,'MANGANITU',7103),
(710314,14,'TABUKAN TENGAH',7103),
(710315,15,'TABUKAN SELATAN',7103),
(710316,16,'KENDAHE',7103),
(710317,17,'TAHUNA',7103),
(710401,1,'LIRUNG',7104),
(710402,2,'BEO',7104),
(710403,3,'RAINIS',7104),
(710404,4,'ESSANG',7104),
(710405,5,'NANUSA',7104),
(710406,6,'KABARUAN',7104),
(710407,7,'MELONGUANE',7104),
(710501,1,'MODOINDING',7105),
(710502,2,'TOMPASO BARU',7105),
(710503,3,'RANOYAPO',7105),
(710504,4,'BELANG',7105),
(710505,5,'TOMBATU',7105),
(710506,6,'TOULUAAN',7105),
(710507,7,'MOTOLING',7105),
(710508,8,'SINON SAYANG',7105),
(710509,9,'TENGA',7105),
(710510,10,'TOMBASIAN',7105),
(710511,11,'RATAHAN',7105),
(710512,12,'TUMPAAN',7105),
(710513,13,'TARERAN',7105),
(710601,1,'KEMA',7106),
(710602,2,'KAUDITAN',7106),
(710603,3,'AIRMADIDI',7106),
(710604,4,'WORI',7106),
(710605,5,'DIMEMBE',7106),
(710606,6,'LEKUPANG BARAT',7106),
(710607,7,'LEKUPANG TIMUR',7106),
(710608,8,'KALAWAT',7106),
(710701,1,'RATAHAN',7107),
(710702,2,'PUSOMAEN',7107),
(710703,3,'BLANG',7107),
(710704,4,'RATATOTOK',7107),
(710705,5,'TOMBATU',7107),
(710706,6,'TOLUAAN',7107),
(710801,1,'SANGKUB',7108),
(710802,2,'BINTAUNA',7108),
(710803,3,'BOLANG ITANG TIMUR',7108),
(710804,4,'BOLANG ITANG BARAT',7108),
(710805,5,'KAIDIPANG',7108),
(710806,6,'PINOGALUMAN',7108),
(710901,1,'SIAU TIMUR',7109),
(710902,2,'SIAU BARAT',7109),
(710903,3,'TAGULANDANG',7109),
(710904,4,'SIAU TIMUR SELATAN',7109),
(710905,5,'SIAU BARAT SELATAN',7109),
(710906,6,'TAGULANDANG UTARA',7109),
(710907,7,'BIARO',7109),
(710908,8,'SIAU BARAT UTARA',7109),
(710909,9,'SIAU TENGAH',7109),
(710910,10,'TAGULANDANG SELATAN',7109),
(717101,1,'BUNAKEN',7171),
(717102,2,'TUMINTING',7171),
(717103,3,'SINGKIL',7171),
(717104,4,'WENANG',7171),
(717105,5,'TIKALA',7171),
(717106,6,'SARIO',7171),
(717107,7,'WANEA',7171),
(717108,8,'MAPANGET',7171),
(717109,9,'MALALAYANG',7171),
(717201,1,'BITUNG SELATAN',7172),
(717202,2,'BITUNG TENGAH',7172),
(717203,3,'BITUNG UTARA',7172),
(717204,4,'BITUNG TIMUR',7172),
(717205,5,'BITUNG BARAT',7172),
(717301,1,'TOMOHON SELATAN',7173),
(717302,2,'TOMOHON TENGAH',7173),
(717303,3,'TOMOHON UTARA',7173),
(717401,1,'KOTAMOBAGU UTARA',7174),
(717402,2,'KOTAMOBAGU TIMUR',7174),
(717403,3,'KOTAMOBAGU SELATAN',7174),
(717404,4,'KOTAMOBAGU BARAT',7174),
(720101,1,'BATUI',7201),
(720102,2,'BUNTA',7201),
(720103,3,'KINTOM',7201),
(720104,4,'LUWUK',7201),
(720105,5,'LAMALA',7201),
(720106,6,'BALANTAK',7201),
(720107,7,'PAGIMANA',7201),
(720108,8,'BUALEMO',7201),
(720109,9,'TOILI',7201),
(720201,1,'POSO KOTA',7202),
(720202,2,'POSO PESISIR',7202),
(720203,3,'LAGE',7202),
(720204,4,'PAMONA UTARA',7202),
(720205,5,'PAMONA TIMUR',7202),
(720206,6,'PAMONA SELATAN',7202),
(720207,7,'LORE UTARA',7202),
(720208,8,'LORE TENGAH',7202),
(720209,9,'LORE SELATAN',7202),
(720218,18,'POSO PESISIR UTARA',7202),
(720219,19,'POSO PESISIR SELATAN',7202),
(720220,20,'PAMONA BARAT',7202),
(720221,21,'POSO KOTA SELATAN',7202),
(720222,22,'POSO KOTA UTARA',7202),
(720223,23,'LORE BARAT',7202),
(720301,1,'KULAWI',7203),
(720302,2,'PIPIKORO',7203),
(720303,3,'DOLO',7203),
(720304,4,'RIOPAKAWA',7203),
(720305,5,'SIGI BIROMARU',7203),
(720306,6,'DAMSOL',7203),
(720307,7,'MARAWOLA',7203),
(720308,8,'BANAWA',7203),
(720309,9,'TAWAELI',7203),
(720310,10,'SINDUE',7203),
(720311,11,'SIRENJA',7203),
(720312,12,'BALAESANG',7203),
(720313,13,'PALOLO',7203),
(720314,14,'SOJOL',7203),
(720401,1,'DAMPAL SELATAN',7204),
(720402,2,'DAMPAL UTARA',7204),
(720403,3,'DONDO',7204),
(720404,4,'BASIDONDO',7204),
(720405,5,'OGODEIDE',7204),
(720406,6,'LAMPASIO',7204),
(720407,7,'BAOLAN',7204),
(720408,8,'GALANG',7204),
(720409,9,'TOLI-TOLI UTARA',7204),
(720501,1,'MOMUNU',7205),
(720502,2,'BIAU',7205),
(720503,3,'BOKAT',7205),
(720504,4,'BUNOBOGU',7205),
(720505,5,'PALELEH',7205),
(720601,1,'MORI ATAS',7206),
(720602,2,'LEMBO',7206),
(720603,3,'PETASIA',7206),
(720604,4,'BUNGKU UTARA',7206),
(720605,5,'BUNGKU TENGAH',7206),
(720606,6,'BUNGKU SELATAN',7206),
(720607,7,'MENUI KEPULAUAN',7206),
(720608,8,'BUNGKU BARAT',7206),
(720701,1,'LOBANGKURUNG',7207),
(720702,2,'BANGGAI',7207),
(720703,3,'TOTIKUM',7207),
(720704,4,'TINANGKUNG',7207),
(720705,5,'LIANG',7207),
(720706,6,'BULAGI',7207),
(720707,7,'BUKO',7207),
(720801,1,'PARIGI',7208),
(720802,2,'AMPIBABO',7208),
(720803,3,'TINOMBO',7208),
(720804,4,'MOUTONG',7208),
(720805,5,'TOMINI',7208),
(720806,6,'SAUSU',7208),
(720901,1,'UNA UNA',7209),
(720902,2,'TOGIYAN',7209),
(720903,3,'WALEA KEPULAUAN',7209),
(720904,4,'AMPANA TETE',7209),
(720905,5,'AMPANA KOTA',7209),
(720906,6,'ULU BONGKA',7209),
(720907,7,'TOJO BARAT',7209),
(720908,8,'TOJO',7209),
(720909,9,'WALEA BESAR',7209),
(730101,1,'BENTENG',7301),
(730102,2,'BONTOHARU',7301),
(730103,3,'BONTOMATENE',7301),
(730104,4,'BONTO MANAI',7301),
(730105,5,'BONTOSIKUYU',7301),
(730106,6,'PASIMASSUNGGU',7301),
(730107,7,'PASIMARANNU',7301),
(730108,8,'TAKA BONERATE',7301),
(730109,9,'PASILAMBENA',7301),
(730201,1,'GANTARANG',7302),
(730202,2,'UJUNG BULU',7302),
(730203,3,'BONTO BAHARI',7302),
(730204,4,'BONTOTIRO',7302),
(730205,5,'HERO LANGE-LANGE',7302),
(730206,6,'KAJANG',7302),
(730207,7,'BULUKUMPA',7302),
(730208,8,'KINDANG',7302),
(730209,9,'UJUNG LOE',7302),
(730210,10,'RILAU ALE',7302),
(730301,1,'BISSAPPU',7303),
(730302,2,'BANTAENG',7303),
(730303,3,'EREMERASA',7303),
(730304,4,'TOMPOBULU',7303),
(730305,5,'PAJUKUKANG',7303),
(730306,6,'ULUERE',7303),
(730401,1,'BANGKALA',7304),
(730402,2,'TAMALATEA',7304),
(730403,3,'BINAMU',7304),
(730404,4,'BATANG',7304),
(730405,5,'KELARA',7304),
(730406,6,'BANGKALA BARAT',7304),
(730407,7,'BONTORAMBA',7304),
(730408,8,'TURATEA',7304),
(730409,9,'ARUNGKEKE',7304),
(730501,1,'MAPPAKASUNGGU',7305),
(730502,2,'MANGARA BOMBANG',7305),
(730503,3,'POLOBANGKENG SELATAN',7305),
(730504,4,'POLOBANGKENG UTARA',7305),
(730505,5,'GALESONG SELATAN',7305),
(730506,6,'GALESONG UTARA',7305),
(730507,7,'PATTALLASSANG',7305),
(730601,1,'BONTONOMPO',7306),
(730602,2,'BAJENG',7306),
(730603,3,'TOMPOBULU',7306),
(730604,4,'TINGGIMONCONG',7306),
(730605,5,'PARANGLOE',7306),
(730606,6,'BONTOMARANNU',7306),
(730607,7,'PALLANGGA',7306),
(730608,8,'SOMBA OPU',7306),
(730609,9,'BUNGAYA',7306),
(730610,10,'TOMBOLO PAO',7306),
(730611,11,'BIRINGBULU',7306),
(730612,12,'BAROMBONG',7306),
(730701,1,'SINJAI BARAT',7307),
(730702,2,'SINJAI SELATAN',7307),
(730703,3,'SINJAI TIMUR',7307),
(730704,4,'SINJAI TENGAH',7307),
(730705,5,'SINJAI UTARA',7307),
(730706,6,'BULUPODDO',7307),
(730707,7,'SINJAI BORONG',7307),
(730708,8,'TELLU LIMPOE',7307),
(730709,9,'PULAU SEMBILAN',7307),
(730801,1,'BONTOCANI',7308),
(730802,2,'KAHU',7308),
(730803,3,'KAJUARA',7308),
(730804,4,'SALOMEKKO',7308),
(730805,5,'TONRA',7308),
(730806,6,'LIBURENG',7308),
(730807,7,'MARE',7308),
(730808,8,'SIBULUE',7308),
(730809,9,'BAREBBO',7308),
(730810,10,'CINA',7308),
(730811,11,'PONRE',7308),
(730812,12,'LAPPARIAJA',7308),
(730813,13,'LAMURU',7308),
(730814,14,'ULAWENG',7308),
(730815,15,'PALAKKA',7308),
(730816,16,'AWANGPONE',7308),
(730817,17,'TELLU SIATTINGE',7308),
(730818,18,'AJANGALE',7308),
(730819,19,'DUA BOCCOE',7308),
(730820,20,'CENRANA',7308),
(730821,21,'TANETE RIATTANG',7308),
(730822,22,'TANETE RIATTANG BARAT',7308),
(730823,23,'TANETE RIATTANG TIMUR',7308),
(730824,24,'AMALI',7308),
(730825,25,'TELLU LIMPOE',7308),
(730826,26,'BENGO',7308),
(730827,27,'PATIMPENG',7308),
(730901,1,'MANDAI',7309),
(730902,2,'CAMBA',7309),
(730903,3,'BANTIMURUNG',7309),
(730904,4,'MAROS BARU',7309),
(730905,5,'MAROS UTARA',7309),
(730906,6,'MALLAWA',7309),
(730907,7,'TANRALILI',7309),
(730908,8,'MARUSU',7309),
(730909,9,'SIMBANG',7309),
(730910,10,'CENRANA',7309),
(730911,11,'TOMPO BULU',7309),
(730912,12,'LAU',7309),
(730913,13,'MONCONGLOE',7309),
(730914,14,'TURIKALE',7309),
(731001,1,'LIUKANG TANGAYA',7310),
(731002,2,'KALUKUANG MASALIMA',7310),
(731003,3,'LIUKANG TUPABBIRING',7310),
(731004,4,'PANGKAJENE',7310),
(731005,5,'BALOCCI',7310),
(731006,6,'BUNGORO',7310),
(731007,7,'LABAKKANG',7310),
(731008,8,'MARANG',7310),
(731009,9,'SEGERI',7310),
(731010,10,'MINASA TENE',7310),
(731011,11,'MANDALLE',7310),
(731012,12,'TONDONG TALLASA',7310),
(731101,1,'TANETE RIAJA',7311),
(731102,2,'TANETE RILAU',7311),
(731103,3,'BARRU',7311),
(731104,4,'SOPPENG RIAJA',7311),
(731105,5,'MALLUSETASI',7311),
(731106,6,'PUJANANTING',7311),
(731107,7,'BALUSU',7311),
(731201,1,'MARIO RIWAWO',7312),
(731202,2,'LILI RIAJA',7312),
(731203,3,'LILI RILAU',7312),
(731204,4,'LALABATA',7312),
(731205,5,'MARIO RIAWA',7312),
(731206,6,'DONRI DONRI',7312),
(731207,7,'GANRA',7312),
(731301,1,'SABBANG PARU',7313),
(731302,2,'PAMMANA',7313),
(731303,3,'TAKKALALLA',7313),
(731304,4,'SAJOANGING',7313),
(731305,5,'MAJAULENG',7313),
(731306,6,'TEMPE',7313),
(731307,7,'BELAWA',7313),
(731308,8,'TANA SITOLO',7313),
(731309,9,'MANIANG PAJO',7313),
(731310,10,'PITUMPANUA',7313),
(731311,11,'BOLA',7313),
(731312,12,'PENRANG',7313),
(731313,13,'GILIRENG',7313),
(731314,14,'KEERA',7313),
(731401,1,'PANCA LAUTANG',7314),
(731402,2,'TELLULIMPO E',7314),
(731403,3,'WATANG PULU',7314),
(731404,4,'BARANTI',7314),
(731405,5,'PANCA RIJANG',7314),
(731406,6,'KULO',7314),
(731407,7,'MARITENGNGAE',7314),
(731408,8,'SIDENRENG',7314),
(731409,9,'DUAPITUE',7314),
(731410,10,'PITU RIAWA',7314),
(731411,11,'PITU RIASE',7314),
(731501,1,'MATTIROSOMPE',7315),
(731502,2,'SUPPA',7315),
(731503,3,'MATTIRO BULU',7315),
(731504,4,'WATANG SAWITTO',7315),
(731505,5,'PATAMPANUA',7315),
(731506,6,'DUAMPANUA',7315),
(731507,7,'LEMBANG',7315),
(731508,8,'CEMPA',7315),
(731509,9,'TIROANG',7315),
(731510,10,'LANRISANG',7315),
(731511,11,'PALETEANG',7315),
(731512,12,'BATULAPPA',7315),
(731601,1,'MAIWA',7316),
(731602,2,'ENREKANG',7316),
(731603,3,'BARAKA',7316),
(731604,4,'ANGGERAJA',7316),
(731605,5,'ALLA',7316),
(731606,6,'MAIWA ATAS',7316),
(731607,7,'ENREKANG SELATAN',7316),
(731608,8,'ALLA TIMUR',7316),
(731609,9,'ANGGERAJA TIMUR',7316),
(731701,1,'BASSESANGTEMPE',7317),
(731702,2,'LAROMPONG',7317),
(731703,3,'SULI',7317),
(731704,4,'BAJO',7317),
(731705,5,'BUA PONRANG',7317),
(731706,6,'WALENRANG',7317),
(731707,7,'BELOPA',7317),
(731708,8,'BUA',7317),
(731709,9,'LAMASI',7317),
(731710,10,'LARONPONG SELATAN',7317),
(731711,11,'PONRANG',7317),
(731712,12,'LATIMOJONG',7317),
(731713,13,'KAMANRE',7317),
(731801,1,'SALUPUTTI',7318),
(731802,2,'BITTUANG',7318),
(731803,3,'BONGGAKARADENG',7318),
(731804,4,'SANGGALANGI',7318),
(731805,5,'MAKALE',7318),
(731806,6,'SESEAN',7318),
(731807,7,'TONDON NANGGALA',7318),
(731808,8,'BOKIN',7318),
(731809,9,'SIMBUANG',7318),
(731810,10,'RINDINGALO',7318),
(731811,11,'RANTETAYO',7318),
(731812,12,'MENGKENDEK',7318),
(731813,13,'SANGALLA',7318),
(731814,14,'SA\'DAN BALUSU',7318),
(731815,15,'RANTEPAO',7318),
(732201,1,'MALANGKE',7322),
(732202,2,'BONE-BONE',7322),
(732203,3,'MASAMBA',7322),
(732204,4,'SABBANG',7322),
(732205,5,'LIMBONG',7322),
(732206,6,'SUKAMAJU',7322),
(732207,7,'SEKO',7322),
(732208,8,'MALANGKE BARAT',7322),
(732209,9,'RAMPI',7322),
(732210,10,'MAPPEDECENG',7322),
(732211,11,'BAEBUNTA',7322),
(732212,12,'BURAU',7322),
(732213,13,'TOMONI',7322),
(732214,14,'WOTU',7322),
(732215,15,'MALILI',7322),
(732216,16,'ANGKONA',7322),
(732217,17,'NUHA',7322),
(732218,18,'TOWUTI',7322),
(732219,19,'MANGKUTANA',7322),
(732401,1,'MANGKUTANA',7324),
(732402,2,'NUHA',7324),
(732403,3,'TOWUTI',7324),
(732404,4,'MALILI',7324),
(732405,5,'ANGKONA',7324),
(732406,6,'WOTU',7324),
(732407,7,'BURAU',7324),
(732408,8,'TOMONI',7324),
(737101,1,'MARISO',7371),
(737102,2,'MAMAJANG',7371),
(737103,3,'MAKASSAR',7371),
(737104,4,'UJUNG PANDANG',7371),
(737105,5,'WAJO',7371),
(737106,6,'BONTOALA',7371),
(737107,7,'TALLO',7371),
(737108,8,'UJUNG TANAH',7371),
(737109,9,'PANAKKUKANG',7371),
(737110,10,'TAMALATE',7371),
(737111,11,'BIRING KANAYA',7371),
(737112,12,'MANGGALA',7371),
(737113,13,'RAPPOCINI',7371),
(737114,14,'TAMALANREA',7371),
(737201,1,'BACUKIKI',7372),
(737202,2,'UJUNG',7372),
(737203,3,'SOREANG',7372),
(737301,1,'WARA',7373),
(737302,2,'WARA UTARA',7373),
(737303,3,'WARA SELATAN',7373),
(737304,4,'TELLUWANUA',7373),
(740101,1,'WUNDULAKO',7401),
(740102,2,'TIRAWUTA',7401),
(740103,3,'MOWEWE',7401),
(740104,4,'KOLAKA',7401),
(740107,7,'POMALAA',7401),
(740108,8,'WATUBANGGA',7401),
(740109,9,'LADONGI',7401),
(740110,10,'WOLO',7401),
(740112,12,'BAULA',7401),
(740113,13,'ULUIWOI',7401),
(740114,14,'LATAMBAGA',7401),
(740118,18,'TANGGETADA',7401),
(740119,19,'LAMBADIA',7401),
(740120,20,'SAMATURU',7401),
(740201,1,'LAMBUYA',7402),
(740202,2,'UNAAHA',7402),
(740203,3,'WAWOTOBI',7402),
(740204,4,'PONDIDAHA',7402),
(740205,5,'SAMPARA',7402),
(740206,6,'WAWONII BARAT',7402),
(740207,7,'WAWONII TIMUR',7402),
(740210,10,'ABUKI',7402),
(740211,11,'SOROPIA',7402),
(740213,13,'WAWONII SELATAN',7402),
(740214,14,'WAWONII UTARA',7402),
(740215,15,'TONGAUNA',7402),
(740216,16,'LATOMA',7402),
(740217,17,'PURIALA',7402),
(740218,18,'UWEPAI',7402),
(740219,19,'WONGGEDUKU',7402),
(740220,20,'BESULUTU',7402),
(740221,21,'BONDOWALA',7402),
(740223,23,'ROUTA',7402),
(740224,24,'ANGGABERI',7402),
(740225,25,'MELUHU',7402),
(740228,28,'AMONGGEDO',7402),
(740230,30,'WAMONII TENGAH',7402),
(740301,1,'MAGINTI',7403),
(740302,2,'TIWORO TENGAH',7403),
(740303,3,'TIWORO KEPULAUAN',7403),
(740304,4,'SAWERIGADI',7403),
(740305,5,'KUSAMBI',7403),
(740306,6,'NAPABALANO',7403),
(740307,7,'MALIGANO',7403),
(740308,8,'WAKORUMBA',7403),
(740313,13,'WAKORUMBA SELATAN',7403),
(740314,14,'LASALEPA',7403),
(740315,15,'BATALAIWORU',7403),
(740316,16,'KATOBU',7403),
(740317,17,'DURUKA',7403),
(740318,18,'LOHIA',7403),
(740319,19,'WATOPUTE',7403),
(740320,20,'KONTUNAGA',7403),
(740321,21,'BARANGKA',7403),
(740322,22,'LAWA',7403),
(740323,23,'KABANGKA',7403),
(740324,24,'KABAWO',7403),
(740325,25,'PARIGI',7403),
(740326,26,'BONE',7403),
(740327,27,'TONGKUNO',7403),
(740328,28,'PASIR PUTIH',7403),
(740405,5,'MAWASANGKA',7404),
(740406,6,'MAWASANGKA TIMUR',7404),
(740407,7,'LAKUDO',7404),
(740408,8,'GU',7404),
(740409,9,'BATAUGA',7404),
(740410,10,'SAMPOLAWA',7404),
(740411,11,'PASAR WAJO',7404),
(740418,18,'TALAGA RAYA',7404),
(740419,19,'KADATUA',7404),
(740420,20,'SIOMPU',7404),
(740421,21,'BATU ATAS',7404),
(740422,22,'KAPONTORI',7404),
(740423,23,'LASALIMU',7404),
(740424,24,'LASALIMU SELATAN',7404),
(740501,1,'TINANGGEA',7405),
(740502,2,'ANGATA',7405),
(740503,3,'ANDOOLO',7405),
(740504,4,'PALANGGA',7405),
(740505,5,'LANDONO',7405),
(740506,6,'LAINEA',7405),
(740507,7,'KONDA',7405),
(740508,8,'RANOMEETO',7405),
(740509,9,'KOLONO',7405),
(740510,10,'MORAMO',7405),
(740511,11,'LAONTI',7405),
(740601,1,'KABAENA',7406),
(740602,2,'KABAENA TIMUR',7406),
(740603,3,'RUMBIA',7406),
(740604,4,'RAROWATU',7406),
(740605,5,'POLEANG',7406),
(740606,6,'POLEANG TIMUR',7406),
(740701,1,'WANGI-WANGI',7407),
(740702,2,'KALEDUPA',7407),
(740703,3,'TOMIA',7407),
(740704,4,'BINONGKO',7407),
(740705,5,'WANGI-WANGI SELATAN',7407),
(740801,1,'RANTEANGIN',7408),
(740802,2,'LASUSUA',7408),
(740803,3,'KODEOHA',7408),
(740804,4,'NGAPA',7408),
(740805,5,'PAKUE',7408),
(740806,6,'BATU PUTIH',7408),
(740901,1,'ASERA',7409),
(740902,2,'WIWIRANO',7409),
(740903,3,'LANGGIKIMA',7409),
(740904,4,'MOLAWE',7409),
(740905,5,'LASOLO',7409),
(740906,6,'LEMBO',7409),
(740907,7,'SAWA',7409),
(741001,1,'KULISUSU',7410),
(741002,2,'KAMBOWA',7410),
(741003,3,'BONEGUNU',7410),
(741004,4,'KALISUSU BARAT',7410),
(741005,5,'KALISUSU UTARA',7410),
(741006,6,'WAKORUMBA UTARA',7410),
(747101,1,'MANDONGA',7471),
(747102,2,'KENDARI',7471),
(747103,3,'BARUGA',7471),
(747104,4,'POASIA',7471),
(747105,5,'KENDARI BARAT',7471),
(747106,6,'ABELI',7471),
(747201,1,'BETOAMBARI',7472),
(747202,2,'WOLIO',7472),
(747203,3,'SOROWALIO',7472),
(747204,4,'BUNGI',7472),
(750101,1,'LIMBOTO',7501),
(750102,2,'TELAGA',7501),
(750103,3,'BATUDAA',7501),
(750104,4,'TIBAWA',7501),
(750105,5,'BATUDAA PANTAI',7501),
(750109,9,'BOLIYOHUTO',7501),
(750110,10,'TELAGA BIRU',7501),
(750111,11,'BONGOMEME',7501),
(750113,13,'TOLANGOHULA',7501),
(750114,14,'MOOTILANGO',7501),
(750116,16,'PULUBALA',7501),
(750117,17,'LIMBOTO BARAT',7501),
(750201,1,'PAGUYAMAN',7502),
(750202,2,'WONOSARI',7502),
(750203,3,'DULUPI',7502),
(750204,4,'TILAMUTA',7502),
(750205,5,'MANANGGU',7502),
(750206,6,'BOTUMOITO',7502),
(750207,7,'PAGUYAMAN PANTAI',7502),
(750301,1,'TAPA',7503),
(750302,2,'KABILA',7503),
(750303,3,'SUWAWA',7503),
(750304,4,'BONEPANTAI',7503),
(750401,1,'POPAYATO',7504),
(750402,2,'LEMITO',7504),
(750403,3,'RANDANGAN',7504),
(750404,4,'MARISA',7504),
(750405,5,'PAGUAT',7504),
(750406,6,'PATILANGGIO',7504),
(750407,7,'TALUDITI',7504),
(750501,1,'ATINGGOLA',7505),
(750502,2,'KWANDANG',7505),
(750503,3,'ANGGREK',7505),
(750504,4,'SUMALATA',7505),
(750505,5,'TOLINGGULA',7505),
(757101,1,'KOTA BARAT',7571),
(757102,2,'KOTA SELATAN',7571),
(757103,3,'KOTA UTARA',7571),
(757104,4,'DUNGINGI',7571),
(757105,5,'KOTA TIMUR',7571),
(760101,1,'BAMBALAMOTU',7601),
(760102,2,'PASANGKAYU',7601),
(760103,3,'BARAS',7601),
(760104,4,'SARUDU',7601),
(760201,1,'MAMUJU',7602),
(760202,2,'TAPALANG',7602),
(760203,3,'KALUKKU',7602),
(760204,4,'KALUMPANG',7602),
(760205,5,'BUDONG BUDONG',7602),
(760206,6,'PANGALE',7602),
(760207,7,'PAPALANG',7602),
(760208,8,'SAMPAGA',7602),
(760209,9,'TOPOYO',7602),
(760210,10,'KAROSSA',7602),
(760211,11,'TOMMO',7602),
(760212,12,'SIMBORO DAN KEPULAUAN',7602),
(760213,13,'TAPALANG BARAT',7602),
(760214,14,'TOBADAK',7602),
(760215,15,'BONEHAU',7602),
(760301,1,'MAMBI',7603),
(760302,2,'ARALLE',7603),
(760303,3,'MAMASA',7603),
(760304,4,'PANA',7603),
(760305,5,'TABULAHAN',7603),
(760306,6,'SUMARORONG',7603),
(760307,7,'MESSAWA',7603),
(760308,8,'SESENA PADANG',7603),
(760309,9,'TANDUK KALUA',7603),
(760310,10,'TABANG',7603),
(760311,11,'BAMBANG',7603),
(760312,12,'BALLA',7603),
(760313,13,'NOSU',7603),
(760401,1,'TINABUNG',7604),
(760402,2,'CAMPALAGIAN',7604),
(760403,3,'WONOMULYO',7604),
(760404,4,'POLEWALI',7604),
(760405,5,'TUTALLU',7604),
(760406,6,'BINUANG',7604),
(760407,7,'TAPANGO',7604),
(760408,8,'MAPILI',7604),
(760409,9,'MATANGNGA',7604),
(760410,10,'LUYO',7604),
(760411,11,'LIMBORO',7604),
(760412,12,'BALANIPA',7604),
(760413,13,'ANREAPI',7604),
(760414,14,'MATAKALI',7604),
(760415,15,'ALLU',7604),
(760501,1,'BANGGAE',7605),
(760502,2,'PAMBOANG',7605),
(760503,3,'SENDANA',7605),
(760504,4,'MALUNDA',7605),
(760505,5,'ULUMUNDA',7605),
(760506,6,'TAMMERODO SENDANA',7605),
(760507,7,'TUBO SENDANA',7605),
(760508,8,'BANGGAE TIMUR',7605),
(810101,1,'AMAHAI',8101),
(810102,2,'TEON NILA SERUA',8101),
(810106,6,'SERAM UTARA',8101),
(810109,9,'BANDA',8101),
(810111,11,'TEHORU',8101),
(810112,12,'SAPARUA',8101),
(810113,13,'PULAU HARUKU',8101),
(810114,14,'SALAHUTU',8101),
(810115,15,'LEIHITU',8101),
(810116,16,'NUSA LAUT',8101),
(810117,17,'KOTA MASOHI',8101),
(810201,1,'KEI KECIL',8102),
(810202,2,'PP. KUR MANGUR',8102),
(810203,3,'KEI BESAR',8102),
(810204,4,'KEI BESAR SELATAN',8102),
(810205,5,'KEI BESAR UTARA TIMUR',8102),
(810301,1,'TANIMBAR SELATAN',8103),
(810302,2,'SELARU',8103),
(810303,3,'WERTAMRIAN',8103),
(810304,4,'WERMAKATIAN',8103),
(810305,5,'TANIMBAR UTARA',8103),
(810306,6,'YARU',8103),
(810307,7,'WUARLABOBAR',8103),
(810308,8,'KORMOMOLIN',8103),
(810309,9,'NIRUNMAS',8103),
(810310,10,'PP. BABAR',8103),
(810311,11,'BABAR TIMUR',8103),
(810312,12,'MDONAHIERA',8103),
(810313,13,'PP. LETI MOA LAKOR',8103),
(810314,14,'MOA LAKOR',8103),
(810315,15,'PP. TERSELATAN',8103),
(810316,16,'DAMER',8103),
(810317,17,'PP. WETAR',8103),
(810401,1,'BURU SELATAN',8104),
(810402,2,'BURU SELATAN TIMUR',8104),
(810403,3,'BURU UTARA TIMUR',8104),
(810404,4,'BURU UTARA SELATAN',8104),
(810405,5,'BURU UTARA BARAT',8104),
(810501,1,'PP. ARU',8105),
(810502,2,'PP. ARU SELATAN',8105),
(810503,3,'PP. ARU TENGAH',8105),
(810601,1,'HUAMUAL BELAKANG',8106),
(810602,2,'SERAM BARAT',8106),
(810603,3,'KAIRATU',8106),
(810604,4,'TANIWEL',8106),
(810701,1,'P.P. GOROM',8107),
(810702,2,'SERAM TIMUR',8107),
(810703,3,'WERINAMA',8107),
(810704,4,'B U L A',8107),
(817101,1,'NUSANIWE',8171),
(817102,2,'SIRIMAU',8171),
(817103,3,'TELUK AMBON BAGUALA',8171),
(820101,1,'JAILOLO',8201),
(820102,2,'LOLODA',8201),
(820103,3,'IBU',8201),
(820104,4,'SAHU',8201),
(820105,5,'JAILOLO SELATAN',8201),
(820201,1,'WEDA',8202),
(820202,2,'PATANI',8202),
(820203,3,'PULAU GEBE',8202),
(820204,4,'WEDA UTARA',8202),
(820205,5,'WEDA SELATAN',8202),
(820206,6,'PATANI UTARA',8202),
(820301,1,'MOROTAI UTARA',8203),
(820302,2,'MOROTAI SELATAN BARAT',8203),
(820303,3,'MOROTAI SELATAN',8203),
(820304,4,'GALELA',8203),
(820305,5,'TOBELO',8203),
(820306,6,'TOBELO SELATAN',8203),
(820307,7,'KAO',8203),
(820308,8,'MALIFUT',8203),
(820309,9,'LOLODA UTARA',8203),
(820310,10,'TOBELO UTARA',8203),
(820311,11,'TOBELO TENGAH',8203),
(820312,12,'TOBELO TIMUR',8203),
(820313,13,'TOBELO BARAT',8203),
(820314,14,'GALELA BARAT',8203),
(820315,15,'GALELA UTARA',8203),
(820316,16,'GALELA SELATAN',8203),
(820317,17,'MOROTAI TIMUR',8203),
(820318,18,'MOROTAI JAYA',8203),
(820319,19,'LOLODA KEPULAUAN',8203),
(820320,20,'KAO UTARA',8203),
(820321,21,'KAO BARAT',8203),
(820322,22,'KAO TELUK',8203),
(820401,1,'PULAU MAKIAN',8204),
(820402,2,'KAYOA',8204),
(820403,3,'GANE TIMUR',8204),
(820404,4,'GANE BARAT',8204),
(820405,5,'OBI SELATAN',8204),
(820406,6,'OBI',8204),
(820407,7,'BACAN TIMUR',8204),
(820408,8,'BACAN',8204),
(820409,9,'BACAN BARAT',8204),
(820501,1,'MANGOLI TIMUR',8205),
(820502,2,'SANANA',8205),
(820503,3,'SULA BESI BARAT',8205),
(820504,4,'TALIABU BARAT',8205),
(820505,5,'TALIABU TIMUR',8205),
(820506,6,'MANGOLI BARAT',8205),
(820601,1,'WASILE',8206),
(820602,2,'MABA',8206),
(820603,3,'MABA SELATAN',8206),
(820604,4,'WASILE SELATAN',8206),
(820605,5,'WASILE TENGAH',8206),
(820606,6,'WASILE UTARA',8206),
(820607,7,'WASILE TIMUR',8206),
(820608,8,'MABA TENGAH',8206),
(820609,9,'MABA UTARA ',8206),
(820610,10,'KOTA MABA',8206),
(827101,1,'PULAU TERNATE',8271),
(827102,2,'KOTA TERNATE SELATAN',8271),
(827103,3,'KOTA TERNATE UTARA',8271),
(827104,4,'PULAU MOTI',8271),
(827105,5,'PULAU BATANG DUA',8271),
(827106,6,'KOTA TERNATE TENGAH',8271),
(827201,1,'PULAU TIDORE',8272),
(827202,2,'OBA UTARA',8272),
(827203,3,'OBA',8272),
(827204,4,'TIDORE SELATAN',8272),
(827205,5,'TIDORE UTARA',8272),
(827206,6,'OBA TENGAH',8272),
(827207,7,'OBA SELATAN',8272),
(910101,1,'MERAUKE',9101),
(910102,2,'MUTING',9101),
(910103,3,'OKABA',9101),
(910104,4,'KIMAAM',9101),
(910105,5,'SEMANGGA',9101),
(910106,6,'TANAH MIRING',9101),
(910107,7,'JAGEBOB',9101),
(910108,8,'SOTA',9101),
(910109,9,' ULILIN',9101),
(910110,10,'ELIGOBEL',9101),
(910111,11,'KURIK',9101),
(910201,1,'WAMENA',9102),
(910202,2,'TIOM',9102),
(910203,3,'KURULU',9102),
(910204,4,'ASOLOGAIMA',9102),
(910205,5,'MAKKI',9102),
(910206,6,'KELILA',9102),
(910207,7,'KENYAM',9102),
(910208,8,'MAPENDUMA',9102),
(910209,9,'PIRIME',9102),
(910210,10,'KOBAKMA',9102),
(910211,11,'APALAPSILI',9102),
(910212,12,'HUBIKOSI',9102),
(910213,13,'ABENAHO',9102),
(910214,14,'GAMELIA',9102),
(910215,15,'BOLAKME',9102),
(910301,1,'SENTANI',9103),
(910302,2,'SENTANI TIMUR',9103),
(910303,3,'DEPAPRE',9103),
(910304,4,'SENTANI BARAT',9103),
(910305,5,'KEMTUK',9103),
(910306,6,'KEMTUK GRESI',9103),
(910307,7,'NIMBORAN',9103),
(910308,8,'NIMBOKRANG',9103),
(910309,9,'UNURUM GUAY',9103),
(910310,10,'DEMTA',9103),
(910311,11,'KAUREH',9103),
(910401,1,'NABIRE',9104),
(910402,2,'NAPAN',9104),
(910403,3,'YAUR',9104),
(910404,4,'KAMU',9104),
(910405,5,'MAPIA',9104),
(910406,6,'UWAPA',9104),
(910407,7,'WANGGAR',9104),
(910408,8,'IKRAR',9104),
(910409,9,'SUKIKAI',9104),
(910410,10,'SIRIWO',9104),
(910501,1,'YAPEN SELATAN',9105),
(910502,2,'YAPEN BARAT',9105),
(910503,3,'YAPEN TIMUR',9105),
(910504,4,'ANGKAISERA',9105),
(910505,5,'POOM',9105),
(910601,1,'BIAK KOTA',9106),
(910602,2,'BIAK UTARA',9106),
(910603,3,'BIAK TIMUR',9106),
(910604,4,'NUMFOR BARAT',9106),
(910605,5,'NUMFOR TIMUR',9106),
(910606,6,'SUPIORI SELATAN',9106),
(910607,7,'SUPIORI UTARA',9106),
(910608,8,'BIAK BARAT',9106),
(910609,9,'WARSA',9106),
(910610,10,'PADAIDO',9106),
(910611,11,'YENDIDORI',9106),
(910612,12,'SAMOFA',9106),
(910613,13,'SUPRIORI TIMUR',9106),
(910701,1,'MULIA',9107),
(910702,2,'ILAGA',9107),
(910703,3,'ILU',9107),
(910704,4,'SINAK',9107),
(910705,5,'BEOGA',9107),
(910706,6,'FAWI',9107),
(910801,1,'PANIAI TIMUR',9108),
(910802,2,'PANIAI BARAT',9108),
(910803,3,'TIGI',9108),
(910804,4,'ARADIDE',9108),
(910805,5,'SUGAPA',9108),
(910806,6,'HOMEO',9108),
(910807,7,'BOGOBAIDA',9108),
(910808,8,'BIANDOGA',9108),
(910809,9,'BIBIDA',9108),
(910810,10,'TIGI TIMUR',9108),
(910811,11,'AGISIGA',9108),
(910901,1,'MIMIKA BARU',9109),
(910902,2,'AGIMUGA',9109),
(910903,3,'MIMIKA TIMUR',9109),
(910904,4,'MIMIKA BARAT',9109),
(910905,5,'JITA',9109),
(910906,6,'JILA',9109),
(910907,7,'MIMIKA TIMUR JAUH',9109),
(910908,8,'MIMIKA TIMUR TENGAH',9109),
(910909,9,'KUALA KENCANA',9109),
(910910,10,'TEMBAGAPURA',9109),
(910911,11,'MIMIKA BARAT JAUH',9109),
(910912,12,'MIMIKA BARAT TENGAH',9109),
(910913,13,'MIMIKA TIMUR',9109),
(911001,1,'SARMI',9110),
(911002,2,'TOR ATAS',9110),
(911003,3,'PANTAI BARAT',9110),
(911004,4,'PANTAI TIMUR',9110),
(911005,5,'BONGGO',9110),
(911006,6,'MAMBERAMO ILIR',9110),
(911007,7,'MAMBERAMO TENGAH',9110),
(911008,8,'MAMBERAMO HULU',9110),
(911101,1,'WARIS',9111),
(911102,2,'ARSO',9111),
(911103,3,'SENGGI',9111),
(911104,4,'WEMBI',9111),
(911105,5,'SKAMTO',9111),
(911201,1,'OKSIBIL',9112),
(911202,2,'KIWIROK',9112),
(911203,3,'OKBIBAB',9112),
(911204,4,'IWUR',9112),
(911205,5,'BATOM',9112),
(911206,6,'BORME',9112),
(911301,1,'KURIMA',9113),
(911302,2,'ANGGRUK',9113),
(911303,3,'NINIA',9113),
(911401,1,'KARUBAGA',9114),
(911402,2,'BOKONDINI',9114),
(911403,3,'KANGGIME',9114),
(911404,4,'KEMBU',9114),
(911501,1,'WAROPEN BAWAH',9115),
(911503,3,'MASIREI',9115),
(911601,1,'MANDOBO',9116),
(911602,2,'MINDIPTANA',9116),
(911603,3,'WAROPKO',9116),
(911604,4,'KOUH',9116),
(911605,5,'JAIR',9116),
(911606,6,'BOMAKIA',9116),
(911701,1,'OBAA',9117),
(911702,2,'NAMBIOMAN BAPAI',9117),
(911703,3,'CITAK MITAK',9117),
(911704,4,'EDERA',9117),
(911705,5,'HAJU',9117),
(911706,6,'ASSUE',9117),
(911801,1,'AGATS',9118),
(911802,2,'ATSY',9118),
(911803,3,'SAWAERMA',9118),
(911804,4,'AKAT',9118),
(911805,5,'FAYIT',9118),
(911806,6,'PANTAI KASUARI',9118),
(911807,7,'SUATOR',9118),
(911901,1,'SUPIORI SELATAN',9119),
(911902,2,'SUPIORI UTARA',9119),
(911903,3,'SUPIORI TIMUR',9119),
(912001,1,'MABERAMO TENGAH',9120),
(912002,2,'MAMBERAMO HULU',9120),
(912003,3,'RUFAER',9120),
(912004,4,'MEMBERAMO TENGAH TIMUR',9120),
(912005,5,'MEMBRAMO HILIR',9120),
(912006,6,'WAROPEN ATAS',9120),
(912007,7,'BENUKI',9120),
(912008,8,'SAWAI',9120),
(917101,1,'JAYAPURA UTARA',9171),
(917102,2,'JAYAPURA SELATAN',9171),
(917103,3,'ABEPURA',9171),
(917104,4,'MUARA TAMI',9171),
(917105,5,'HERAM',9171),
(920101,1,'MAKBON',9201),
(920102,2,'MORAID',9201),
(920103,3,'SAUSAPOR',9201),
(920104,4,'BERAUR',9201),
(920105,5,'SALAWATI',9201),
(920106,6,'SEGET',9201),
(920107,7,'AIMAS',9201),
(920108,8,'KLAMONO',9201),
(920109,9,'PEEF',9201),
(920110,10,'SAYOSA',9201),
(920111,11,'ABUN',9201),
(920112,12,'SEGUN',9201),
(920201,1,'MANOKWARI BARAT',9202),
(920202,2,'RANSIKI',9202),
(920203,3,'WARMARE',9202),
(920204,4,'PRAFI',9202),
(920205,5,'MASNI',9202),
(920206,6,'ORANSBARI',9202),
(920207,7,'KEBAR',9202),
(920208,8,'ANGGI',9202),
(920209,9,'SURUREY',9202),
(920210,10,'AMBERBAKEN',9202),
(920211,11,'MENYAMBOUW',9202),
(920212,12,'TESTEGA',9202),
(920213,13,'TANAH RUBU',9202),
(920214,14,'MANOKWARI TIMUR',9202),
(920215,15,'MANOKWARI UTARA',9202),
(920216,16,'MANOKWARI SELATAN',9202),
(920301,1,'FAKFAK',9203),
(920302,2,'FAKFAK BARAT',9203),
(920303,3,'FAKFAK TIMUR',9203),
(920304,4,'KOKAS',9203),
(920305,5,'FAKFAK TENGAH',9203),
(920306,6,'KARAS',9203),
(920307,7,'BOMBERAY',9203),
(920308,8,'KRAMAMONGGA',9203),
(920309,9,'TELUK PATIPI',9203),
(920401,1,'TEMINABUAN',9204),
(920402,2,'AITINYO',9204),
(920403,3,'AYAMARU',9204),
(920404,4,'INANWATAN',9204),
(920405,5,'AIFAT',9204),
(920406,6,'SAWIAT',9204),
(920407,7,'MARE',9204),
(920408,8,'AIFAT TIMUR',9204),
(920409,9,'KOKODA',9204),
(920410,10,'MOSWAREN',9204),
(920411,11,'MATEMANI KAIS',9204),
(920412,12,'WAYER',9204),
(920413,13,'SEREMUK',9204),
(920414,14,'AYAMARU UTARA',9204),
(920501,1,'MISOOL',9205),
(920502,2,'WAIGEO UTARA',9205),
(920503,3,'WAIGEO SELATAN',9205),
(920504,4,'SAMATE',9205),
(920505,5,'KEPULAUAN AYAU',9205),
(920506,6,'MISOOL TIMUR SELATAN',9205),
(920507,7,'WAIGEO BARAT',9205),
(920508,8,'KOFIAU',9205),
(920509,9,'TELUK MAYALIBIT',9205),
(920510,10,'WAIGEO TIMUR',9205),
(920601,1,'BINTUNI',9206),
(920602,2,'MERDEY',9206),
(920603,3,'BABO',9206),
(920604,4,'ARANDAY',9206),
(920605,5,'MOSKONA UTARA',9206),
(920606,6,'MOSKONA SELATAN',9206),
(920607,7,'IDOOR',9206),
(920609,9,'IRORUTU',9206),
(920610,10,'TEMBUNI',9206),
(920611,11,'KURI',9206),
(920701,1,'WASIOR',9207),
(920702,2,'WINDESI',9207),
(920703,3,'WASIOR UTARA',9207),
(920704,4,'WASIOR SELATAN',9207),
(920705,5,'WAMESA',9207),
(920706,6,'RUMBERPON',9207),
(920707,7,'WASIOR BARAT',9207),
(920801,1,'KAIMANA',9208),
(920802,2,'BURUWAY',9208),
(920803,3,'TELUK ARGUNI',9208),
(920804,4,'TELUK ETNA',9208),
(927101,1,'SORONG',9271),
(927102,2,'SORONG TIMUR',9271),
(927103,3,'SORONG BARAT',9271),
(927104,4,'SORONG KEPULAUAN',9271),
(927105,5,'SORONG UTARA',9271),
(0,0,'KANIGARAN',3574);

/*Table structure for table `m_propinsi` */

CREATE TABLE `m_propinsi` (
  `id_propinsi` int(3) NOT NULL AUTO_INCREMENT,
  `Nama_Prop` varchar(255) NOT NULL,
  PRIMARY KEY (`id_propinsi`)
) ENGINE=MyISAM AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;

/*Data for the table `m_propinsi` */

insert  into `m_propinsi`(`id_propinsi`,`Nama_Prop`) values 
(11,'NANGGROE ACEH DARUSSALAM'),
(12,'SUMATERA UTARA'),
(13,'SUMATERA BARAT'),
(14,'RIAU'),
(15,'JAMBI'),
(16,'SUMATERA SELATAN'),
(17,'BENGKULU'),
(18,'LAMPUNG'),
(19,'KEPULAUAN BANGKA BELITUNG'),
(21,'KEPULAUAN RIAU'),
(31,'DKI JAKARTA'),
(32,'JAWA BARAT'),
(33,'JAWA TENGAH'),
(34,'DAERAH ISTIMEWA YOGYAKARTA'),
(35,'JAWA TIMUR'),
(36,'BANTEN'),
(51,'BALI'),
(52,'NUSA TENGGARA BARAT'),
(53,'NUSA TENGGARA TIMUR'),
(61,'KALIMANTAN BARAT'),
(62,'KALIMANTAN TENGAH'),
(63,'KALIMANTAN SELATAN'),
(64,'KALIMANTAN TIMUR'),
(71,'SULAWESI UTARA'),
(72,'SULAWESI TENGAH'),
(73,'SULAWESI SELATAN'),
(74,'SULAWESI TENGGARA'),
(75,'GORONTALO'),
(76,'SULAWESI BARAT'),
(81,'MALUKU'),
(82,'MALUKU UTARA'),
(91,'PAPUA'),
(92,'PAPUA BARAT');

/*Table structure for table `mitra` */

CREATE TABLE `mitra` (
  `mtr_id` int(11) NOT NULL AUTO_INCREMENT,
  `mtr_parent` int(11) DEFAULT NULL,
  `mtr_namainstansi` varchar(255) DEFAULT NULL,
  `mtr_alamat` text,
  `mtr_kecamatan` int(11) DEFAULT NULL,
  `mtr_kota` int(11) DEFAULT NULL,
  `mtr_propinsi` int(11) DEFAULT NULL,
  `mtr_telepon` varchar(15) DEFAULT NULL,
  `mtr_handphone` varchar(15) DEFAULT NULL,
  `mtr_email` varchar(50) DEFAULT NULL,
  `mtr_username` varchar(10) DEFAULT NULL,
  `mtr_password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`mtr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `mitra` */

insert  into `mitra`(`mtr_id`,`mtr_parent`,`mtr_namainstansi`,`mtr_alamat`,`mtr_kecamatan`,`mtr_kota`,`mtr_propinsi`,`mtr_telepon`,`mtr_handphone`,`mtr_email`,`mtr_username`,`mtr_password`) values 
(1,1,'Kemenag','jalan kemenag ',NULL,NULL,2,'085142645664','085142645664','kemenag@gmail.com','kemenag','28b662d883b6d76fd96e4ddc5e9ba780'),
(2,0,'Telkom Sigma','Surabaya',357802,3578,35,'0812345678','0812345678','asd@gmail.com','telkomsigm','068fefafde9dff06037e0ec6733d2e82'),
(3,2,'Telkomsel Kartu','Jalan A.Yani Surabaya',357829,3578,35,'0854785156454','0874587121596','sidomoro@gmail.com','sidomoro','14cf2cbe29f304747f205c90fa419523'),
(4,0,'Pertamina','Blora',352211,3522,35,'0854152638264','08226523672323','najwasihab@gmail.com','najwasihab','b18b08804b39720d035ffe5ab37d6d05'),
(5,0,'Testing','Sidoarjo kota cak',352417,3524,35,'0853234567890','0853234567890','testing@gmail.com','testing','fa6a5a3224d7da66d9e0bdec25f62cf0');

/*Table structure for table `permohonan` */

CREATE TABLE `permohonan` (
  `prm_id` int(11) NOT NULL AUTO_INCREMENT,
  `mtr_id` int(11) DEFAULT NULL,
  `jns_id` int(11) DEFAULT NULL,
  `prm_judul` varchar(255) DEFAULT NULL,
  `prm_deskripsi` text,
  `prm_tanggal` date DEFAULT NULL,
  `prm_unit` int(11) DEFAULT NULL,
  `prm_status` smallint(6) DEFAULT NULL COMMENT '1 = proses, 2 disposisi, 3 batal',
  `prm_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `prm_file` text,
  `prm_internal` tinyint(1) DEFAULT NULL COMMENT '1 itu internal 2 external 3 untuk yang input bagian kerja sama',
  PRIMARY KEY (`prm_id`),
  KEY `FK_jenis_kerjasama` (`jns_id`),
  KEY `FK_permohonan_kerjasama` (`mtr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `permohonan` */

insert  into `permohonan`(`prm_id`,`mtr_id`,`jns_id`,`prm_judul`,`prm_deskripsi`,`prm_tanggal`,`prm_unit`,`prm_status`,`prm_created`,`prm_file`,`prm_internal`) values 
(1,1,2,'Sertifikat Informasi Teknologi','Deskripsi untuk permohonan','2019-01-03',60,2,'2019-01-03 22:19:21',NULL,2),
(2,4,1,'Magang untuk Mahasiswa','Magang untuk mahasiswa yang sudah mulai magang','2019-01-03',60,2,'2019-01-04 06:47:28',NULL,1),
(4,4,3,'Magang Nak Pom','ini adalah magang untuk mahasiswa yaitu ngisi bensin nag POM ','2019-01-04',60,3,'2019-01-04 21:30:52','17b28bd778618c0dd16375aa35a03176.jpg',1),
(5,4,3,'Magang Ngisi POM 2.0','ini adalah deskripsi ','2019-01-04',60,3,'2019-01-04 21:41:09','99f3e44f9bb1d23a608dc4996ca6c3d8.jpg',1),
(6,2,1,'Masang Wifi ','Magang masang wifi nag kene ','2019-01-04',60,3,'2019-01-04 22:10:35','69bbf185535994d4987cff6ab175f6eb.jpg',1),
(7,2,2,'judul permohonan kerja sama','ini deskripsi judul permohonan kerja sama','2019-01-04',60,3,'2019-01-04 22:57:51','5641d455663201b3e9376f85319ca131.png',1),
(9,1,1,'judul hehe','deskripsi heh','2019-01-04',9,3,'2019-01-04 23:16:13','55da506aea24ee4d3078d943e5dffe01.docx',2),
(10,1,2,'judul dengan prodi MPI','deskripsi judul dengan MPI','2019-01-04',39,3,'2019-01-04 23:18:27','a5fb3004985a099832e36bc10386e44e.xlsx',2),
(11,1,3,'judul permohonan fakultas dakwah dan ilmu komunikasi','deskripsi permohonan fakultas dakwah','2019-01-04',9,3,'2019-01-04 23:21:27','8ad01deda83655957293d06501dc772d.docx',2),
(12,3,2,'Instal ulang Komputer di Lab Intregasi edit','ini adalah deskripsi Instal ulang Komputer di Lab Intregasi edit','2019-01-05',92,3,'2019-01-05 17:42:27','9a0daee9dceda0c2d0a618bc4f40de76.docx',3),
(13,5,3,'permohonan kerjasama exchange ke australia','deskripsi permohonan kerjasama exchange ke australia','2019-01-06',9,2,'2019-01-06 12:08:19','b56410d6fbd4dd6eea949d397dcca00b.docx',2),
(14,5,2,'Sertifikasi Jurusan Pemikiran Islam mbuh ga jelas kwkwkwk','ini deskripsi ini Sertifikasi Jurusan Pemikiran Islam mbuh ga jelas kwkwkwk','2019-01-06',41,3,'2019-01-06 12:21:36','9e45fe311b76201e109bcf4aee239b6f.pdf',2),
(15,5,3,'ini sertifikat jatah.e warek 3','ini deskripsi ini sertifikat jatah.e warek 3','2019-01-06',25,3,'2019-01-06 12:24:42','7b4a480f6b44bf362e60570d8f71f4d1.docx',2),
(16,5,2,'permohonan untuk dibatalkan biro AAK ','ini diskrpsi untuk permohonan permohonan untuk dibatalkan biro AAK ','2019-01-06',65,3,'2019-01-06 12:26:48','f85ccb9438600b3d93f3a05efe6d30e5.xlsx',2),
(17,5,2,'permohonan untuk dibatalkan kabag kerjasama','ini deskripsi permohonan untuk dibatalkan kabag kerjasama','2019-01-06',61,3,'2019-01-06 12:28:27','1e4656a529d3765d4fac236d755271fa.xlsx',2),
(18,5,3,'permohonan untuk dibatalkan kasubag kerjasama ','ini deskripsi untuk permohonan untuk dibatalkan kasubag kerjasama ','2019-01-06',129,3,'2019-01-06 12:29:20','e8d9f1739eeb91e13acd351d4565d191.docx',2),
(22,5,1,'sapi','deskripsi sapi','2019-01-06',60,1,'2019-01-06 13:06:45','5c6a3f3566bb98fafbdf9627a34fc1df.png',1),
(23,4,2,'permohonan uswati ','deskripsi permohonan uswati ','2019-01-06',5,2,'2019-01-06 13:12:07','693863b9f8584b403fe86a728cea7381.png',1),
(24,3,2,'judul heheh','deskripsi','2019-01-06',5,3,'2019-01-06 13:13:14','8f82d7901f2034bda8765eeba4a2e9fb.jpg',1);

/*Table structure for table `potensi` */

CREATE TABLE `potensi` (
  `pts_id` int(11) NOT NULL AUTO_INCREMENT,
  `pts_unit` int(11) DEFAULT NULL,
  `pts_deskripsi` text,
  `pts_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pts_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `potensi` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
